﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32 (void);
// 0x00000002 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231 (void);
// 0x00000003 System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D (void);
// 0x00000004 System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1 (void);
// 0x00000005 System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81 (void);
// 0x00000006 System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F (void);
// 0x00000007 System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0 (void);
// 0x00000008 System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C (void);
// 0x00000009 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3 (void);
// 0x0000000A UnityEngine.GameObject AnchorCreator::get_AnchorPrefab()
extern void AnchorCreator_get_AnchorPrefab_m816C40114BC2B3BE2C5EF9EAFB0AFCC0836231D4 (void);
// 0x0000000B System.Void AnchorCreator::set_AnchorPrefab(UnityEngine.GameObject)
extern void AnchorCreator_set_AnchorPrefab_mD630D90875B7762D4AF9972DE380D76AA4D51119 (void);
// 0x0000000C System.Void AnchorCreator::RemoveAllAnchors()
extern void AnchorCreator_RemoveAllAnchors_mE38D4FA6701343059EE6A23D6C7CAAD286756474 (void);
// 0x0000000D System.Void AnchorCreator::Awake()
extern void AnchorCreator_Awake_mCF79FEA375F4FCEB23C7798C423C545E2641238E (void);
// 0x0000000E System.Void AnchorCreator::Update()
extern void AnchorCreator_Update_m37BE24FD3C57C8D1CCD9E7A33466231742A8E3A4 (void);
// 0x0000000F System.Void AnchorCreator::.ctor()
extern void AnchorCreator__ctor_mF2C9E47BDC804A2D50778130F1241088BCD76560 (void);
// 0x00000010 System.Void AnchorCreator::.cctor()
extern void AnchorCreator__cctor_mF25A90D54AD45591F5CA5325BBA568E0505B6A08 (void);
// 0x00000011 System.Void Boot::Awake()
extern void Boot_Awake_m88DE01755A6BA6F939F1A135BA4F8529DCBE0785 (void);
// 0x00000012 System.Collections.IEnumerator Boot::Start()
extern void Boot_Start_m713799084BDD2BA20E8B6308EB8E41AF6B8DF199 (void);
// 0x00000013 System.Void Boot::OnClickSimple()
extern void Boot_OnClickSimple_m892BC121197727A4B61591AA9B7E1554E6B2A0DA (void);
// 0x00000014 System.Void Boot::OnContinuous()
extern void Boot_OnContinuous_m80572C64FD80D172993AD15A1775513A92542FAE (void);
// 0x00000015 System.Void Boot::.ctor()
extern void Boot__ctor_m855DDAA7D321D951B1D58758D55ACA928468DC48 (void);
// 0x00000016 System.Void Boot/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m91FFE029B64FAD38811168D9D735476899B9607E (void);
// 0x00000017 System.Void Boot/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m3DA72C1FF3DC4E81FB329BF9C81CA6C09A06ABDD (void);
// 0x00000018 System.Boolean Boot/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m3BED12C4525A41AC89F80B3F95842A2525E6FB95 (void);
// 0x00000019 System.Object Boot/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AF07986C6DA587FD3CD43EFCFC630A79CFB3F8 (void);
// 0x0000001A System.Void Boot/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mB0B8ACBD12C7F74D27B5E38BD6D7DDB3215CB7AA (void);
// 0x0000001B System.Object Boot/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m470025E7D87A1BB3F940348FB142104EB42B0075 (void);
// 0x0000001C System.Void ContinuousDemo::Awake()
extern void ContinuousDemo_Awake_m501E873AC47F5254561B1404DD54E89EB82A2122 (void);
// 0x0000001D System.Void ContinuousDemo::Start()
extern void ContinuousDemo_Start_m6F869CEE4A4422E04B1792A0FF884048680B980B (void);
// 0x0000001E System.Void ContinuousDemo::StartScanner()
extern void ContinuousDemo_StartScanner_mD35AC4DFED5F974DA672B2FF14C4432D905E2F23 (void);
// 0x0000001F System.Void ContinuousDemo::Update()
extern void ContinuousDemo_Update_m6DA43C4EA9FCB2FF18B6D5DEDD06175EBF3F0C30 (void);
// 0x00000020 System.Void ContinuousDemo::ClickBack()
extern void ContinuousDemo_ClickBack_m092D223D5F86A28A9C351B32E836A43C4BA99762 (void);
// 0x00000021 System.Collections.IEnumerator ContinuousDemo::StopCamera(System.Action)
extern void ContinuousDemo_StopCamera_m2D7C97AE549B24EDA851AE1A9B0B22A32A98B46B (void);
// 0x00000022 System.Void ContinuousDemo::.ctor()
extern void ContinuousDemo__ctor_m6F9AE2A60AE25CCFB6FAC8185BA861080A2E2135 (void);
// 0x00000023 System.Void ContinuousDemo::<Start>b__6_0(System.Object,System.EventArgs)
extern void ContinuousDemo_U3CStartU3Eb__6_0_mF26C1CA8F83583FCB06D3CB9BC757B2399963C8A (void);
// 0x00000024 System.Void ContinuousDemo::<StartScanner>b__7_0(System.String,System.String)
extern void ContinuousDemo_U3CStartScannerU3Eb__7_0_m8B08D5EE8D663F636B82139EB3CCA19CC0163587 (void);
// 0x00000025 System.Void ContinuousDemo/<>c::.cctor()
extern void U3CU3Ec__cctor_m86BA18E23F396C00D1BACC583AFB9B77F76D8300 (void);
// 0x00000026 System.Void ContinuousDemo/<>c::.ctor()
extern void U3CU3Ec__ctor_m55F8DEB76643EA1500E38F7E6AB7E3CFDA1FE2DD (void);
// 0x00000027 System.Void ContinuousDemo/<>c::<ClickBack>b__9_0()
extern void U3CU3Ec_U3CClickBackU3Eb__9_0_m6747D6435F9F4799560809A1755E33FCFA0C1DC8 (void);
// 0x00000028 System.Void ContinuousDemo/<StopCamera>d__10::.ctor(System.Int32)
extern void U3CStopCameraU3Ed__10__ctor_mE711FFD044D12BB867729F1079DEBC3A5144CE08 (void);
// 0x00000029 System.Void ContinuousDemo/<StopCamera>d__10::System.IDisposable.Dispose()
extern void U3CStopCameraU3Ed__10_System_IDisposable_Dispose_mF0242993A91684E9EC29FBC83CBED66831C0224D (void);
// 0x0000002A System.Boolean ContinuousDemo/<StopCamera>d__10::MoveNext()
extern void U3CStopCameraU3Ed__10_MoveNext_m83FC6B87ED99A0DB43CD282C523F536F4B99B0C8 (void);
// 0x0000002B System.Object ContinuousDemo/<StopCamera>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopCameraU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31D3F3B514452F068EF5AB4027897DCC54259B9E (void);
// 0x0000002C System.Void ContinuousDemo/<StopCamera>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStopCameraU3Ed__10_System_Collections_IEnumerator_Reset_mBE80A0ED3DDDE86947F1BF1B05D56DB0B0CC4F54 (void);
// 0x0000002D System.Object ContinuousDemo/<StopCamera>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStopCameraU3Ed__10_System_Collections_IEnumerator_get_Current_mF21306CBD49E8C433227496983503C82AC4033E8 (void);
// 0x0000002E System.Void SimpleDemo::Awake()
extern void SimpleDemo_Awake_m09C055D7D85678487A1FAE97B19A412F9BBBA547 (void);
// 0x0000002F System.Void SimpleDemo::Start()
extern void SimpleDemo_Start_mD195DD1BFAF836284AF56FC272D6B6A4FA16DE13 (void);
// 0x00000030 System.Void SimpleDemo::Update()
extern void SimpleDemo_Update_m1238FB039443E00F22C304256B8B15DBA3E31ED7 (void);
// 0x00000031 System.Void SimpleDemo::ClickStart()
extern void SimpleDemo_ClickStart_mB97BF48526B421A2310B92A745D18D63BEBBDB63 (void);
// 0x00000032 System.Void SimpleDemo::ClickStop()
extern void SimpleDemo_ClickStop_m15A298EEA3815A29AD88EA514CDF8ADD0CA1DDEC (void);
// 0x00000033 System.Void SimpleDemo::ClickBack()
extern void SimpleDemo_ClickBack_mD7E303F51C34CBCA013F7225ABA9ADAA8CE9E018 (void);
// 0x00000034 System.Collections.IEnumerator SimpleDemo::StopCamera(System.Action)
extern void SimpleDemo_StopCamera_m9053976A94CF7B14D44FF7588A942BE63396FD4E (void);
// 0x00000035 System.Void SimpleDemo::.ctor()
extern void SimpleDemo__ctor_m3F50F73AD5E0484045E011CD71EE0EBAF8BEC3A1 (void);
// 0x00000036 System.Void SimpleDemo::<Start>b__6_0(System.Object,System.EventArgs)
extern void SimpleDemo_U3CStartU3Eb__6_0_mE6F6FB94DDB4A246E2E18FC25A78EC9AA3A903D6 (void);
// 0x00000037 System.Void SimpleDemo::<Start>b__6_1(System.Object,System.EventArgs)
extern void SimpleDemo_U3CStartU3Eb__6_1_mE14C94AAB4D35147205E6E0984F6E67D0F413EAA (void);
// 0x00000038 System.Void SimpleDemo::<ClickStart>b__8_0(System.String,System.String)
extern void SimpleDemo_U3CClickStartU3Eb__8_0_mC598EF2FE96F70469D1EEC4C242940F9EB062484 (void);
// 0x00000039 System.Void SimpleDemo/<>c::.cctor()
extern void U3CU3Ec__cctor_m29D0EFED99B53F9DC6E2FA6D71D2018927156347 (void);
// 0x0000003A System.Void SimpleDemo/<>c::.ctor()
extern void U3CU3Ec__ctor_mED134324C583AF27FFBDAC3A9BD73E370339059C (void);
// 0x0000003B System.Void SimpleDemo/<>c::<ClickBack>b__10_0()
extern void U3CU3Ec_U3CClickBackU3Eb__10_0_mABC77B6D0E3EB1BC84B9D54115CC1C1D26D1A168 (void);
// 0x0000003C System.Void SimpleDemo/<StopCamera>d__11::.ctor(System.Int32)
extern void U3CStopCameraU3Ed__11__ctor_mF62E222984E96AC8A8202F3985530CB8513A0DDA (void);
// 0x0000003D System.Void SimpleDemo/<StopCamera>d__11::System.IDisposable.Dispose()
extern void U3CStopCameraU3Ed__11_System_IDisposable_Dispose_mAB5F7F4CDA859B81FEE78953768607974A1DCF01 (void);
// 0x0000003E System.Boolean SimpleDemo/<StopCamera>d__11::MoveNext()
extern void U3CStopCameraU3Ed__11_MoveNext_mBD4DCCA3CDB44234C90E05D7844DF53B389BDC19 (void);
// 0x0000003F System.Object SimpleDemo/<StopCamera>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopCameraU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD10073582060761509ED394ABAC39A5F8113C4B2 (void);
// 0x00000040 System.Void SimpleDemo/<StopCamera>d__11::System.Collections.IEnumerator.Reset()
extern void U3CStopCameraU3Ed__11_System_Collections_IEnumerator_Reset_m56FD04EC25DF76EEF770BE9BE1FDC71E83F28AE3 (void);
// 0x00000041 System.Object SimpleDemo/<StopCamera>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CStopCameraU3Ed__11_System_Collections_IEnumerator_get_Current_m3DB44A6016DC72090DEB6DB2EAA5DB5AEF47A123 (void);
// 0x00000042 System.Single[] color_change_2::getcolors(System.String)
extern void color_change_2_getcolors_mB788C1DA786E4A39DA9ACE5F51EC28787258AE59 (void);
// 0x00000043 System.Void color_change_2::Start()
extern void color_change_2_Start_m257BFE01AA29D1FCEEF4584A2B444181170A8400 (void);
// 0x00000044 System.Void color_change_2::.ctor()
extern void color_change_2__ctor_mE7400A2B75431FEFB0B2CB2C407BEC22B0F4FE75 (void);
// 0x00000045 System.String container::get_CrossSceneInformation()
extern void container_get_CrossSceneInformation_m2B0AAFFD85CABD0E57D8B566FCAE9C343536902E (void);
// 0x00000046 System.Void container::set_CrossSceneInformation(System.String)
extern void container_set_CrossSceneInformation_m4C885BCC9EB0CDB6F422CB98DDD0046DEE1B1B17 (void);
// 0x00000047 UnityEngine.Camera UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::get_worldSpaceCanvasCamera()
extern void TrackedImageInfoManager_get_worldSpaceCanvasCamera_m2009EB2480A1FF118343F604EFE5AC5A9D1EAF98 (void);
// 0x00000048 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::set_worldSpaceCanvasCamera(UnityEngine.Camera)
extern void TrackedImageInfoManager_set_worldSpaceCanvasCamera_m425A097A896E7ACFB54A58818F8A9E93012F00D8 (void);
// 0x00000049 UnityEngine.Texture2D UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::get_defaultTexture()
extern void TrackedImageInfoManager_get_defaultTexture_m6A0D413300AF5BAC5E00557530B03CF16CEE7EBF (void);
// 0x0000004A System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::set_defaultTexture(UnityEngine.Texture2D)
extern void TrackedImageInfoManager_set_defaultTexture_m9FE88BD89D3C1A0E724B56E3DD57598EACBCD647 (void);
// 0x0000004B System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::Awake()
extern void TrackedImageInfoManager_Awake_m5CF4E3E359D5EE51D9CAC08F04ED16EA949BD609 (void);
// 0x0000004C System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnEnable()
extern void TrackedImageInfoManager_OnEnable_m2F61DD659F81B38E8DA09622DEA0C0B851E35948 (void);
// 0x0000004D System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnDisable()
extern void TrackedImageInfoManager_OnDisable_m25E3579157C28DC1ABA8A19CAF5572D96587E700 (void);
// 0x0000004E System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::UpdateInfo(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void TrackedImageInfoManager_UpdateInfo_m5B077255A0CF8EB647C37D9C27862B550BA45045 (void);
// 0x0000004F System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoManager_OnTrackedImagesChanged_m7D340AF3DC1C788A581C43A4AA47C4D3286D0C01 (void);
// 0x00000050 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::.ctor()
extern void TrackedImageInfoManager__ctor_mF3CC8344AE3000C2C0BD654B6853371F86B0972C (void);
// 0x00000051 BarcodeScanner.Parser.ParserResult BarcodeScanner.IParser::Decode(UnityEngine.Color32[],System.Int32,System.Int32)
// 0x00000052 System.Void BarcodeScanner.IScanner::add_StatusChanged(System.EventHandler)
// 0x00000053 System.Void BarcodeScanner.IScanner::remove_StatusChanged(System.EventHandler)
// 0x00000054 System.Void BarcodeScanner.IScanner::add_OnReady(System.EventHandler)
// 0x00000055 System.Void BarcodeScanner.IScanner::remove_OnReady(System.EventHandler)
// 0x00000056 BarcodeScanner.Scanner.ScannerStatus BarcodeScanner.IScanner::get_Status()
// 0x00000057 BarcodeScanner.IParser BarcodeScanner.IScanner::get_Parser()
// 0x00000058 BarcodeScanner.IWebcam BarcodeScanner.IScanner::get_Camera()
// 0x00000059 BarcodeScanner.ScannerSettings BarcodeScanner.IScanner::get_Settings()
// 0x0000005A System.Void BarcodeScanner.IScanner::Scan(System.Action`2<System.String,System.String>)
// 0x0000005B System.Void BarcodeScanner.IScanner::Stop()
// 0x0000005C System.Void BarcodeScanner.IScanner::Update()
// 0x0000005D System.Void BarcodeScanner.IScanner::Destroy()
// 0x0000005E UnityEngine.Texture BarcodeScanner.IWebcam::get_Texture()
// 0x0000005F System.Int32 BarcodeScanner.IWebcam::get_Width()
// 0x00000060 System.Int32 BarcodeScanner.IWebcam::get_Height()
// 0x00000061 System.Void BarcodeScanner.IWebcam::SetSize()
// 0x00000062 System.Boolean BarcodeScanner.IWebcam::IsReady()
// 0x00000063 System.Boolean BarcodeScanner.IWebcam::IsPlaying()
// 0x00000064 System.Void BarcodeScanner.IWebcam::Play()
// 0x00000065 System.Void BarcodeScanner.IWebcam::Stop()
// 0x00000066 System.Void BarcodeScanner.IWebcam::Destroy()
// 0x00000067 UnityEngine.Color32[] BarcodeScanner.IWebcam::GetPixels(UnityEngine.Color32[])
// 0x00000068 System.Single BarcodeScanner.IWebcam::GetRotation()
// 0x00000069 System.Boolean BarcodeScanner.IWebcam::IsVerticalyMirrored()
// 0x0000006A UnityEngine.Vector3 BarcodeScanner.IWebcam::GetEulerAngles()
// 0x0000006B UnityEngine.Vector3 BarcodeScanner.IWebcam::GetScale()
// 0x0000006C System.Int32 BarcodeScanner.IWebcam::GetChecksum()
// 0x0000006D System.Boolean BarcodeScanner.ScannerSettings::get_ScannerBackgroundThread()
extern void ScannerSettings_get_ScannerBackgroundThread_m30335460F26921379021162312F800A63645AACB (void);
// 0x0000006E System.Void BarcodeScanner.ScannerSettings::set_ScannerBackgroundThread(System.Boolean)
extern void ScannerSettings_set_ScannerBackgroundThread_m46BA08B898DCC0BB55F9AABDA5F78DD4B5B65DCB (void);
// 0x0000006F System.Int32 BarcodeScanner.ScannerSettings::get_ScannerDelayFrameMin()
extern void ScannerSettings_get_ScannerDelayFrameMin_m9EDA45261DA466F651AC68944CBA8A38792A4CF4 (void);
// 0x00000070 System.Void BarcodeScanner.ScannerSettings::set_ScannerDelayFrameMin(System.Int32)
extern void ScannerSettings_set_ScannerDelayFrameMin_m7A49D8BCC5B3E287280DA9095BBC0BDC8233E799 (void);
// 0x00000071 System.Single BarcodeScanner.ScannerSettings::get_ScannerDecodeInterval()
extern void ScannerSettings_get_ScannerDecodeInterval_mB6F8777B72605B4C3C146855B2FC150F12402CF9 (void);
// 0x00000072 System.Void BarcodeScanner.ScannerSettings::set_ScannerDecodeInterval(System.Single)
extern void ScannerSettings_set_ScannerDecodeInterval_m039B28B47CA18C439CD5334A884881D6002536CA (void);
// 0x00000073 System.Boolean BarcodeScanner.ScannerSettings::get_ParserAutoRotate()
extern void ScannerSettings_get_ParserAutoRotate_m3B9364BCC55021926EB0C6FCC2F7E1008ABC1C59 (void);
// 0x00000074 System.Void BarcodeScanner.ScannerSettings::set_ParserAutoRotate(System.Boolean)
extern void ScannerSettings_set_ParserAutoRotate_mAE3BC88B88006968560D6A02EBE1D7BFE47AC5E2 (void);
// 0x00000075 System.Boolean BarcodeScanner.ScannerSettings::get_ParserTryInverted()
extern void ScannerSettings_get_ParserTryInverted_m32741B4798B5F5DCAA875B474046E3342E0F34CB (void);
// 0x00000076 System.Void BarcodeScanner.ScannerSettings::set_ParserTryInverted(System.Boolean)
extern void ScannerSettings_set_ParserTryInverted_m356F4555134854FC5C59A0B930CD46081E20B1EE (void);
// 0x00000077 System.Boolean BarcodeScanner.ScannerSettings::get_ParserTryHarder()
extern void ScannerSettings_get_ParserTryHarder_mE214CF4477BAB4413CC2ACF386826E4A3C16E3B9 (void);
// 0x00000078 System.Void BarcodeScanner.ScannerSettings::set_ParserTryHarder(System.Boolean)
extern void ScannerSettings_set_ParserTryHarder_mD5201D1AA79D5BAA779B148E1834AF128EEB64A1 (void);
// 0x00000079 System.String BarcodeScanner.ScannerSettings::get_WebcamDefaultDeviceName()
extern void ScannerSettings_get_WebcamDefaultDeviceName_m4E8ABEFB6E1447E49C91CBB2F522D09A727A81CE (void);
// 0x0000007A System.Void BarcodeScanner.ScannerSettings::set_WebcamDefaultDeviceName(System.String)
extern void ScannerSettings_set_WebcamDefaultDeviceName_mAE9190A77D628E999EEC8F3A18F00D171A5C75E0 (void);
// 0x0000007B System.Int32 BarcodeScanner.ScannerSettings::get_WebcamRequestedWidth()
extern void ScannerSettings_get_WebcamRequestedWidth_m7F8B8287DE5346A72DC23B67EC9088876D2C36CD (void);
// 0x0000007C System.Void BarcodeScanner.ScannerSettings::set_WebcamRequestedWidth(System.Int32)
extern void ScannerSettings_set_WebcamRequestedWidth_m04E8C518E69E992CE38A902796FAB08BFED9685F (void);
// 0x0000007D System.Int32 BarcodeScanner.ScannerSettings::get_WebcamRequestedHeight()
extern void ScannerSettings_get_WebcamRequestedHeight_mF2B29359EE5BBAB0053ED7EDC8EFAF28F7E6A1E6 (void);
// 0x0000007E System.Void BarcodeScanner.ScannerSettings::set_WebcamRequestedHeight(System.Int32)
extern void ScannerSettings_set_WebcamRequestedHeight_m59376A7A86CF0A9C21A71E28D74D46A99ADCEB5B (void);
// 0x0000007F UnityEngine.FilterMode BarcodeScanner.ScannerSettings::get_WebcamFilterMode()
extern void ScannerSettings_get_WebcamFilterMode_mD4CA807F6A1299A860454720EC2B19CE36D5EEAA (void);
// 0x00000080 System.Void BarcodeScanner.ScannerSettings::set_WebcamFilterMode(UnityEngine.FilterMode)
extern void ScannerSettings_set_WebcamFilterMode_m1C1AA8C5F3106FE42723F893926326AB46D632F4 (void);
// 0x00000081 System.Void BarcodeScanner.ScannerSettings::.ctor()
extern void ScannerSettings__ctor_m3C3A32EA59B87FD89CF52ED5E1A4B201EFBBB73F (void);
// 0x00000082 UnityEngine.Texture BarcodeScanner.Webcam.UnityWebcam::get_Texture()
extern void UnityWebcam_get_Texture_m55038972336B810E9F0F25ABE266A0E49D68F338 (void);
// 0x00000083 UnityEngine.WebCamTexture BarcodeScanner.Webcam.UnityWebcam::get_Webcam()
extern void UnityWebcam_get_Webcam_m4D5A6823091FE84175E369D9F243FC10327C635C (void);
// 0x00000084 System.Void BarcodeScanner.Webcam.UnityWebcam::set_Webcam(UnityEngine.WebCamTexture)
extern void UnityWebcam_set_Webcam_mEA9871998A2ACA0ED81B3596D3475336BB018DAA (void);
// 0x00000085 UnityEngine.Vector2 BarcodeScanner.Webcam.UnityWebcam::get_Size()
extern void UnityWebcam_get_Size_mA4453B59FF1DAFAD7087870B16F4F8E5BC5AC2A5 (void);
// 0x00000086 System.Int32 BarcodeScanner.Webcam.UnityWebcam::get_Width()
extern void UnityWebcam_get_Width_m1807B99CA5172C7EBB8CF724F89A5F6E27B4B568 (void);
// 0x00000087 System.Void BarcodeScanner.Webcam.UnityWebcam::set_Width(System.Int32)
extern void UnityWebcam_set_Width_m2F857D8C4816D13CC35865FC57C2795A923E7010 (void);
// 0x00000088 System.Int32 BarcodeScanner.Webcam.UnityWebcam::get_Height()
extern void UnityWebcam_get_Height_m6FF981B572AAF2F19E221AEA4ACB8F3C82C44DB4 (void);
// 0x00000089 System.Void BarcodeScanner.Webcam.UnityWebcam::set_Height(System.Int32)
extern void UnityWebcam_set_Height_m3431919282FAF8347BEE87E24CF9B0617BAA2666 (void);
// 0x0000008A System.Void BarcodeScanner.Webcam.UnityWebcam::.ctor()
extern void UnityWebcam__ctor_mD1F0CA2EAB773FB2C0497B471074D782629EE42F (void);
// 0x0000008B System.Void BarcodeScanner.Webcam.UnityWebcam::.ctor(BarcodeScanner.ScannerSettings)
extern void UnityWebcam__ctor_mE8C5C1E297C69A407257B530E66DEDB5FC6F3154 (void);
// 0x0000008C System.Void BarcodeScanner.Webcam.UnityWebcam::SetSize()
extern void UnityWebcam_SetSize_m654E6529018357D8335177C90C75E42311578EAB (void);
// 0x0000008D System.Boolean BarcodeScanner.Webcam.UnityWebcam::IsReady()
extern void UnityWebcam_IsReady_m2D6F23832A1C715C2F1C3270B81E4BB216C8B0D2 (void);
// 0x0000008E System.Boolean BarcodeScanner.Webcam.UnityWebcam::IsPlaying()
extern void UnityWebcam_IsPlaying_m111AC13986E55D56EC0A0FE68325464C624DDCFC (void);
// 0x0000008F System.Void BarcodeScanner.Webcam.UnityWebcam::Play()
extern void UnityWebcam_Play_m09567552F920B7208A8AF253B71F9B680CAEF9F0 (void);
// 0x00000090 System.Void BarcodeScanner.Webcam.UnityWebcam::Stop()
extern void UnityWebcam_Stop_m770F4F4470D0C3B19F47521FADC810695EFCE271 (void);
// 0x00000091 System.Void BarcodeScanner.Webcam.UnityWebcam::Destroy()
extern void UnityWebcam_Destroy_m8614D2584AA2E0F4F0ED374499EED0C506A5A50B (void);
// 0x00000092 UnityEngine.Color32[] BarcodeScanner.Webcam.UnityWebcam::GetPixels(UnityEngine.Color32[])
extern void UnityWebcam_GetPixels_mD9C5B0E1119E2B7CFE2ABCBCC679595C91F0D4AC (void);
// 0x00000093 System.Single BarcodeScanner.Webcam.UnityWebcam::GetRotation()
extern void UnityWebcam_GetRotation_m00783591A1F0427B942F6BBA21046633F32033E2 (void);
// 0x00000094 System.Boolean BarcodeScanner.Webcam.UnityWebcam::IsVerticalyMirrored()
extern void UnityWebcam_IsVerticalyMirrored_mE51B0B82BDD18B59E015A092720EE3690A7C8BB2 (void);
// 0x00000095 UnityEngine.Vector3 BarcodeScanner.Webcam.UnityWebcam::GetEulerAngles()
extern void UnityWebcam_GetEulerAngles_m5E925473001ADFFAFB92471595D3FE3B30645EBD (void);
// 0x00000096 UnityEngine.Vector3 BarcodeScanner.Webcam.UnityWebcam::GetScale()
extern void UnityWebcam_GetScale_mC37F212CEED0B310ABD8DFF7B824802E2915CC5C (void);
// 0x00000097 System.Int32 BarcodeScanner.Webcam.UnityWebcam::GetChecksum()
extern void UnityWebcam_GetChecksum_mB9C3A926CE9DC8097AAB85177CB50AD7DC882E03 (void);
// 0x00000098 System.String BarcodeScanner.Webcam.UnityWebcam::ToString()
extern void UnityWebcam_ToString_m6892E5CB5DA6CFC636A8A3FE2435D68DD5AC3932 (void);
// 0x00000099 System.Void BarcodeScanner.Scanner.Scanner::add_OnReady(System.EventHandler)
extern void Scanner_add_OnReady_m413D28FBE215D8F706C0ABD2E6DCD3C7E8E502CB (void);
// 0x0000009A System.Void BarcodeScanner.Scanner.Scanner::remove_OnReady(System.EventHandler)
extern void Scanner_remove_OnReady_m929E68F109920C70D25DDACA1A64A664556BE2C4 (void);
// 0x0000009B System.Void BarcodeScanner.Scanner.Scanner::add_StatusChanged(System.EventHandler)
extern void Scanner_add_StatusChanged_m6AD196433549E3772C39B4B8FA6F8012EAC82D04 (void);
// 0x0000009C System.Void BarcodeScanner.Scanner.Scanner::remove_StatusChanged(System.EventHandler)
extern void Scanner_remove_StatusChanged_m2BCF093B3C4D420DA6F3E9C2587AF33799D7C992 (void);
// 0x0000009D BarcodeScanner.IWebcam BarcodeScanner.Scanner.Scanner::get_Camera()
extern void Scanner_get_Camera_m621E84F2274A55EEB80CD44B4C90CD7F1CF01DA8 (void);
// 0x0000009E System.Void BarcodeScanner.Scanner.Scanner::set_Camera(BarcodeScanner.IWebcam)
extern void Scanner_set_Camera_mFEA22095BAE5B6C34B471545E89EF0D030B67A57 (void);
// 0x0000009F BarcodeScanner.IParser BarcodeScanner.Scanner.Scanner::get_Parser()
extern void Scanner_get_Parser_m979901F0DAA8C01C504E6063CEB81CAF7763B105 (void);
// 0x000000A0 System.Void BarcodeScanner.Scanner.Scanner::set_Parser(BarcodeScanner.IParser)
extern void Scanner_set_Parser_m99E56792CCF4FBEB43EE9796576ED4B2536FFF83 (void);
// 0x000000A1 BarcodeScanner.ScannerSettings BarcodeScanner.Scanner.Scanner::get_Settings()
extern void Scanner_get_Settings_m91DB6300E69A0E0CD0FBE347601A5E0227BA0730 (void);
// 0x000000A2 System.Void BarcodeScanner.Scanner.Scanner::set_Settings(BarcodeScanner.ScannerSettings)
extern void Scanner_set_Settings_m8D20BA0EFE8551F0FD047707633104CBAE8F8843 (void);
// 0x000000A3 BarcodeScanner.Scanner.ScannerStatus BarcodeScanner.Scanner.Scanner::get_Status()
extern void Scanner_get_Status_mA753D4F71E7D530A39BA37B9D7FFFF936C2659BB (void);
// 0x000000A4 System.Void BarcodeScanner.Scanner.Scanner::set_Status(BarcodeScanner.Scanner.ScannerStatus)
extern void Scanner_set_Status_mD5552C7F68DEF30F9FBFEEF879F470F67F827887 (void);
// 0x000000A5 System.Void BarcodeScanner.Scanner.Scanner::.ctor()
extern void Scanner__ctor_m94FED2B991A823E729E6C84B80906D5D5368CBF4 (void);
// 0x000000A6 System.Void BarcodeScanner.Scanner.Scanner::.ctor(BarcodeScanner.ScannerSettings)
extern void Scanner__ctor_m887D7BED6B0AA395AABBE4A381A26B44B4912F7B (void);
// 0x000000A7 System.Void BarcodeScanner.Scanner.Scanner::.ctor(BarcodeScanner.IParser,BarcodeScanner.IWebcam)
extern void Scanner__ctor_mB7EDD167F4261E8DFFB2EE27EDC98BC5C018AAB7 (void);
// 0x000000A8 System.Void BarcodeScanner.Scanner.Scanner::.ctor(BarcodeScanner.ScannerSettings,BarcodeScanner.IParser,BarcodeScanner.IWebcam)
extern void Scanner__ctor_mCAF2318769E64B22DFDA5CFF6A79637975B28783 (void);
// 0x000000A9 System.Void BarcodeScanner.Scanner.Scanner::Scan(System.Action`2<System.String,System.String>)
extern void Scanner_Scan_mB62AC7D2ADB9E4E8CA1C4D4FFC34ED571284A09C (void);
// 0x000000AA System.Void BarcodeScanner.Scanner.Scanner::Stop()
extern void Scanner_Stop_m64D1627BB63A655638A98708DBD8E97CFC938679 (void);
// 0x000000AB System.Void BarcodeScanner.Scanner.Scanner::Stop(System.Boolean)
extern void Scanner_Stop_m397CF092F7B4DEBCC7768882FABDB61025D1624D (void);
// 0x000000AC System.Void BarcodeScanner.Scanner.Scanner::Destroy()
extern void Scanner_Destroy_m1212C19CCF6D5C5CE294A843F84BEEA4A0E5C6D2 (void);
// 0x000000AD System.Void BarcodeScanner.Scanner.Scanner::DecodeQR()
extern void Scanner_DecodeQR_m6A9B976C1ACD3FA5AD0B415E154E8CAF7EB9169C (void);
// 0x000000AE System.Void BarcodeScanner.Scanner.Scanner::ThreadDecodeQR()
extern void Scanner_ThreadDecodeQR_m5C483CAAC14D06FFB3E65D53C523A569552AED50 (void);
// 0x000000AF System.Boolean BarcodeScanner.Scanner.Scanner::WebcamInitialized()
extern void Scanner_WebcamInitialized_m887CBD07C520E90D559874BB192129D4A43D58C1 (void);
// 0x000000B0 System.Void BarcodeScanner.Scanner.Scanner::Update()
extern void Scanner_Update_mA5ED3103A8E6FC3B23BB50086597E68859F4353E (void);
// 0x000000B1 System.String BarcodeScanner.Scanner.Scanner::ToString()
extern void Scanner_ToString_mAB715A9148F0E7BCF572B6814C193BC7939BBD6B (void);
// 0x000000B2 System.String BarcodeScanner.Parser.ParserResult::get_Type()
extern void ParserResult_get_Type_mA9D9FB01A1E6BE522943B5287D0B566797B3EB4F (void);
// 0x000000B3 System.Void BarcodeScanner.Parser.ParserResult::set_Type(System.String)
extern void ParserResult_set_Type_mC55BF4CD90AC16AC67151A0F74BCC780F703DCB2 (void);
// 0x000000B4 System.String BarcodeScanner.Parser.ParserResult::get_Value()
extern void ParserResult_get_Value_mFB25627E1855F1C1F2C5928712290B96D2E340B4 (void);
// 0x000000B5 System.Void BarcodeScanner.Parser.ParserResult::set_Value(System.String)
extern void ParserResult_set_Value_mBBA5CA6D153BBBE4509DF950E3B6F053B71630A1 (void);
// 0x000000B6 System.Void BarcodeScanner.Parser.ParserResult::.ctor(System.String,System.String)
extern void ParserResult__ctor_m668DE86C9BE8BCEE63B2449C0D82D0466B74317B (void);
// 0x000000B7 System.String BarcodeScanner.Parser.ParserResult::ToString()
extern void ParserResult_ToString_m43EB3C043312316607AD9DF22F6A19596AC82348 (void);
// 0x000000B8 ZXing.BarcodeReader BarcodeScanner.Parser.ZXingParser::get_Scanner()
extern void ZXingParser_get_Scanner_m08F85842878738605F55014A1D66B1B00AD570E0 (void);
// 0x000000B9 System.Void BarcodeScanner.Parser.ZXingParser::set_Scanner(ZXing.BarcodeReader)
extern void ZXingParser_set_Scanner_m86C8758F9C833C5F4044BA6D8DC0BB9F6FEFFE9E (void);
// 0x000000BA System.Void BarcodeScanner.Parser.ZXingParser::.ctor()
extern void ZXingParser__ctor_m48F359C0541D5B18AC9862A25923A89B116AACD8 (void);
// 0x000000BB System.Void BarcodeScanner.Parser.ZXingParser::.ctor(BarcodeScanner.ScannerSettings)
extern void ZXingParser__ctor_m61AF43D86FBDB54DEB1FB8532A276A13D1BC6091 (void);
// 0x000000BC BarcodeScanner.Parser.ParserResult BarcodeScanner.Parser.ZXingParser::Decode(UnityEngine.Color32[],System.Int32,System.Int32)
extern void ZXingParser_Decode_mD01E5648B1C7BF66EEFC92240C0D890CCFEB9F1A (void);
// 0x000000BD System.Void Wizcorp.Utils.Logger.Log::.ctor()
extern void Log__ctor_mE537CDA36F47E96D030CAB8336EF4B954C70FCC1 (void);
// 0x000000BE System.Collections.Generic.IList`1<Wizcorp.Utils.Logger.Service.ILogService> Wizcorp.Utils.Logger.Log::get_Services()
extern void Log_get_Services_m2B5A3A08A43C67AFB866534CE20E9D2883F4D0EF (void);
// 0x000000BF System.Void Wizcorp.Utils.Logger.Log::AddService(Wizcorp.Utils.Logger.Service.ILogService)
extern void Log_AddService_m8DC3847686097BDDF06EA5ED9F1B1F86E013E26B (void);
// 0x000000C0 System.Void Wizcorp.Utils.Logger.Log::RemoveService(Wizcorp.Utils.Logger.Service.ILogService)
extern void Log_RemoveService_mC81E976611080435EF40785182F2F548D4EF5976 (void);
// 0x000000C1 System.Void Wizcorp.Utils.Logger.Log::ClearServices()
extern void Log_ClearServices_m6F38636223985130BE82E50F308A175FBBBCF097 (void);
// 0x000000C2 System.Void Wizcorp.Utils.Logger.Log::AddLog(Wizcorp.Utils.Logger.LogLevel,System.Object,System.Object)
extern void Log_AddLog_m92CD33524203EAB6340C753B326A5737DA28A4B7 (void);
// 0x000000C3 System.Void Wizcorp.Utils.Logger.Log::Verbose(System.Object,System.Object)
extern void Log_Verbose_m7D77DAD7A1DE115719DCFCA598840971693F55FF (void);
// 0x000000C4 System.Void Wizcorp.Utils.Logger.Log::Info(System.Object,System.Object)
extern void Log_Info_mB1B5C80A77D04DC8AE3E735741A87FC844A7A724 (void);
// 0x000000C5 System.Void Wizcorp.Utils.Logger.Log::Debug(System.Object,System.Object)
extern void Log_Debug_mA32A143C3995DC39BF6A935BED41D349774CC464 (void);
// 0x000000C6 System.Void Wizcorp.Utils.Logger.Log::Warning(System.Object,System.Object)
extern void Log_Warning_mBFF44B144DE5FE3211D48DAF0A6C238D8998E921 (void);
// 0x000000C7 System.Void Wizcorp.Utils.Logger.Log::Error(System.Object,System.Object)
extern void Log_Error_m1BDB8E56C078EB3FACE36C0C45A6ACA9E554F9CD (void);
// 0x000000C8 System.Void Wizcorp.Utils.Logger.Log::Critical(System.Object,System.Object)
extern void Log_Critical_m7C64FD7A03E4AA4A213394D84EB67A180F80FCB2 (void);
// 0x000000C9 System.Void Wizcorp.Utils.Logger.Log::.cctor()
extern void Log__cctor_m512BC1F6172B05532208D7C27827B3EE1C5E3BC4 (void);
// 0x000000CA System.Void Wizcorp.Utils.Logger.Service.ConsoleService::.ctor(Wizcorp.Utils.Logger.LogLevel)
extern void ConsoleService__ctor_m73D5F1B84712C6A0E5F9215E46E3252088B7A78C (void);
// 0x000000CB System.Void Wizcorp.Utils.Logger.Service.ConsoleService::AddLog(Wizcorp.Utils.Logger.LogLevel,System.Object,System.Object)
extern void ConsoleService_AddLog_m5F279E2859471FD3FD4B737BF93498DBAAE4C683 (void);
// 0x000000CC System.String Wizcorp.Utils.Logger.Service.ConsoleService::Format(System.Object,System.Object)
extern void ConsoleService_Format_m98BFB9F4A7BA52D68223B327D427C64255CBC8A1 (void);
// 0x000000CD System.String Wizcorp.Utils.Logger.Service.ConsoleService::LogObject(System.Object)
extern void ConsoleService_LogObject_mEA4F017E953066698B34D2F1B89C35C541C04809 (void);
// 0x000000CE System.Void Wizcorp.Utils.Logger.Service.FileService::.ctor(System.String,Wizcorp.Utils.Logger.LogLevel,System.Boolean)
extern void FileService__ctor_m18A791B5B4AE203CD12603E945AA1DF01DE35071 (void);
// 0x000000CF System.Void Wizcorp.Utils.Logger.Service.FileService::AddLog(Wizcorp.Utils.Logger.LogLevel,System.Object,System.Object)
extern void FileService_AddLog_m95BC8561C1A11D04401079C538F27F21531872EE (void);
// 0x000000D0 System.String Wizcorp.Utils.Logger.Service.FileService::Format(Wizcorp.Utils.Logger.LogLevel,System.Object,System.Object)
extern void FileService_Format_m2C6E34395E23225857D33B8F3C8C4198B19ECC72 (void);
// 0x000000D1 System.Void Wizcorp.Utils.Logger.Service.ILogService::AddLog(Wizcorp.Utils.Logger.LogLevel,System.Object,System.Object)
static Il2CppMethodPointer s_methodPointers[209] = 
{
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231,
	ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D,
	ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0,
	ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C,
	ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3,
	AnchorCreator_get_AnchorPrefab_m816C40114BC2B3BE2C5EF9EAFB0AFCC0836231D4,
	AnchorCreator_set_AnchorPrefab_mD630D90875B7762D4AF9972DE380D76AA4D51119,
	AnchorCreator_RemoveAllAnchors_mE38D4FA6701343059EE6A23D6C7CAAD286756474,
	AnchorCreator_Awake_mCF79FEA375F4FCEB23C7798C423C545E2641238E,
	AnchorCreator_Update_m37BE24FD3C57C8D1CCD9E7A33466231742A8E3A4,
	AnchorCreator__ctor_mF2C9E47BDC804A2D50778130F1241088BCD76560,
	AnchorCreator__cctor_mF25A90D54AD45591F5CA5325BBA568E0505B6A08,
	Boot_Awake_m88DE01755A6BA6F939F1A135BA4F8529DCBE0785,
	Boot_Start_m713799084BDD2BA20E8B6308EB8E41AF6B8DF199,
	Boot_OnClickSimple_m892BC121197727A4B61591AA9B7E1554E6B2A0DA,
	Boot_OnContinuous_m80572C64FD80D172993AD15A1775513A92542FAE,
	Boot__ctor_m855DDAA7D321D951B1D58758D55ACA928468DC48,
	U3CStartU3Ed__1__ctor_m91FFE029B64FAD38811168D9D735476899B9607E,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m3DA72C1FF3DC4E81FB329BF9C81CA6C09A06ABDD,
	U3CStartU3Ed__1_MoveNext_m3BED12C4525A41AC89F80B3F95842A2525E6FB95,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AF07986C6DA587FD3CD43EFCFC630A79CFB3F8,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mB0B8ACBD12C7F74D27B5E38BD6D7DDB3215CB7AA,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m470025E7D87A1BB3F940348FB142104EB42B0075,
	ContinuousDemo_Awake_m501E873AC47F5254561B1404DD54E89EB82A2122,
	ContinuousDemo_Start_m6F869CEE4A4422E04B1792A0FF884048680B980B,
	ContinuousDemo_StartScanner_mD35AC4DFED5F974DA672B2FF14C4432D905E2F23,
	ContinuousDemo_Update_m6DA43C4EA9FCB2FF18B6D5DEDD06175EBF3F0C30,
	ContinuousDemo_ClickBack_m092D223D5F86A28A9C351B32E836A43C4BA99762,
	ContinuousDemo_StopCamera_m2D7C97AE549B24EDA851AE1A9B0B22A32A98B46B,
	ContinuousDemo__ctor_m6F9AE2A60AE25CCFB6FAC8185BA861080A2E2135,
	ContinuousDemo_U3CStartU3Eb__6_0_mF26C1CA8F83583FCB06D3CB9BC757B2399963C8A,
	ContinuousDemo_U3CStartScannerU3Eb__7_0_m8B08D5EE8D663F636B82139EB3CCA19CC0163587,
	U3CU3Ec__cctor_m86BA18E23F396C00D1BACC583AFB9B77F76D8300,
	U3CU3Ec__ctor_m55F8DEB76643EA1500E38F7E6AB7E3CFDA1FE2DD,
	U3CU3Ec_U3CClickBackU3Eb__9_0_m6747D6435F9F4799560809A1755E33FCFA0C1DC8,
	U3CStopCameraU3Ed__10__ctor_mE711FFD044D12BB867729F1079DEBC3A5144CE08,
	U3CStopCameraU3Ed__10_System_IDisposable_Dispose_mF0242993A91684E9EC29FBC83CBED66831C0224D,
	U3CStopCameraU3Ed__10_MoveNext_m83FC6B87ED99A0DB43CD282C523F536F4B99B0C8,
	U3CStopCameraU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31D3F3B514452F068EF5AB4027897DCC54259B9E,
	U3CStopCameraU3Ed__10_System_Collections_IEnumerator_Reset_mBE80A0ED3DDDE86947F1BF1B05D56DB0B0CC4F54,
	U3CStopCameraU3Ed__10_System_Collections_IEnumerator_get_Current_mF21306CBD49E8C433227496983503C82AC4033E8,
	SimpleDemo_Awake_m09C055D7D85678487A1FAE97B19A412F9BBBA547,
	SimpleDemo_Start_mD195DD1BFAF836284AF56FC272D6B6A4FA16DE13,
	SimpleDemo_Update_m1238FB039443E00F22C304256B8B15DBA3E31ED7,
	SimpleDemo_ClickStart_mB97BF48526B421A2310B92A745D18D63BEBBDB63,
	SimpleDemo_ClickStop_m15A298EEA3815A29AD88EA514CDF8ADD0CA1DDEC,
	SimpleDemo_ClickBack_mD7E303F51C34CBCA013F7225ABA9ADAA8CE9E018,
	SimpleDemo_StopCamera_m9053976A94CF7B14D44FF7588A942BE63396FD4E,
	SimpleDemo__ctor_m3F50F73AD5E0484045E011CD71EE0EBAF8BEC3A1,
	SimpleDemo_U3CStartU3Eb__6_0_mE6F6FB94DDB4A246E2E18FC25A78EC9AA3A903D6,
	SimpleDemo_U3CStartU3Eb__6_1_mE14C94AAB4D35147205E6E0984F6E67D0F413EAA,
	SimpleDemo_U3CClickStartU3Eb__8_0_mC598EF2FE96F70469D1EEC4C242940F9EB062484,
	U3CU3Ec__cctor_m29D0EFED99B53F9DC6E2FA6D71D2018927156347,
	U3CU3Ec__ctor_mED134324C583AF27FFBDAC3A9BD73E370339059C,
	U3CU3Ec_U3CClickBackU3Eb__10_0_mABC77B6D0E3EB1BC84B9D54115CC1C1D26D1A168,
	U3CStopCameraU3Ed__11__ctor_mF62E222984E96AC8A8202F3985530CB8513A0DDA,
	U3CStopCameraU3Ed__11_System_IDisposable_Dispose_mAB5F7F4CDA859B81FEE78953768607974A1DCF01,
	U3CStopCameraU3Ed__11_MoveNext_mBD4DCCA3CDB44234C90E05D7844DF53B389BDC19,
	U3CStopCameraU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD10073582060761509ED394ABAC39A5F8113C4B2,
	U3CStopCameraU3Ed__11_System_Collections_IEnumerator_Reset_m56FD04EC25DF76EEF770BE9BE1FDC71E83F28AE3,
	U3CStopCameraU3Ed__11_System_Collections_IEnumerator_get_Current_m3DB44A6016DC72090DEB6DB2EAA5DB5AEF47A123,
	color_change_2_getcolors_mB788C1DA786E4A39DA9ACE5F51EC28787258AE59,
	color_change_2_Start_m257BFE01AA29D1FCEEF4584A2B444181170A8400,
	color_change_2__ctor_mE7400A2B75431FEFB0B2CB2C407BEC22B0F4FE75,
	container_get_CrossSceneInformation_m2B0AAFFD85CABD0E57D8B566FCAE9C343536902E,
	container_set_CrossSceneInformation_m4C885BCC9EB0CDB6F422CB98DDD0046DEE1B1B17,
	TrackedImageInfoManager_get_worldSpaceCanvasCamera_m2009EB2480A1FF118343F604EFE5AC5A9D1EAF98,
	TrackedImageInfoManager_set_worldSpaceCanvasCamera_m425A097A896E7ACFB54A58818F8A9E93012F00D8,
	TrackedImageInfoManager_get_defaultTexture_m6A0D413300AF5BAC5E00557530B03CF16CEE7EBF,
	TrackedImageInfoManager_set_defaultTexture_m9FE88BD89D3C1A0E724B56E3DD57598EACBCD647,
	TrackedImageInfoManager_Awake_m5CF4E3E359D5EE51D9CAC08F04ED16EA949BD609,
	TrackedImageInfoManager_OnEnable_m2F61DD659F81B38E8DA09622DEA0C0B851E35948,
	TrackedImageInfoManager_OnDisable_m25E3579157C28DC1ABA8A19CAF5572D96587E700,
	TrackedImageInfoManager_UpdateInfo_m5B077255A0CF8EB647C37D9C27862B550BA45045,
	TrackedImageInfoManager_OnTrackedImagesChanged_m7D340AF3DC1C788A581C43A4AA47C4D3286D0C01,
	TrackedImageInfoManager__ctor_mF3CC8344AE3000C2C0BD654B6853371F86B0972C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ScannerSettings_get_ScannerBackgroundThread_m30335460F26921379021162312F800A63645AACB,
	ScannerSettings_set_ScannerBackgroundThread_m46BA08B898DCC0BB55F9AABDA5F78DD4B5B65DCB,
	ScannerSettings_get_ScannerDelayFrameMin_m9EDA45261DA466F651AC68944CBA8A38792A4CF4,
	ScannerSettings_set_ScannerDelayFrameMin_m7A49D8BCC5B3E287280DA9095BBC0BDC8233E799,
	ScannerSettings_get_ScannerDecodeInterval_mB6F8777B72605B4C3C146855B2FC150F12402CF9,
	ScannerSettings_set_ScannerDecodeInterval_m039B28B47CA18C439CD5334A884881D6002536CA,
	ScannerSettings_get_ParserAutoRotate_m3B9364BCC55021926EB0C6FCC2F7E1008ABC1C59,
	ScannerSettings_set_ParserAutoRotate_mAE3BC88B88006968560D6A02EBE1D7BFE47AC5E2,
	ScannerSettings_get_ParserTryInverted_m32741B4798B5F5DCAA875B474046E3342E0F34CB,
	ScannerSettings_set_ParserTryInverted_m356F4555134854FC5C59A0B930CD46081E20B1EE,
	ScannerSettings_get_ParserTryHarder_mE214CF4477BAB4413CC2ACF386826E4A3C16E3B9,
	ScannerSettings_set_ParserTryHarder_mD5201D1AA79D5BAA779B148E1834AF128EEB64A1,
	ScannerSettings_get_WebcamDefaultDeviceName_m4E8ABEFB6E1447E49C91CBB2F522D09A727A81CE,
	ScannerSettings_set_WebcamDefaultDeviceName_mAE9190A77D628E999EEC8F3A18F00D171A5C75E0,
	ScannerSettings_get_WebcamRequestedWidth_m7F8B8287DE5346A72DC23B67EC9088876D2C36CD,
	ScannerSettings_set_WebcamRequestedWidth_m04E8C518E69E992CE38A902796FAB08BFED9685F,
	ScannerSettings_get_WebcamRequestedHeight_mF2B29359EE5BBAB0053ED7EDC8EFAF28F7E6A1E6,
	ScannerSettings_set_WebcamRequestedHeight_m59376A7A86CF0A9C21A71E28D74D46A99ADCEB5B,
	ScannerSettings_get_WebcamFilterMode_mD4CA807F6A1299A860454720EC2B19CE36D5EEAA,
	ScannerSettings_set_WebcamFilterMode_m1C1AA8C5F3106FE42723F893926326AB46D632F4,
	ScannerSettings__ctor_m3C3A32EA59B87FD89CF52ED5E1A4B201EFBBB73F,
	UnityWebcam_get_Texture_m55038972336B810E9F0F25ABE266A0E49D68F338,
	UnityWebcam_get_Webcam_m4D5A6823091FE84175E369D9F243FC10327C635C,
	UnityWebcam_set_Webcam_mEA9871998A2ACA0ED81B3596D3475336BB018DAA,
	UnityWebcam_get_Size_mA4453B59FF1DAFAD7087870B16F4F8E5BC5AC2A5,
	UnityWebcam_get_Width_m1807B99CA5172C7EBB8CF724F89A5F6E27B4B568,
	UnityWebcam_set_Width_m2F857D8C4816D13CC35865FC57C2795A923E7010,
	UnityWebcam_get_Height_m6FF981B572AAF2F19E221AEA4ACB8F3C82C44DB4,
	UnityWebcam_set_Height_m3431919282FAF8347BEE87E24CF9B0617BAA2666,
	UnityWebcam__ctor_mD1F0CA2EAB773FB2C0497B471074D782629EE42F,
	UnityWebcam__ctor_mE8C5C1E297C69A407257B530E66DEDB5FC6F3154,
	UnityWebcam_SetSize_m654E6529018357D8335177C90C75E42311578EAB,
	UnityWebcam_IsReady_m2D6F23832A1C715C2F1C3270B81E4BB216C8B0D2,
	UnityWebcam_IsPlaying_m111AC13986E55D56EC0A0FE68325464C624DDCFC,
	UnityWebcam_Play_m09567552F920B7208A8AF253B71F9B680CAEF9F0,
	UnityWebcam_Stop_m770F4F4470D0C3B19F47521FADC810695EFCE271,
	UnityWebcam_Destroy_m8614D2584AA2E0F4F0ED374499EED0C506A5A50B,
	UnityWebcam_GetPixels_mD9C5B0E1119E2B7CFE2ABCBCC679595C91F0D4AC,
	UnityWebcam_GetRotation_m00783591A1F0427B942F6BBA21046633F32033E2,
	UnityWebcam_IsVerticalyMirrored_mE51B0B82BDD18B59E015A092720EE3690A7C8BB2,
	UnityWebcam_GetEulerAngles_m5E925473001ADFFAFB92471595D3FE3B30645EBD,
	UnityWebcam_GetScale_mC37F212CEED0B310ABD8DFF7B824802E2915CC5C,
	UnityWebcam_GetChecksum_mB9C3A926CE9DC8097AAB85177CB50AD7DC882E03,
	UnityWebcam_ToString_m6892E5CB5DA6CFC636A8A3FE2435D68DD5AC3932,
	Scanner_add_OnReady_m413D28FBE215D8F706C0ABD2E6DCD3C7E8E502CB,
	Scanner_remove_OnReady_m929E68F109920C70D25DDACA1A64A664556BE2C4,
	Scanner_add_StatusChanged_m6AD196433549E3772C39B4B8FA6F8012EAC82D04,
	Scanner_remove_StatusChanged_m2BCF093B3C4D420DA6F3E9C2587AF33799D7C992,
	Scanner_get_Camera_m621E84F2274A55EEB80CD44B4C90CD7F1CF01DA8,
	Scanner_set_Camera_mFEA22095BAE5B6C34B471545E89EF0D030B67A57,
	Scanner_get_Parser_m979901F0DAA8C01C504E6063CEB81CAF7763B105,
	Scanner_set_Parser_m99E56792CCF4FBEB43EE9796576ED4B2536FFF83,
	Scanner_get_Settings_m91DB6300E69A0E0CD0FBE347601A5E0227BA0730,
	Scanner_set_Settings_m8D20BA0EFE8551F0FD047707633104CBAE8F8843,
	Scanner_get_Status_mA753D4F71E7D530A39BA37B9D7FFFF936C2659BB,
	Scanner_set_Status_mD5552C7F68DEF30F9FBFEEF879F470F67F827887,
	Scanner__ctor_m94FED2B991A823E729E6C84B80906D5D5368CBF4,
	Scanner__ctor_m887D7BED6B0AA395AABBE4A381A26B44B4912F7B,
	Scanner__ctor_mB7EDD167F4261E8DFFB2EE27EDC98BC5C018AAB7,
	Scanner__ctor_mCAF2318769E64B22DFDA5CFF6A79637975B28783,
	Scanner_Scan_mB62AC7D2ADB9E4E8CA1C4D4FFC34ED571284A09C,
	Scanner_Stop_m64D1627BB63A655638A98708DBD8E97CFC938679,
	Scanner_Stop_m397CF092F7B4DEBCC7768882FABDB61025D1624D,
	Scanner_Destroy_m1212C19CCF6D5C5CE294A843F84BEEA4A0E5C6D2,
	Scanner_DecodeQR_m6A9B976C1ACD3FA5AD0B415E154E8CAF7EB9169C,
	Scanner_ThreadDecodeQR_m5C483CAAC14D06FFB3E65D53C523A569552AED50,
	Scanner_WebcamInitialized_m887CBD07C520E90D559874BB192129D4A43D58C1,
	Scanner_Update_mA5ED3103A8E6FC3B23BB50086597E68859F4353E,
	Scanner_ToString_mAB715A9148F0E7BCF572B6814C193BC7939BBD6B,
	ParserResult_get_Type_mA9D9FB01A1E6BE522943B5287D0B566797B3EB4F,
	ParserResult_set_Type_mC55BF4CD90AC16AC67151A0F74BCC780F703DCB2,
	ParserResult_get_Value_mFB25627E1855F1C1F2C5928712290B96D2E340B4,
	ParserResult_set_Value_mBBA5CA6D153BBBE4509DF950E3B6F053B71630A1,
	ParserResult__ctor_m668DE86C9BE8BCEE63B2449C0D82D0466B74317B,
	ParserResult_ToString_m43EB3C043312316607AD9DF22F6A19596AC82348,
	ZXingParser_get_Scanner_m08F85842878738605F55014A1D66B1B00AD570E0,
	ZXingParser_set_Scanner_m86C8758F9C833C5F4044BA6D8DC0BB9F6FEFFE9E,
	ZXingParser__ctor_m48F359C0541D5B18AC9862A25923A89B116AACD8,
	ZXingParser__ctor_m61AF43D86FBDB54DEB1FB8532A276A13D1BC6091,
	ZXingParser_Decode_mD01E5648B1C7BF66EEFC92240C0D890CCFEB9F1A,
	Log__ctor_mE537CDA36F47E96D030CAB8336EF4B954C70FCC1,
	Log_get_Services_m2B5A3A08A43C67AFB866534CE20E9D2883F4D0EF,
	Log_AddService_m8DC3847686097BDDF06EA5ED9F1B1F86E013E26B,
	Log_RemoveService_mC81E976611080435EF40785182F2F548D4EF5976,
	Log_ClearServices_m6F38636223985130BE82E50F308A175FBBBCF097,
	Log_AddLog_m92CD33524203EAB6340C753B326A5737DA28A4B7,
	Log_Verbose_m7D77DAD7A1DE115719DCFCA598840971693F55FF,
	Log_Info_mB1B5C80A77D04DC8AE3E735741A87FC844A7A724,
	Log_Debug_mA32A143C3995DC39BF6A935BED41D349774CC464,
	Log_Warning_mBFF44B144DE5FE3211D48DAF0A6C238D8998E921,
	Log_Error_m1BDB8E56C078EB3FACE36C0C45A6ACA9E554F9CD,
	Log_Critical_m7C64FD7A03E4AA4A213394D84EB67A180F80FCB2,
	Log__cctor_m512BC1F6172B05532208D7C27827B3EE1C5E3BC4,
	ConsoleService__ctor_m73D5F1B84712C6A0E5F9215E46E3252088B7A78C,
	ConsoleService_AddLog_m5F279E2859471FD3FD4B737BF93498DBAAE4C683,
	ConsoleService_Format_m98BFB9F4A7BA52D68223B327D427C64255CBC8A1,
	ConsoleService_LogObject_mEA4F017E953066698B34D2F1B89C35C541C04809,
	FileService__ctor_m18A791B5B4AE203CD12603E945AA1DF01DE35071,
	FileService_AddLog_m95BC8561C1A11D04401079C538F27F21531872EE,
	FileService_Format_m2C6E34395E23225857D33B8F3C8C4198B19ECC72,
	NULL,
};
static const int32_t s_InvokerIndices[209] = 
{
	2171,
	1833,
	2183,
	2183,
	2183,
	1760,
	1812,
	2183,
	3676,
	2147,
	1812,
	2183,
	2183,
	2183,
	2183,
	3676,
	2183,
	2147,
	2183,
	2183,
	2183,
	1798,
	2183,
	2169,
	2147,
	2183,
	2147,
	2183,
	2183,
	2183,
	2183,
	2183,
	1337,
	2183,
	1056,
	1056,
	3676,
	2183,
	2183,
	1798,
	2183,
	2169,
	2147,
	2183,
	2147,
	2183,
	2183,
	2183,
	2183,
	2183,
	2183,
	1337,
	2183,
	1056,
	1056,
	1056,
	3676,
	2183,
	2183,
	1798,
	2183,
	2169,
	2147,
	2183,
	2147,
	1337,
	2183,
	2183,
	3660,
	3629,
	2147,
	1812,
	2147,
	1812,
	2183,
	2183,
	2183,
	1812,
	1769,
	2183,
	502,
	1812,
	1812,
	1812,
	1812,
	2133,
	2147,
	2147,
	2147,
	1812,
	2183,
	2183,
	2183,
	2147,
	2133,
	2133,
	2183,
	2169,
	2169,
	2183,
	2183,
	2183,
	1337,
	2171,
	2169,
	2181,
	2181,
	2133,
	2169,
	1831,
	2133,
	1798,
	2171,
	1833,
	2169,
	1831,
	2169,
	1831,
	2169,
	1831,
	2147,
	1812,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2183,
	2147,
	2147,
	1812,
	2179,
	2133,
	1798,
	2133,
	1798,
	2183,
	1812,
	2183,
	2169,
	2169,
	2183,
	2183,
	2183,
	1337,
	2171,
	2169,
	2181,
	2181,
	2133,
	2147,
	1812,
	1812,
	1812,
	1812,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2133,
	1798,
	2183,
	1812,
	1056,
	640,
	1812,
	2183,
	1831,
	2183,
	2183,
	2183,
	2169,
	2183,
	2147,
	2147,
	1812,
	2147,
	1812,
	1056,
	2147,
	2147,
	1812,
	2183,
	1812,
	502,
	2183,
	3660,
	3629,
	3629,
	3676,
	610,
	3369,
	3369,
	3369,
	3369,
	3369,
	3369,
	3676,
	1798,
	610,
	775,
	1337,
	633,
	610,
	494,
	610,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	209,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
