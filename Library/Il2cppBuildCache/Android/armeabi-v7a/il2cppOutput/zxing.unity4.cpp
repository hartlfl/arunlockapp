﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Int64[][]
struct Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
// ZXing.QrCode.Internal.ErrorCorrectionLevel[]
struct ErrorCorrectionLevelU5BU5D_t1A4BFABB8B39B327CE617DFE2B1343C5B5E09421;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Int64[]
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F;
// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B;
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C;
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// ZXing.Binarizer
struct Binarizer_t9B50BEB9FC790D2DE58CB4F5757F97F8F26CD6F0;
// ZXing.Common.BitMatrix
struct BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB;
// ZXing.Color32LuminanceSource
struct Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134;
// ZXing.Common.HybridBinarizer
struct HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// ZXing.LuminanceSource
struct LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167;
// ZXing.RGBLuminanceSource
struct RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493;
// ZXing.ResultPoint
struct ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// ZXing.QrCode.Internal.Version
struct Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// ZXing.Common.Detector.WhiteRectangleDetector
struct WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8;
// ZXing.BarcodeReader/<>c
struct U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE;
// ZXing.BarcodeReaderGeneric/<>c
struct U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30;
// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F;
// BigIntegerLibrary.BigInteger/DigitContainer
struct DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107;
// ZXing.QrCode.Internal.DataMask/<>c
struct U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D;
// ZXing.Aztec.Internal.Detector/Point
struct Point_t671952EEFD2B77F47039C983FD785221F65BC8F2;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator
struct ResultPointsAndTransitionsComparator_tDC383B70BD00047FA5D5D536E6799F1F4F529561;
// ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator
struct CenterComparator_t95A3068152BE5AADC25E56F759AD55997F96B5CE;
// ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator
struct FurthestFromAverageComparator_t5EA62ECFC96D9750AD52441A424D004C060208E0;
// ZXing.Datamatrix.Internal.Version/ECB
struct ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A;
// ZXing.Datamatrix.Internal.Version/ECBlocks
struct ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4;
// ZXing.QrCode.Internal.Version/ECB
struct ECB_t21A54092AE86393FFF1F286B70E027F59ED07064;
// ZXing.QrCode.Internal.Version/ECBlocks
struct ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FormatInformation_tA8C2953407BB1A490AD4DE2CA6E0A4CC0634B6E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____00E14C77230AEF0974CFF3930481157AABDA07B4_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____0222E151247DAE31A8D697EAA14F43F718BD1F1C_2_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____11D4D9FC526D047E01891BEE9949EA42408A800E_17_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____188E7A480B871C554F3F805D3E1184673960EED5_21_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____20D740A7E661D50B06DC32EF6577367D264F6235_34_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____259B7EF54A535F34001B95480B4BB11245EA41B5_42_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____28F991026781E1BC119F02498D75FB3A84F4ED37_44_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____29AB17251F5A730B539AB351FFCB45B9F2FA8027_46_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____40B82CCF037B966E2A6B5F4BAC86904796965A1C_73_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____56F75F925774608B6BF88F69D6124CB44DDF42E1_98_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6882914B984B24C4880E3BEA646DC549B3C34ABC_121_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6AA66657BB3292572C8F56AB3D015A7990C43606_126_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6DE89499E8F85867289E1690E176E1DE58EE6DD3_131_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8D1369A0D8832C941BD6D09849525D65D807A1DD_180_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9B3E34C734351ED3DE1984119EAB9572C48FA15E_197_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9BBAB86ACB95EF8C028708733458CDBAC4715197_198_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9E548860392B66258417232F41EF03D48EBDD686_201_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____AECEE3733C380177F0A39ACF3AF99BFE360C156F_216_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C0935C173C7A144FE24DA09584F4892153D01F3C_236_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5;
IL2CPP_EXTERN_C const RuntimeMethod* Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E;
struct VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F;
struct ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B;
struct ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C;
struct ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// ZXing.Binarizer
struct  Binarizer_t9B50BEB9FC790D2DE58CB4F5757F97F8F26CD6F0  : public RuntimeObject
{
public:
	// ZXing.LuminanceSource ZXing.Binarizer::source
	LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(Binarizer_t9B50BEB9FC790D2DE58CB4F5757F97F8F26CD6F0, ___source_0)); }
	inline LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * get_source_0() const { return ___source_0; }
	inline LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_0), (void*)value);
	}
};


// ZXing.Common.BitMatrix
struct  BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Common.BitMatrix::width
	int32_t ___width_0;
	// System.Int32 ZXing.Common.BitMatrix::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.BitMatrix::rowSize
	int32_t ___rowSize_2;
	// System.Int32[] ZXing.Common.BitMatrix::bits
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___bits_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_rowSize_2() { return static_cast<int32_t>(offsetof(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB, ___rowSize_2)); }
	inline int32_t get_rowSize_2() const { return ___rowSize_2; }
	inline int32_t* get_address_of_rowSize_2() { return &___rowSize_2; }
	inline void set_rowSize_2(int32_t value)
	{
		___rowSize_2 = value;
	}

	inline static int32_t get_offset_of_bits_3() { return static_cast<int32_t>(offsetof(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB, ___bits_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_bits_3() const { return ___bits_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_bits_3() { return &___bits_3; }
	inline void set_bits_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___bits_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bits_3), (void*)value);
	}
};


// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct  ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::bits
	int32_t ___bits_5;
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal_Renamed_Field
	int32_t ___ordinal_Renamed_Field_6;
	// System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_bits_5() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045, ___bits_5)); }
	inline int32_t get_bits_5() const { return ___bits_5; }
	inline int32_t* get_address_of_bits_5() { return &___bits_5; }
	inline void set_bits_5(int32_t value)
	{
		___bits_5 = value;
	}

	inline static int32_t get_offset_of_ordinal_Renamed_Field_6() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045, ___ordinal_Renamed_Field_6)); }
	inline int32_t get_ordinal_Renamed_Field_6() const { return ___ordinal_Renamed_Field_6; }
	inline int32_t* get_address_of_ordinal_Renamed_Field_6() { return &___ordinal_Renamed_Field_6; }
	inline void set_ordinal_Renamed_Field_6(int32_t value)
	{
		___ordinal_Renamed_Field_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_7), (void*)value);
	}
};

struct ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::L
	ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * ___L_0;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::M
	ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * ___M_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::Q
	ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * ___Q_2;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::H
	ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * ___H_3;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel[] ZXing.QrCode.Internal.ErrorCorrectionLevel::FOR_BITS
	ErrorCorrectionLevelU5BU5D_t1A4BFABB8B39B327CE617DFE2B1343C5B5E09421* ___FOR_BITS_4;

public:
	inline static int32_t get_offset_of_L_0() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields, ___L_0)); }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * get_L_0() const { return ___L_0; }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 ** get_address_of_L_0() { return &___L_0; }
	inline void set_L_0(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * value)
	{
		___L_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___L_0), (void*)value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields, ___M_1)); }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * get_M_1() const { return ___M_1; }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 ** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___M_1), (void*)value);
	}

	inline static int32_t get_offset_of_Q_2() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields, ___Q_2)); }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * get_Q_2() const { return ___Q_2; }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 ** get_address_of_Q_2() { return &___Q_2; }
	inline void set_Q_2(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * value)
	{
		___Q_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Q_2), (void*)value);
	}

	inline static int32_t get_offset_of_H_3() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields, ___H_3)); }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * get_H_3() const { return ___H_3; }
	inline ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 ** get_address_of_H_3() { return &___H_3; }
	inline void set_H_3(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * value)
	{
		___H_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___H_3), (void*)value);
	}

	inline static int32_t get_offset_of_FOR_BITS_4() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045_StaticFields, ___FOR_BITS_4)); }
	inline ErrorCorrectionLevelU5BU5D_t1A4BFABB8B39B327CE617DFE2B1343C5B5E09421* get_FOR_BITS_4() const { return ___FOR_BITS_4; }
	inline ErrorCorrectionLevelU5BU5D_t1A4BFABB8B39B327CE617DFE2B1343C5B5E09421** get_address_of_FOR_BITS_4() { return &___FOR_BITS_4; }
	inline void set_FOR_BITS_4(ErrorCorrectionLevelU5BU5D_t1A4BFABB8B39B327CE617DFE2B1343C5B5E09421* value)
	{
		___FOR_BITS_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FOR_BITS_4), (void*)value);
	}
};


// ZXing.LuminanceSource
struct  LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167  : public RuntimeObject
{
public:
	// System.Int32 ZXing.LuminanceSource::width
	int32_t ___width_0;
	// System.Int32 ZXing.LuminanceSource::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};


// ZXing.ResultPoint
struct  ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636  : public RuntimeObject
{
public:
	// System.Single ZXing.ResultPoint::x
	float ___x_0;
	// System.Single ZXing.ResultPoint::y
	float ___y_1;
	// System.Byte[] ZXing.ResultPoint::bytesX
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytesX_2;
	// System.Byte[] ZXing.ResultPoint::bytesY
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytesY_3;
	// System.String ZXing.ResultPoint::toString
	String_t* ___toString_4;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_bytesX_2() { return static_cast<int32_t>(offsetof(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636, ___bytesX_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_bytesX_2() const { return ___bytesX_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_bytesX_2() { return &___bytesX_2; }
	inline void set_bytesX_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___bytesX_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bytesX_2), (void*)value);
	}

	inline static int32_t get_offset_of_bytesY_3() { return static_cast<int32_t>(offsetof(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636, ___bytesY_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_bytesY_3() const { return ___bytesY_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_bytesY_3() { return &___bytesY_3; }
	inline void set_bytesY_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___bytesY_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bytesY_3), (void*)value);
	}

	inline static int32_t get_offset_of_toString_4() { return static_cast<int32_t>(offsetof(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636, ___toString_4)); }
	inline String_t* get_toString_4() const { return ___toString_4; }
	inline String_t** get_address_of_toString_4() { return &___toString_4; }
	inline void set_toString_4(String_t* value)
	{
		___toString_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toString_4), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// ZXing.QrCode.Internal.Version
struct  Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version::versionNumber
	int32_t ___versionNumber_2;
	// System.Int32[] ZXing.QrCode.Internal.Version::alignmentPatternCenters
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___alignmentPatternCenters_3;
	// ZXing.QrCode.Internal.Version/ECBlocks[] ZXing.QrCode.Internal.Version::ecBlocks
	ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* ___ecBlocks_4;
	// System.Int32 ZXing.QrCode.Internal.Version::totalCodewords
	int32_t ___totalCodewords_5;

public:
	inline static int32_t get_offset_of_versionNumber_2() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB, ___versionNumber_2)); }
	inline int32_t get_versionNumber_2() const { return ___versionNumber_2; }
	inline int32_t* get_address_of_versionNumber_2() { return &___versionNumber_2; }
	inline void set_versionNumber_2(int32_t value)
	{
		___versionNumber_2 = value;
	}

	inline static int32_t get_offset_of_alignmentPatternCenters_3() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB, ___alignmentPatternCenters_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_alignmentPatternCenters_3() const { return ___alignmentPatternCenters_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_alignmentPatternCenters_3() { return &___alignmentPatternCenters_3; }
	inline void set_alignmentPatternCenters_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___alignmentPatternCenters_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alignmentPatternCenters_3), (void*)value);
	}

	inline static int32_t get_offset_of_ecBlocks_4() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB, ___ecBlocks_4)); }
	inline ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* get_ecBlocks_4() const { return ___ecBlocks_4; }
	inline ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22** get_address_of_ecBlocks_4() { return &___ecBlocks_4; }
	inline void set_ecBlocks_4(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* value)
	{
		___ecBlocks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ecBlocks_4), (void*)value);
	}

	inline static int32_t get_offset_of_totalCodewords_5() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB, ___totalCodewords_5)); }
	inline int32_t get_totalCodewords_5() const { return ___totalCodewords_5; }
	inline int32_t* get_address_of_totalCodewords_5() { return &___totalCodewords_5; }
	inline void set_totalCodewords_5(int32_t value)
	{
		___totalCodewords_5 = value;
	}
};

struct Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Version::VERSION_DECODE_INFO
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___VERSION_DECODE_INFO_0;
	// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::VERSIONS
	VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* ___VERSIONS_1;

public:
	inline static int32_t get_offset_of_VERSION_DECODE_INFO_0() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields, ___VERSION_DECODE_INFO_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_VERSION_DECODE_INFO_0() const { return ___VERSION_DECODE_INFO_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_VERSION_DECODE_INFO_0() { return &___VERSION_DECODE_INFO_0; }
	inline void set_VERSION_DECODE_INFO_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___VERSION_DECODE_INFO_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VERSION_DECODE_INFO_0), (void*)value);
	}

	inline static int32_t get_offset_of_VERSIONS_1() { return static_cast<int32_t>(offsetof(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields, ___VERSIONS_1)); }
	inline VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* get_VERSIONS_1() const { return ___VERSIONS_1; }
	inline VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F** get_address_of_VERSIONS_1() { return &___VERSIONS_1; }
	inline void set_VERSIONS_1(VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* value)
	{
		___VERSIONS_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VERSIONS_1), (void*)value);
	}
};


// ZXing.Common.Detector.WhiteRectangleDetector
struct  WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.Detector.WhiteRectangleDetector::image
	BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image_0;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::width
	int32_t ___width_2;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::leftInit
	int32_t ___leftInit_3;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::rightInit
	int32_t ___rightInit_4;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::downInit
	int32_t ___downInit_5;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::upInit
	int32_t ___upInit_6;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___image_0)); }
	inline BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * get_image_0() const { return ___image_0; }
	inline BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_0), (void*)value);
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_leftInit_3() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___leftInit_3)); }
	inline int32_t get_leftInit_3() const { return ___leftInit_3; }
	inline int32_t* get_address_of_leftInit_3() { return &___leftInit_3; }
	inline void set_leftInit_3(int32_t value)
	{
		___leftInit_3 = value;
	}

	inline static int32_t get_offset_of_rightInit_4() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___rightInit_4)); }
	inline int32_t get_rightInit_4() const { return ___rightInit_4; }
	inline int32_t* get_address_of_rightInit_4() { return &___rightInit_4; }
	inline void set_rightInit_4(int32_t value)
	{
		___rightInit_4 = value;
	}

	inline static int32_t get_offset_of_downInit_5() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___downInit_5)); }
	inline int32_t get_downInit_5() const { return ___downInit_5; }
	inline int32_t* get_address_of_downInit_5() { return &___downInit_5; }
	inline void set_downInit_5(int32_t value)
	{
		___downInit_5 = value;
	}

	inline static int32_t get_offset_of_upInit_6() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8, ___upInit_6)); }
	inline int32_t get_upInit_6() const { return ___upInit_6; }
	inline int32_t* get_address_of_upInit_6() { return &___upInit_6; }
	inline void set_upInit_6(int32_t value)
	{
		___upInit_6 = value;
	}
};


// ZXing.BarcodeReader/<>c
struct  U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_StaticFields
{
public:
	// ZXing.BarcodeReader/<>c ZXing.BarcodeReader/<>c::<>9
	U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// ZXing.BarcodeReaderGeneric/<>c
struct  U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_StaticFields
{
public:
	// ZXing.BarcodeReaderGeneric/<>c ZXing.BarcodeReaderGeneric/<>c::<>9
	U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct  DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F  : public RuntimeObject
{
public:
	// System.Int64[][] BigIntegerLibrary.Base10BigInteger/DigitContainer::digits
	Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* ___digits_0;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F, ___digits_0)); }
	inline Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* get_digits_0() const { return ___digits_0; }
	inline Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___digits_0), (void*)value);
	}
};


// BigIntegerLibrary.BigInteger/DigitContainer
struct  DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107  : public RuntimeObject
{
public:
	// System.Int64[][] BigIntegerLibrary.BigInteger/DigitContainer::digits
	Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* ___digits_0;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107, ___digits_0)); }
	inline Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* get_digits_0() const { return ___digits_0; }
	inline Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___digits_0), (void*)value);
	}
};


// ZXing.QrCode.Internal.DataMask/<>c
struct  U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_StaticFields
{
public:
	// ZXing.QrCode.Internal.DataMask/<>c ZXing.QrCode.Internal.DataMask/<>c::<>9
	U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// ZXing.Aztec.Internal.Detector/Point
struct  Point_t671952EEFD2B77F47039C983FD785221F65BC8F2  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Aztec.Internal.Detector/Point::<X>k__BackingField
	int32_t ___U3CXU3Ek__BackingField_0;
	// System.Int32 ZXing.Aztec.Internal.Detector/Point::<Y>k__BackingField
	int32_t ___U3CYU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Point_t671952EEFD2B77F47039C983FD785221F65BC8F2, ___U3CXU3Ek__BackingField_0)); }
	inline int32_t get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(int32_t value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Point_t671952EEFD2B77F47039C983FD785221F65BC8F2, ___U3CYU3Ek__BackingField_1)); }
	inline int32_t get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(int32_t value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}
};


// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct  ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1  : public RuntimeObject
{
public:
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<From>k__BackingField
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___U3CFromU3Ek__BackingField_0;
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<To>k__BackingField
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___U3CToU3Ek__BackingField_1;
	// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<Transitions>k__BackingField
	int32_t ___U3CTransitionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1, ___U3CFromU3Ek__BackingField_0)); }
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * get_U3CFromU3Ek__BackingField_0() const { return ___U3CFromU3Ek__BackingField_0; }
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 ** get_address_of_U3CFromU3Ek__BackingField_0() { return &___U3CFromU3Ek__BackingField_0; }
	inline void set_U3CFromU3Ek__BackingField_0(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * value)
	{
		___U3CFromU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFromU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1, ___U3CToU3Ek__BackingField_1)); }
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * get_U3CToU3Ek__BackingField_1() const { return ___U3CToU3Ek__BackingField_1; }
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 ** get_address_of_U3CToU3Ek__BackingField_1() { return &___U3CToU3Ek__BackingField_1; }
	inline void set_U3CToU3Ek__BackingField_1(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * value)
	{
		___U3CToU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CToU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTransitionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1, ___U3CTransitionsU3Ek__BackingField_2)); }
	inline int32_t get_U3CTransitionsU3Ek__BackingField_2() const { return ___U3CTransitionsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTransitionsU3Ek__BackingField_2() { return &___U3CTransitionsU3Ek__BackingField_2; }
	inline void set_U3CTransitionsU3Ek__BackingField_2(int32_t value)
	{
		___U3CTransitionsU3Ek__BackingField_2 = value;
	}
};


// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator
struct  ResultPointsAndTransitionsComparator_tDC383B70BD00047FA5D5D536E6799F1F4F529561  : public RuntimeObject
{
public:

public:
};


// ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator
struct  CenterComparator_t95A3068152BE5AADC25E56F759AD55997F96B5CE  : public RuntimeObject
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::average
	float ___average_0;

public:
	inline static int32_t get_offset_of_average_0() { return static_cast<int32_t>(offsetof(CenterComparator_t95A3068152BE5AADC25E56F759AD55997F96B5CE, ___average_0)); }
	inline float get_average_0() const { return ___average_0; }
	inline float* get_address_of_average_0() { return &___average_0; }
	inline void set_average_0(float value)
	{
		___average_0 = value;
	}
};


// ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator
struct  FurthestFromAverageComparator_t5EA62ECFC96D9750AD52441A424D004C060208E0  : public RuntimeObject
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::average
	float ___average_0;

public:
	inline static int32_t get_offset_of_average_0() { return static_cast<int32_t>(offsetof(FurthestFromAverageComparator_t5EA62ECFC96D9750AD52441A424D004C060208E0, ___average_0)); }
	inline float get_average_0() const { return ___average_0; }
	inline float* get_address_of_average_0() { return &___average_0; }
	inline void set_average_0(float value)
	{
		___average_0 = value;
	}
};


// ZXing.Datamatrix.Internal.Version/ECB
struct  ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::count
	int32_t ___count_0;
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::dataCodewords
	int32_t ___dataCodewords_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dataCodewords_1() { return static_cast<int32_t>(offsetof(ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A, ___dataCodewords_1)); }
	inline int32_t get_dataCodewords_1() const { return ___dataCodewords_1; }
	inline int32_t* get_address_of_dataCodewords_1() { return &___dataCodewords_1; }
	inline void set_dataCodewords_1(int32_t value)
	{
		___dataCodewords_1 = value;
	}
};


// ZXing.Datamatrix.Internal.Version/ECBlocks
struct  ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::ecCodewords
	int32_t ___ecCodewords_0;
	// ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::_ecBlocksValue
	ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* ____ecBlocksValue_1;

public:
	inline static int32_t get_offset_of_ecCodewords_0() { return static_cast<int32_t>(offsetof(ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4, ___ecCodewords_0)); }
	inline int32_t get_ecCodewords_0() const { return ___ecCodewords_0; }
	inline int32_t* get_address_of_ecCodewords_0() { return &___ecCodewords_0; }
	inline void set_ecCodewords_0(int32_t value)
	{
		___ecCodewords_0 = value;
	}

	inline static int32_t get_offset_of__ecBlocksValue_1() { return static_cast<int32_t>(offsetof(ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4, ____ecBlocksValue_1)); }
	inline ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* get__ecBlocksValue_1() const { return ____ecBlocksValue_1; }
	inline ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B** get_address_of__ecBlocksValue_1() { return &____ecBlocksValue_1; }
	inline void set__ecBlocksValue_1(ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* value)
	{
		____ecBlocksValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ecBlocksValue_1), (void*)value);
	}
};


// ZXing.QrCode.Internal.Version/ECB
struct  ECB_t21A54092AE86393FFF1F286B70E027F59ED07064  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version/ECB::count
	int32_t ___count_0;
	// System.Int32 ZXing.QrCode.Internal.Version/ECB::dataCodewords
	int32_t ___dataCodewords_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dataCodewords_1() { return static_cast<int32_t>(offsetof(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064, ___dataCodewords_1)); }
	inline int32_t get_dataCodewords_1() const { return ___dataCodewords_1; }
	inline int32_t* get_address_of_dataCodewords_1() { return &___dataCodewords_1; }
	inline void set_dataCodewords_1(int32_t value)
	{
		___dataCodewords_1 = value;
	}
};


// ZXing.QrCode.Internal.Version/ECBlocks
struct  ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::ecCodewordsPerBlock
	int32_t ___ecCodewordsPerBlock_0;
	// ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::ecBlocks
	ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ___ecBlocks_1;

public:
	inline static int32_t get_offset_of_ecCodewordsPerBlock_0() { return static_cast<int32_t>(offsetof(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0, ___ecCodewordsPerBlock_0)); }
	inline int32_t get_ecCodewordsPerBlock_0() const { return ___ecCodewordsPerBlock_0; }
	inline int32_t* get_address_of_ecCodewordsPerBlock_0() { return &___ecCodewordsPerBlock_0; }
	inline void set_ecCodewordsPerBlock_0(int32_t value)
	{
		___ecCodewordsPerBlock_0 = value;
	}

	inline static int32_t get_offset_of_ecBlocks_1() { return static_cast<int32_t>(offsetof(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0, ___ecBlocks_1)); }
	inline ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* get_ecBlocks_1() const { return ___ecBlocks_1; }
	inline ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C** get_address_of_ecBlocks_1() { return &___ecBlocks_1; }
	inline void set_ecBlocks_1(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* value)
	{
		___ecBlocks_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ecBlocks_1), (void*)value);
	}
};


// ZXing.BaseLuminanceSource
struct  BaseLuminanceSource_tFEDADACD44E500F99D11BFD0F3EF60D2CF33E1B5  : public LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167
{
public:
	// System.Byte[] ZXing.BaseLuminanceSource::luminances
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___luminances_2;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(BaseLuminanceSource_tFEDADACD44E500F99D11BFD0F3EF60D2CF33E1B5, ___luminances_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___luminances_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color32
struct  Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// ZXing.QrCode.Internal.FinderPattern
struct  FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134  : public ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPattern::estimatedModuleSize
	float ___estimatedModuleSize_5;
	// System.Int32 ZXing.QrCode.Internal.FinderPattern::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_estimatedModuleSize_5() { return static_cast<int32_t>(offsetof(FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134, ___estimatedModuleSize_5)); }
	inline float get_estimatedModuleSize_5() const { return ___estimatedModuleSize_5; }
	inline float* get_address_of_estimatedModuleSize_5() { return &___estimatedModuleSize_5; }
	inline void set_estimatedModuleSize_5(float value)
	{
		___estimatedModuleSize_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};


// ZXing.Common.GlobalHistogramBinarizer
struct  GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043  : public Binarizer_t9B50BEB9FC790D2DE58CB4F5757F97F8F26CD6F0
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::luminances
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___luminances_2;
	// System.Int32[] ZXing.Common.GlobalHistogramBinarizer::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_3;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043, ___luminances_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___luminances_2), (void*)value);
	}

	inline static int32_t get_offset_of_buckets_3() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043, ___buckets_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_3() const { return ___buckets_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_3() { return &___buckets_3; }
	inline void set_buckets_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_3), (void*)value);
	}
};

struct GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043_StaticFields
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::EMPTY
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___EMPTY_1;

public:
	inline static int32_t get_offset_of_EMPTY_1() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043_StaticFields, ___EMPTY_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_EMPTY_1() const { return ___EMPTY_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_EMPTY_1() { return &___EMPTY_1; }
	inline void set_EMPTY_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___EMPTY_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EMPTY_1), (void*)value);
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C__padding[10];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148
struct  __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137__padding[11148];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4__padding[12];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A__padding[120];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=136
struct  __StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67__padding[136];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=148
struct  __StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD__padding[148];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=156
struct  __StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716__padding[156];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0__padding[16];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=176
struct  __StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9__padding[176];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=192
struct  __StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8__padding[192];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2__padding[20];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D__padding[24];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2574
struct  __StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55__padding[2574];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26
struct  __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA__padding[26];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9__padding[28];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30
struct  __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F__padding[30];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E__padding[32];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625__padding[36];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3__padding[40];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17__padding[44];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0__padding[52];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=54
struct  __StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69__padding[54];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832__padding[6];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2__padding[64];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80
struct  __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6__padding[80];
	};

public:
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::00E14C77230AEF0974CFF3930481157AABDA07B4
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___00E14C77230AEF0974CFF3930481157AABDA07B4_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::01A7BB0BE820D25E52A42C3D28DE27822EABCD4C
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::0222E151247DAE31A8D697EAA14F43F718BD1F1C
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::03AB6BB65AC7B8ECF309D37CACECE4B53B38D824
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::0443C73B92DF9C67128EF5F0702DCBC469EDD2EA
	__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::061E1E805CBF51D839C3B91E8ACD0326E5C77FFE
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::07438A85BDEE90305A6125B92084A5F3F2FD5F18
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::094D7DFD41161273E784D60EAE6D844127D77F0B
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___094D7DFD41161273E784D60EAE6D844127D77F0B_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::0C7D347178E5302733AED896AA3A7B5DDEDC3A90
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::0D0AD555C6DA0BD4268F8573DC103F455F62A610
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::0E4585982EA19664C33EA213EE69EC548F7322B0
	__StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17  ___0E4585982EA19664C33EA213EE69EC548F7322B0_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::10111111FB24B2D331FB3F698193046CBCB8AAFE
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___10111111FB24B2D331FB3F698193046CBCB8AAFE_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::1037DC6EAB9D7D3850660726ED132EAD961A41E9
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::10486CAECBA5E73DA05910F94513409DEB6316FD
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___10486CAECBA5E73DA05910F94513409DEB6316FD_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::11D4D9FC526D047E01891BEE9949EA42408A800E
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___11D4D9FC526D047E01891BEE9949EA42408A800E_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::12625AC94FB97DAA377449A877D25786C8ADFFC5
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___12625AC94FB97DAA377449A877D25786C8ADFFC5_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::12F1DB2AC68CD455638374E8DF9E9D604B15F693
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::142E1469231D592823559059166B32655910D0CF
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___142E1469231D592823559059166B32655910D0CF_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::188E7A480B871C554F3F805D3E1184673960EED5
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___188E7A480B871C554F3F805D3E1184673960EED5_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::193330123B5DF8BA7DDD1FD73E3941FD113C71C7
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1A3C01816F6E750EC65FDEF4FEB7758235456890
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___1A3C01816F6E750EC65FDEF4FEB7758235456890_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1A85140B8FCA30191EE9347F53782234F0781F79
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___1A85140B8FCA30191EE9347F53782234F0781F79_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1BCA1A912FF22DA69AA3E91A68492D4BA5179162
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1D0501D14FE31729CD75A4073F4404AF7E53B024
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___1D0501D14FE31729CD75A4073F4404AF7E53B024_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1D562AD827598FC3F5C82FC2091E65C8F8A07ADC
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1E45F828069EAF87A0B553BE1E65B98F20E377ED
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::1EB30BCD6A1576A001C2906D3AF45955E19C48BC
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::1F5771BF2009B14CC123999096615351A0BE9527
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___1F5771BF2009B14CC123999096615351A0BE9527_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::1FFDEC0F27BC9DCF708F067786205F164F8A6D32
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::20D740A7E661D50B06DC32EF6577367D264F6235
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___20D740A7E661D50B06DC32EF6577367D264F6235_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::227C649F55292B0A293EBB2D56AB8ABAECC7F629
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::22BF5B86DC5E0B87331903141C5973C117DA716E
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___22BF5B86DC5E0B87331903141C5973C117DA716E_36;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::2357B210FC2522A4A9D0F465AEAC250A79FA9449
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_38;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::241FC42CDC9A5435B317D1D425FB9AB8A93B16CD
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::24E7D35141138E4668DE547A98CE3A84AF0E6FEB
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_41;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::259B7EF54A535F34001B95480B4BB11245EA41B5
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___259B7EF54A535F34001B95480B4BB11245EA41B5_42;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::28F991026781E1BC119F02498D75FB3A84F4ED37
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___28F991026781E1BC119F02498D75FB3A84F4ED37_44;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2923D4191FF7FBBF11BFC643B6D93CC8074D31E1
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::29AB17251F5A730B539AB351FFCB45B9F2FA8027
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_46;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::2A84324D54FDC0473BE021004C50F363ADCD89AF
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___2A84324D54FDC0473BE021004C50F363ADCD89AF_47;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::2D3DF989BE3BBA2B353F845C37992DFF85579505
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___2D3DF989BE3BBA2B353F845C37992DFF85579505_49;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2DFCD7C13A7339CC41C473220CE80CDD5933C64C
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_50;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::2EC69895F74DBAE74600E960EFFB6448597BDEEB
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_51;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::3102377CA043644BFD52C56AE2A9F554723263FD
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___3102377CA043644BFD52C56AE2A9F554723263FD_53;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3199DD35AA6A041AF9F66BFBAEC3563E838D3731
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_54;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::322BC3569CD0270EC3179B30382F24148B533D10
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___322BC3569CD0270EC3179B30382F24148B533D10_55;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=156 <PrivateImplementationDetails>::33B8540230E7366A64AC520E4F819103EB00ECD0
	__StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716  ___33B8540230E7366A64AC520E4F819103EB00ECD0_56;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::34617E1F9E901BD9DD9FEB1839483F5ED406E7B3
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::35859E8329070E55C5D9282930023DD6B1594141
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___35859E8329070E55C5D9282930023DD6B1594141_58;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::38017E50AF503681BFC2BD0CDED5F7565471B574
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___38017E50AF503681BFC2BD0CDED5F7565471B574_61;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::38CAE5BC1B31E24A88061CEC362093BB4D8F0523
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_62;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=54 <PrivateImplementationDetails>::3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C
	__StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69  ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3C22A633B79B510613E37BC5A02FF91A4AB006ED
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_64;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::3D3C46CC589F65C2443688952CBCD9C499839622
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___3D3C46CC589F65C2443688952CBCD9C499839622_65;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3D8521A958BBFCC708EE0728993AE8CAFEB154D6
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_66;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3DF543E51AB43F1F561B11B8789C82E26C427F72
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___3DF543E51AB43F1F561B11B8789C82E26C427F72_67;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148 <PrivateImplementationDetails>::3E12851A90C6A08ACCD1D67634AC7FD1639B898D
	__StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_68;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD
	__StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3F152B3DFAF72A55D78439E5015C55A20967621A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___3F152B3DFAF72A55D78439E5015C55A20967621A_71;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=136 <PrivateImplementationDetails>::40B82CCF037B966E2A6B5F4BAC86904796965A1C
	__StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67  ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_73;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::40FEF102AB67B5038E6D9E2EE2BE16854A49810D
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_74;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=176 <PrivateImplementationDetails>::42A99199D41E8832E72CD942706DDA326C0BB356
	__StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9  ___42A99199D41E8832E72CD942706DDA326C0BB356_75;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::445E0C138E7D9647FCD08184FB23A833C4D00D0D
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_76;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::44DB2483C3365FFD6E86B5C807C599AAF3EEE238
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_77;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::463C3B3709985BB32747ACC9AE3406BB6836368B
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___463C3B3709985BB32747ACC9AE3406BB6836368B_79;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::465B4A79F3CD93D22FD6AFE37CB544AC27728D1D
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::471648086F9480CB553B7802323E0A75FFE09C5A
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___471648086F9480CB553B7802323E0A75FFE09C5A_81;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::484B207F3CBCE8338BDE26F04E87EE256891BE0F
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_82;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::49594851C4B6C279C35B162F0236A99E7C31DD36
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___49594851C4B6C279C35B162F0236A99E7C31DD36_85;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::4D317202204E9741354BC1A1628BC10A05E2AB7B
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___4D317202204E9741354BC1A1628BC10A05E2AB7B_89;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::4DE742FEB32578958FCFC33D6732EFF616C8A94B
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_90;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::50DACD1562B0163C9FC5B673478D4231A4B37B1F
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_91;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::53173106B161530CA26C69983B44BBE2FAB890D0
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___53173106B161530CA26C69983B44BBE2FAB890D0_93;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::539B26D17C2DEB88FB9E07611D164408E23C35AB
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___539B26D17C2DEB88FB9E07611D164408E23C35AB_94;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::55A6E22CC24364881F62CCF67CBE9A348DC602FA
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_96;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::56F75F925774608B6BF88F69D6124CB44DDF42E1
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___56F75F925774608B6BF88F69D6124CB44DDF42E1_98;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5741ABC064FCDEF4C5EE02F31DB992C205EA7231
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_99;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::583A438479F42C7D8DE8C6699B0C37A4A026A643
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___583A438479F42C7D8DE8C6699B0C37A4A026A643_100;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::58B2CCE2545886B61E58C3C60831C2B049814E09
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___58B2CCE2545886B61E58C3C60831C2B049814E09_101;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5984617C3B1EF1E6C9F13221059A1E78C2226ADF
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_102;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::59A84F660FFFFA4B9C037456F9D56E26ACF3663E
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_103;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5B02FED4CA0B61476F1AAA5C6445F024374B075A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_104;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::5C563BFAB6400B2824F5E41D78FB3097E8244C26
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_109;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5CB924E7461B9011EC8197A30815EA263595B3A4
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___5CB924E7461B9011EC8197A30815EA263595B3A4_110;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5D160627930BB84F7867AEEC7143FB2399C7CA79
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___5D160627930BB84F7867AEEC7143FB2399C7CA79_111;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5DD69D93CBAA5623AE286E97265890A7693A0AC2
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_112;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5E3F56450E340B21A3A13E39A66941353F8C7C1A
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_113;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5FD971B7E6A431E6F3F91D5A95F2006A24F48780
	__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_114;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::60E3C19D774F8C210299534335ED5D3154717218
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___60E3C19D774F8C210299534335ED5D3154717218_115;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::61A1021606CAD0A5990E00FD305478E75F1983D6
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___61A1021606CAD0A5990E00FD305478E75F1983D6_116;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::648C489642395164C2A2AA2379A6E7F44CAB018B
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___648C489642395164C2A2AA2379A6E7F44CAB018B_117;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::661F9E9FEA16C3A9CDF452AB61743578471F7447
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___661F9E9FEA16C3A9CDF452AB61743578471F7447_118;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::67013075958AAD75C4545796BF80B16683B33A59
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___67013075958AAD75C4545796BF80B16683B33A59_119;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6727D77DB74B08CC9003029AC92B7AC68E258811
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6727D77DB74B08CC9003029AC92B7AC68E258811_120;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6882914B984B24C4880E3BEA646DC549B3C34ABC
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___6882914B984B24C4880E3BEA646DC549B3C34ABC_121;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::6891D97C19F4C291AD821EF05E4C5D1BD3C34796
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_122;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::69499AF6438C222AAADFC92FF3D3DF3902A454FC
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_123;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6957A133E0DC21707BCAAF62194EAF4FF75E0768
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_124;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::6A09CB785087F5A9F6EC748C7C10F98FA2683534
	__StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_125;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6AA66657BB3292572C8F56AB3D015A7990C43606
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6AA66657BB3292572C8F56AB3D015A7990C43606_126;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::6AF652181FBB423ED995C5CF63D5B83726DE74CF
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_127;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6B38D84409B2091CBC93686439AC2C04C76DB96E
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6B38D84409B2091CBC93686439AC2C04C76DB96E_128;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6CC1359B9ED72500183D5566D70BBEE1BBD46FFF
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6CEDD7504235CF66C53FB70A17ACBDD440D77C62
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_130;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6DE89499E8F85867289E1690E176E1DE58EE6DD3
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_131;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::6F604C33212E20CBFCE9F2221492DC6FD823857C
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___6F604C33212E20CBFCE9F2221492DC6FD823857C_133;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::7102A07EDF52C052D5625D73275A579DC6E76C92
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___7102A07EDF52C052D5625D73275A579DC6E76C92_134;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::712B24DD3C17671C78BE33C83316576AD9285273
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___712B24DD3C17671C78BE33C83316576AD9285273_135;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::71F4D547D52F31EDEC55B1909114455FDAE5A97E
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_137;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::72FF92F22AB5BD5A344C652E176645C52D825AEC
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___72FF92F22AB5BD5A344C652E176645C52D825AEC_139;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::73B101BBEDC241CE765CA03BB6E4D467FA769312
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___73B101BBEDC241CE765CA03BB6E4D467FA769312_140;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::749EB212DEC39495349481F49F3DEF480777A197
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___749EB212DEC39495349481F49F3DEF480777A197_141;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2574 <PrivateImplementationDetails>::74AE47B2D78B62583BA50045F6B288CACC24EA7D
	__StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55  ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_142;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7616E4F44CA76DF539C11B9C8FD911D667FEFC52
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::761EC2969B9D1960DB3CB7FC05EEC96944B59BC6
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::76D7CB1729978C5437409A8C56AA1C7E24890D5C
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_145;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::770616E567083077933DC0B29A8F8368FC2EB431
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___770616E567083077933DC0B29A8F8368FC2EB431_146;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79123EF44D00F5B0ED365348D1FD49F9FA2CB222
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_148;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::79A4B717DAF4312A78F1A75A72221DB9690D1B25
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_149;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79BEC582299CE5F46902B9C091C19931E26D9323
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___79BEC582299CE5F46902B9C091C19931E26D9323_150;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79F14B68470B4B7AA3BCDCA6758072F2B9320320
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_151;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::79F9F064F81690C950BF4EFFAA5245BA966607C9
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___79F9F064F81690C950BF4EFFAA5245BA966607C9_152;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::7BC6404D48DA137303466471F7131DC1C9B13BAA
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___7BC6404D48DA137303466471F7131DC1C9B13BAA_153;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::7CF92A83536E2264EF41D8C407D246E41E2DBCD6
	__StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_154;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=192 <PrivateImplementationDetails>::7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739
	__StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8  ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::7FDC1CA6ED532684E45DDAE8484274399FCE4C59
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_157;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::7FF393DF5D10580FA38BF854328B3072169E0A1A
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___7FF393DF5D10580FA38BF854328B3072169E0A1A_158;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::802AD21B222E489DAFEADD5E18868203D9FC2A89
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___802AD21B222E489DAFEADD5E18868203D9FC2A89_159;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::806D200653BD7335D0077F2759E715D8BE8DF9BF
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___806D200653BD7335D0077F2759E715D8BE8DF9BF_160;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::808AD19CA818077F71100880E7D7AA21147E987E
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___808AD19CA818077F71100880E7D7AA21147E987E_161;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::80DD6A282D563836522C10A78AAA17F0B865EB5A
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___80DD6A282D563836522C10A78AAA17F0B865EB5A_162;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::80E21576779C61DA5B185D15A732CF7A8BD6F88C
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_163;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::80E9217618F8C2C7FED61E4E7653CD0E66C74872
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_164;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::80F763BA90E27F755F2DEFA7FE720AD9371D6E6B
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::836DA48724BC66F3257E70FFF165ACCE037666A6
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___836DA48724BC66F3257E70FFF165ACCE037666A6_166;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8523C737ED49D8A2E9BB85218190E4E83B902E28
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___8523C737ED49D8A2E9BB85218190E4E83B902E28_167;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::85666B25692AB814E91F685384EEA0389F147E70
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___85666B25692AB814E91F685384EEA0389F147E70_168;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::866652192B68681CC538B2CB21C5979544DF35AB
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___866652192B68681CC538B2CB21C5979544DF35AB_169;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::8683FBE90F936E0F2A1080E053EBA3EE9F44A02B
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::868C8972C4058443A2D131C22083401956DF81C7
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___868C8972C4058443A2D131C22083401956DF81C7_171;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::86EA004891DE06B357581B1885C9C0EFD73DE2E9
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_172;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::881C2E8F71772ECA0CFC4DB357196C4EB788D6F8
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::88B6AF1079C72775E54D0AC3BEEDCCC6506B6269
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::88CFCB31ED9AFFB90483973D02D5DEE8A82A4892
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8909772CC771C2FF1D61415555452F2ED4ADA536
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___8909772CC771C2FF1D61415555452F2ED4ADA536_176;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8971045D9FA23669DC4A204D478DC63888A9B420
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___8971045D9FA23669DC4A204D478DC63888A9B420_177;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE
	__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::8C301DDB45068B145D6C11F79591061EEE2AABA8
	__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  ___8C301DDB45068B145D6C11F79591061EEE2AABA8_179;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::8D1369A0D8832C941BD6D09849525D65D807A1DD
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___8D1369A0D8832C941BD6D09849525D65D807A1DD_180;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8EED33571FFED1BBCA4954A909498A1D3316EDC8
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_184;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8EEE72A955DF384253C5C458726A8A7299A05164
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___8EEE72A955DF384253C5C458726A8A7299A05164_185;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8FD55399F538FAF9608E938EAB08D4BC88D0E131
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_186;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9032D4E1ED9EBAC2417A10654FF0C658D2CC9825
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::91EA1528B7E2B59DB78E5E7A749312DD20AA43F1
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9623BB2649942957F680075A5AD3A16DE427F7A2
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___9623BB2649942957F680075A5AD3A16DE427F7A2_190;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9858ACE49B9B39354CFCB05E54A9790070E48D39
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___9858ACE49B9B39354CFCB05E54A9790070E48D39_192;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::99CC0827BC16027263E3737FF59411A5D4BFDE31
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___99CC0827BC16027263E3737FF59411A5D4BFDE31_194;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9ACE167D7580B82857BBF66A5A9BC1E2D139353C
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_196;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9B3E34C734351ED3DE1984119EAB9572C48FA15E
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_197;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9BBAB86ACB95EF8C028708733458CDBAC4715197
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___9BBAB86ACB95EF8C028708733458CDBAC4715197_198;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9E548860392B66258417232F41EF03D48EBDD686
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___9E548860392B66258417232F41EF03D48EBDD686_201;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::9F8CAB0A98B00335FDDBCC16C748FEA524A25435
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_202;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::A0324640CCF369A6794A9A8BB204ED22651ECBCC
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_203;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::A2978A4F76015511E3741ABF9B58117B348B19C2
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___A2978A4F76015511E3741ABF9B58117B348B19C2_204;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86
	__StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A350EF43D37B02EFF537EB2B57DD4CF05DD72C47
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::A3F55D69F9A97411DB4065F0FA414D912A181B69
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___A3F55D69F9A97411DB4065F0FA414D912A181B69_207;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::A6E975E72D8A987E6B9AA9400DE3025C1E91BE96
	__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A7977502040EE08933752F579060577814723508
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___A7977502040EE08933752F579060577814723508_209;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A98B8F0C923F5DFC97332FCC80F65C96F9F27181
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AA607BA0FA20D3E80F456E68768C517D17D073D5
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___AA607BA0FA20D3E80F456E68768C517D17D073D5_212;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::AB44F69765F4A8ECD5D47354EA625039C43CC4D6
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AE2DCC8D6F51ED60D95A61A92C489BB896645BEA
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AECEE3733C380177F0A39ACF3AF99BFE360C156F
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_216;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AEE03822C3725428610E6E7DAAF7970DE35F7675
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___AEE03822C3725428610E6E7DAAF7970DE35F7675_217;
	// System.Int64 <PrivateImplementationDetails>::AF35D391D56E5153255BA0E0810F3EF49FC4CE6A
	int64_t ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::B15D37D4738DBDC32C38C2E1B6A1833322C22868
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_219;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::B19381C60E723A535EF856EC118DB867503FCC63
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___B19381C60E723A535EF856EC118DB867503FCC63_220;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B5DDF34D20E879DBA76358A13661F61E6682B937
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___B5DDF34D20E879DBA76358A13661F61E6682B937_222;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::B6BCDFDEDD07A19F1028F226DE019E32E7FB2333
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BB40482B7503DCF14BB6DAE300C2717C3021C7B2
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::BBA3BFEF194A76998C4874D107EBEA8A81357762
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___BBA3BFEF194A76998C4874D107EBEA8A81357762_226;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::BBAA25706062E8AA738F96C541E3A6D6357F92E3
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_227;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BBE42ED1645F9D77581D1D5B6EE11B933512F832
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_228;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BC1ACC3AC4459BD0D19A953A64E05727DA98279A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BC5EFA0F409033551EB01FD9CAA887739FBB267C
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_230;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::BDA0E80EABB31C2B339BA084415FF30AE14ECF9F
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BF4FBD059EA47B09FA514A81515DD83A7F22A498
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_233;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::C04B3C75D3B52DBFE727A123E3E6A157786BBD6F
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::C0935C173C7A144FE24DA09584F4892153D01F3C
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___C0935C173C7A144FE24DA09584F4892153D01F3C_236;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::C4449C115C6F0E639E5F53D593F6E85D55D18DB0
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::C530C580972A28DBE79D791F61E5916A541E6A1A
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___C530C580972A28DBE79D791F61E5916A541E6A1A_239;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C62717FC35BA54563F42385BF79E44DD0A510124
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___C62717FC35BA54563F42385BF79E44DD0A510124_241;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::C6B6783FDC75202733D0518585E649906008DBC5
	__StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  ___C6B6783FDC75202733D0518585E649906008DBC5_242;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::C6CA93B7FFF2C18F722DDBD501761C1381E2E55E
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::C7787E3098145FA28D38DE952D634BA862284127
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___C7787E3098145FA28D38DE952D634BA862284127_244;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::C7BD442E02179DC75AB7FEF343C297E1F46A7806
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_245;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C7FBA0F208A16658206056EC89481A5EE74A7259
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___C7FBA0F208A16658206056EC89481A5EE74A7259_246;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C82F7744F78AA5830EDBD9AD774CFF76737F7D40
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C92559E411FAF1D47ADD6CCA7374DAF066069D00
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_248;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CA03F848D4A9DD85B5CF092E36317D483E3B0864
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_250;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CB780513B9E4B4E2590F7104B066F632E7BC3E38
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_251;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CC74BF89CF621AAC29665426A911E4B8BBB60B91
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_252;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::CC95A63361E95F5A0BDF7771CE1AF37BE35817C6
	__StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::CC9EE2CF247B3534102C1324C2679503686EF031
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___CC9EE2CF247B3534102C1324C2679503686EF031_254;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::CDDC3D4212D35722E3E8BAA7B5275932A6567037
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_255;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CED1DB702E32A812D587254A4E1BAAA409E30859
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___CED1DB702E32A812D587254A4E1BAAA409E30859_256;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::CF1C9B07580D3992A644F840EBBB156E0E92B758
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___CF1C9B07580D3992A644F840EBBB156E0E92B758_257;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D2BCB25441A9A8F3DE94D1EF27A870BB77553833
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D41FD917303D010F26005BDAF1F31ABC8A512A13
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___D41FD917303D010F26005BDAF1F31ABC8A512A13_260;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D5B6993D49960DF01393AD8095091B12332C2FA3
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D5B6993D49960DF01393AD8095091B12332C2FA3_261;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D6B85A234C187D0FC82CE310F6423237C934B93A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D6B85A234C187D0FC82CE310F6423237C934B93A_262;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D6D9691C10001AC45C1A8A68D4B08412F42D9F5D
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D7207A48A00E499EF63FC2990337CC8B05C7AE9C
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D73BDC60FA2857E7CE2F742530B056A1866C2177
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___D73BDC60FA2857E7CE2F742530B056A1866C2177_265;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D7AE4E850949E37E7359595699CCDC751E1C45E4
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___D7AE4E850949E37E7359595699CCDC751E1C45E4_266;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70
	__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D923777F48CDA51F21CABCED4FE36A2D620288A2
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___D923777F48CDA51F21CABCED4FE36A2D620288A2_268;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::DA8D7694A42602F7DC0356F30B9E1DFF8196724A
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DC7D58FD4F690A68811E226229F74D0089F018B3
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___DC7D58FD4F690A68811E226229F74D0089F018B3_270;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DD04181543E071971F6379A33D631064A9B051D8
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___DD04181543E071971F6379A33D631064A9B051D8_271;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DEDEF9E8D2052710E15C0CC080004BFB33F47665
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_272;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148 <PrivateImplementationDetails>::E02E3EC94504AFEEB34CE409836A3797D7E43964
	__StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  ___E02E3EC94504AFEEB34CE409836A3797D7E43964_273;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::E1A877959C9E369159A2433060A6781C6C5C1D5C
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___E1A877959C9E369159A2433060A6781C6C5C1D5C_274;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E2654B2181D84518EB5EE7BC342CFFD1F528E908
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_275;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E2C0239EBBF1FD45925FAA7909458304DFD37A84
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_277;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2
	__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E5AD60E2BE68FA0155888A7E87B34F03905E7584
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_282;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::E62D0D793D6BCA39079E6441B162E59F33ED2B07
	__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_283;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E6C34D5D4381C0E6151B26FB9A314D8A57292366
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_284;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::EA86E31C9F6288681516E2AA22EF6B5CD441AB94
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::EB5C89140591CED42C39A8AE698A41F3593C3C62
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___EB5C89140591CED42C39A8AE698A41F3593C3C62_286;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=148 <PrivateImplementationDetails>::EB774035880443AEFBF8B61E7760DA5273A8A44C
	__StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD  ___EB774035880443AEFBF8B61E7760DA5273A8A44C_287;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::EDF6280B8AEAF6DDC736AF7B745668475CE8552F
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::EF1366407DE423137F5977D7AF18B0C058147698
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___EF1366407DE423137F5977D7AF18B0C058147698_290;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::EFAA45999AF190FC718690DC0121EB49DE7FFCE5
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F09F029091837E12B4E60500BED0749D79F77FC2
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___F09F029091837E12B4E60500BED0749D79F77FC2_292;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F0E174E706EA1C2F8D181561F84976C942B1DE4B
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_294;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F100C709518544CED81B98D3F1C862F094DAF46A
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___F100C709518544CED81B98D3F1C862F094DAF46A_295;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F1F22748A7C1A4CC79E97C2063C48D7FF070D941
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::F20B4E4CCB47B4F0631333A4DEE30DB71336110C
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55
	__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::F44B79D8208EDC17F9C96AA7DEA4983114BF698D
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::F5183037A4B449D12439C624D76065B310A6614A
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___F5183037A4B449D12439C624D76065B310A6614A_302;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F65CD55AD5CBB0D49B45E05C5C0369616C658AB1
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F865498CC0010E6ACE166D01EEA92F0F3C87743D
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_305;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::F86A89C59555BE5241482CB0FD35624F05D127CD
	__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  ___F86A89C59555BE5241482CB0FD35624F05D127CD_306;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::F975859F5AFA5A4AE7703BA3B943AFC04919E9B7
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::FA200C2489E9868CC920129D1E2D694172C2BA49
	__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  ___FA200C2489E9868CC920129D1E2D694172C2BA49_308;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B
	__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA
	__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::FB977BD3A3BBAD68C2BBDBC28188F1084531E78E
	__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::FCDF24D1C633613549296EFD3DC9250D840CA996
	__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  ___FCDF24D1C633613549296EFD3DC9250D840CA996_313;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::FCF9CE737F38E152A767F1291260BF9E15F80078
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___FCF9CE737F38E152A767F1291260BF9E15F80078_314;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97
	__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::FE68796AD0908776C9F23F326A4181C1F53CF4DF
	__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_316;

public:
	inline static int32_t get_offset_of_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___00E14C77230AEF0974CFF3930481157AABDA07B4_0)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() const { return ___00E14C77230AEF0974CFF3930481157AABDA07B4_0; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() { return &___00E14C77230AEF0974CFF3930481157AABDA07B4_0; }
	inline void set_U300E14C77230AEF0974CFF3930481157AABDA07B4_0(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___00E14C77230AEF0974CFF3930481157AABDA07B4_0 = value;
	}

	inline static int32_t get_offset_of_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1() const { return ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1() { return &___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1; }
	inline void set_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1 = value;
	}

	inline static int32_t get_offset_of_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_2)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_2() const { return ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_2; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_2() { return &___0222E151247DAE31A8D697EAA14F43F718BD1F1C_2; }
	inline void set_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_2(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___0222E151247DAE31A8D697EAA14F43F718BD1F1C_2 = value;
	}

	inline static int32_t get_offset_of_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3() const { return ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3() { return &___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3; }
	inline void set_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_3 = value;
	}

	inline static int32_t get_offset_of_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4)); }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  get_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4() const { return ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4; }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6 * get_address_of_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4() { return &___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4; }
	inline void set_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4(__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  value)
	{
		___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_4 = value;
	}

	inline static int32_t get_offset_of_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5() const { return ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5() { return &___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5; }
	inline void set_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_5 = value;
	}

	inline static int32_t get_offset_of_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_6)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_6() const { return ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_6; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_6() { return &___07438A85BDEE90305A6125B92084A5F3F2FD5F18_6; }
	inline void set_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_6(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___07438A85BDEE90305A6125B92084A5F3F2FD5F18_6 = value;
	}

	inline static int32_t get_offset_of_U3094D7DFD41161273E784D60EAE6D844127D77F0B_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___094D7DFD41161273E784D60EAE6D844127D77F0B_7)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3094D7DFD41161273E784D60EAE6D844127D77F0B_7() const { return ___094D7DFD41161273E784D60EAE6D844127D77F0B_7; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3094D7DFD41161273E784D60EAE6D844127D77F0B_7() { return &___094D7DFD41161273E784D60EAE6D844127D77F0B_7; }
	inline void set_U3094D7DFD41161273E784D60EAE6D844127D77F0B_7(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___094D7DFD41161273E784D60EAE6D844127D77F0B_7 = value;
	}

	inline static int32_t get_offset_of_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8() const { return ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8() { return &___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8; }
	inline void set_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_8 = value;
	}

	inline static int32_t get_offset_of_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9() const { return ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9() { return &___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9; }
	inline void set_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_9 = value;
	}

	inline static int32_t get_offset_of_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10() const { return ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10() { return &___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10; }
	inline void set_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_10 = value;
	}

	inline static int32_t get_offset_of_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_11)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_11() const { return ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_11; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_11() { return &___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_11; }
	inline void set_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_11(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_11 = value;
	}

	inline static int32_t get_offset_of_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_12)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_12() const { return ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_12; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_12() { return &___0D0AD555C6DA0BD4268F8573DC103F455F62A610_12; }
	inline void set_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_12(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___0D0AD555C6DA0BD4268F8573DC103F455F62A610_12 = value;
	}

	inline static int32_t get_offset_of_U30E4585982EA19664C33EA213EE69EC548F7322B0_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___0E4585982EA19664C33EA213EE69EC548F7322B0_13)); }
	inline __StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17  get_U30E4585982EA19664C33EA213EE69EC548F7322B0_13() const { return ___0E4585982EA19664C33EA213EE69EC548F7322B0_13; }
	inline __StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17 * get_address_of_U30E4585982EA19664C33EA213EE69EC548F7322B0_13() { return &___0E4585982EA19664C33EA213EE69EC548F7322B0_13; }
	inline void set_U30E4585982EA19664C33EA213EE69EC548F7322B0_13(__StaticArrayInitTypeSizeU3D44_t548DCF05F597EFBAE862D39E6DFC7E3A0E336D17  value)
	{
		___0E4585982EA19664C33EA213EE69EC548F7322B0_13 = value;
	}

	inline static int32_t get_offset_of_U310111111FB24B2D331FB3F698193046CBCB8AAFE_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___10111111FB24B2D331FB3F698193046CBCB8AAFE_14)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U310111111FB24B2D331FB3F698193046CBCB8AAFE_14() const { return ___10111111FB24B2D331FB3F698193046CBCB8AAFE_14; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U310111111FB24B2D331FB3F698193046CBCB8AAFE_14() { return &___10111111FB24B2D331FB3F698193046CBCB8AAFE_14; }
	inline void set_U310111111FB24B2D331FB3F698193046CBCB8AAFE_14(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___10111111FB24B2D331FB3F698193046CBCB8AAFE_14 = value;
	}

	inline static int32_t get_offset_of_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_15)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_15() const { return ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_15; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_15() { return &___1037DC6EAB9D7D3850660726ED132EAD961A41E9_15; }
	inline void set_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_15(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___1037DC6EAB9D7D3850660726ED132EAD961A41E9_15 = value;
	}

	inline static int32_t get_offset_of_U310486CAECBA5E73DA05910F94513409DEB6316FD_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___10486CAECBA5E73DA05910F94513409DEB6316FD_16)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U310486CAECBA5E73DA05910F94513409DEB6316FD_16() const { return ___10486CAECBA5E73DA05910F94513409DEB6316FD_16; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U310486CAECBA5E73DA05910F94513409DEB6316FD_16() { return &___10486CAECBA5E73DA05910F94513409DEB6316FD_16; }
	inline void set_U310486CAECBA5E73DA05910F94513409DEB6316FD_16(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___10486CAECBA5E73DA05910F94513409DEB6316FD_16 = value;
	}

	inline static int32_t get_offset_of_U311D4D9FC526D047E01891BEE9949EA42408A800E_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___11D4D9FC526D047E01891BEE9949EA42408A800E_17)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_U311D4D9FC526D047E01891BEE9949EA42408A800E_17() const { return ___11D4D9FC526D047E01891BEE9949EA42408A800E_17; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_U311D4D9FC526D047E01891BEE9949EA42408A800E_17() { return &___11D4D9FC526D047E01891BEE9949EA42408A800E_17; }
	inline void set_U311D4D9FC526D047E01891BEE9949EA42408A800E_17(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___11D4D9FC526D047E01891BEE9949EA42408A800E_17 = value;
	}

	inline static int32_t get_offset_of_U312625AC94FB97DAA377449A877D25786C8ADFFC5_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___12625AC94FB97DAA377449A877D25786C8ADFFC5_18)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U312625AC94FB97DAA377449A877D25786C8ADFFC5_18() const { return ___12625AC94FB97DAA377449A877D25786C8ADFFC5_18; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U312625AC94FB97DAA377449A877D25786C8ADFFC5_18() { return &___12625AC94FB97DAA377449A877D25786C8ADFFC5_18; }
	inline void set_U312625AC94FB97DAA377449A877D25786C8ADFFC5_18(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___12625AC94FB97DAA377449A877D25786C8ADFFC5_18 = value;
	}

	inline static int32_t get_offset_of_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_19)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_19() const { return ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_19; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_19() { return &___12F1DB2AC68CD455638374E8DF9E9D604B15F693_19; }
	inline void set_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_19(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___12F1DB2AC68CD455638374E8DF9E9D604B15F693_19 = value;
	}

	inline static int32_t get_offset_of_U3142E1469231D592823559059166B32655910D0CF_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___142E1469231D592823559059166B32655910D0CF_20)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U3142E1469231D592823559059166B32655910D0CF_20() const { return ___142E1469231D592823559059166B32655910D0CF_20; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U3142E1469231D592823559059166B32655910D0CF_20() { return &___142E1469231D592823559059166B32655910D0CF_20; }
	inline void set_U3142E1469231D592823559059166B32655910D0CF_20(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___142E1469231D592823559059166B32655910D0CF_20 = value;
	}

	inline static int32_t get_offset_of_U3188E7A480B871C554F3F805D3E1184673960EED5_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___188E7A480B871C554F3F805D3E1184673960EED5_21)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3188E7A480B871C554F3F805D3E1184673960EED5_21() const { return ___188E7A480B871C554F3F805D3E1184673960EED5_21; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3188E7A480B871C554F3F805D3E1184673960EED5_21() { return &___188E7A480B871C554F3F805D3E1184673960EED5_21; }
	inline void set_U3188E7A480B871C554F3F805D3E1184673960EED5_21(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___188E7A480B871C554F3F805D3E1184673960EED5_21 = value;
	}

	inline static int32_t get_offset_of_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22() const { return ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22() { return &___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22; }
	inline void set_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_22 = value;
	}

	inline static int32_t get_offset_of_U31A3C01816F6E750EC65FDEF4FEB7758235456890_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1A3C01816F6E750EC65FDEF4FEB7758235456890_23)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U31A3C01816F6E750EC65FDEF4FEB7758235456890_23() const { return ___1A3C01816F6E750EC65FDEF4FEB7758235456890_23; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U31A3C01816F6E750EC65FDEF4FEB7758235456890_23() { return &___1A3C01816F6E750EC65FDEF4FEB7758235456890_23; }
	inline void set_U31A3C01816F6E750EC65FDEF4FEB7758235456890_23(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___1A3C01816F6E750EC65FDEF4FEB7758235456890_23 = value;
	}

	inline static int32_t get_offset_of_U31A85140B8FCA30191EE9347F53782234F0781F79_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1A85140B8FCA30191EE9347F53782234F0781F79_24)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U31A85140B8FCA30191EE9347F53782234F0781F79_24() const { return ___1A85140B8FCA30191EE9347F53782234F0781F79_24; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U31A85140B8FCA30191EE9347F53782234F0781F79_24() { return &___1A85140B8FCA30191EE9347F53782234F0781F79_24; }
	inline void set_U31A85140B8FCA30191EE9347F53782234F0781F79_24(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___1A85140B8FCA30191EE9347F53782234F0781F79_24 = value;
	}

	inline static int32_t get_offset_of_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_25)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_25() const { return ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_25; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_25() { return &___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_25; }
	inline void set_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_25(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_25 = value;
	}

	inline static int32_t get_offset_of_U31D0501D14FE31729CD75A4073F4404AF7E53B024_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1D0501D14FE31729CD75A4073F4404AF7E53B024_26)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U31D0501D14FE31729CD75A4073F4404AF7E53B024_26() const { return ___1D0501D14FE31729CD75A4073F4404AF7E53B024_26; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U31D0501D14FE31729CD75A4073F4404AF7E53B024_26() { return &___1D0501D14FE31729CD75A4073F4404AF7E53B024_26; }
	inline void set_U31D0501D14FE31729CD75A4073F4404AF7E53B024_26(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___1D0501D14FE31729CD75A4073F4404AF7E53B024_26 = value;
	}

	inline static int32_t get_offset_of_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27() const { return ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27() { return &___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27; }
	inline void set_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_27 = value;
	}

	inline static int32_t get_offset_of_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28() const { return ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28() { return &___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28; }
	inline void set_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_28 = value;
	}

	inline static int32_t get_offset_of_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29() const { return ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29() { return &___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29; }
	inline void set_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_29 = value;
	}

	inline static int32_t get_offset_of_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_30)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_30() const { return ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_30; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_30() { return &___1E45F828069EAF87A0B553BE1E65B98F20E377ED_30; }
	inline void set_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_30(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___1E45F828069EAF87A0B553BE1E65B98F20E377ED_30 = value;
	}

	inline static int32_t get_offset_of_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_31)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_31() const { return ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_31; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_31() { return &___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_31; }
	inline void set_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_31(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_31 = value;
	}

	inline static int32_t get_offset_of_U31F5771BF2009B14CC123999096615351A0BE9527_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1F5771BF2009B14CC123999096615351A0BE9527_32)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U31F5771BF2009B14CC123999096615351A0BE9527_32() const { return ___1F5771BF2009B14CC123999096615351A0BE9527_32; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U31F5771BF2009B14CC123999096615351A0BE9527_32() { return &___1F5771BF2009B14CC123999096615351A0BE9527_32; }
	inline void set_U31F5771BF2009B14CC123999096615351A0BE9527_32(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___1F5771BF2009B14CC123999096615351A0BE9527_32 = value;
	}

	inline static int32_t get_offset_of_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_33)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_33() const { return ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_33; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_33() { return &___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_33; }
	inline void set_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_33(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_33 = value;
	}

	inline static int32_t get_offset_of_U320D740A7E661D50B06DC32EF6577367D264F6235_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___20D740A7E661D50B06DC32EF6577367D264F6235_34)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U320D740A7E661D50B06DC32EF6577367D264F6235_34() const { return ___20D740A7E661D50B06DC32EF6577367D264F6235_34; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U320D740A7E661D50B06DC32EF6577367D264F6235_34() { return &___20D740A7E661D50B06DC32EF6577367D264F6235_34; }
	inline void set_U320D740A7E661D50B06DC32EF6577367D264F6235_34(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___20D740A7E661D50B06DC32EF6577367D264F6235_34 = value;
	}

	inline static int32_t get_offset_of_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_35)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_35() const { return ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_35; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_35() { return &___227C649F55292B0A293EBB2D56AB8ABAECC7F629_35; }
	inline void set_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_35(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___227C649F55292B0A293EBB2D56AB8ABAECC7F629_35 = value;
	}

	inline static int32_t get_offset_of_U322BF5B86DC5E0B87331903141C5973C117DA716E_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___22BF5B86DC5E0B87331903141C5973C117DA716E_36)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U322BF5B86DC5E0B87331903141C5973C117DA716E_36() const { return ___22BF5B86DC5E0B87331903141C5973C117DA716E_36; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U322BF5B86DC5E0B87331903141C5973C117DA716E_36() { return &___22BF5B86DC5E0B87331903141C5973C117DA716E_36; }
	inline void set_U322BF5B86DC5E0B87331903141C5973C117DA716E_36(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___22BF5B86DC5E0B87331903141C5973C117DA716E_36 = value;
	}

	inline static int32_t get_offset_of_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37() const { return ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37() { return &___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37; }
	inline void set_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37 = value;
	}

	inline static int32_t get_offset_of_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_38)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_38() const { return ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_38; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_38() { return &___2357B210FC2522A4A9D0F465AEAC250A79FA9449_38; }
	inline void set_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_38(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___2357B210FC2522A4A9D0F465AEAC250A79FA9449_38 = value;
	}

	inline static int32_t get_offset_of_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39() const { return ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39() { return &___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39; }
	inline void set_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_39 = value;
	}

	inline static int32_t get_offset_of_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40() const { return ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40() { return &___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40; }
	inline void set_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_40 = value;
	}

	inline static int32_t get_offset_of_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_41)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_41() const { return ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_41; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_41() { return &___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_41; }
	inline void set_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_41(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_41 = value;
	}

	inline static int32_t get_offset_of_U3259B7EF54A535F34001B95480B4BB11245EA41B5_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___259B7EF54A535F34001B95480B4BB11245EA41B5_42)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U3259B7EF54A535F34001B95480B4BB11245EA41B5_42() const { return ___259B7EF54A535F34001B95480B4BB11245EA41B5_42; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U3259B7EF54A535F34001B95480B4BB11245EA41B5_42() { return &___259B7EF54A535F34001B95480B4BB11245EA41B5_42; }
	inline void set_U3259B7EF54A535F34001B95480B4BB11245EA41B5_42(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___259B7EF54A535F34001B95480B4BB11245EA41B5_42 = value;
	}

	inline static int32_t get_offset_of_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43() const { return ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43() { return &___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43; }
	inline void set_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_43 = value;
	}

	inline static int32_t get_offset_of_U328F991026781E1BC119F02498D75FB3A84F4ED37_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___28F991026781E1BC119F02498D75FB3A84F4ED37_44)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U328F991026781E1BC119F02498D75FB3A84F4ED37_44() const { return ___28F991026781E1BC119F02498D75FB3A84F4ED37_44; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U328F991026781E1BC119F02498D75FB3A84F4ED37_44() { return &___28F991026781E1BC119F02498D75FB3A84F4ED37_44; }
	inline void set_U328F991026781E1BC119F02498D75FB3A84F4ED37_44(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___28F991026781E1BC119F02498D75FB3A84F4ED37_44 = value;
	}

	inline static int32_t get_offset_of_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45() const { return ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45() { return &___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45; }
	inline void set_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_45 = value;
	}

	inline static int32_t get_offset_of_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_46)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_46() const { return ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_46; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_46() { return &___29AB17251F5A730B539AB351FFCB45B9F2FA8027_46; }
	inline void set_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_46(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___29AB17251F5A730B539AB351FFCB45B9F2FA8027_46 = value;
	}

	inline static int32_t get_offset_of_U32A84324D54FDC0473BE021004C50F363ADCD89AF_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2A84324D54FDC0473BE021004C50F363ADCD89AF_47)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U32A84324D54FDC0473BE021004C50F363ADCD89AF_47() const { return ___2A84324D54FDC0473BE021004C50F363ADCD89AF_47; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U32A84324D54FDC0473BE021004C50F363ADCD89AF_47() { return &___2A84324D54FDC0473BE021004C50F363ADCD89AF_47; }
	inline void set_U32A84324D54FDC0473BE021004C50F363ADCD89AF_47(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___2A84324D54FDC0473BE021004C50F363ADCD89AF_47 = value;
	}

	inline static int32_t get_offset_of_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48() const { return ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48() { return &___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48; }
	inline void set_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48 = value;
	}

	inline static int32_t get_offset_of_U32D3DF989BE3BBA2B353F845C37992DFF85579505_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2D3DF989BE3BBA2B353F845C37992DFF85579505_49)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U32D3DF989BE3BBA2B353F845C37992DFF85579505_49() const { return ___2D3DF989BE3BBA2B353F845C37992DFF85579505_49; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U32D3DF989BE3BBA2B353F845C37992DFF85579505_49() { return &___2D3DF989BE3BBA2B353F845C37992DFF85579505_49; }
	inline void set_U32D3DF989BE3BBA2B353F845C37992DFF85579505_49(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___2D3DF989BE3BBA2B353F845C37992DFF85579505_49 = value;
	}

	inline static int32_t get_offset_of_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_50)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_50() const { return ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_50; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_50() { return &___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_50; }
	inline void set_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_50(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_50 = value;
	}

	inline static int32_t get_offset_of_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_51)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_51() const { return ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_51; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_51() { return &___2EC69895F74DBAE74600E960EFFB6448597BDEEB_51; }
	inline void set_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_51(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___2EC69895F74DBAE74600E960EFFB6448597BDEEB_51 = value;
	}

	inline static int32_t get_offset_of_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52() const { return ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52() { return &___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52; }
	inline void set_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_52 = value;
	}

	inline static int32_t get_offset_of_U33102377CA043644BFD52C56AE2A9F554723263FD_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3102377CA043644BFD52C56AE2A9F554723263FD_53)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U33102377CA043644BFD52C56AE2A9F554723263FD_53() const { return ___3102377CA043644BFD52C56AE2A9F554723263FD_53; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U33102377CA043644BFD52C56AE2A9F554723263FD_53() { return &___3102377CA043644BFD52C56AE2A9F554723263FD_53; }
	inline void set_U33102377CA043644BFD52C56AE2A9F554723263FD_53(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___3102377CA043644BFD52C56AE2A9F554723263FD_53 = value;
	}

	inline static int32_t get_offset_of_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_54)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_54() const { return ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_54; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_54() { return &___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_54; }
	inline void set_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_54(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_54 = value;
	}

	inline static int32_t get_offset_of_U3322BC3569CD0270EC3179B30382F24148B533D10_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___322BC3569CD0270EC3179B30382F24148B533D10_55)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3322BC3569CD0270EC3179B30382F24148B533D10_55() const { return ___322BC3569CD0270EC3179B30382F24148B533D10_55; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3322BC3569CD0270EC3179B30382F24148B533D10_55() { return &___322BC3569CD0270EC3179B30382F24148B533D10_55; }
	inline void set_U3322BC3569CD0270EC3179B30382F24148B533D10_55(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___322BC3569CD0270EC3179B30382F24148B533D10_55 = value;
	}

	inline static int32_t get_offset_of_U333B8540230E7366A64AC520E4F819103EB00ECD0_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___33B8540230E7366A64AC520E4F819103EB00ECD0_56)); }
	inline __StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716  get_U333B8540230E7366A64AC520E4F819103EB00ECD0_56() const { return ___33B8540230E7366A64AC520E4F819103EB00ECD0_56; }
	inline __StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716 * get_address_of_U333B8540230E7366A64AC520E4F819103EB00ECD0_56() { return &___33B8540230E7366A64AC520E4F819103EB00ECD0_56; }
	inline void set_U333B8540230E7366A64AC520E4F819103EB00ECD0_56(__StaticArrayInitTypeSizeU3D156_t47D3BE1DFA1C1924E9CF77FB95DE6F1F96999716  value)
	{
		___33B8540230E7366A64AC520E4F819103EB00ECD0_56 = value;
	}

	inline static int32_t get_offset_of_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57() const { return ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57() { return &___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57; }
	inline void set_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_57 = value;
	}

	inline static int32_t get_offset_of_U335859E8329070E55C5D9282930023DD6B1594141_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___35859E8329070E55C5D9282930023DD6B1594141_58)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U335859E8329070E55C5D9282930023DD6B1594141_58() const { return ___35859E8329070E55C5D9282930023DD6B1594141_58; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U335859E8329070E55C5D9282930023DD6B1594141_58() { return &___35859E8329070E55C5D9282930023DD6B1594141_58; }
	inline void set_U335859E8329070E55C5D9282930023DD6B1594141_58(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___35859E8329070E55C5D9282930023DD6B1594141_58 = value;
	}

	inline static int32_t get_offset_of_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59() const { return ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59() { return &___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59; }
	inline void set_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_59 = value;
	}

	inline static int32_t get_offset_of_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60() const { return ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60() { return &___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60; }
	inline void set_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_60 = value;
	}

	inline static int32_t get_offset_of_U338017E50AF503681BFC2BD0CDED5F7565471B574_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___38017E50AF503681BFC2BD0CDED5F7565471B574_61)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U338017E50AF503681BFC2BD0CDED5F7565471B574_61() const { return ___38017E50AF503681BFC2BD0CDED5F7565471B574_61; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U338017E50AF503681BFC2BD0CDED5F7565471B574_61() { return &___38017E50AF503681BFC2BD0CDED5F7565471B574_61; }
	inline void set_U338017E50AF503681BFC2BD0CDED5F7565471B574_61(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___38017E50AF503681BFC2BD0CDED5F7565471B574_61 = value;
	}

	inline static int32_t get_offset_of_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_62)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_62() const { return ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_62; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_62() { return &___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_62; }
	inline void set_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_62(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_62 = value;
	}

	inline static int32_t get_offset_of_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63)); }
	inline __StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69  get_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63() const { return ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63; }
	inline __StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69 * get_address_of_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63() { return &___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63; }
	inline void set_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63(__StaticArrayInitTypeSizeU3D54_t1C64A594E9DCC6567FB9D15959F03765935E7B69  value)
	{
		___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_63 = value;
	}

	inline static int32_t get_offset_of_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_64)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_64() const { return ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_64; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_64() { return &___3C22A633B79B510613E37BC5A02FF91A4AB006ED_64; }
	inline void set_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_64(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___3C22A633B79B510613E37BC5A02FF91A4AB006ED_64 = value;
	}

	inline static int32_t get_offset_of_U33D3C46CC589F65C2443688952CBCD9C499839622_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3D3C46CC589F65C2443688952CBCD9C499839622_65)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U33D3C46CC589F65C2443688952CBCD9C499839622_65() const { return ___3D3C46CC589F65C2443688952CBCD9C499839622_65; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U33D3C46CC589F65C2443688952CBCD9C499839622_65() { return &___3D3C46CC589F65C2443688952CBCD9C499839622_65; }
	inline void set_U33D3C46CC589F65C2443688952CBCD9C499839622_65(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___3D3C46CC589F65C2443688952CBCD9C499839622_65 = value;
	}

	inline static int32_t get_offset_of_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_66)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_66() const { return ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_66; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_66() { return &___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_66; }
	inline void set_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_66(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_66 = value;
	}

	inline static int32_t get_offset_of_U33DF543E51AB43F1F561B11B8789C82E26C427F72_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3DF543E51AB43F1F561B11B8789C82E26C427F72_67)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U33DF543E51AB43F1F561B11B8789C82E26C427F72_67() const { return ___3DF543E51AB43F1F561B11B8789C82E26C427F72_67; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U33DF543E51AB43F1F561B11B8789C82E26C427F72_67() { return &___3DF543E51AB43F1F561B11B8789C82E26C427F72_67; }
	inline void set_U33DF543E51AB43F1F561B11B8789C82E26C427F72_67(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___3DF543E51AB43F1F561B11B8789C82E26C427F72_67 = value;
	}

	inline static int32_t get_offset_of_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_68)); }
	inline __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  get_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_68() const { return ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_68; }
	inline __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137 * get_address_of_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_68() { return &___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_68; }
	inline void set_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_68(__StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  value)
	{
		___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_68 = value;
	}

	inline static int32_t get_offset_of_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69)); }
	inline __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  get_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69() const { return ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69; }
	inline __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2 * get_address_of_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69() { return &___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69; }
	inline void set_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69(__StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  value)
	{
		___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_69 = value;
	}

	inline static int32_t get_offset_of_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70() const { return ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70() { return &___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70; }
	inline void set_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_70 = value;
	}

	inline static int32_t get_offset_of_U33F152B3DFAF72A55D78439E5015C55A20967621A_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3F152B3DFAF72A55D78439E5015C55A20967621A_71)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U33F152B3DFAF72A55D78439E5015C55A20967621A_71() const { return ___3F152B3DFAF72A55D78439E5015C55A20967621A_71; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U33F152B3DFAF72A55D78439E5015C55A20967621A_71() { return &___3F152B3DFAF72A55D78439E5015C55A20967621A_71; }
	inline void set_U33F152B3DFAF72A55D78439E5015C55A20967621A_71(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___3F152B3DFAF72A55D78439E5015C55A20967621A_71 = value;
	}

	inline static int32_t get_offset_of_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72() const { return ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72() { return &___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72; }
	inline void set_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_72 = value;
	}

	inline static int32_t get_offset_of_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_73)); }
	inline __StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67  get_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_73() const { return ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_73; }
	inline __StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67 * get_address_of_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_73() { return &___40B82CCF037B966E2A6B5F4BAC86904796965A1C_73; }
	inline void set_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_73(__StaticArrayInitTypeSizeU3D136_t5DD0D64EE6BE13529480BAC98086AD96C27C0B67  value)
	{
		___40B82CCF037B966E2A6B5F4BAC86904796965A1C_73 = value;
	}

	inline static int32_t get_offset_of_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_74)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_74() const { return ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_74; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_74() { return &___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_74; }
	inline void set_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_74(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_74 = value;
	}

	inline static int32_t get_offset_of_U342A99199D41E8832E72CD942706DDA326C0BB356_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___42A99199D41E8832E72CD942706DDA326C0BB356_75)); }
	inline __StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9  get_U342A99199D41E8832E72CD942706DDA326C0BB356_75() const { return ___42A99199D41E8832E72CD942706DDA326C0BB356_75; }
	inline __StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9 * get_address_of_U342A99199D41E8832E72CD942706DDA326C0BB356_75() { return &___42A99199D41E8832E72CD942706DDA326C0BB356_75; }
	inline void set_U342A99199D41E8832E72CD942706DDA326C0BB356_75(__StaticArrayInitTypeSizeU3D176_tD84E2E32D224D4E8B1BB9D5B1F5EBB8BE30EEBC9  value)
	{
		___42A99199D41E8832E72CD942706DDA326C0BB356_75 = value;
	}

	inline static int32_t get_offset_of_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_76)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_76() const { return ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_76; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_76() { return &___445E0C138E7D9647FCD08184FB23A833C4D00D0D_76; }
	inline void set_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_76(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___445E0C138E7D9647FCD08184FB23A833C4D00D0D_76 = value;
	}

	inline static int32_t get_offset_of_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_77)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_77() const { return ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_77; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_77() { return &___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_77; }
	inline void set_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_77(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_77 = value;
	}

	inline static int32_t get_offset_of_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78() const { return ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78() { return &___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78; }
	inline void set_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_78 = value;
	}

	inline static int32_t get_offset_of_U3463C3B3709985BB32747ACC9AE3406BB6836368B_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___463C3B3709985BB32747ACC9AE3406BB6836368B_79)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3463C3B3709985BB32747ACC9AE3406BB6836368B_79() const { return ___463C3B3709985BB32747ACC9AE3406BB6836368B_79; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3463C3B3709985BB32747ACC9AE3406BB6836368B_79() { return &___463C3B3709985BB32747ACC9AE3406BB6836368B_79; }
	inline void set_U3463C3B3709985BB32747ACC9AE3406BB6836368B_79(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___463C3B3709985BB32747ACC9AE3406BB6836368B_79 = value;
	}

	inline static int32_t get_offset_of_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80() const { return ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80() { return &___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80; }
	inline void set_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_80 = value;
	}

	inline static int32_t get_offset_of_U3471648086F9480CB553B7802323E0A75FFE09C5A_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___471648086F9480CB553B7802323E0A75FFE09C5A_81)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U3471648086F9480CB553B7802323E0A75FFE09C5A_81() const { return ___471648086F9480CB553B7802323E0A75FFE09C5A_81; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U3471648086F9480CB553B7802323E0A75FFE09C5A_81() { return &___471648086F9480CB553B7802323E0A75FFE09C5A_81; }
	inline void set_U3471648086F9480CB553B7802323E0A75FFE09C5A_81(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___471648086F9480CB553B7802323E0A75FFE09C5A_81 = value;
	}

	inline static int32_t get_offset_of_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_82)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_82() const { return ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_82; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_82() { return &___484B207F3CBCE8338BDE26F04E87EE256891BE0F_82; }
	inline void set_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_82(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___484B207F3CBCE8338BDE26F04E87EE256891BE0F_82 = value;
	}

	inline static int32_t get_offset_of_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83() const { return ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83() { return &___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83; }
	inline void set_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_83 = value;
	}

	inline static int32_t get_offset_of_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84() const { return ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84() { return &___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84; }
	inline void set_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84 = value;
	}

	inline static int32_t get_offset_of_U349594851C4B6C279C35B162F0236A99E7C31DD36_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___49594851C4B6C279C35B162F0236A99E7C31DD36_85)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U349594851C4B6C279C35B162F0236A99E7C31DD36_85() const { return ___49594851C4B6C279C35B162F0236A99E7C31DD36_85; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U349594851C4B6C279C35B162F0236A99E7C31DD36_85() { return &___49594851C4B6C279C35B162F0236A99E7C31DD36_85; }
	inline void set_U349594851C4B6C279C35B162F0236A99E7C31DD36_85(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___49594851C4B6C279C35B162F0236A99E7C31DD36_85 = value;
	}

	inline static int32_t get_offset_of_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86() const { return ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86() { return &___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86; }
	inline void set_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_86 = value;
	}

	inline static int32_t get_offset_of_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87() const { return ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87() { return &___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87; }
	inline void set_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_87 = value;
	}

	inline static int32_t get_offset_of_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88() const { return ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88() { return &___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88; }
	inline void set_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_88 = value;
	}

	inline static int32_t get_offset_of_U34D317202204E9741354BC1A1628BC10A05E2AB7B_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___4D317202204E9741354BC1A1628BC10A05E2AB7B_89)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U34D317202204E9741354BC1A1628BC10A05E2AB7B_89() const { return ___4D317202204E9741354BC1A1628BC10A05E2AB7B_89; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U34D317202204E9741354BC1A1628BC10A05E2AB7B_89() { return &___4D317202204E9741354BC1A1628BC10A05E2AB7B_89; }
	inline void set_U34D317202204E9741354BC1A1628BC10A05E2AB7B_89(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___4D317202204E9741354BC1A1628BC10A05E2AB7B_89 = value;
	}

	inline static int32_t get_offset_of_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_90)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_90() const { return ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_90; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_90() { return &___4DE742FEB32578958FCFC33D6732EFF616C8A94B_90; }
	inline void set_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_90(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___4DE742FEB32578958FCFC33D6732EFF616C8A94B_90 = value;
	}

	inline static int32_t get_offset_of_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_91)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_91() const { return ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_91; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_91() { return &___50DACD1562B0163C9FC5B673478D4231A4B37B1F_91; }
	inline void set_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_91(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___50DACD1562B0163C9FC5B673478D4231A4B37B1F_91 = value;
	}

	inline static int32_t get_offset_of_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92() const { return ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92() { return &___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92; }
	inline void set_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_92 = value;
	}

	inline static int32_t get_offset_of_U353173106B161530CA26C69983B44BBE2FAB890D0_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___53173106B161530CA26C69983B44BBE2FAB890D0_93)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U353173106B161530CA26C69983B44BBE2FAB890D0_93() const { return ___53173106B161530CA26C69983B44BBE2FAB890D0_93; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U353173106B161530CA26C69983B44BBE2FAB890D0_93() { return &___53173106B161530CA26C69983B44BBE2FAB890D0_93; }
	inline void set_U353173106B161530CA26C69983B44BBE2FAB890D0_93(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___53173106B161530CA26C69983B44BBE2FAB890D0_93 = value;
	}

	inline static int32_t get_offset_of_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___539B26D17C2DEB88FB9E07611D164408E23C35AB_94)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_94() const { return ___539B26D17C2DEB88FB9E07611D164408E23C35AB_94; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_94() { return &___539B26D17C2DEB88FB9E07611D164408E23C35AB_94; }
	inline void set_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_94(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___539B26D17C2DEB88FB9E07611D164408E23C35AB_94 = value;
	}

	inline static int32_t get_offset_of_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95() const { return ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95() { return &___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95; }
	inline void set_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_95 = value;
	}

	inline static int32_t get_offset_of_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_96)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_96() const { return ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_96; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_96() { return &___55A6E22CC24364881F62CCF67CBE9A348DC602FA_96; }
	inline void set_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_96(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___55A6E22CC24364881F62CCF67CBE9A348DC602FA_96 = value;
	}

	inline static int32_t get_offset_of_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97() const { return ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97() { return &___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97; }
	inline void set_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_97 = value;
	}

	inline static int32_t get_offset_of_U356F75F925774608B6BF88F69D6124CB44DDF42E1_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___56F75F925774608B6BF88F69D6124CB44DDF42E1_98)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U356F75F925774608B6BF88F69D6124CB44DDF42E1_98() const { return ___56F75F925774608B6BF88F69D6124CB44DDF42E1_98; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U356F75F925774608B6BF88F69D6124CB44DDF42E1_98() { return &___56F75F925774608B6BF88F69D6124CB44DDF42E1_98; }
	inline void set_U356F75F925774608B6BF88F69D6124CB44DDF42E1_98(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___56F75F925774608B6BF88F69D6124CB44DDF42E1_98 = value;
	}

	inline static int32_t get_offset_of_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_99)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_99() const { return ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_99; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_99() { return &___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_99; }
	inline void set_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_99(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_99 = value;
	}

	inline static int32_t get_offset_of_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___583A438479F42C7D8DE8C6699B0C37A4A026A643_100)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_100() const { return ___583A438479F42C7D8DE8C6699B0C37A4A026A643_100; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_100() { return &___583A438479F42C7D8DE8C6699B0C37A4A026A643_100; }
	inline void set_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_100(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___583A438479F42C7D8DE8C6699B0C37A4A026A643_100 = value;
	}

	inline static int32_t get_offset_of_U358B2CCE2545886B61E58C3C60831C2B049814E09_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___58B2CCE2545886B61E58C3C60831C2B049814E09_101)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U358B2CCE2545886B61E58C3C60831C2B049814E09_101() const { return ___58B2CCE2545886B61E58C3C60831C2B049814E09_101; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U358B2CCE2545886B61E58C3C60831C2B049814E09_101() { return &___58B2CCE2545886B61E58C3C60831C2B049814E09_101; }
	inline void set_U358B2CCE2545886B61E58C3C60831C2B049814E09_101(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___58B2CCE2545886B61E58C3C60831C2B049814E09_101 = value;
	}

	inline static int32_t get_offset_of_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_102)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_102() const { return ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_102; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_102() { return &___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_102; }
	inline void set_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_102(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_102 = value;
	}

	inline static int32_t get_offset_of_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_103)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_103() const { return ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_103; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_103() { return &___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_103; }
	inline void set_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_103(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_103 = value;
	}

	inline static int32_t get_offset_of_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_104)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_104() const { return ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_104; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_104() { return &___5B02FED4CA0B61476F1AAA5C6445F024374B075A_104; }
	inline void set_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_104(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___5B02FED4CA0B61476F1AAA5C6445F024374B075A_104 = value;
	}

	inline static int32_t get_offset_of_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105() const { return ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105() { return &___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105; }
	inline void set_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_105 = value;
	}

	inline static int32_t get_offset_of_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106() const { return ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106() { return &___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106; }
	inline void set_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_106 = value;
	}

	inline static int32_t get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107() const { return ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107() { return &___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107; }
	inline void set_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_107 = value;
	}

	inline static int32_t get_offset_of_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108() const { return ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108() { return &___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108; }
	inline void set_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_108 = value;
	}

	inline static int32_t get_offset_of_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_109)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_109() const { return ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_109; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_109() { return &___5C563BFAB6400B2824F5E41D78FB3097E8244C26_109; }
	inline void set_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_109(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___5C563BFAB6400B2824F5E41D78FB3097E8244C26_109 = value;
	}

	inline static int32_t get_offset_of_U35CB924E7461B9011EC8197A30815EA263595B3A4_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5CB924E7461B9011EC8197A30815EA263595B3A4_110)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U35CB924E7461B9011EC8197A30815EA263595B3A4_110() const { return ___5CB924E7461B9011EC8197A30815EA263595B3A4_110; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U35CB924E7461B9011EC8197A30815EA263595B3A4_110() { return &___5CB924E7461B9011EC8197A30815EA263595B3A4_110; }
	inline void set_U35CB924E7461B9011EC8197A30815EA263595B3A4_110(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___5CB924E7461B9011EC8197A30815EA263595B3A4_110 = value;
	}

	inline static int32_t get_offset_of_U35D160627930BB84F7867AEEC7143FB2399C7CA79_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5D160627930BB84F7867AEEC7143FB2399C7CA79_111)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U35D160627930BB84F7867AEEC7143FB2399C7CA79_111() const { return ___5D160627930BB84F7867AEEC7143FB2399C7CA79_111; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U35D160627930BB84F7867AEEC7143FB2399C7CA79_111() { return &___5D160627930BB84F7867AEEC7143FB2399C7CA79_111; }
	inline void set_U35D160627930BB84F7867AEEC7143FB2399C7CA79_111(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___5D160627930BB84F7867AEEC7143FB2399C7CA79_111 = value;
	}

	inline static int32_t get_offset_of_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_112)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_112() const { return ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_112; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_112() { return &___5DD69D93CBAA5623AE286E97265890A7693A0AC2_112; }
	inline void set_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_112(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___5DD69D93CBAA5623AE286E97265890A7693A0AC2_112 = value;
	}

	inline static int32_t get_offset_of_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_113)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_113() const { return ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_113; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_113() { return &___5E3F56450E340B21A3A13E39A66941353F8C7C1A_113; }
	inline void set_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_113(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___5E3F56450E340B21A3A13E39A66941353F8C7C1A_113 = value;
	}

	inline static int32_t get_offset_of_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_114)); }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  get_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_114() const { return ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_114; }
	inline __StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832 * get_address_of_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_114() { return &___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_114; }
	inline void set_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_114(__StaticArrayInitTypeSizeU3D6_tBB4E159E85095CF92D0243B762E02FC1B5A25832  value)
	{
		___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_114 = value;
	}

	inline static int32_t get_offset_of_U360E3C19D774F8C210299534335ED5D3154717218_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___60E3C19D774F8C210299534335ED5D3154717218_115)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U360E3C19D774F8C210299534335ED5D3154717218_115() const { return ___60E3C19D774F8C210299534335ED5D3154717218_115; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U360E3C19D774F8C210299534335ED5D3154717218_115() { return &___60E3C19D774F8C210299534335ED5D3154717218_115; }
	inline void set_U360E3C19D774F8C210299534335ED5D3154717218_115(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___60E3C19D774F8C210299534335ED5D3154717218_115 = value;
	}

	inline static int32_t get_offset_of_U361A1021606CAD0A5990E00FD305478E75F1983D6_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___61A1021606CAD0A5990E00FD305478E75F1983D6_116)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U361A1021606CAD0A5990E00FD305478E75F1983D6_116() const { return ___61A1021606CAD0A5990E00FD305478E75F1983D6_116; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U361A1021606CAD0A5990E00FD305478E75F1983D6_116() { return &___61A1021606CAD0A5990E00FD305478E75F1983D6_116; }
	inline void set_U361A1021606CAD0A5990E00FD305478E75F1983D6_116(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___61A1021606CAD0A5990E00FD305478E75F1983D6_116 = value;
	}

	inline static int32_t get_offset_of_U3648C489642395164C2A2AA2379A6E7F44CAB018B_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___648C489642395164C2A2AA2379A6E7F44CAB018B_117)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3648C489642395164C2A2AA2379A6E7F44CAB018B_117() const { return ___648C489642395164C2A2AA2379A6E7F44CAB018B_117; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3648C489642395164C2A2AA2379A6E7F44CAB018B_117() { return &___648C489642395164C2A2AA2379A6E7F44CAB018B_117; }
	inline void set_U3648C489642395164C2A2AA2379A6E7F44CAB018B_117(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___648C489642395164C2A2AA2379A6E7F44CAB018B_117 = value;
	}

	inline static int32_t get_offset_of_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___661F9E9FEA16C3A9CDF452AB61743578471F7447_118)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_118() const { return ___661F9E9FEA16C3A9CDF452AB61743578471F7447_118; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_118() { return &___661F9E9FEA16C3A9CDF452AB61743578471F7447_118; }
	inline void set_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_118(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___661F9E9FEA16C3A9CDF452AB61743578471F7447_118 = value;
	}

	inline static int32_t get_offset_of_U367013075958AAD75C4545796BF80B16683B33A59_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___67013075958AAD75C4545796BF80B16683B33A59_119)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U367013075958AAD75C4545796BF80B16683B33A59_119() const { return ___67013075958AAD75C4545796BF80B16683B33A59_119; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U367013075958AAD75C4545796BF80B16683B33A59_119() { return &___67013075958AAD75C4545796BF80B16683B33A59_119; }
	inline void set_U367013075958AAD75C4545796BF80B16683B33A59_119(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___67013075958AAD75C4545796BF80B16683B33A59_119 = value;
	}

	inline static int32_t get_offset_of_U36727D77DB74B08CC9003029AC92B7AC68E258811_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6727D77DB74B08CC9003029AC92B7AC68E258811_120)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36727D77DB74B08CC9003029AC92B7AC68E258811_120() const { return ___6727D77DB74B08CC9003029AC92B7AC68E258811_120; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36727D77DB74B08CC9003029AC92B7AC68E258811_120() { return &___6727D77DB74B08CC9003029AC92B7AC68E258811_120; }
	inline void set_U36727D77DB74B08CC9003029AC92B7AC68E258811_120(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6727D77DB74B08CC9003029AC92B7AC68E258811_120 = value;
	}

	inline static int32_t get_offset_of_U36882914B984B24C4880E3BEA646DC549B3C34ABC_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6882914B984B24C4880E3BEA646DC549B3C34ABC_121)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U36882914B984B24C4880E3BEA646DC549B3C34ABC_121() const { return ___6882914B984B24C4880E3BEA646DC549B3C34ABC_121; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U36882914B984B24C4880E3BEA646DC549B3C34ABC_121() { return &___6882914B984B24C4880E3BEA646DC549B3C34ABC_121; }
	inline void set_U36882914B984B24C4880E3BEA646DC549B3C34ABC_121(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___6882914B984B24C4880E3BEA646DC549B3C34ABC_121 = value;
	}

	inline static int32_t get_offset_of_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_122)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_122() const { return ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_122; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_122() { return &___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_122; }
	inline void set_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_122(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_122 = value;
	}

	inline static int32_t get_offset_of_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_123)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_123() const { return ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_123; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_123() { return &___69499AF6438C222AAADFC92FF3D3DF3902A454FC_123; }
	inline void set_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_123(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___69499AF6438C222AAADFC92FF3D3DF3902A454FC_123 = value;
	}

	inline static int32_t get_offset_of_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_124() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_124)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_124() const { return ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_124; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_124() { return &___6957A133E0DC21707BCAAF62194EAF4FF75E0768_124; }
	inline void set_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_124(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6957A133E0DC21707BCAAF62194EAF4FF75E0768_124 = value;
	}

	inline static int32_t get_offset_of_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_125() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_125)); }
	inline __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  get_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_125() const { return ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_125; }
	inline __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625 * get_address_of_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_125() { return &___6A09CB785087F5A9F6EC748C7C10F98FA2683534_125; }
	inline void set_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_125(__StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  value)
	{
		___6A09CB785087F5A9F6EC748C7C10F98FA2683534_125 = value;
	}

	inline static int32_t get_offset_of_U36AA66657BB3292572C8F56AB3D015A7990C43606_126() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6AA66657BB3292572C8F56AB3D015A7990C43606_126)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36AA66657BB3292572C8F56AB3D015A7990C43606_126() const { return ___6AA66657BB3292572C8F56AB3D015A7990C43606_126; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36AA66657BB3292572C8F56AB3D015A7990C43606_126() { return &___6AA66657BB3292572C8F56AB3D015A7990C43606_126; }
	inline void set_U36AA66657BB3292572C8F56AB3D015A7990C43606_126(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6AA66657BB3292572C8F56AB3D015A7990C43606_126 = value;
	}

	inline static int32_t get_offset_of_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_127() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_127)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_127() const { return ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_127; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_127() { return &___6AF652181FBB423ED995C5CF63D5B83726DE74CF_127; }
	inline void set_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_127(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___6AF652181FBB423ED995C5CF63D5B83726DE74CF_127 = value;
	}

	inline static int32_t get_offset_of_U36B38D84409B2091CBC93686439AC2C04C76DB96E_128() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6B38D84409B2091CBC93686439AC2C04C76DB96E_128)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36B38D84409B2091CBC93686439AC2C04C76DB96E_128() const { return ___6B38D84409B2091CBC93686439AC2C04C76DB96E_128; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36B38D84409B2091CBC93686439AC2C04C76DB96E_128() { return &___6B38D84409B2091CBC93686439AC2C04C76DB96E_128; }
	inline void set_U36B38D84409B2091CBC93686439AC2C04C76DB96E_128(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6B38D84409B2091CBC93686439AC2C04C76DB96E_128 = value;
	}

	inline static int32_t get_offset_of_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129() const { return ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129() { return &___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129; }
	inline void set_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_129 = value;
	}

	inline static int32_t get_offset_of_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_130() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_130)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_130() const { return ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_130; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_130() { return &___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_130; }
	inline void set_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_130(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_130 = value;
	}

	inline static int32_t get_offset_of_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_131() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_131)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_131() const { return ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_131; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_131() { return &___6DE89499E8F85867289E1690E176E1DE58EE6DD3_131; }
	inline void set_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_131(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___6DE89499E8F85867289E1690E176E1DE58EE6DD3_131 = value;
	}

	inline static int32_t get_offset_of_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132() const { return ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132() { return &___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132; }
	inline void set_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_132 = value;
	}

	inline static int32_t get_offset_of_U36F604C33212E20CBFCE9F2221492DC6FD823857C_133() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___6F604C33212E20CBFCE9F2221492DC6FD823857C_133)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U36F604C33212E20CBFCE9F2221492DC6FD823857C_133() const { return ___6F604C33212E20CBFCE9F2221492DC6FD823857C_133; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U36F604C33212E20CBFCE9F2221492DC6FD823857C_133() { return &___6F604C33212E20CBFCE9F2221492DC6FD823857C_133; }
	inline void set_U36F604C33212E20CBFCE9F2221492DC6FD823857C_133(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___6F604C33212E20CBFCE9F2221492DC6FD823857C_133 = value;
	}

	inline static int32_t get_offset_of_U37102A07EDF52C052D5625D73275A579DC6E76C92_134() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7102A07EDF52C052D5625D73275A579DC6E76C92_134)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U37102A07EDF52C052D5625D73275A579DC6E76C92_134() const { return ___7102A07EDF52C052D5625D73275A579DC6E76C92_134; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U37102A07EDF52C052D5625D73275A579DC6E76C92_134() { return &___7102A07EDF52C052D5625D73275A579DC6E76C92_134; }
	inline void set_U37102A07EDF52C052D5625D73275A579DC6E76C92_134(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___7102A07EDF52C052D5625D73275A579DC6E76C92_134 = value;
	}

	inline static int32_t get_offset_of_U3712B24DD3C17671C78BE33C83316576AD9285273_135() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___712B24DD3C17671C78BE33C83316576AD9285273_135)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3712B24DD3C17671C78BE33C83316576AD9285273_135() const { return ___712B24DD3C17671C78BE33C83316576AD9285273_135; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3712B24DD3C17671C78BE33C83316576AD9285273_135() { return &___712B24DD3C17671C78BE33C83316576AD9285273_135; }
	inline void set_U3712B24DD3C17671C78BE33C83316576AD9285273_135(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___712B24DD3C17671C78BE33C83316576AD9285273_135 = value;
	}

	inline static int32_t get_offset_of_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136() const { return ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136() { return &___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136; }
	inline void set_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_136 = value;
	}

	inline static int32_t get_offset_of_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_137() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_137)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_137() const { return ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_137; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_137() { return &___71F4D547D52F31EDEC55B1909114455FDAE5A97E_137; }
	inline void set_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_137(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___71F4D547D52F31EDEC55B1909114455FDAE5A97E_137 = value;
	}

	inline static int32_t get_offset_of_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138() const { return ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138() { return &___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138; }
	inline void set_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_138 = value;
	}

	inline static int32_t get_offset_of_U372FF92F22AB5BD5A344C652E176645C52D825AEC_139() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___72FF92F22AB5BD5A344C652E176645C52D825AEC_139)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U372FF92F22AB5BD5A344C652E176645C52D825AEC_139() const { return ___72FF92F22AB5BD5A344C652E176645C52D825AEC_139; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U372FF92F22AB5BD5A344C652E176645C52D825AEC_139() { return &___72FF92F22AB5BD5A344C652E176645C52D825AEC_139; }
	inline void set_U372FF92F22AB5BD5A344C652E176645C52D825AEC_139(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___72FF92F22AB5BD5A344C652E176645C52D825AEC_139 = value;
	}

	inline static int32_t get_offset_of_U373B101BBEDC241CE765CA03BB6E4D467FA769312_140() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___73B101BBEDC241CE765CA03BB6E4D467FA769312_140)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U373B101BBEDC241CE765CA03BB6E4D467FA769312_140() const { return ___73B101BBEDC241CE765CA03BB6E4D467FA769312_140; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U373B101BBEDC241CE765CA03BB6E4D467FA769312_140() { return &___73B101BBEDC241CE765CA03BB6E4D467FA769312_140; }
	inline void set_U373B101BBEDC241CE765CA03BB6E4D467FA769312_140(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___73B101BBEDC241CE765CA03BB6E4D467FA769312_140 = value;
	}

	inline static int32_t get_offset_of_U3749EB212DEC39495349481F49F3DEF480777A197_141() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___749EB212DEC39495349481F49F3DEF480777A197_141)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3749EB212DEC39495349481F49F3DEF480777A197_141() const { return ___749EB212DEC39495349481F49F3DEF480777A197_141; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3749EB212DEC39495349481F49F3DEF480777A197_141() { return &___749EB212DEC39495349481F49F3DEF480777A197_141; }
	inline void set_U3749EB212DEC39495349481F49F3DEF480777A197_141(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___749EB212DEC39495349481F49F3DEF480777A197_141 = value;
	}

	inline static int32_t get_offset_of_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_142() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_142)); }
	inline __StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55  get_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_142() const { return ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_142; }
	inline __StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55 * get_address_of_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_142() { return &___74AE47B2D78B62583BA50045F6B288CACC24EA7D_142; }
	inline void set_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_142(__StaticArrayInitTypeSizeU3D2574_tA20F8B9A879D0675E1AD9DA97CF9950596E6ED55  value)
	{
		___74AE47B2D78B62583BA50045F6B288CACC24EA7D_142 = value;
	}

	inline static int32_t get_offset_of_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_143() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_143() const { return ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_143() { return &___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143; }
	inline void set_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_143(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143 = value;
	}

	inline static int32_t get_offset_of_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144() const { return ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144() { return &___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144; }
	inline void set_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_144 = value;
	}

	inline static int32_t get_offset_of_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_145() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_145)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_145() const { return ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_145; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_145() { return &___76D7CB1729978C5437409A8C56AA1C7E24890D5C_145; }
	inline void set_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_145(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___76D7CB1729978C5437409A8C56AA1C7E24890D5C_145 = value;
	}

	inline static int32_t get_offset_of_U3770616E567083077933DC0B29A8F8368FC2EB431_146() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___770616E567083077933DC0B29A8F8368FC2EB431_146)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3770616E567083077933DC0B29A8F8368FC2EB431_146() const { return ___770616E567083077933DC0B29A8F8368FC2EB431_146; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3770616E567083077933DC0B29A8F8368FC2EB431_146() { return &___770616E567083077933DC0B29A8F8368FC2EB431_146; }
	inline void set_U3770616E567083077933DC0B29A8F8368FC2EB431_146(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___770616E567083077933DC0B29A8F8368FC2EB431_146 = value;
	}

	inline static int32_t get_offset_of_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147() const { return ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147() { return &___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147; }
	inline void set_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147 = value;
	}

	inline static int32_t get_offset_of_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_148() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_148)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_148() const { return ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_148; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_148() { return &___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_148; }
	inline void set_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_148(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_148 = value;
	}

	inline static int32_t get_offset_of_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_149() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_149)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_149() const { return ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_149; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_149() { return &___79A4B717DAF4312A78F1A75A72221DB9690D1B25_149; }
	inline void set_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_149(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___79A4B717DAF4312A78F1A75A72221DB9690D1B25_149 = value;
	}

	inline static int32_t get_offset_of_U379BEC582299CE5F46902B9C091C19931E26D9323_150() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___79BEC582299CE5F46902B9C091C19931E26D9323_150)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U379BEC582299CE5F46902B9C091C19931E26D9323_150() const { return ___79BEC582299CE5F46902B9C091C19931E26D9323_150; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U379BEC582299CE5F46902B9C091C19931E26D9323_150() { return &___79BEC582299CE5F46902B9C091C19931E26D9323_150; }
	inline void set_U379BEC582299CE5F46902B9C091C19931E26D9323_150(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___79BEC582299CE5F46902B9C091C19931E26D9323_150 = value;
	}

	inline static int32_t get_offset_of_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_151() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_151)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_151() const { return ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_151; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_151() { return &___79F14B68470B4B7AA3BCDCA6758072F2B9320320_151; }
	inline void set_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_151(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___79F14B68470B4B7AA3BCDCA6758072F2B9320320_151 = value;
	}

	inline static int32_t get_offset_of_U379F9F064F81690C950BF4EFFAA5245BA966607C9_152() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___79F9F064F81690C950BF4EFFAA5245BA966607C9_152)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U379F9F064F81690C950BF4EFFAA5245BA966607C9_152() const { return ___79F9F064F81690C950BF4EFFAA5245BA966607C9_152; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U379F9F064F81690C950BF4EFFAA5245BA966607C9_152() { return &___79F9F064F81690C950BF4EFFAA5245BA966607C9_152; }
	inline void set_U379F9F064F81690C950BF4EFFAA5245BA966607C9_152(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___79F9F064F81690C950BF4EFFAA5245BA966607C9_152 = value;
	}

	inline static int32_t get_offset_of_U37BC6404D48DA137303466471F7131DC1C9B13BAA_153() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7BC6404D48DA137303466471F7131DC1C9B13BAA_153)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U37BC6404D48DA137303466471F7131DC1C9B13BAA_153() const { return ___7BC6404D48DA137303466471F7131DC1C9B13BAA_153; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U37BC6404D48DA137303466471F7131DC1C9B13BAA_153() { return &___7BC6404D48DA137303466471F7131DC1C9B13BAA_153; }
	inline void set_U37BC6404D48DA137303466471F7131DC1C9B13BAA_153(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___7BC6404D48DA137303466471F7131DC1C9B13BAA_153 = value;
	}

	inline static int32_t get_offset_of_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_154() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_154)); }
	inline __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  get_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_154() const { return ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_154; }
	inline __StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625 * get_address_of_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_154() { return &___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_154; }
	inline void set_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_154(__StaticArrayInitTypeSizeU3D36_t28723DE5858E06D972193D8A31D6CA058A020625  value)
	{
		___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_154 = value;
	}

	inline static int32_t get_offset_of_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155() const { return ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155() { return &___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155; }
	inline void set_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_155 = value;
	}

	inline static int32_t get_offset_of_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156)); }
	inline __StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8  get_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156() const { return ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156; }
	inline __StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8 * get_address_of_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156() { return &___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156; }
	inline void set_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156(__StaticArrayInitTypeSizeU3D192_t432522A706A7F263CA52EFBDA4822326F22AC7D8  value)
	{
		___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_156 = value;
	}

	inline static int32_t get_offset_of_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_157() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_157)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_157() const { return ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_157; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_157() { return &___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_157; }
	inline void set_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_157(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_157 = value;
	}

	inline static int32_t get_offset_of_U37FF393DF5D10580FA38BF854328B3072169E0A1A_158() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___7FF393DF5D10580FA38BF854328B3072169E0A1A_158)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U37FF393DF5D10580FA38BF854328B3072169E0A1A_158() const { return ___7FF393DF5D10580FA38BF854328B3072169E0A1A_158; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U37FF393DF5D10580FA38BF854328B3072169E0A1A_158() { return &___7FF393DF5D10580FA38BF854328B3072169E0A1A_158; }
	inline void set_U37FF393DF5D10580FA38BF854328B3072169E0A1A_158(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___7FF393DF5D10580FA38BF854328B3072169E0A1A_158 = value;
	}

	inline static int32_t get_offset_of_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_159() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___802AD21B222E489DAFEADD5E18868203D9FC2A89_159)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_159() const { return ___802AD21B222E489DAFEADD5E18868203D9FC2A89_159; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_159() { return &___802AD21B222E489DAFEADD5E18868203D9FC2A89_159; }
	inline void set_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_159(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___802AD21B222E489DAFEADD5E18868203D9FC2A89_159 = value;
	}

	inline static int32_t get_offset_of_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_160() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___806D200653BD7335D0077F2759E715D8BE8DF9BF_160)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_160() const { return ___806D200653BD7335D0077F2759E715D8BE8DF9BF_160; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_160() { return &___806D200653BD7335D0077F2759E715D8BE8DF9BF_160; }
	inline void set_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_160(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___806D200653BD7335D0077F2759E715D8BE8DF9BF_160 = value;
	}

	inline static int32_t get_offset_of_U3808AD19CA818077F71100880E7D7AA21147E987E_161() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___808AD19CA818077F71100880E7D7AA21147E987E_161)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U3808AD19CA818077F71100880E7D7AA21147E987E_161() const { return ___808AD19CA818077F71100880E7D7AA21147E987E_161; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U3808AD19CA818077F71100880E7D7AA21147E987E_161() { return &___808AD19CA818077F71100880E7D7AA21147E987E_161; }
	inline void set_U3808AD19CA818077F71100880E7D7AA21147E987E_161(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___808AD19CA818077F71100880E7D7AA21147E987E_161 = value;
	}

	inline static int32_t get_offset_of_U380DD6A282D563836522C10A78AAA17F0B865EB5A_162() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___80DD6A282D563836522C10A78AAA17F0B865EB5A_162)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U380DD6A282D563836522C10A78AAA17F0B865EB5A_162() const { return ___80DD6A282D563836522C10A78AAA17F0B865EB5A_162; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U380DD6A282D563836522C10A78AAA17F0B865EB5A_162() { return &___80DD6A282D563836522C10A78AAA17F0B865EB5A_162; }
	inline void set_U380DD6A282D563836522C10A78AAA17F0B865EB5A_162(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___80DD6A282D563836522C10A78AAA17F0B865EB5A_162 = value;
	}

	inline static int32_t get_offset_of_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_163() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_163)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_163() const { return ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_163; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_163() { return &___80E21576779C61DA5B185D15A732CF7A8BD6F88C_163; }
	inline void set_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_163(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___80E21576779C61DA5B185D15A732CF7A8BD6F88C_163 = value;
	}

	inline static int32_t get_offset_of_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_164() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_164)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_164() const { return ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_164; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_164() { return &___80E9217618F8C2C7FED61E4E7653CD0E66C74872_164; }
	inline void set_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_164(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___80E9217618F8C2C7FED61E4E7653CD0E66C74872_164 = value;
	}

	inline static int32_t get_offset_of_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165() const { return ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165() { return &___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165; }
	inline void set_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_165 = value;
	}

	inline static int32_t get_offset_of_U3836DA48724BC66F3257E70FFF165ACCE037666A6_166() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___836DA48724BC66F3257E70FFF165ACCE037666A6_166)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_U3836DA48724BC66F3257E70FFF165ACCE037666A6_166() const { return ___836DA48724BC66F3257E70FFF165ACCE037666A6_166; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_U3836DA48724BC66F3257E70FFF165ACCE037666A6_166() { return &___836DA48724BC66F3257E70FFF165ACCE037666A6_166; }
	inline void set_U3836DA48724BC66F3257E70FFF165ACCE037666A6_166(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___836DA48724BC66F3257E70FFF165ACCE037666A6_166 = value;
	}

	inline static int32_t get_offset_of_U38523C737ED49D8A2E9BB85218190E4E83B902E28_167() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8523C737ED49D8A2E9BB85218190E4E83B902E28_167)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U38523C737ED49D8A2E9BB85218190E4E83B902E28_167() const { return ___8523C737ED49D8A2E9BB85218190E4E83B902E28_167; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U38523C737ED49D8A2E9BB85218190E4E83B902E28_167() { return &___8523C737ED49D8A2E9BB85218190E4E83B902E28_167; }
	inline void set_U38523C737ED49D8A2E9BB85218190E4E83B902E28_167(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___8523C737ED49D8A2E9BB85218190E4E83B902E28_167 = value;
	}

	inline static int32_t get_offset_of_U385666B25692AB814E91F685384EEA0389F147E70_168() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___85666B25692AB814E91F685384EEA0389F147E70_168)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U385666B25692AB814E91F685384EEA0389F147E70_168() const { return ___85666B25692AB814E91F685384EEA0389F147E70_168; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U385666B25692AB814E91F685384EEA0389F147E70_168() { return &___85666B25692AB814E91F685384EEA0389F147E70_168; }
	inline void set_U385666B25692AB814E91F685384EEA0389F147E70_168(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___85666B25692AB814E91F685384EEA0389F147E70_168 = value;
	}

	inline static int32_t get_offset_of_U3866652192B68681CC538B2CB21C5979544DF35AB_169() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___866652192B68681CC538B2CB21C5979544DF35AB_169)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3866652192B68681CC538B2CB21C5979544DF35AB_169() const { return ___866652192B68681CC538B2CB21C5979544DF35AB_169; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3866652192B68681CC538B2CB21C5979544DF35AB_169() { return &___866652192B68681CC538B2CB21C5979544DF35AB_169; }
	inline void set_U3866652192B68681CC538B2CB21C5979544DF35AB_169(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___866652192B68681CC538B2CB21C5979544DF35AB_169 = value;
	}

	inline static int32_t get_offset_of_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170() const { return ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170() { return &___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170; }
	inline void set_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_170 = value;
	}

	inline static int32_t get_offset_of_U3868C8972C4058443A2D131C22083401956DF81C7_171() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___868C8972C4058443A2D131C22083401956DF81C7_171)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_U3868C8972C4058443A2D131C22083401956DF81C7_171() const { return ___868C8972C4058443A2D131C22083401956DF81C7_171; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_U3868C8972C4058443A2D131C22083401956DF81C7_171() { return &___868C8972C4058443A2D131C22083401956DF81C7_171; }
	inline void set_U3868C8972C4058443A2D131C22083401956DF81C7_171(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___868C8972C4058443A2D131C22083401956DF81C7_171 = value;
	}

	inline static int32_t get_offset_of_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_172() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_172)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_172() const { return ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_172; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_172() { return &___86EA004891DE06B357581B1885C9C0EFD73DE2E9_172; }
	inline void set_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_172(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___86EA004891DE06B357581B1885C9C0EFD73DE2E9_172 = value;
	}

	inline static int32_t get_offset_of_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173() const { return ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173() { return &___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173; }
	inline void set_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_173 = value;
	}

	inline static int32_t get_offset_of_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174() const { return ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174() { return &___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174; }
	inline void set_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_174 = value;
	}

	inline static int32_t get_offset_of_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175() const { return ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175() { return &___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175; }
	inline void set_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_175 = value;
	}

	inline static int32_t get_offset_of_U38909772CC771C2FF1D61415555452F2ED4ADA536_176() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8909772CC771C2FF1D61415555452F2ED4ADA536_176)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U38909772CC771C2FF1D61415555452F2ED4ADA536_176() const { return ___8909772CC771C2FF1D61415555452F2ED4ADA536_176; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U38909772CC771C2FF1D61415555452F2ED4ADA536_176() { return &___8909772CC771C2FF1D61415555452F2ED4ADA536_176; }
	inline void set_U38909772CC771C2FF1D61415555452F2ED4ADA536_176(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___8909772CC771C2FF1D61415555452F2ED4ADA536_176 = value;
	}

	inline static int32_t get_offset_of_U38971045D9FA23669DC4A204D478DC63888A9B420_177() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8971045D9FA23669DC4A204D478DC63888A9B420_177)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U38971045D9FA23669DC4A204D478DC63888A9B420_177() const { return ___8971045D9FA23669DC4A204D478DC63888A9B420_177; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U38971045D9FA23669DC4A204D478DC63888A9B420_177() { return &___8971045D9FA23669DC4A204D478DC63888A9B420_177; }
	inline void set_U38971045D9FA23669DC4A204D478DC63888A9B420_177(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___8971045D9FA23669DC4A204D478DC63888A9B420_177 = value;
	}

	inline static int32_t get_offset_of_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178)); }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  get_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178() const { return ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178; }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F * get_address_of_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178() { return &___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178; }
	inline void set_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178(__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  value)
	{
		___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_178 = value;
	}

	inline static int32_t get_offset_of_U38C301DDB45068B145D6C11F79591061EEE2AABA8_179() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8C301DDB45068B145D6C11F79591061EEE2AABA8_179)); }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  get_U38C301DDB45068B145D6C11F79591061EEE2AABA8_179() const { return ___8C301DDB45068B145D6C11F79591061EEE2AABA8_179; }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F * get_address_of_U38C301DDB45068B145D6C11F79591061EEE2AABA8_179() { return &___8C301DDB45068B145D6C11F79591061EEE2AABA8_179; }
	inline void set_U38C301DDB45068B145D6C11F79591061EEE2AABA8_179(__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  value)
	{
		___8C301DDB45068B145D6C11F79591061EEE2AABA8_179 = value;
	}

	inline static int32_t get_offset_of_U38D1369A0D8832C941BD6D09849525D65D807A1DD_180() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8D1369A0D8832C941BD6D09849525D65D807A1DD_180)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U38D1369A0D8832C941BD6D09849525D65D807A1DD_180() const { return ___8D1369A0D8832C941BD6D09849525D65D807A1DD_180; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U38D1369A0D8832C941BD6D09849525D65D807A1DD_180() { return &___8D1369A0D8832C941BD6D09849525D65D807A1DD_180; }
	inline void set_U38D1369A0D8832C941BD6D09849525D65D807A1DD_180(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___8D1369A0D8832C941BD6D09849525D65D807A1DD_180 = value;
	}

	inline static int32_t get_offset_of_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181() const { return ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181() { return &___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181; }
	inline void set_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181 = value;
	}

	inline static int32_t get_offset_of_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182() const { return ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182() { return &___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182; }
	inline void set_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_182 = value;
	}

	inline static int32_t get_offset_of_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183() const { return ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183() { return &___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183; }
	inline void set_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_183 = value;
	}

	inline static int32_t get_offset_of_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_184() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_184)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_184() const { return ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_184; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_184() { return &___8EED33571FFED1BBCA4954A909498A1D3316EDC8_184; }
	inline void set_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_184(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___8EED33571FFED1BBCA4954A909498A1D3316EDC8_184 = value;
	}

	inline static int32_t get_offset_of_U38EEE72A955DF384253C5C458726A8A7299A05164_185() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8EEE72A955DF384253C5C458726A8A7299A05164_185)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U38EEE72A955DF384253C5C458726A8A7299A05164_185() const { return ___8EEE72A955DF384253C5C458726A8A7299A05164_185; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U38EEE72A955DF384253C5C458726A8A7299A05164_185() { return &___8EEE72A955DF384253C5C458726A8A7299A05164_185; }
	inline void set_U38EEE72A955DF384253C5C458726A8A7299A05164_185(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___8EEE72A955DF384253C5C458726A8A7299A05164_185 = value;
	}

	inline static int32_t get_offset_of_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_186() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_186)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_186() const { return ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_186; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_186() { return &___8FD55399F538FAF9608E938EAB08D4BC88D0E131_186; }
	inline void set_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_186(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___8FD55399F538FAF9608E938EAB08D4BC88D0E131_186 = value;
	}

	inline static int32_t get_offset_of_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187() const { return ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187() { return &___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187; }
	inline void set_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_187 = value;
	}

	inline static int32_t get_offset_of_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188() const { return ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188() { return &___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188; }
	inline void set_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_188 = value;
	}

	inline static int32_t get_offset_of_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189() const { return ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189() { return &___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189; }
	inline void set_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_189 = value;
	}

	inline static int32_t get_offset_of_U39623BB2649942957F680075A5AD3A16DE427F7A2_190() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9623BB2649942957F680075A5AD3A16DE427F7A2_190)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_U39623BB2649942957F680075A5AD3A16DE427F7A2_190() const { return ___9623BB2649942957F680075A5AD3A16DE427F7A2_190; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_U39623BB2649942957F680075A5AD3A16DE427F7A2_190() { return &___9623BB2649942957F680075A5AD3A16DE427F7A2_190; }
	inline void set_U39623BB2649942957F680075A5AD3A16DE427F7A2_190(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___9623BB2649942957F680075A5AD3A16DE427F7A2_190 = value;
	}

	inline static int32_t get_offset_of_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191() const { return ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191() { return &___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191; }
	inline void set_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_191 = value;
	}

	inline static int32_t get_offset_of_U39858ACE49B9B39354CFCB05E54A9790070E48D39_192() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9858ACE49B9B39354CFCB05E54A9790070E48D39_192)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U39858ACE49B9B39354CFCB05E54A9790070E48D39_192() const { return ___9858ACE49B9B39354CFCB05E54A9790070E48D39_192; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U39858ACE49B9B39354CFCB05E54A9790070E48D39_192() { return &___9858ACE49B9B39354CFCB05E54A9790070E48D39_192; }
	inline void set_U39858ACE49B9B39354CFCB05E54A9790070E48D39_192(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___9858ACE49B9B39354CFCB05E54A9790070E48D39_192 = value;
	}

	inline static int32_t get_offset_of_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193() const { return ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193() { return &___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193; }
	inline void set_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_193 = value;
	}

	inline static int32_t get_offset_of_U399CC0827BC16027263E3737FF59411A5D4BFDE31_194() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___99CC0827BC16027263E3737FF59411A5D4BFDE31_194)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_U399CC0827BC16027263E3737FF59411A5D4BFDE31_194() const { return ___99CC0827BC16027263E3737FF59411A5D4BFDE31_194; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_U399CC0827BC16027263E3737FF59411A5D4BFDE31_194() { return &___99CC0827BC16027263E3737FF59411A5D4BFDE31_194; }
	inline void set_U399CC0827BC16027263E3737FF59411A5D4BFDE31_194(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___99CC0827BC16027263E3737FF59411A5D4BFDE31_194 = value;
	}

	inline static int32_t get_offset_of_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195() const { return ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195() { return &___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195; }
	inline void set_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_195 = value;
	}

	inline static int32_t get_offset_of_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_196() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_196)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_196() const { return ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_196; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_196() { return &___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_196; }
	inline void set_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_196(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_196 = value;
	}

	inline static int32_t get_offset_of_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_197() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_197)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_197() const { return ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_197; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_197() { return &___9B3E34C734351ED3DE1984119EAB9572C48FA15E_197; }
	inline void set_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_197(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___9B3E34C734351ED3DE1984119EAB9572C48FA15E_197 = value;
	}

	inline static int32_t get_offset_of_U39BBAB86ACB95EF8C028708733458CDBAC4715197_198() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9BBAB86ACB95EF8C028708733458CDBAC4715197_198)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U39BBAB86ACB95EF8C028708733458CDBAC4715197_198() const { return ___9BBAB86ACB95EF8C028708733458CDBAC4715197_198; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U39BBAB86ACB95EF8C028708733458CDBAC4715197_198() { return &___9BBAB86ACB95EF8C028708733458CDBAC4715197_198; }
	inline void set_U39BBAB86ACB95EF8C028708733458CDBAC4715197_198(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___9BBAB86ACB95EF8C028708733458CDBAC4715197_198 = value;
	}

	inline static int32_t get_offset_of_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199() const { return ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199() { return &___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199; }
	inline void set_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_199 = value;
	}

	inline static int32_t get_offset_of_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200() const { return ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200() { return &___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200; }
	inline void set_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200 = value;
	}

	inline static int32_t get_offset_of_U39E548860392B66258417232F41EF03D48EBDD686_201() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9E548860392B66258417232F41EF03D48EBDD686_201)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_U39E548860392B66258417232F41EF03D48EBDD686_201() const { return ___9E548860392B66258417232F41EF03D48EBDD686_201; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_U39E548860392B66258417232F41EF03D48EBDD686_201() { return &___9E548860392B66258417232F41EF03D48EBDD686_201; }
	inline void set_U39E548860392B66258417232F41EF03D48EBDD686_201(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___9E548860392B66258417232F41EF03D48EBDD686_201 = value;
	}

	inline static int32_t get_offset_of_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_202() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_202)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_202() const { return ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_202; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_202() { return &___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_202; }
	inline void set_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_202(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_202 = value;
	}

	inline static int32_t get_offset_of_A0324640CCF369A6794A9A8BB204ED22651ECBCC_203() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_203)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_A0324640CCF369A6794A9A8BB204ED22651ECBCC_203() const { return ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_203; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_A0324640CCF369A6794A9A8BB204ED22651ECBCC_203() { return &___A0324640CCF369A6794A9A8BB204ED22651ECBCC_203; }
	inline void set_A0324640CCF369A6794A9A8BB204ED22651ECBCC_203(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___A0324640CCF369A6794A9A8BB204ED22651ECBCC_203 = value;
	}

	inline static int32_t get_offset_of_A2978A4F76015511E3741ABF9B58117B348B19C2_204() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A2978A4F76015511E3741ABF9B58117B348B19C2_204)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_A2978A4F76015511E3741ABF9B58117B348B19C2_204() const { return ___A2978A4F76015511E3741ABF9B58117B348B19C2_204; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_A2978A4F76015511E3741ABF9B58117B348B19C2_204() { return &___A2978A4F76015511E3741ABF9B58117B348B19C2_204; }
	inline void set_A2978A4F76015511E3741ABF9B58117B348B19C2_204(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___A2978A4F76015511E3741ABF9B58117B348B19C2_204 = value;
	}

	inline static int32_t get_offset_of_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205)); }
	inline __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  get_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205() const { return ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205; }
	inline __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C * get_address_of_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205() { return &___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205; }
	inline void set_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205(__StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  value)
	{
		___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_205 = value;
	}

	inline static int32_t get_offset_of_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206() const { return ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206() { return &___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206; }
	inline void set_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_206 = value;
	}

	inline static int32_t get_offset_of_A3F55D69F9A97411DB4065F0FA414D912A181B69_207() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A3F55D69F9A97411DB4065F0FA414D912A181B69_207)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_A3F55D69F9A97411DB4065F0FA414D912A181B69_207() const { return ___A3F55D69F9A97411DB4065F0FA414D912A181B69_207; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_A3F55D69F9A97411DB4065F0FA414D912A181B69_207() { return &___A3F55D69F9A97411DB4065F0FA414D912A181B69_207; }
	inline void set_A3F55D69F9A97411DB4065F0FA414D912A181B69_207(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___A3F55D69F9A97411DB4065F0FA414D912A181B69_207 = value;
	}

	inline static int32_t get_offset_of_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208)); }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  get_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208() const { return ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208; }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6 * get_address_of_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208() { return &___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208; }
	inline void set_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208(__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  value)
	{
		___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_208 = value;
	}

	inline static int32_t get_offset_of_A7977502040EE08933752F579060577814723508_209() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A7977502040EE08933752F579060577814723508_209)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_A7977502040EE08933752F579060577814723508_209() const { return ___A7977502040EE08933752F579060577814723508_209; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_A7977502040EE08933752F579060577814723508_209() { return &___A7977502040EE08933752F579060577814723508_209; }
	inline void set_A7977502040EE08933752F579060577814723508_209(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___A7977502040EE08933752F579060577814723508_209 = value;
	}

	inline static int32_t get_offset_of_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210() const { return ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210() { return &___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210; }
	inline void set_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_210 = value;
	}

	inline static int32_t get_offset_of_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211() const { return ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211() { return &___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211; }
	inline void set_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_211 = value;
	}

	inline static int32_t get_offset_of_AA607BA0FA20D3E80F456E68768C517D17D073D5_212() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AA607BA0FA20D3E80F456E68768C517D17D073D5_212)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_AA607BA0FA20D3E80F456E68768C517D17D073D5_212() const { return ___AA607BA0FA20D3E80F456E68768C517D17D073D5_212; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_AA607BA0FA20D3E80F456E68768C517D17D073D5_212() { return &___AA607BA0FA20D3E80F456E68768C517D17D073D5_212; }
	inline void set_AA607BA0FA20D3E80F456E68768C517D17D073D5_212(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___AA607BA0FA20D3E80F456E68768C517D17D073D5_212 = value;
	}

	inline static int32_t get_offset_of_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213() const { return ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213() { return &___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213; }
	inline void set_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_213 = value;
	}

	inline static int32_t get_offset_of_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214() const { return ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214() { return &___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214; }
	inline void set_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_214 = value;
	}

	inline static int32_t get_offset_of_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215() const { return ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215() { return &___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215; }
	inline void set_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_215 = value;
	}

	inline static int32_t get_offset_of_AECEE3733C380177F0A39ACF3AF99BFE360C156F_216() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_216)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_AECEE3733C380177F0A39ACF3AF99BFE360C156F_216() const { return ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_216; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_AECEE3733C380177F0A39ACF3AF99BFE360C156F_216() { return &___AECEE3733C380177F0A39ACF3AF99BFE360C156F_216; }
	inline void set_AECEE3733C380177F0A39ACF3AF99BFE360C156F_216(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___AECEE3733C380177F0A39ACF3AF99BFE360C156F_216 = value;
	}

	inline static int32_t get_offset_of_AEE03822C3725428610E6E7DAAF7970DE35F7675_217() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AEE03822C3725428610E6E7DAAF7970DE35F7675_217)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_AEE03822C3725428610E6E7DAAF7970DE35F7675_217() const { return ___AEE03822C3725428610E6E7DAAF7970DE35F7675_217; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_AEE03822C3725428610E6E7DAAF7970DE35F7675_217() { return &___AEE03822C3725428610E6E7DAAF7970DE35F7675_217; }
	inline void set_AEE03822C3725428610E6E7DAAF7970DE35F7675_217(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___AEE03822C3725428610E6E7DAAF7970DE35F7675_217 = value;
	}

	inline static int32_t get_offset_of_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218)); }
	inline int64_t get_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218() const { return ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218; }
	inline int64_t* get_address_of_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218() { return &___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218; }
	inline void set_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218(int64_t value)
	{
		___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_218 = value;
	}

	inline static int32_t get_offset_of_B15D37D4738DBDC32C38C2E1B6A1833322C22868_219() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_219)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_B15D37D4738DBDC32C38C2E1B6A1833322C22868_219() const { return ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_219; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_B15D37D4738DBDC32C38C2E1B6A1833322C22868_219() { return &___B15D37D4738DBDC32C38C2E1B6A1833322C22868_219; }
	inline void set_B15D37D4738DBDC32C38C2E1B6A1833322C22868_219(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___B15D37D4738DBDC32C38C2E1B6A1833322C22868_219 = value;
	}

	inline static int32_t get_offset_of_B19381C60E723A535EF856EC118DB867503FCC63_220() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B19381C60E723A535EF856EC118DB867503FCC63_220)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_B19381C60E723A535EF856EC118DB867503FCC63_220() const { return ___B19381C60E723A535EF856EC118DB867503FCC63_220; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_B19381C60E723A535EF856EC118DB867503FCC63_220() { return &___B19381C60E723A535EF856EC118DB867503FCC63_220; }
	inline void set_B19381C60E723A535EF856EC118DB867503FCC63_220(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___B19381C60E723A535EF856EC118DB867503FCC63_220 = value;
	}

	inline static int32_t get_offset_of_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221() const { return ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221() { return &___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221; }
	inline void set_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_221 = value;
	}

	inline static int32_t get_offset_of_B5DDF34D20E879DBA76358A13661F61E6682B937_222() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B5DDF34D20E879DBA76358A13661F61E6682B937_222)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_B5DDF34D20E879DBA76358A13661F61E6682B937_222() const { return ___B5DDF34D20E879DBA76358A13661F61E6682B937_222; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_B5DDF34D20E879DBA76358A13661F61E6682B937_222() { return &___B5DDF34D20E879DBA76358A13661F61E6682B937_222; }
	inline void set_B5DDF34D20E879DBA76358A13661F61E6682B937_222(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___B5DDF34D20E879DBA76358A13661F61E6682B937_222 = value;
	}

	inline static int32_t get_offset_of_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223() const { return ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223() { return &___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223; }
	inline void set_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_223 = value;
	}

	inline static int32_t get_offset_of_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224() const { return ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224() { return &___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224; }
	inline void set_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_224 = value;
	}

	inline static int32_t get_offset_of_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225() const { return ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225() { return &___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225; }
	inline void set_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225 = value;
	}

	inline static int32_t get_offset_of_BBA3BFEF194A76998C4874D107EBEA8A81357762_226() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BBA3BFEF194A76998C4874D107EBEA8A81357762_226)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_BBA3BFEF194A76998C4874D107EBEA8A81357762_226() const { return ___BBA3BFEF194A76998C4874D107EBEA8A81357762_226; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_BBA3BFEF194A76998C4874D107EBEA8A81357762_226() { return &___BBA3BFEF194A76998C4874D107EBEA8A81357762_226; }
	inline void set_BBA3BFEF194A76998C4874D107EBEA8A81357762_226(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___BBA3BFEF194A76998C4874D107EBEA8A81357762_226 = value;
	}

	inline static int32_t get_offset_of_BBAA25706062E8AA738F96C541E3A6D6357F92E3_227() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_227)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_BBAA25706062E8AA738F96C541E3A6D6357F92E3_227() const { return ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_227; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_BBAA25706062E8AA738F96C541E3A6D6357F92E3_227() { return &___BBAA25706062E8AA738F96C541E3A6D6357F92E3_227; }
	inline void set_BBAA25706062E8AA738F96C541E3A6D6357F92E3_227(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___BBAA25706062E8AA738F96C541E3A6D6357F92E3_227 = value;
	}

	inline static int32_t get_offset_of_BBE42ED1645F9D77581D1D5B6EE11B933512F832_228() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_228)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_BBE42ED1645F9D77581D1D5B6EE11B933512F832_228() const { return ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_228; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_BBE42ED1645F9D77581D1D5B6EE11B933512F832_228() { return &___BBE42ED1645F9D77581D1D5B6EE11B933512F832_228; }
	inline void set_BBE42ED1645F9D77581D1D5B6EE11B933512F832_228(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___BBE42ED1645F9D77581D1D5B6EE11B933512F832_228 = value;
	}

	inline static int32_t get_offset_of_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229() const { return ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229() { return &___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229; }
	inline void set_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_229 = value;
	}

	inline static int32_t get_offset_of_BC5EFA0F409033551EB01FD9CAA887739FBB267C_230() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_230)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_BC5EFA0F409033551EB01FD9CAA887739FBB267C_230() const { return ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_230; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_BC5EFA0F409033551EB01FD9CAA887739FBB267C_230() { return &___BC5EFA0F409033551EB01FD9CAA887739FBB267C_230; }
	inline void set_BC5EFA0F409033551EB01FD9CAA887739FBB267C_230(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___BC5EFA0F409033551EB01FD9CAA887739FBB267C_230 = value;
	}

	inline static int32_t get_offset_of_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231() const { return ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231() { return &___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231; }
	inline void set_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_231 = value;
	}

	inline static int32_t get_offset_of_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232() const { return ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232() { return &___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232; }
	inline void set_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_232 = value;
	}

	inline static int32_t get_offset_of_BF4FBD059EA47B09FA514A81515DD83A7F22A498_233() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_233)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_BF4FBD059EA47B09FA514A81515DD83A7F22A498_233() const { return ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_233; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_BF4FBD059EA47B09FA514A81515DD83A7F22A498_233() { return &___BF4FBD059EA47B09FA514A81515DD83A7F22A498_233; }
	inline void set_BF4FBD059EA47B09FA514A81515DD83A7F22A498_233(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___BF4FBD059EA47B09FA514A81515DD83A7F22A498_233 = value;
	}

	inline static int32_t get_offset_of_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234() const { return ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234() { return &___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234; }
	inline void set_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_234 = value;
	}

	inline static int32_t get_offset_of_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235() const { return ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235() { return &___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235; }
	inline void set_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235 = value;
	}

	inline static int32_t get_offset_of_C0935C173C7A144FE24DA09584F4892153D01F3C_236() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C0935C173C7A144FE24DA09584F4892153D01F3C_236)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_C0935C173C7A144FE24DA09584F4892153D01F3C_236() const { return ___C0935C173C7A144FE24DA09584F4892153D01F3C_236; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_C0935C173C7A144FE24DA09584F4892153D01F3C_236() { return &___C0935C173C7A144FE24DA09584F4892153D01F3C_236; }
	inline void set_C0935C173C7A144FE24DA09584F4892153D01F3C_236(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___C0935C173C7A144FE24DA09584F4892153D01F3C_236 = value;
	}

	inline static int32_t get_offset_of_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237() const { return ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237() { return &___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237; }
	inline void set_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_237 = value;
	}

	inline static int32_t get_offset_of_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238() const { return ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238() { return &___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238; }
	inline void set_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_238 = value;
	}

	inline static int32_t get_offset_of_C530C580972A28DBE79D791F61E5916A541E6A1A_239() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C530C580972A28DBE79D791F61E5916A541E6A1A_239)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_C530C580972A28DBE79D791F61E5916A541E6A1A_239() const { return ___C530C580972A28DBE79D791F61E5916A541E6A1A_239; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_C530C580972A28DBE79D791F61E5916A541E6A1A_239() { return &___C530C580972A28DBE79D791F61E5916A541E6A1A_239; }
	inline void set_C530C580972A28DBE79D791F61E5916A541E6A1A_239(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___C530C580972A28DBE79D791F61E5916A541E6A1A_239 = value;
	}

	inline static int32_t get_offset_of_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240() const { return ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240() { return &___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240; }
	inline void set_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_240 = value;
	}

	inline static int32_t get_offset_of_C62717FC35BA54563F42385BF79E44DD0A510124_241() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C62717FC35BA54563F42385BF79E44DD0A510124_241)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_C62717FC35BA54563F42385BF79E44DD0A510124_241() const { return ___C62717FC35BA54563F42385BF79E44DD0A510124_241; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_C62717FC35BA54563F42385BF79E44DD0A510124_241() { return &___C62717FC35BA54563F42385BF79E44DD0A510124_241; }
	inline void set_C62717FC35BA54563F42385BF79E44DD0A510124_241(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___C62717FC35BA54563F42385BF79E44DD0A510124_241 = value;
	}

	inline static int32_t get_offset_of_C6B6783FDC75202733D0518585E649906008DBC5_242() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C6B6783FDC75202733D0518585E649906008DBC5_242)); }
	inline __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  get_C6B6783FDC75202733D0518585E649906008DBC5_242() const { return ___C6B6783FDC75202733D0518585E649906008DBC5_242; }
	inline __StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2 * get_address_of_C6B6783FDC75202733D0518585E649906008DBC5_242() { return &___C6B6783FDC75202733D0518585E649906008DBC5_242; }
	inline void set_C6B6783FDC75202733D0518585E649906008DBC5_242(__StaticArrayInitTypeSizeU3D64_t2D0D6315C5BDD2C913BD3A90BD22EA07D19C87E2  value)
	{
		___C6B6783FDC75202733D0518585E649906008DBC5_242 = value;
	}

	inline static int32_t get_offset_of_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243() const { return ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243() { return &___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243; }
	inline void set_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_243 = value;
	}

	inline static int32_t get_offset_of_C7787E3098145FA28D38DE952D634BA862284127_244() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C7787E3098145FA28D38DE952D634BA862284127_244)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_C7787E3098145FA28D38DE952D634BA862284127_244() const { return ___C7787E3098145FA28D38DE952D634BA862284127_244; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_C7787E3098145FA28D38DE952D634BA862284127_244() { return &___C7787E3098145FA28D38DE952D634BA862284127_244; }
	inline void set_C7787E3098145FA28D38DE952D634BA862284127_244(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___C7787E3098145FA28D38DE952D634BA862284127_244 = value;
	}

	inline static int32_t get_offset_of_C7BD442E02179DC75AB7FEF343C297E1F46A7806_245() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_245)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_C7BD442E02179DC75AB7FEF343C297E1F46A7806_245() const { return ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_245; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_C7BD442E02179DC75AB7FEF343C297E1F46A7806_245() { return &___C7BD442E02179DC75AB7FEF343C297E1F46A7806_245; }
	inline void set_C7BD442E02179DC75AB7FEF343C297E1F46A7806_245(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___C7BD442E02179DC75AB7FEF343C297E1F46A7806_245 = value;
	}

	inline static int32_t get_offset_of_C7FBA0F208A16658206056EC89481A5EE74A7259_246() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C7FBA0F208A16658206056EC89481A5EE74A7259_246)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_C7FBA0F208A16658206056EC89481A5EE74A7259_246() const { return ___C7FBA0F208A16658206056EC89481A5EE74A7259_246; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_C7FBA0F208A16658206056EC89481A5EE74A7259_246() { return &___C7FBA0F208A16658206056EC89481A5EE74A7259_246; }
	inline void set_C7FBA0F208A16658206056EC89481A5EE74A7259_246(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___C7FBA0F208A16658206056EC89481A5EE74A7259_246 = value;
	}

	inline static int32_t get_offset_of_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247() const { return ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247() { return &___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247; }
	inline void set_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_247 = value;
	}

	inline static int32_t get_offset_of_C92559E411FAF1D47ADD6CCA7374DAF066069D00_248() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_248)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_C92559E411FAF1D47ADD6CCA7374DAF066069D00_248() const { return ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_248; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_C92559E411FAF1D47ADD6CCA7374DAF066069D00_248() { return &___C92559E411FAF1D47ADD6CCA7374DAF066069D00_248; }
	inline void set_C92559E411FAF1D47ADD6CCA7374DAF066069D00_248(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___C92559E411FAF1D47ADD6CCA7374DAF066069D00_248 = value;
	}

	inline static int32_t get_offset_of_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249() const { return ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249() { return &___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249; }
	inline void set_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_249 = value;
	}

	inline static int32_t get_offset_of_CA03F848D4A9DD85B5CF092E36317D483E3B0864_250() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_250)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_CA03F848D4A9DD85B5CF092E36317D483E3B0864_250() const { return ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_250; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_CA03F848D4A9DD85B5CF092E36317D483E3B0864_250() { return &___CA03F848D4A9DD85B5CF092E36317D483E3B0864_250; }
	inline void set_CA03F848D4A9DD85B5CF092E36317D483E3B0864_250(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___CA03F848D4A9DD85B5CF092E36317D483E3B0864_250 = value;
	}

	inline static int32_t get_offset_of_CB780513B9E4B4E2590F7104B066F632E7BC3E38_251() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_251)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_CB780513B9E4B4E2590F7104B066F632E7BC3E38_251() const { return ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_251; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_CB780513B9E4B4E2590F7104B066F632E7BC3E38_251() { return &___CB780513B9E4B4E2590F7104B066F632E7BC3E38_251; }
	inline void set_CB780513B9E4B4E2590F7104B066F632E7BC3E38_251(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___CB780513B9E4B4E2590F7104B066F632E7BC3E38_251 = value;
	}

	inline static int32_t get_offset_of_CC74BF89CF621AAC29665426A911E4B8BBB60B91_252() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_252)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_CC74BF89CF621AAC29665426A911E4B8BBB60B91_252() const { return ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_252; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_CC74BF89CF621AAC29665426A911E4B8BBB60B91_252() { return &___CC74BF89CF621AAC29665426A911E4B8BBB60B91_252; }
	inline void set_CC74BF89CF621AAC29665426A911E4B8BBB60B91_252(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___CC74BF89CF621AAC29665426A911E4B8BBB60B91_252 = value;
	}

	inline static int32_t get_offset_of_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253)); }
	inline __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  get_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253() const { return ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253; }
	inline __StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C * get_address_of_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253() { return &___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253; }
	inline void set_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253(__StaticArrayInitTypeSizeU3D10_tFBDE6DA90EF22D1726EA653F010075E98E2B508C  value)
	{
		___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_253 = value;
	}

	inline static int32_t get_offset_of_CC9EE2CF247B3534102C1324C2679503686EF031_254() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CC9EE2CF247B3534102C1324C2679503686EF031_254)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_CC9EE2CF247B3534102C1324C2679503686EF031_254() const { return ___CC9EE2CF247B3534102C1324C2679503686EF031_254; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_CC9EE2CF247B3534102C1324C2679503686EF031_254() { return &___CC9EE2CF247B3534102C1324C2679503686EF031_254; }
	inline void set_CC9EE2CF247B3534102C1324C2679503686EF031_254(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___CC9EE2CF247B3534102C1324C2679503686EF031_254 = value;
	}

	inline static int32_t get_offset_of_CDDC3D4212D35722E3E8BAA7B5275932A6567037_255() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_255)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_CDDC3D4212D35722E3E8BAA7B5275932A6567037_255() const { return ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_255; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_CDDC3D4212D35722E3E8BAA7B5275932A6567037_255() { return &___CDDC3D4212D35722E3E8BAA7B5275932A6567037_255; }
	inline void set_CDDC3D4212D35722E3E8BAA7B5275932A6567037_255(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___CDDC3D4212D35722E3E8BAA7B5275932A6567037_255 = value;
	}

	inline static int32_t get_offset_of_CED1DB702E32A812D587254A4E1BAAA409E30859_256() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CED1DB702E32A812D587254A4E1BAAA409E30859_256)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_CED1DB702E32A812D587254A4E1BAAA409E30859_256() const { return ___CED1DB702E32A812D587254A4E1BAAA409E30859_256; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_CED1DB702E32A812D587254A4E1BAAA409E30859_256() { return &___CED1DB702E32A812D587254A4E1BAAA409E30859_256; }
	inline void set_CED1DB702E32A812D587254A4E1BAAA409E30859_256(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___CED1DB702E32A812D587254A4E1BAAA409E30859_256 = value;
	}

	inline static int32_t get_offset_of_CF1C9B07580D3992A644F840EBBB156E0E92B758_257() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___CF1C9B07580D3992A644F840EBBB156E0E92B758_257)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_CF1C9B07580D3992A644F840EBBB156E0E92B758_257() const { return ___CF1C9B07580D3992A644F840EBBB156E0E92B758_257; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_CF1C9B07580D3992A644F840EBBB156E0E92B758_257() { return &___CF1C9B07580D3992A644F840EBBB156E0E92B758_257; }
	inline void set_CF1C9B07580D3992A644F840EBBB156E0E92B758_257(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___CF1C9B07580D3992A644F840EBBB156E0E92B758_257 = value;
	}

	inline static int32_t get_offset_of_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258() const { return ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258() { return &___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258; }
	inline void set_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_258 = value;
	}

	inline static int32_t get_offset_of_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259() const { return ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259() { return &___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259; }
	inline void set_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_259 = value;
	}

	inline static int32_t get_offset_of_D41FD917303D010F26005BDAF1F31ABC8A512A13_260() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D41FD917303D010F26005BDAF1F31ABC8A512A13_260)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_D41FD917303D010F26005BDAF1F31ABC8A512A13_260() const { return ___D41FD917303D010F26005BDAF1F31ABC8A512A13_260; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_D41FD917303D010F26005BDAF1F31ABC8A512A13_260() { return &___D41FD917303D010F26005BDAF1F31ABC8A512A13_260; }
	inline void set_D41FD917303D010F26005BDAF1F31ABC8A512A13_260(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___D41FD917303D010F26005BDAF1F31ABC8A512A13_260 = value;
	}

	inline static int32_t get_offset_of_D5B6993D49960DF01393AD8095091B12332C2FA3_261() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D5B6993D49960DF01393AD8095091B12332C2FA3_261)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D5B6993D49960DF01393AD8095091B12332C2FA3_261() const { return ___D5B6993D49960DF01393AD8095091B12332C2FA3_261; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D5B6993D49960DF01393AD8095091B12332C2FA3_261() { return &___D5B6993D49960DF01393AD8095091B12332C2FA3_261; }
	inline void set_D5B6993D49960DF01393AD8095091B12332C2FA3_261(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D5B6993D49960DF01393AD8095091B12332C2FA3_261 = value;
	}

	inline static int32_t get_offset_of_D6B85A234C187D0FC82CE310F6423237C934B93A_262() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D6B85A234C187D0FC82CE310F6423237C934B93A_262)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D6B85A234C187D0FC82CE310F6423237C934B93A_262() const { return ___D6B85A234C187D0FC82CE310F6423237C934B93A_262; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D6B85A234C187D0FC82CE310F6423237C934B93A_262() { return &___D6B85A234C187D0FC82CE310F6423237C934B93A_262; }
	inline void set_D6B85A234C187D0FC82CE310F6423237C934B93A_262(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D6B85A234C187D0FC82CE310F6423237C934B93A_262 = value;
	}

	inline static int32_t get_offset_of_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263() const { return ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263() { return &___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263; }
	inline void set_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_263 = value;
	}

	inline static int32_t get_offset_of_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264() const { return ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264() { return &___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264; }
	inline void set_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_264 = value;
	}

	inline static int32_t get_offset_of_D73BDC60FA2857E7CE2F742530B056A1866C2177_265() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D73BDC60FA2857E7CE2F742530B056A1866C2177_265)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_D73BDC60FA2857E7CE2F742530B056A1866C2177_265() const { return ___D73BDC60FA2857E7CE2F742530B056A1866C2177_265; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_D73BDC60FA2857E7CE2F742530B056A1866C2177_265() { return &___D73BDC60FA2857E7CE2F742530B056A1866C2177_265; }
	inline void set_D73BDC60FA2857E7CE2F742530B056A1866C2177_265(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___D73BDC60FA2857E7CE2F742530B056A1866C2177_265 = value;
	}

	inline static int32_t get_offset_of_D7AE4E850949E37E7359595699CCDC751E1C45E4_266() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D7AE4E850949E37E7359595699CCDC751E1C45E4_266)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_D7AE4E850949E37E7359595699CCDC751E1C45E4_266() const { return ___D7AE4E850949E37E7359595699CCDC751E1C45E4_266; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_D7AE4E850949E37E7359595699CCDC751E1C45E4_266() { return &___D7AE4E850949E37E7359595699CCDC751E1C45E4_266; }
	inline void set_D7AE4E850949E37E7359595699CCDC751E1C45E4_266(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___D7AE4E850949E37E7359595699CCDC751E1C45E4_266 = value;
	}

	inline static int32_t get_offset_of_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267)); }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  get_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267() const { return ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267; }
	inline __StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3 * get_address_of_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267() { return &___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267; }
	inline void set_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267(__StaticArrayInitTypeSizeU3D40_tB87A12D6BA98B7B88C7B84305560EBC8D7003FA3  value)
	{
		___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_267 = value;
	}

	inline static int32_t get_offset_of_D923777F48CDA51F21CABCED4FE36A2D620288A2_268() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___D923777F48CDA51F21CABCED4FE36A2D620288A2_268)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_D923777F48CDA51F21CABCED4FE36A2D620288A2_268() const { return ___D923777F48CDA51F21CABCED4FE36A2D620288A2_268; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_D923777F48CDA51F21CABCED4FE36A2D620288A2_268() { return &___D923777F48CDA51F21CABCED4FE36A2D620288A2_268; }
	inline void set_D923777F48CDA51F21CABCED4FE36A2D620288A2_268(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___D923777F48CDA51F21CABCED4FE36A2D620288A2_268 = value;
	}

	inline static int32_t get_offset_of_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269() const { return ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269() { return &___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269; }
	inline void set_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269 = value;
	}

	inline static int32_t get_offset_of_DC7D58FD4F690A68811E226229F74D0089F018B3_270() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___DC7D58FD4F690A68811E226229F74D0089F018B3_270)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_DC7D58FD4F690A68811E226229F74D0089F018B3_270() const { return ___DC7D58FD4F690A68811E226229F74D0089F018B3_270; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_DC7D58FD4F690A68811E226229F74D0089F018B3_270() { return &___DC7D58FD4F690A68811E226229F74D0089F018B3_270; }
	inline void set_DC7D58FD4F690A68811E226229F74D0089F018B3_270(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___DC7D58FD4F690A68811E226229F74D0089F018B3_270 = value;
	}

	inline static int32_t get_offset_of_DD04181543E071971F6379A33D631064A9B051D8_271() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___DD04181543E071971F6379A33D631064A9B051D8_271)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_DD04181543E071971F6379A33D631064A9B051D8_271() const { return ___DD04181543E071971F6379A33D631064A9B051D8_271; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_DD04181543E071971F6379A33D631064A9B051D8_271() { return &___DD04181543E071971F6379A33D631064A9B051D8_271; }
	inline void set_DD04181543E071971F6379A33D631064A9B051D8_271(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___DD04181543E071971F6379A33D631064A9B051D8_271 = value;
	}

	inline static int32_t get_offset_of_DEDEF9E8D2052710E15C0CC080004BFB33F47665_272() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_272)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_DEDEF9E8D2052710E15C0CC080004BFB33F47665_272() const { return ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_272; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_DEDEF9E8D2052710E15C0CC080004BFB33F47665_272() { return &___DEDEF9E8D2052710E15C0CC080004BFB33F47665_272; }
	inline void set_DEDEF9E8D2052710E15C0CC080004BFB33F47665_272(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___DEDEF9E8D2052710E15C0CC080004BFB33F47665_272 = value;
	}

	inline static int32_t get_offset_of_E02E3EC94504AFEEB34CE409836A3797D7E43964_273() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E02E3EC94504AFEEB34CE409836A3797D7E43964_273)); }
	inline __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  get_E02E3EC94504AFEEB34CE409836A3797D7E43964_273() const { return ___E02E3EC94504AFEEB34CE409836A3797D7E43964_273; }
	inline __StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137 * get_address_of_E02E3EC94504AFEEB34CE409836A3797D7E43964_273() { return &___E02E3EC94504AFEEB34CE409836A3797D7E43964_273; }
	inline void set_E02E3EC94504AFEEB34CE409836A3797D7E43964_273(__StaticArrayInitTypeSizeU3D11148_t63EECE3D1F999949900CCE842FC93D85BC68B137  value)
	{
		___E02E3EC94504AFEEB34CE409836A3797D7E43964_273 = value;
	}

	inline static int32_t get_offset_of_E1A877959C9E369159A2433060A6781C6C5C1D5C_274() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E1A877959C9E369159A2433060A6781C6C5C1D5C_274)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_E1A877959C9E369159A2433060A6781C6C5C1D5C_274() const { return ___E1A877959C9E369159A2433060A6781C6C5C1D5C_274; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_E1A877959C9E369159A2433060A6781C6C5C1D5C_274() { return &___E1A877959C9E369159A2433060A6781C6C5C1D5C_274; }
	inline void set_E1A877959C9E369159A2433060A6781C6C5C1D5C_274(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___E1A877959C9E369159A2433060A6781C6C5C1D5C_274 = value;
	}

	inline static int32_t get_offset_of_E2654B2181D84518EB5EE7BC342CFFD1F528E908_275() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_275)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_E2654B2181D84518EB5EE7BC342CFFD1F528E908_275() const { return ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_275; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_E2654B2181D84518EB5EE7BC342CFFD1F528E908_275() { return &___E2654B2181D84518EB5EE7BC342CFFD1F528E908_275; }
	inline void set_E2654B2181D84518EB5EE7BC342CFFD1F528E908_275(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___E2654B2181D84518EB5EE7BC342CFFD1F528E908_275 = value;
	}

	inline static int32_t get_offset_of_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276() const { return ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276() { return &___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276; }
	inline void set_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_276 = value;
	}

	inline static int32_t get_offset_of_E2C0239EBBF1FD45925FAA7909458304DFD37A84_277() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_277)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_E2C0239EBBF1FD45925FAA7909458304DFD37A84_277() const { return ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_277; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_E2C0239EBBF1FD45925FAA7909458304DFD37A84_277() { return &___E2C0239EBBF1FD45925FAA7909458304DFD37A84_277; }
	inline void set_E2C0239EBBF1FD45925FAA7909458304DFD37A84_277(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___E2C0239EBBF1FD45925FAA7909458304DFD37A84_277 = value;
	}

	inline static int32_t get_offset_of_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278() const { return ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278() { return &___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278; }
	inline void set_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278 = value;
	}

	inline static int32_t get_offset_of_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279() const { return ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279() { return &___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279; }
	inline void set_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279 = value;
	}

	inline static int32_t get_offset_of_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280)); }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  get_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280() const { return ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280; }
	inline __StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9 * get_address_of_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280() { return &___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280; }
	inline void set_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280(__StaticArrayInitTypeSizeU3D28_t7A06B07ACA064A6140D7090BD5C52668C421B8F9  value)
	{
		___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_280 = value;
	}

	inline static int32_t get_offset_of_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281() const { return ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281() { return &___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281; }
	inline void set_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_281 = value;
	}

	inline static int32_t get_offset_of_E5AD60E2BE68FA0155888A7E87B34F03905E7584_282() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_282)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_E5AD60E2BE68FA0155888A7E87B34F03905E7584_282() const { return ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_282; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_E5AD60E2BE68FA0155888A7E87B34F03905E7584_282() { return &___E5AD60E2BE68FA0155888A7E87B34F03905E7584_282; }
	inline void set_E5AD60E2BE68FA0155888A7E87B34F03905E7584_282(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___E5AD60E2BE68FA0155888A7E87B34F03905E7584_282 = value;
	}

	inline static int32_t get_offset_of_E62D0D793D6BCA39079E6441B162E59F33ED2B07_283() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_283)); }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  get_E62D0D793D6BCA39079E6441B162E59F33ED2B07_283() const { return ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_283; }
	inline __StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2 * get_address_of_E62D0D793D6BCA39079E6441B162E59F33ED2B07_283() { return &___E62D0D793D6BCA39079E6441B162E59F33ED2B07_283; }
	inline void set_E62D0D793D6BCA39079E6441B162E59F33ED2B07_283(__StaticArrayInitTypeSizeU3D20_t35715B37710392B44BE5BC9E948EB492A892AFB2  value)
	{
		___E62D0D793D6BCA39079E6441B162E59F33ED2B07_283 = value;
	}

	inline static int32_t get_offset_of_E6C34D5D4381C0E6151B26FB9A314D8A57292366_284() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_284)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_E6C34D5D4381C0E6151B26FB9A314D8A57292366_284() const { return ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_284; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_E6C34D5D4381C0E6151B26FB9A314D8A57292366_284() { return &___E6C34D5D4381C0E6151B26FB9A314D8A57292366_284; }
	inline void set_E6C34D5D4381C0E6151B26FB9A314D8A57292366_284(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___E6C34D5D4381C0E6151B26FB9A314D8A57292366_284 = value;
	}

	inline static int32_t get_offset_of_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285() const { return ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285() { return &___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285; }
	inline void set_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_285 = value;
	}

	inline static int32_t get_offset_of_EB5C89140591CED42C39A8AE698A41F3593C3C62_286() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EB5C89140591CED42C39A8AE698A41F3593C3C62_286)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_EB5C89140591CED42C39A8AE698A41F3593C3C62_286() const { return ___EB5C89140591CED42C39A8AE698A41F3593C3C62_286; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_EB5C89140591CED42C39A8AE698A41F3593C3C62_286() { return &___EB5C89140591CED42C39A8AE698A41F3593C3C62_286; }
	inline void set_EB5C89140591CED42C39A8AE698A41F3593C3C62_286(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___EB5C89140591CED42C39A8AE698A41F3593C3C62_286 = value;
	}

	inline static int32_t get_offset_of_EB774035880443AEFBF8B61E7760DA5273A8A44C_287() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EB774035880443AEFBF8B61E7760DA5273A8A44C_287)); }
	inline __StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD  get_EB774035880443AEFBF8B61E7760DA5273A8A44C_287() const { return ___EB774035880443AEFBF8B61E7760DA5273A8A44C_287; }
	inline __StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD * get_address_of_EB774035880443AEFBF8B61E7760DA5273A8A44C_287() { return &___EB774035880443AEFBF8B61E7760DA5273A8A44C_287; }
	inline void set_EB774035880443AEFBF8B61E7760DA5273A8A44C_287(__StaticArrayInitTypeSizeU3D148_t321C03C227482B538D2889E2752E0C97517747DD  value)
	{
		___EB774035880443AEFBF8B61E7760DA5273A8A44C_287 = value;
	}

	inline static int32_t get_offset_of_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288() const { return ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288() { return &___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288; }
	inline void set_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_288 = value;
	}

	inline static int32_t get_offset_of_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289() const { return ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289() { return &___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289; }
	inline void set_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_289 = value;
	}

	inline static int32_t get_offset_of_EF1366407DE423137F5977D7AF18B0C058147698_290() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EF1366407DE423137F5977D7AF18B0C058147698_290)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_EF1366407DE423137F5977D7AF18B0C058147698_290() const { return ___EF1366407DE423137F5977D7AF18B0C058147698_290; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_EF1366407DE423137F5977D7AF18B0C058147698_290() { return &___EF1366407DE423137F5977D7AF18B0C058147698_290; }
	inline void set_EF1366407DE423137F5977D7AF18B0C058147698_290(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___EF1366407DE423137F5977D7AF18B0C058147698_290 = value;
	}

	inline static int32_t get_offset_of_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291() const { return ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291() { return &___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291; }
	inline void set_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_291 = value;
	}

	inline static int32_t get_offset_of_F09F029091837E12B4E60500BED0749D79F77FC2_292() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F09F029091837E12B4E60500BED0749D79F77FC2_292)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_F09F029091837E12B4E60500BED0749D79F77FC2_292() const { return ___F09F029091837E12B4E60500BED0749D79F77FC2_292; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_F09F029091837E12B4E60500BED0749D79F77FC2_292() { return &___F09F029091837E12B4E60500BED0749D79F77FC2_292; }
	inline void set_F09F029091837E12B4E60500BED0749D79F77FC2_292(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___F09F029091837E12B4E60500BED0749D79F77FC2_292 = value;
	}

	inline static int32_t get_offset_of_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293() const { return ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293() { return &___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293; }
	inline void set_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_293 = value;
	}

	inline static int32_t get_offset_of_F0E174E706EA1C2F8D181561F84976C942B1DE4B_294() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_294)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_F0E174E706EA1C2F8D181561F84976C942B1DE4B_294() const { return ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_294; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_F0E174E706EA1C2F8D181561F84976C942B1DE4B_294() { return &___F0E174E706EA1C2F8D181561F84976C942B1DE4B_294; }
	inline void set_F0E174E706EA1C2F8D181561F84976C942B1DE4B_294(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___F0E174E706EA1C2F8D181561F84976C942B1DE4B_294 = value;
	}

	inline static int32_t get_offset_of_F100C709518544CED81B98D3F1C862F094DAF46A_295() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F100C709518544CED81B98D3F1C862F094DAF46A_295)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_F100C709518544CED81B98D3F1C862F094DAF46A_295() const { return ___F100C709518544CED81B98D3F1C862F094DAF46A_295; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_F100C709518544CED81B98D3F1C862F094DAF46A_295() { return &___F100C709518544CED81B98D3F1C862F094DAF46A_295; }
	inline void set_F100C709518544CED81B98D3F1C862F094DAF46A_295(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___F100C709518544CED81B98D3F1C862F094DAF46A_295 = value;
	}

	inline static int32_t get_offset_of_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296() const { return ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296() { return &___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296; }
	inline void set_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_296 = value;
	}

	inline static int32_t get_offset_of_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297() const { return ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297() { return &___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297; }
	inline void set_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_297 = value;
	}

	inline static int32_t get_offset_of_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298() const { return ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298() { return &___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298; }
	inline void set_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_298 = value;
	}

	inline static int32_t get_offset_of_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299() const { return ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299() { return &___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299; }
	inline void set_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_299 = value;
	}

	inline static int32_t get_offset_of_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300)); }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  get_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300() const { return ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300; }
	inline __StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F * get_address_of_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300() { return &___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300; }
	inline void set_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300(__StaticArrayInitTypeSizeU3D30_tB9F439E8DB3D01FADEB7A2261AA799DE8648806F  value)
	{
		___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_300 = value;
	}

	inline static int32_t get_offset_of_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301() const { return ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301() { return &___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301; }
	inline void set_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301 = value;
	}

	inline static int32_t get_offset_of_F5183037A4B449D12439C624D76065B310A6614A_302() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F5183037A4B449D12439C624D76065B310A6614A_302)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_F5183037A4B449D12439C624D76065B310A6614A_302() const { return ___F5183037A4B449D12439C624D76065B310A6614A_302; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_F5183037A4B449D12439C624D76065B310A6614A_302() { return &___F5183037A4B449D12439C624D76065B310A6614A_302; }
	inline void set_F5183037A4B449D12439C624D76065B310A6614A_302(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___F5183037A4B449D12439C624D76065B310A6614A_302 = value;
	}

	inline static int32_t get_offset_of_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303() const { return ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303() { return &___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303; }
	inline void set_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_303 = value;
	}

	inline static int32_t get_offset_of_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304() const { return ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304() { return &___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304; }
	inline void set_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_304 = value;
	}

	inline static int32_t get_offset_of_F865498CC0010E6ACE166D01EEA92F0F3C87743D_305() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_305)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_F865498CC0010E6ACE166D01EEA92F0F3C87743D_305() const { return ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_305; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_F865498CC0010E6ACE166D01EEA92F0F3C87743D_305() { return &___F865498CC0010E6ACE166D01EEA92F0F3C87743D_305; }
	inline void set_F865498CC0010E6ACE166D01EEA92F0F3C87743D_305(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___F865498CC0010E6ACE166D01EEA92F0F3C87743D_305 = value;
	}

	inline static int32_t get_offset_of_F86A89C59555BE5241482CB0FD35624F05D127CD_306() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F86A89C59555BE5241482CB0FD35624F05D127CD_306)); }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  get_F86A89C59555BE5241482CB0FD35624F05D127CD_306() const { return ___F86A89C59555BE5241482CB0FD35624F05D127CD_306; }
	inline __StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E * get_address_of_F86A89C59555BE5241482CB0FD35624F05D127CD_306() { return &___F86A89C59555BE5241482CB0FD35624F05D127CD_306; }
	inline void set_F86A89C59555BE5241482CB0FD35624F05D127CD_306(__StaticArrayInitTypeSizeU3D32_t01CBF036707206763E17B413A7C0698F38F3D66E  value)
	{
		___F86A89C59555BE5241482CB0FD35624F05D127CD_306 = value;
	}

	inline static int32_t get_offset_of_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307() const { return ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307() { return &___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307; }
	inline void set_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_307 = value;
	}

	inline static int32_t get_offset_of_FA200C2489E9868CC920129D1E2D694172C2BA49_308() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FA200C2489E9868CC920129D1E2D694172C2BA49_308)); }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  get_FA200C2489E9868CC920129D1E2D694172C2BA49_308() const { return ___FA200C2489E9868CC920129D1E2D694172C2BA49_308; }
	inline __StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A * get_address_of_FA200C2489E9868CC920129D1E2D694172C2BA49_308() { return &___FA200C2489E9868CC920129D1E2D694172C2BA49_308; }
	inline void set_FA200C2489E9868CC920129D1E2D694172C2BA49_308(__StaticArrayInitTypeSizeU3D120_tB262F170F9E5BDC3B397FAC80239042E5DA2DC1A  value)
	{
		___FA200C2489E9868CC920129D1E2D694172C2BA49_308 = value;
	}

	inline static int32_t get_offset_of_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309() const { return ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309() { return &___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309; }
	inline void set_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_309 = value;
	}

	inline static int32_t get_offset_of_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310)); }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  get_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310() const { return ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310; }
	inline __StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0 * get_address_of_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310() { return &___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310; }
	inline void set_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310(__StaticArrayInitTypeSizeU3D52_tDEEFC6D36AC13CA25C22FA97FBF799025C9ACAC0  value)
	{
		___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_310 = value;
	}

	inline static int32_t get_offset_of_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311)); }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  get_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311() const { return ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311; }
	inline __StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6 * get_address_of_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311() { return &___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311; }
	inline void set_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311(__StaticArrayInitTypeSizeU3D80_t3779A925679E206205155BA84C6A223A79580AF6  value)
	{
		___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_311 = value;
	}

	inline static int32_t get_offset_of_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312)); }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  get_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312() const { return ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312; }
	inline __StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4 * get_address_of_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312() { return &___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312; }
	inline void set_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312(__StaticArrayInitTypeSizeU3D12_tA65C72302006309B0419BB84C225CBC2F0ACCCC4  value)
	{
		___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312 = value;
	}

	inline static int32_t get_offset_of_FCDF24D1C633613549296EFD3DC9250D840CA996_313() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FCDF24D1C633613549296EFD3DC9250D840CA996_313)); }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  get_FCDF24D1C633613549296EFD3DC9250D840CA996_313() const { return ___FCDF24D1C633613549296EFD3DC9250D840CA996_313; }
	inline __StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA * get_address_of_FCDF24D1C633613549296EFD3DC9250D840CA996_313() { return &___FCDF24D1C633613549296EFD3DC9250D840CA996_313; }
	inline void set_FCDF24D1C633613549296EFD3DC9250D840CA996_313(__StaticArrayInitTypeSizeU3D26_t02E1A8523D2579D5C3211F557876184BF7433CCA  value)
	{
		___FCDF24D1C633613549296EFD3DC9250D840CA996_313 = value;
	}

	inline static int32_t get_offset_of_FCF9CE737F38E152A767F1291260BF9E15F80078_314() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FCF9CE737F38E152A767F1291260BF9E15F80078_314)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_FCF9CE737F38E152A767F1291260BF9E15F80078_314() const { return ___FCF9CE737F38E152A767F1291260BF9E15F80078_314; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_FCF9CE737F38E152A767F1291260BF9E15F80078_314() { return &___FCF9CE737F38E152A767F1291260BF9E15F80078_314; }
	inline void set_FCF9CE737F38E152A767F1291260BF9E15F80078_314(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___FCF9CE737F38E152A767F1291260BF9E15F80078_314 = value;
	}

	inline static int32_t get_offset_of_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315)); }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  get_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315() const { return ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315; }
	inline __StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0 * get_address_of_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315() { return &___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315; }
	inline void set_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315(__StaticArrayInitTypeSizeU3D16_t44836579AD799EE4206153C676D752AFA6A7D9F0  value)
	{
		___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315 = value;
	}

	inline static int32_t get_offset_of_FE68796AD0908776C9F23F326A4181C1F53CF4DF_316() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_StaticFields, ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_316)); }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  get_FE68796AD0908776C9F23F326A4181C1F53CF4DF_316() const { return ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_316; }
	inline __StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D * get_address_of_FE68796AD0908776C9F23F326A4181C1F53CF4DF_316() { return &___FE68796AD0908776C9F23F326A4181C1F53CF4DF_316; }
	inline void set_FE68796AD0908776C9F23F326A4181C1F53CF4DF_316(__StaticArrayInitTypeSizeU3D24_t9B5721E62F5DB04D747FFE0082BA9F66017B186D  value)
	{
		___FE68796AD0908776C9F23F326A4181C1F53CF4DF_316 = value;
	}
};


// ZXing.Color32LuminanceSource
struct  Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB  : public BaseLuminanceSource_tFEDADACD44E500F99D11BFD0F3EF60D2CF33E1B5
{
public:

public:
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// ZXing.Common.HybridBinarizer
struct  HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016  : public GlobalHistogramBinarizer_t047306539F4D80014339015D0303CD8CE419A043
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::matrix
	BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___matrix_4;

public:
	inline static int32_t get_offset_of_matrix_4() { return static_cast<int32_t>(offsetof(HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016, ___matrix_4)); }
	inline BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * get_matrix_4() const { return ___matrix_4; }
	inline BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB ** get_address_of_matrix_4() { return &___matrix_4; }
	inline void set_matrix_4(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * value)
	{
		___matrix_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___matrix_4), (void*)value);
	}
};


// ZXing.RGBLuminanceSource
struct  RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493  : public BaseLuminanceSource_tFEDADACD44E500F99D11BFD0F3EF60D2CF33E1B5
{
public:

public:
};


// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State
struct  State_t11F12043E945FC57E677AE557E1D51920930BADA 
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t11F12043E945FC57E677AE557E1D51920930BADA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode
struct  Mode_t53C4195CC795D1FEB3922E60DEF268B46E8A1D74 
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t53C4195CC795D1FEB3922E60DEF268B46E8A1D74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.PDF417.Internal.DecodedBitStreamParser/Mode
struct  Mode_t35BEDCAB68A4625FFB254F265AC6E1478DDCD156 
{
public:
	// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t35BEDCAB68A4625FFB254F265AC6E1478DDCD156, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.Aztec.Internal.Decoder/Table
struct  Table_tF7DB7A2C946CE3933373ED91CB2F92B350661403 
{
public:
	// System.Int32 ZXing.Aztec.Internal.Decoder/Table::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Table_tF7DB7A2C946CE3933373ED91CB2F92B350661403, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.QrCode.Internal.Mode/Names
struct  Names_tD6A1E4D3B585F119A53536DFBD12EBE99BBA7546 
{
public:
	// System.Int32 ZXing.QrCode.Internal.Mode/Names::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Names_tD6A1E4D3B585F119A53536DFBD12EBE99BBA7546, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.RGBLuminanceSource/BitmapFormat
struct  BitmapFormat_t18ED05AAC0CA50AF68B6B57AF87A6A249ECA3737 
{
public:
	// System.Int32 ZXing.RGBLuminanceSource/BitmapFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BitmapFormat_t18ED05AAC0CA50AF68B6B57AF87A6A249ECA3737, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * m_Items[1];

public:
	inline ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * m_Items[1];

public:
	inline ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * m_Items[1];

public:
	inline Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * m_Items[1];

public:
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  m_Items[1];

public:
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* m_Items[1];

public:
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * m_Items[1];

public:
	inline ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_ECCodewordsPerBlock()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA_inline (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method);
// ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::getECBlocks()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3_inline (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version/ECB::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567_inline (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version/ECB::get_DataCodewords()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F_inline (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorCorrectionLevel_ordinal_mACCE779D514AF2084CA531514F85028357ABE219_inline (ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * __this, const RuntimeMethod* method);
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572 (int32_t ___versionNumber0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m789B4E75608A673F2CF5DDFC2E67DA20AF440A34 (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.FormatInformation::numBitsDiffering(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FormatInformation_numBitsDiffering_mD6BF158CF9F2D2AE0A41801B858CBECFBC2FBC17 (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Version_get_DimensionForVersion_m9589220D4223CADC2D0E87E0949E3B181128B705 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method);
// System.Void ZXing.Common.BitMatrix::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BitMatrix__ctor_mB63ED20D6DC99156079AA7C2F2E1209817F387B2 (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, int32_t ___dimension0, const RuntimeMethod* method);
// System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614 (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, int32_t ___left0, int32_t ___top1, int32_t ___width2, int32_t ___height3, const RuntimeMethod* method);
// System.String System.Convert::ToString(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Convert_ToString_m591519A05955A00954A48E0EA3F5CB9921C13969 (int32_t ___value0, const RuntimeMethod* method);
// System.Void ZXing.QrCode.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784 (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, int32_t ___count0, int32_t ___dataCodewords1, const RuntimeMethod* method);
// System.Void ZXing.QrCode.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version/ECB[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017 (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, int32_t ___ecCodewordsPerBlock0, ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ___ecBlocks1, const RuntimeMethod* method);
// System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version/ECBlocks[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, int32_t ___versionNumber0, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___alignmentPatternCenters1, ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* ___ecBlocks2, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F (RuntimeArray * ___array0, RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  ___fldHandle1, const RuntimeMethod* method);
// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* Version_buildVersions_m4FF9D42DFED8563CAE3AF70AB90A30046EC1026E (const RuntimeMethod* method);
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteRectangleDetector__ctor_m65D6591012DC7B1053F586C40156AE32937F67DE (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, const RuntimeMethod* method);
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, int32_t ___initSize1, int32_t ___x2, int32_t ___y3, const RuntimeMethod* method);
// System.Int32 ZXing.Common.BitMatrix::get_Width()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8_inline (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, const RuntimeMethod* method);
// System.Int32 ZXing.Common.BitMatrix::get_Height()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433_inline (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, const RuntimeMethod* method);
// System.Boolean ZXing.Common.Detector.WhiteRectangleDetector::containsBlackPoint(System.Int32,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, int32_t ___a0, int32_t ___b1, int32_t ___fixed2, bool ___horizontal3, const RuntimeMethod* method);
// ZXing.ResultPoint ZXing.Common.Detector.WhiteRectangleDetector::getBlackPointOnSegment(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, float ___aX0, float ___aY1, float ___bX2, float ___bY3, const RuntimeMethod* method);
// ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::centerEdges(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* WhiteRectangleDetector_centerEdges_m627FE076E322023CD2E80416A1D2AE41CEBAA6D3 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___y0, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___z1, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___x2, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___t3, const RuntimeMethod* method);
// System.Single ZXing.Common.Detector.MathUtils::distance(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MathUtils_distance_mFD733773EEBEB9B2DE497DB56F877DE8241841EF (float ___aX0, float ___aY1, float ___bX2, float ___bY3, const RuntimeMethod* method);
// System.Int32 ZXing.Common.Detector.MathUtils::round(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60 (float ___d0, const RuntimeMethod* method);
// System.Boolean ZXing.Common.BitMatrix::get_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475 (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Void ZXing.ResultPoint::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765 (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void ZXing.BarcodeReader/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC582116316DB1F8235AB27A5BE58C6B17EA9F24D (U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * __this, const RuntimeMethod* method);
// System.Void ZXing.Color32LuminanceSource::.ctor(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677 (Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB * __this, Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___color32s0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method);
// System.Void ZXing.BarcodeReaderGeneric/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0EAA14E42607DB22B6B1DBA9E1EEBDD21948DB20 (U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * __this, const RuntimeMethod* method);
// System.Void ZXing.Common.HybridBinarizer::.ctor(ZXing.LuminanceSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HybridBinarizer__ctor_mAFDF1DB99F02C26A219AFF9F31A442BBD6C1C1B2 (HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016 * __this, LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * ___source0, const RuntimeMethod* method);
// System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RGBLuminanceSource__ctor_m3521EC26C240CFA8FA8AF04A3AC09B97ABDB6CB3 (RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___rgbRawBytes0, int32_t ___width1, int32_t ___height2, int32_t ___bitmapFormat3, const RuntimeMethod* method);
// System.Void ZXing.QrCode.Internal.DataMask/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8EBA2F4F5936544E00D4494EBF428E326B8614DC (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, const RuntimeMethod* method);
// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_X()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_Y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method);
// System.Void ZXing.Aztec.Internal.Detector/Point::set_X(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void ZXing.Aztec.Internal.Detector/Point::set_Y(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306 (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args0, const RuntimeMethod* method);
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_From(ZXing.ResultPoint)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method);
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_To(ZXing.ResultPoint)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method);
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_Transitions(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, int32_t ___value0, const RuntimeMethod* method);
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_From()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method);
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_To()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_Transitions()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.FinderPattern::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline (FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * __this, const RuntimeMethod* method);
// System.Single ZXing.QrCode.Internal.FinderPattern::get_EstimatedModuleSize()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline (FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version/ECBlocks[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, int32_t ___versionNumber0, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___alignmentPatternCenters1, ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* ___ecBlocks2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* V_2 = NULL;
	int32_t V_3 = 0;
	ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * V_4 = NULL;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___versionNumber0;
		__this->set_versionNumber_2(L_0);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = ___alignmentPatternCenters1;
		__this->set_alignmentPatternCenters_3(L_1);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_2 = ___ecBlocks2;
		__this->set_ecBlocks_4(L_2);
		V_0 = 0;
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_3 = ___ecBlocks2;
		NullCheck(L_3);
		int32_t L_4 = 0;
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		int32_t L_6;
		L_6 = ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA_inline(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_7 = ___ecBlocks2;
		NullCheck(L_7);
		int32_t L_8 = 0;
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_10;
		L_10 = ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3_inline(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		V_3 = 0;
		goto IL_0050;
	}

IL_0033:
	{
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_11 = V_2;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_4 = L_14;
		int32_t L_15 = V_0;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_16 = V_4;
		NullCheck(L_16);
		int32_t L_17;
		L_17 = ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567_inline(L_16, /*hidden argument*/NULL);
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_18 = V_4;
		NullCheck(L_18);
		int32_t L_19;
		L_19 = ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F_inline(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_17, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_20))))));
		int32_t L_21 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_22 = V_3;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_23 = V_2;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_24 = V_0;
		__this->set_totalCodewords_5(L_24);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version::get_VersionNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Version_get_VersionNumber_mFF023A4E25FDB34A4001A0355A2FCDBA9CBB8D89 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_versionNumber_2();
		return L_0;
	}
}
// System.Int32[] ZXing.QrCode.Internal.Version::get_AlignmentPatternCenters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* Version_get_AlignmentPatternCenters_mADD8DB8B178CE125E1AA4F2EE89146BA5E460FBF (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = __this->get_alignmentPatternCenters_3();
		return L_0;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version::get_TotalCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Version_get_TotalCodewords_m434BD18856962BE8E10F85144B29D13779377269 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_totalCodewords_5();
		return L_0;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Version_get_DimensionForVersion_m9589220D4223CADC2D0E87E0949E3B181128B705 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_versionNumber_2();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)17), (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)4, (int32_t)L_0))));
	}
}
// ZXing.QrCode.Internal.Version/ECBlocks ZXing.QrCode.Internal.Version::getECBlocksForLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * Version_getECBlocksForLevel_m32EDCF33F6787A0C075284CAA1ED12FAF3321880 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * ___ecLevel0, const RuntimeMethod* method)
{
	{
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_0 = __this->get_ecBlocks_4();
		ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * L_1 = ___ecLevel0;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = ErrorCorrectionLevel_ordinal_mACCE779D514AF2084CA531514F85028357ABE219_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = L_2;
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		return L_4;
	}
}
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getProvisionalVersionForDimension(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * Version_getProvisionalVersionForDimension_mB8DFB555C74A1AE3CCDA5BE3E5C88650A6396FCD (int32_t ___dimension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = ___dimension0;
		if ((((int32_t)((int32_t)((int32_t)L_0%(int32_t)4))) == ((int32_t)1)))
		{
			goto IL_0008;
		}
	}
	{
		return (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)NULL;
	}

IL_0008:
	{
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		int32_t L_1 = ___dimension0;
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_2;
		L_2 = Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572(((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)17)))>>(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0017;
		}
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.ArgumentException)
		V_0 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)NULL;
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_001c;
	} // end catch (depth: 1)

IL_001c:
	{
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_3 = V_0;
		return L_3;
	}
}
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572 (int32_t ___versionNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___versionNumber0;
		if ((((int32_t)L_0) < ((int32_t)1)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___versionNumber0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)40))))
		{
			goto IL_000f;
		}
	}

IL_0009:
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_2 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m789B4E75608A673F2CF5DDFC2E67DA20AF440A34(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572_RuntimeMethod_var)));
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_3 = ((Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields*)il2cpp_codegen_static_fields_for(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var))->get_VERSIONS_1();
		int32_t L_4 = ___versionNumber0;
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1));
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::decodeVersionInformation(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * Version_decodeVersionInformation_mB7EC7E529C9F39F58632DB1A97AE3404DD1CA37D (int32_t ___versionBits0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormatInformation_tA8C2953407BB1A490AD4DE2CA6E0A4CC0634B6E5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = ((int32_t)2147483647LL);
		V_1 = 0;
		V_2 = 0;
		goto IL_003a;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = ((Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields*)il2cpp_codegen_static_fields_for(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var))->get_VERSION_DECODE_INFO_0();
		int32_t L_1 = V_2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_3 = L_3;
		int32_t L_4 = V_3;
		int32_t L_5 = ___versionBits0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_7;
		L_7 = Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)7)), /*hidden argument*/NULL);
		return L_7;
	}

IL_0021:
	{
		int32_t L_8 = ___versionBits0;
		int32_t L_9 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(FormatInformation_tA8C2953407BB1A490AD4DE2CA6E0A4CC0634B6E5_il2cpp_TypeInfo_var);
		int32_t L_10;
		L_10 = FormatInformation_numBitsDiffering_mD6BF158CF9F2D2AE0A41801B858CBECFBC2FBC17(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_4;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_13 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)7));
		int32_t L_14 = V_4;
		V_0 = L_14;
	}

IL_0036:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_17 = ((Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields*)il2cpp_codegen_static_fields_for(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var))->get_VERSION_DECODE_INFO_0();
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length))))))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) > ((int32_t)3)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_20;
		L_20 = Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572(L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_004f:
	{
		return (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)NULL;
	}
}
// ZXing.Common.BitMatrix ZXing.QrCode.Internal.Version::buildFunctionPattern()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * Version_buildFunctionPattern_mD94BB465EDBD252F1A25B1B3EAD67C2468136684 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0;
		L_0 = Version_get_DimensionForVersion_m9589220D4223CADC2D0E87E0949E3B181128B705(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_2 = (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB *)il2cpp_codegen_object_new(BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB_il2cpp_TypeInfo_var);
		BitMatrix__ctor_mB63ED20D6DC99156079AA7C2F2E1209817F387B2(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_3 = V_1;
		NullCheck(L_3);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_3, 0, 0, ((int32_t)9), ((int32_t)9), /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_4 = V_1;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_4, ((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)8)), 0, 8, ((int32_t)9), /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_6, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)8)), ((int32_t)9), 8, /*hidden argument*/NULL);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_8 = __this->get_alignmentPatternCenters_3();
		NullCheck(L_8);
		V_2 = ((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)));
		V_3 = 0;
		goto IL_008e;
	}

IL_0041:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_9 = __this->get_alignmentPatternCenters_3();
		int32_t L_10 = V_3;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)2));
		V_5 = 0;
		goto IL_0085;
	}

IL_0052:
	{
		int32_t L_13 = V_3;
		if (L_13)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_14 = V_5;
		if (!L_14)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_15 = V_5;
		int32_t L_16 = V_2;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)))))
		{
			goto IL_007f;
		}
	}

IL_0060:
	{
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1))))))
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_19 = V_5;
		if (!L_19)
		{
			goto IL_007f;
		}
	}

IL_006a:
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_20 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_21 = __this->get_alignmentPatternCenters_3();
		int32_t L_22 = V_5;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		int32_t L_25 = V_4;
		NullCheck(L_20);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_20, ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)2)), L_25, 5, 5, /*hidden argument*/NULL);
	}

IL_007f:
	{
		int32_t L_26 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_0085:
	{
		int32_t L_27 = V_5;
		int32_t L_28 = V_2;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_008e:
	{
		int32_t L_30 = V_3;
		int32_t L_31 = V_2;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0041;
		}
	}
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_32 = V_1;
		int32_t L_33 = V_0;
		NullCheck(L_32);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_32, 6, ((int32_t)9), 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)((int32_t)17))), /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_34 = V_1;
		int32_t L_35 = V_0;
		NullCheck(L_34);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_34, ((int32_t)9), 6, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)((int32_t)17))), 1, /*hidden argument*/NULL);
		int32_t L_36 = __this->get_versionNumber_2();
		if ((((int32_t)L_36) <= ((int32_t)6)))
		{
			goto IL_00d1;
		}
	}
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_37 = V_1;
		int32_t L_38 = V_0;
		NullCheck(L_37);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_37, ((int32_t)il2cpp_codegen_subtract((int32_t)L_38, (int32_t)((int32_t)11))), 0, 3, 6, /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_39 = V_1;
		int32_t L_40 = V_0;
		NullCheck(L_39);
		BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614(L_39, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_40, (int32_t)((int32_t)11))), 6, 3, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_41 = V_1;
		return L_41;
	}
}
// System.String ZXing.QrCode.Internal.Version::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Version_ToString_m9AEF9ED09794C93CE75C2487757FC2786B8D6A23 (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_versionNumber_2();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Convert_ToString_m591519A05955A00954A48E0EA3F5CB9921C13969(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* Version_buildVersions_m4FF9D42DFED8563CAE3AF70AB90A30046EC1026E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____00E14C77230AEF0974CFF3930481157AABDA07B4_0_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____0222E151247DAE31A8D697EAA14F43F718BD1F1C_2_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____11D4D9FC526D047E01891BEE9949EA42408A800E_17_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____188E7A480B871C554F3F805D3E1184673960EED5_21_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____20D740A7E661D50B06DC32EF6577367D264F6235_34_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____259B7EF54A535F34001B95480B4BB11245EA41B5_42_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____28F991026781E1BC119F02498D75FB3A84F4ED37_44_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____29AB17251F5A730B539AB351FFCB45B9F2FA8027_46_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____56F75F925774608B6BF88F69D6124CB44DDF42E1_98_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6882914B984B24C4880E3BEA646DC549B3C34ABC_121_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6AA66657BB3292572C8F56AB3D015A7990C43606_126_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6DE89499E8F85867289E1690E176E1DE58EE6DD3_131_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8D1369A0D8832C941BD6D09849525D65D807A1DD_180_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9B3E34C734351ED3DE1984119EAB9572C48FA15E_197_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9BBAB86ACB95EF8C028708733458CDBAC4715197_198_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9E548860392B66258417232F41EF03D48EBDD686_201_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____AECEE3733C380177F0A39ACF3AF99BFE360C156F_216_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C0935C173C7A144FE24DA09584F4892153D01F3C_236_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_0 = (VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F*)(VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F*)SZArrayNew(VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F_il2cpp_TypeInfo_var, (uint32_t)((int32_t)40));
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1 = L_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)0);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_3 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_4 = L_3;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_5 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_6 = L_5;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_7 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_7, 1, ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_7);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_8 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_8, 7, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_8);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_8);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_9 = L_4;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_10 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_11 = L_10;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_12 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_12, 1, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_12);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_13 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_13, ((int32_t)10), L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_13);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_14 = L_9;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_15 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_16 = L_15;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_17 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_17, 1, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_17);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_18 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_18, ((int32_t)13), L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_18);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_19 = L_14;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_20 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_21 = L_20;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_22 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_22, 1, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_22);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_23 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_23, ((int32_t)17), L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_23);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_24 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_24, 1, L_2, L_19, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_24);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_24);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_25 = L_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_26 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = L_26;
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_28 = L_27;
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)18));
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_29 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_30 = L_29;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_31 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_32 = L_31;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_33 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_33, 1, ((int32_t)34), /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_33);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_34 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_34, ((int32_t)10), L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_34);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_34);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_35 = L_30;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_36 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_37 = L_36;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_38 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_38, 1, ((int32_t)28), /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_38);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_39 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_39, ((int32_t)16), L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_39);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_39);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_40 = L_35;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_41 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_42 = L_41;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_43 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_43, 1, ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_43);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_44 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_44, ((int32_t)22), L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_44);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_44);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_45 = L_40;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_46 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_47 = L_46;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_48 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_48, 1, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_48);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_49 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_49, ((int32_t)28), L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_49);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_49);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_50 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_50, 2, L_28, L_45, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_50);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_50);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_51 = L_25;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_52 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_53 = L_52;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_54 = L_53;
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)22));
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_55 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_56 = L_55;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_57 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_58 = L_57;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_59 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_59, 1, ((int32_t)55), /*hidden argument*/NULL);
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, L_59);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_59);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_60 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_60, ((int32_t)15), L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_60);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_61 = L_56;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_62 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_63 = L_62;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_64 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_64, 1, ((int32_t)44), /*hidden argument*/NULL);
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_64);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_64);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_65 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_65, ((int32_t)26), L_63, /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_65);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_66 = L_61;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_67 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_68 = L_67;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_69 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_69, 2, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_68);
		ArrayElementTypeCheck (L_68, L_69);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_69);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_70 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_70, ((int32_t)18), L_68, /*hidden argument*/NULL);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_70);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_70);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_71 = L_66;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_72 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_73 = L_72;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_74 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_74, 2, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_74);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_75 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_75, ((int32_t)22), L_73, /*hidden argument*/NULL);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_75);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_75);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_76 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_76, 3, L_54, L_71, /*hidden argument*/NULL);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_76);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(2), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_76);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_77 = L_51;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_78 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_79 = L_78;
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_80 = L_79;
		NullCheck(L_80);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)26));
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_81 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_82 = L_81;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_83 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_84 = L_83;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_85 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_85, 1, ((int32_t)80), /*hidden argument*/NULL);
		NullCheck(L_84);
		ArrayElementTypeCheck (L_84, L_85);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_85);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_86 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_86, ((int32_t)20), L_84, /*hidden argument*/NULL);
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, L_86);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_86);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_87 = L_82;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_88 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_89 = L_88;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_90 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_90, 2, ((int32_t)32), /*hidden argument*/NULL);
		NullCheck(L_89);
		ArrayElementTypeCheck (L_89, L_90);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_90);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_91 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_91, ((int32_t)18), L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		ArrayElementTypeCheck (L_87, L_91);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_91);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_92 = L_87;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_93 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_94 = L_93;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_95 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_95, 2, ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_94);
		ArrayElementTypeCheck (L_94, L_95);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_95);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_96 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_96, ((int32_t)26), L_94, /*hidden argument*/NULL);
		NullCheck(L_92);
		ArrayElementTypeCheck (L_92, L_96);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_96);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_97 = L_92;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_98 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_99 = L_98;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_100 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_100, 4, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_99);
		ArrayElementTypeCheck (L_99, L_100);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_100);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_101 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_101, ((int32_t)16), L_99, /*hidden argument*/NULL);
		NullCheck(L_97);
		ArrayElementTypeCheck (L_97, L_101);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_101);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_102 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_102, 4, L_80, L_97, /*hidden argument*/NULL);
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_102);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(3), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_102);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_103 = L_77;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_104 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_105 = L_104;
		NullCheck(L_105);
		(L_105)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_106 = L_105;
		NullCheck(L_106);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)30));
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_107 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_108 = L_107;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_109 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_110 = L_109;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_111 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_111, 1, ((int32_t)108), /*hidden argument*/NULL);
		NullCheck(L_110);
		ArrayElementTypeCheck (L_110, L_111);
		(L_110)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_111);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_112 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_112, ((int32_t)26), L_110, /*hidden argument*/NULL);
		NullCheck(L_108);
		ArrayElementTypeCheck (L_108, L_112);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_112);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_113 = L_108;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_114 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_115 = L_114;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_116 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_116, 2, ((int32_t)43), /*hidden argument*/NULL);
		NullCheck(L_115);
		ArrayElementTypeCheck (L_115, L_116);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_116);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_117 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_117, ((int32_t)24), L_115, /*hidden argument*/NULL);
		NullCheck(L_113);
		ArrayElementTypeCheck (L_113, L_117);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_117);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_118 = L_113;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_119 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_120 = L_119;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_121 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_121, 2, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_120);
		ArrayElementTypeCheck (L_120, L_121);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_121);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_122 = L_120;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_123 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_123, 2, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_122);
		ArrayElementTypeCheck (L_122, L_123);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_123);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_124 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_124, ((int32_t)18), L_122, /*hidden argument*/NULL);
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, L_124);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_124);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_125 = L_118;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_126 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_127 = L_126;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_128 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_128, 2, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, L_128);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_128);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_129 = L_127;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_130 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_130, 2, ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_129);
		ArrayElementTypeCheck (L_129, L_130);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_130);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_131 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_131, ((int32_t)22), L_129, /*hidden argument*/NULL);
		NullCheck(L_125);
		ArrayElementTypeCheck (L_125, L_131);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_131);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_132 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_132, 5, L_106, L_125, /*hidden argument*/NULL);
		NullCheck(L_103);
		ArrayElementTypeCheck (L_103, L_132);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(4), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_132);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_133 = L_103;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_134 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_135 = L_134;
		NullCheck(L_135);
		(L_135)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_136 = L_135;
		NullCheck(L_136);
		(L_136)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)34));
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_137 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_138 = L_137;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_139 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_140 = L_139;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_141 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_141, 2, ((int32_t)68), /*hidden argument*/NULL);
		NullCheck(L_140);
		ArrayElementTypeCheck (L_140, L_141);
		(L_140)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_141);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_142 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_142, ((int32_t)18), L_140, /*hidden argument*/NULL);
		NullCheck(L_138);
		ArrayElementTypeCheck (L_138, L_142);
		(L_138)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_142);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_143 = L_138;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_144 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_145 = L_144;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_146 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_146, 4, ((int32_t)27), /*hidden argument*/NULL);
		NullCheck(L_145);
		ArrayElementTypeCheck (L_145, L_146);
		(L_145)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_146);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_147 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_147, ((int32_t)16), L_145, /*hidden argument*/NULL);
		NullCheck(L_143);
		ArrayElementTypeCheck (L_143, L_147);
		(L_143)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_147);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_148 = L_143;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_149 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_150 = L_149;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_151 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_151, 4, ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_150);
		ArrayElementTypeCheck (L_150, L_151);
		(L_150)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_151);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_152 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_152, ((int32_t)24), L_150, /*hidden argument*/NULL);
		NullCheck(L_148);
		ArrayElementTypeCheck (L_148, L_152);
		(L_148)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_152);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_153 = L_148;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_154 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_155 = L_154;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_156 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_156, 4, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_155);
		ArrayElementTypeCheck (L_155, L_156);
		(L_155)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_156);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_157 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_157, ((int32_t)28), L_155, /*hidden argument*/NULL);
		NullCheck(L_153);
		ArrayElementTypeCheck (L_153, L_157);
		(L_153)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_157);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_158 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_158, 6, L_136, L_153, /*hidden argument*/NULL);
		NullCheck(L_133);
		ArrayElementTypeCheck (L_133, L_158);
		(L_133)->SetAt(static_cast<il2cpp_array_size_t>(5), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_158);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_159 = L_133;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_160 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_161 = L_160;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_162 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____F44B79D8208EDC17F9C96AA7DEA4983114BF698D_301_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_161, L_162, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_163 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_164 = L_163;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_165 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_166 = L_165;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_167 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_167, 2, ((int32_t)78), /*hidden argument*/NULL);
		NullCheck(L_166);
		ArrayElementTypeCheck (L_166, L_167);
		(L_166)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_167);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_168 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_168, ((int32_t)20), L_166, /*hidden argument*/NULL);
		NullCheck(L_164);
		ArrayElementTypeCheck (L_164, L_168);
		(L_164)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_168);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_169 = L_164;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_170 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_171 = L_170;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_172 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_172, 4, ((int32_t)31), /*hidden argument*/NULL);
		NullCheck(L_171);
		ArrayElementTypeCheck (L_171, L_172);
		(L_171)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_172);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_173 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_173, ((int32_t)18), L_171, /*hidden argument*/NULL);
		NullCheck(L_169);
		ArrayElementTypeCheck (L_169, L_173);
		(L_169)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_173);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_174 = L_169;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_175 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_176 = L_175;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_177 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_177, 2, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_176);
		ArrayElementTypeCheck (L_176, L_177);
		(L_176)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_177);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_178 = L_176;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_179 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_179, 4, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_178);
		ArrayElementTypeCheck (L_178, L_179);
		(L_178)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_179);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_180 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_180, ((int32_t)18), L_178, /*hidden argument*/NULL);
		NullCheck(L_174);
		ArrayElementTypeCheck (L_174, L_180);
		(L_174)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_180);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_181 = L_174;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_182 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_183 = L_182;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_184 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_184, 4, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_183);
		ArrayElementTypeCheck (L_183, L_184);
		(L_183)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_184);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_185 = L_183;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_186 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_186, 1, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_185);
		ArrayElementTypeCheck (L_185, L_186);
		(L_185)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_186);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_187 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_187, ((int32_t)26), L_185, /*hidden argument*/NULL);
		NullCheck(L_181);
		ArrayElementTypeCheck (L_181, L_187);
		(L_181)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_187);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_188 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_188, 7, L_161, L_181, /*hidden argument*/NULL);
		NullCheck(L_159);
		ArrayElementTypeCheck (L_159, L_188);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(6), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_188);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_189 = L_159;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_190 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_191 = L_190;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_192 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____20D740A7E661D50B06DC32EF6577367D264F6235_34_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_191, L_192, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_193 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_194 = L_193;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_195 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_196 = L_195;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_197 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_197, 2, ((int32_t)97), /*hidden argument*/NULL);
		NullCheck(L_196);
		ArrayElementTypeCheck (L_196, L_197);
		(L_196)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_197);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_198 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_198, ((int32_t)24), L_196, /*hidden argument*/NULL);
		NullCheck(L_194);
		ArrayElementTypeCheck (L_194, L_198);
		(L_194)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_198);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_199 = L_194;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_200 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_201 = L_200;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_202 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_202, 2, ((int32_t)38), /*hidden argument*/NULL);
		NullCheck(L_201);
		ArrayElementTypeCheck (L_201, L_202);
		(L_201)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_202);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_203 = L_201;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_204 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_204, 2, ((int32_t)39), /*hidden argument*/NULL);
		NullCheck(L_203);
		ArrayElementTypeCheck (L_203, L_204);
		(L_203)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_204);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_205 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_205, ((int32_t)22), L_203, /*hidden argument*/NULL);
		NullCheck(L_199);
		ArrayElementTypeCheck (L_199, L_205);
		(L_199)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_205);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_206 = L_199;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_207 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_208 = L_207;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_209 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_209, 4, ((int32_t)18), /*hidden argument*/NULL);
		NullCheck(L_208);
		ArrayElementTypeCheck (L_208, L_209);
		(L_208)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_209);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_210 = L_208;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_211 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_211, 2, ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_210);
		ArrayElementTypeCheck (L_210, L_211);
		(L_210)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_211);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_212 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_212, ((int32_t)22), L_210, /*hidden argument*/NULL);
		NullCheck(L_206);
		ArrayElementTypeCheck (L_206, L_212);
		(L_206)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_212);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_213 = L_206;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_214 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_215 = L_214;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_216 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_216, 4, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_215);
		ArrayElementTypeCheck (L_215, L_216);
		(L_215)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_216);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_217 = L_215;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_218 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_218, 2, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_217);
		ArrayElementTypeCheck (L_217, L_218);
		(L_217)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_218);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_219 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_219, ((int32_t)26), L_217, /*hidden argument*/NULL);
		NullCheck(L_213);
		ArrayElementTypeCheck (L_213, L_219);
		(L_213)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_219);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_220 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_220, 8, L_191, L_213, /*hidden argument*/NULL);
		NullCheck(L_189);
		ArrayElementTypeCheck (L_189, L_220);
		(L_189)->SetAt(static_cast<il2cpp_array_size_t>(7), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_220);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_221 = L_189;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_222 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_223 = L_222;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_224 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_147_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_223, L_224, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_225 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_226 = L_225;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_227 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_228 = L_227;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_229 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_229, 2, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_228);
		ArrayElementTypeCheck (L_228, L_229);
		(L_228)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_229);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_230 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_230, ((int32_t)30), L_228, /*hidden argument*/NULL);
		NullCheck(L_226);
		ArrayElementTypeCheck (L_226, L_230);
		(L_226)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_230);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_231 = L_226;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_232 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_233 = L_232;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_234 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_234, 3, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_233);
		ArrayElementTypeCheck (L_233, L_234);
		(L_233)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_234);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_235 = L_233;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_236 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_236, 2, ((int32_t)37), /*hidden argument*/NULL);
		NullCheck(L_235);
		ArrayElementTypeCheck (L_235, L_236);
		(L_235)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_236);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_237 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_237, ((int32_t)22), L_235, /*hidden argument*/NULL);
		NullCheck(L_231);
		ArrayElementTypeCheck (L_231, L_237);
		(L_231)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_237);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_238 = L_231;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_239 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_240 = L_239;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_241 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_241, 4, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_240);
		ArrayElementTypeCheck (L_240, L_241);
		(L_240)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_241);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_242 = L_240;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_243 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_243, 4, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_242);
		ArrayElementTypeCheck (L_242, L_243);
		(L_242)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_243);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_244 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_244, ((int32_t)20), L_242, /*hidden argument*/NULL);
		NullCheck(L_238);
		ArrayElementTypeCheck (L_238, L_244);
		(L_238)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_244);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_245 = L_238;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_246 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_247 = L_246;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_248 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_248, 4, ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_247);
		ArrayElementTypeCheck (L_247, L_248);
		(L_247)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_248);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_249 = L_247;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_250 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_250, 4, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_249);
		ArrayElementTypeCheck (L_249, L_250);
		(L_249)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_250);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_251 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_251, ((int32_t)24), L_249, /*hidden argument*/NULL);
		NullCheck(L_245);
		ArrayElementTypeCheck (L_245, L_251);
		(L_245)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_251);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_252 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_252, ((int32_t)9), L_223, L_245, /*hidden argument*/NULL);
		NullCheck(L_221);
		ArrayElementTypeCheck (L_221, L_252);
		(L_221)->SetAt(static_cast<il2cpp_array_size_t>(8), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_252);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_253 = L_221;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_254 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_255 = L_254;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_256 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_312_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_255, L_256, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_257 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_258 = L_257;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_259 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_260 = L_259;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_261 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_261, 2, ((int32_t)68), /*hidden argument*/NULL);
		NullCheck(L_260);
		ArrayElementTypeCheck (L_260, L_261);
		(L_260)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_261);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_262 = L_260;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_263 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_263, 2, ((int32_t)69), /*hidden argument*/NULL);
		NullCheck(L_262);
		ArrayElementTypeCheck (L_262, L_263);
		(L_262)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_263);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_264 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_264, ((int32_t)18), L_262, /*hidden argument*/NULL);
		NullCheck(L_258);
		ArrayElementTypeCheck (L_258, L_264);
		(L_258)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_264);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_265 = L_258;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_266 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_267 = L_266;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_268 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_268, 4, ((int32_t)43), /*hidden argument*/NULL);
		NullCheck(L_267);
		ArrayElementTypeCheck (L_267, L_268);
		(L_267)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_268);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_269 = L_267;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_270 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_270, 1, ((int32_t)44), /*hidden argument*/NULL);
		NullCheck(L_269);
		ArrayElementTypeCheck (L_269, L_270);
		(L_269)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_270);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_271 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_271, ((int32_t)26), L_269, /*hidden argument*/NULL);
		NullCheck(L_265);
		ArrayElementTypeCheck (L_265, L_271);
		(L_265)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_271);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_272 = L_265;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_273 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_274 = L_273;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_275 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_275, 6, ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_274);
		ArrayElementTypeCheck (L_274, L_275);
		(L_274)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_275);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_276 = L_274;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_277 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_277, 2, ((int32_t)20), /*hidden argument*/NULL);
		NullCheck(L_276);
		ArrayElementTypeCheck (L_276, L_277);
		(L_276)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_277);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_278 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_278, ((int32_t)24), L_276, /*hidden argument*/NULL);
		NullCheck(L_272);
		ArrayElementTypeCheck (L_272, L_278);
		(L_272)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_278);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_279 = L_272;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_280 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_281 = L_280;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_282 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_282, 6, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_281);
		ArrayElementTypeCheck (L_281, L_282);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_282);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_283 = L_281;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_284 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_284, 2, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_283);
		ArrayElementTypeCheck (L_283, L_284);
		(L_283)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_284);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_285 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_285, ((int32_t)28), L_283, /*hidden argument*/NULL);
		NullCheck(L_279);
		ArrayElementTypeCheck (L_279, L_285);
		(L_279)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_285);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_286 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_286, ((int32_t)10), L_255, L_279, /*hidden argument*/NULL);
		NullCheck(L_253);
		ArrayElementTypeCheck (L_253, L_286);
		(L_253)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_286);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_287 = L_253;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_288 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_289 = L_288;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_290 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____7616E4F44CA76DF539C11B9C8FD911D667FEFC52_143_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_289, L_290, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_291 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_292 = L_291;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_293 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_294 = L_293;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_295 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_295, 4, ((int32_t)81), /*hidden argument*/NULL);
		NullCheck(L_294);
		ArrayElementTypeCheck (L_294, L_295);
		(L_294)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_295);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_296 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_296, ((int32_t)20), L_294, /*hidden argument*/NULL);
		NullCheck(L_292);
		ArrayElementTypeCheck (L_292, L_296);
		(L_292)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_296);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_297 = L_292;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_298 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_299 = L_298;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_300 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_300, 1, ((int32_t)50), /*hidden argument*/NULL);
		NullCheck(L_299);
		ArrayElementTypeCheck (L_299, L_300);
		(L_299)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_300);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_301 = L_299;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_302 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_302, 4, ((int32_t)51), /*hidden argument*/NULL);
		NullCheck(L_301);
		ArrayElementTypeCheck (L_301, L_302);
		(L_301)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_302);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_303 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_303, ((int32_t)30), L_301, /*hidden argument*/NULL);
		NullCheck(L_297);
		ArrayElementTypeCheck (L_297, L_303);
		(L_297)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_303);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_304 = L_297;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_305 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_306 = L_305;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_307 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_307, 4, ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_306);
		ArrayElementTypeCheck (L_306, L_307);
		(L_306)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_307);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_308 = L_306;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_309 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_309, 4, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_308);
		ArrayElementTypeCheck (L_308, L_309);
		(L_308)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_309);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_310 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_310, ((int32_t)28), L_308, /*hidden argument*/NULL);
		NullCheck(L_304);
		ArrayElementTypeCheck (L_304, L_310);
		(L_304)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_310);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_311 = L_304;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_312 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_313 = L_312;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_314 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_314, 3, ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_313);
		ArrayElementTypeCheck (L_313, L_314);
		(L_313)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_314);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_315 = L_313;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_316 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_316, 8, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_315);
		ArrayElementTypeCheck (L_315, L_316);
		(L_315)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_316);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_317 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_317, ((int32_t)24), L_315, /*hidden argument*/NULL);
		NullCheck(L_311);
		ArrayElementTypeCheck (L_311, L_317);
		(L_311)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_317);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_318 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_318, ((int32_t)11), L_289, L_311, /*hidden argument*/NULL);
		NullCheck(L_287);
		ArrayElementTypeCheck (L_287, L_318);
		(L_287)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_318);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_319 = L_287;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_320 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_321 = L_320;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_322 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_200_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_321, L_322, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_323 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_324 = L_323;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_325 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_326 = L_325;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_327 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_327, 2, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_326);
		ArrayElementTypeCheck (L_326, L_327);
		(L_326)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_327);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_328 = L_326;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_329 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_329, 2, ((int32_t)93), /*hidden argument*/NULL);
		NullCheck(L_328);
		ArrayElementTypeCheck (L_328, L_329);
		(L_328)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_329);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_330 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_330, ((int32_t)24), L_328, /*hidden argument*/NULL);
		NullCheck(L_324);
		ArrayElementTypeCheck (L_324, L_330);
		(L_324)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_330);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_331 = L_324;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_332 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_333 = L_332;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_334 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_334, 6, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_333);
		ArrayElementTypeCheck (L_333, L_334);
		(L_333)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_334);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_335 = L_333;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_336 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_336, 2, ((int32_t)37), /*hidden argument*/NULL);
		NullCheck(L_335);
		ArrayElementTypeCheck (L_335, L_336);
		(L_335)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_336);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_337 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_337, ((int32_t)22), L_335, /*hidden argument*/NULL);
		NullCheck(L_331);
		ArrayElementTypeCheck (L_331, L_337);
		(L_331)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_337);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_338 = L_331;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_339 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_340 = L_339;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_341 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_341, 4, ((int32_t)20), /*hidden argument*/NULL);
		NullCheck(L_340);
		ArrayElementTypeCheck (L_340, L_341);
		(L_340)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_341);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_342 = L_340;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_343 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_343, 6, ((int32_t)21), /*hidden argument*/NULL);
		NullCheck(L_342);
		ArrayElementTypeCheck (L_342, L_343);
		(L_342)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_343);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_344 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_344, ((int32_t)26), L_342, /*hidden argument*/NULL);
		NullCheck(L_338);
		ArrayElementTypeCheck (L_338, L_344);
		(L_338)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_344);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_345 = L_338;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_346 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_347 = L_346;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_348 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_348, 7, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_347);
		ArrayElementTypeCheck (L_347, L_348);
		(L_347)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_348);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_349 = L_347;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_350 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_350, 4, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_349);
		ArrayElementTypeCheck (L_349, L_350);
		(L_349)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_350);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_351 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_351, ((int32_t)28), L_349, /*hidden argument*/NULL);
		NullCheck(L_345);
		ArrayElementTypeCheck (L_345, L_351);
		(L_345)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_351);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_352 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_352, ((int32_t)12), L_321, L_345, /*hidden argument*/NULL);
		NullCheck(L_319);
		ArrayElementTypeCheck (L_319, L_352);
		(L_319)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_352);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_353 = L_319;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_354 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_355 = L_354;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_356 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_181_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_355, L_356, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_357 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_358 = L_357;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_359 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_360 = L_359;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_361 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_361, 4, ((int32_t)107), /*hidden argument*/NULL);
		NullCheck(L_360);
		ArrayElementTypeCheck (L_360, L_361);
		(L_360)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_361);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_362 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_362, ((int32_t)26), L_360, /*hidden argument*/NULL);
		NullCheck(L_358);
		ArrayElementTypeCheck (L_358, L_362);
		(L_358)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_362);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_363 = L_358;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_364 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_365 = L_364;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_366 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_366, 8, ((int32_t)37), /*hidden argument*/NULL);
		NullCheck(L_365);
		ArrayElementTypeCheck (L_365, L_366);
		(L_365)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_366);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_367 = L_365;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_368 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_368, 1, ((int32_t)38), /*hidden argument*/NULL);
		NullCheck(L_367);
		ArrayElementTypeCheck (L_367, L_368);
		(L_367)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_368);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_369 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_369, ((int32_t)22), L_367, /*hidden argument*/NULL);
		NullCheck(L_363);
		ArrayElementTypeCheck (L_363, L_369);
		(L_363)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_369);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_370 = L_363;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_371 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_372 = L_371;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_373 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_373, 8, ((int32_t)20), /*hidden argument*/NULL);
		NullCheck(L_372);
		ArrayElementTypeCheck (L_372, L_373);
		(L_372)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_373);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_374 = L_372;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_375 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_375, 4, ((int32_t)21), /*hidden argument*/NULL);
		NullCheck(L_374);
		ArrayElementTypeCheck (L_374, L_375);
		(L_374)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_375);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_376 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_376, ((int32_t)24), L_374, /*hidden argument*/NULL);
		NullCheck(L_370);
		ArrayElementTypeCheck (L_370, L_376);
		(L_370)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_376);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_377 = L_370;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_378 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_379 = L_378;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_380 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_380, ((int32_t)12), ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_379);
		ArrayElementTypeCheck (L_379, L_380);
		(L_379)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_380);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_381 = L_379;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_382 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_382, 4, ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_381);
		ArrayElementTypeCheck (L_381, L_382);
		(L_381)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_382);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_383 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_383, ((int32_t)22), L_381, /*hidden argument*/NULL);
		NullCheck(L_377);
		ArrayElementTypeCheck (L_377, L_383);
		(L_377)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_383);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_384 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_384, ((int32_t)13), L_355, L_377, /*hidden argument*/NULL);
		NullCheck(L_353);
		ArrayElementTypeCheck (L_353, L_384);
		(L_353)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_384);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_385 = L_353;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_386 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_387 = L_386;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_388 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____DA8D7694A42602F7DC0356F30B9E1DFF8196724A_269_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_387, L_388, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_389 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_390 = L_389;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_391 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_392 = L_391;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_393 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_393, 3, ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_392);
		ArrayElementTypeCheck (L_392, L_393);
		(L_392)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_393);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_394 = L_392;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_395 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_395, 1, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_394);
		ArrayElementTypeCheck (L_394, L_395);
		(L_394)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_395);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_396 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_396, ((int32_t)30), L_394, /*hidden argument*/NULL);
		NullCheck(L_390);
		ArrayElementTypeCheck (L_390, L_396);
		(L_390)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_396);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_397 = L_390;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_398 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_399 = L_398;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_400 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_400, 4, ((int32_t)40), /*hidden argument*/NULL);
		NullCheck(L_399);
		ArrayElementTypeCheck (L_399, L_400);
		(L_399)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_400);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_401 = L_399;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_402 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_402, 5, ((int32_t)41), /*hidden argument*/NULL);
		NullCheck(L_401);
		ArrayElementTypeCheck (L_401, L_402);
		(L_401)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_402);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_403 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_403, ((int32_t)24), L_401, /*hidden argument*/NULL);
		NullCheck(L_397);
		ArrayElementTypeCheck (L_397, L_403);
		(L_397)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_403);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_404 = L_397;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_405 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_406 = L_405;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_407 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_407, ((int32_t)11), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_406);
		ArrayElementTypeCheck (L_406, L_407);
		(L_406)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_407);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_408 = L_406;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_409 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_409, 5, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_408);
		ArrayElementTypeCheck (L_408, L_409);
		(L_408)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_409);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_410 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_410, ((int32_t)20), L_408, /*hidden argument*/NULL);
		NullCheck(L_404);
		ArrayElementTypeCheck (L_404, L_410);
		(L_404)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_410);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_411 = L_404;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_412 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_413 = L_412;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_414 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_414, ((int32_t)11), ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_413);
		ArrayElementTypeCheck (L_413, L_414);
		(L_413)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_414);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_415 = L_413;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_416 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_416, 5, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_415);
		ArrayElementTypeCheck (L_415, L_416);
		(L_415)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_416);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_417 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_417, ((int32_t)24), L_415, /*hidden argument*/NULL);
		NullCheck(L_411);
		ArrayElementTypeCheck (L_411, L_417);
		(L_411)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_417);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_418 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_418, ((int32_t)14), L_387, L_411, /*hidden argument*/NULL);
		NullCheck(L_385);
		ArrayElementTypeCheck (L_385, L_418);
		(L_385)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_418);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_419 = L_385;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_420 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_421 = L_420;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_422 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____0222E151247DAE31A8D697EAA14F43F718BD1F1C_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_421, L_422, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_423 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_424 = L_423;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_425 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_426 = L_425;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_427 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_427, 5, ((int32_t)87), /*hidden argument*/NULL);
		NullCheck(L_426);
		ArrayElementTypeCheck (L_426, L_427);
		(L_426)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_427);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_428 = L_426;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_429 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_429, 1, ((int32_t)88), /*hidden argument*/NULL);
		NullCheck(L_428);
		ArrayElementTypeCheck (L_428, L_429);
		(L_428)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_429);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_430 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_430, ((int32_t)22), L_428, /*hidden argument*/NULL);
		NullCheck(L_424);
		ArrayElementTypeCheck (L_424, L_430);
		(L_424)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_430);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_431 = L_424;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_432 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_433 = L_432;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_434 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_434, 5, ((int32_t)41), /*hidden argument*/NULL);
		NullCheck(L_433);
		ArrayElementTypeCheck (L_433, L_434);
		(L_433)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_434);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_435 = L_433;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_436 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_436, 5, ((int32_t)42), /*hidden argument*/NULL);
		NullCheck(L_435);
		ArrayElementTypeCheck (L_435, L_436);
		(L_435)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_436);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_437 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_437, ((int32_t)24), L_435, /*hidden argument*/NULL);
		NullCheck(L_431);
		ArrayElementTypeCheck (L_431, L_437);
		(L_431)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_437);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_438 = L_431;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_439 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_440 = L_439;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_441 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_441, 5, ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_440);
		ArrayElementTypeCheck (L_440, L_441);
		(L_440)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_441);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_442 = L_440;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_443 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_443, 7, ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_442);
		ArrayElementTypeCheck (L_442, L_443);
		(L_442)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_443);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_444 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_444, ((int32_t)30), L_442, /*hidden argument*/NULL);
		NullCheck(L_438);
		ArrayElementTypeCheck (L_438, L_444);
		(L_438)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_444);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_445 = L_438;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_446 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_447 = L_446;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_448 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_448, ((int32_t)11), ((int32_t)12), /*hidden argument*/NULL);
		NullCheck(L_447);
		ArrayElementTypeCheck (L_447, L_448);
		(L_447)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_448);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_449 = L_447;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_450 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_450, 7, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_449);
		ArrayElementTypeCheck (L_449, L_450);
		(L_449)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_450);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_451 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_451, ((int32_t)24), L_449, /*hidden argument*/NULL);
		NullCheck(L_445);
		ArrayElementTypeCheck (L_445, L_451);
		(L_445)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_451);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_452 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_452, ((int32_t)15), L_421, L_445, /*hidden argument*/NULL);
		NullCheck(L_419);
		ArrayElementTypeCheck (L_419, L_452);
		(L_419)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_452);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_453 = L_419;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_454 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_455 = L_454;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_456 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_279_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_455, L_456, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_457 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_458 = L_457;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_459 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_460 = L_459;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_461 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_461, 5, ((int32_t)98), /*hidden argument*/NULL);
		NullCheck(L_460);
		ArrayElementTypeCheck (L_460, L_461);
		(L_460)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_461);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_462 = L_460;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_463 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_463, 1, ((int32_t)99), /*hidden argument*/NULL);
		NullCheck(L_462);
		ArrayElementTypeCheck (L_462, L_463);
		(L_462)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_463);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_464 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_464, ((int32_t)24), L_462, /*hidden argument*/NULL);
		NullCheck(L_458);
		ArrayElementTypeCheck (L_458, L_464);
		(L_458)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_464);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_465 = L_458;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_466 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_467 = L_466;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_468 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_468, 7, ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_467);
		ArrayElementTypeCheck (L_467, L_468);
		(L_467)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_468);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_469 = L_467;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_470 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_470, 3, ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_469);
		ArrayElementTypeCheck (L_469, L_470);
		(L_469)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_470);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_471 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_471, ((int32_t)28), L_469, /*hidden argument*/NULL);
		NullCheck(L_465);
		ArrayElementTypeCheck (L_465, L_471);
		(L_465)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_471);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_472 = L_465;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_473 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_474 = L_473;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_475 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_475, ((int32_t)15), ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_474);
		ArrayElementTypeCheck (L_474, L_475);
		(L_474)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_475);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_476 = L_474;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_477 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_477, 2, ((int32_t)20), /*hidden argument*/NULL);
		NullCheck(L_476);
		ArrayElementTypeCheck (L_476, L_477);
		(L_476)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_477);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_478 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_478, ((int32_t)24), L_476, /*hidden argument*/NULL);
		NullCheck(L_472);
		ArrayElementTypeCheck (L_472, L_478);
		(L_472)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_478);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_479 = L_472;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_480 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_481 = L_480;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_482 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_482, 3, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_481);
		ArrayElementTypeCheck (L_481, L_482);
		(L_481)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_482);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_483 = L_481;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_484 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_484, ((int32_t)13), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_483);
		ArrayElementTypeCheck (L_483, L_484);
		(L_483)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_484);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_485 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_485, ((int32_t)30), L_483, /*hidden argument*/NULL);
		NullCheck(L_479);
		ArrayElementTypeCheck (L_479, L_485);
		(L_479)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_485);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_486 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_486, ((int32_t)16), L_455, L_479, /*hidden argument*/NULL);
		NullCheck(L_453);
		ArrayElementTypeCheck (L_453, L_486);
		(L_453)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_486);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_487 = L_453;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_488 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_489 = L_488;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_490 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9E548860392B66258417232F41EF03D48EBDD686_201_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_489, L_490, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_491 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_492 = L_491;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_493 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_494 = L_493;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_495 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_495, 1, ((int32_t)107), /*hidden argument*/NULL);
		NullCheck(L_494);
		ArrayElementTypeCheck (L_494, L_495);
		(L_494)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_495);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_496 = L_494;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_497 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_497, 5, ((int32_t)108), /*hidden argument*/NULL);
		NullCheck(L_496);
		ArrayElementTypeCheck (L_496, L_497);
		(L_496)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_497);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_498 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_498, ((int32_t)28), L_496, /*hidden argument*/NULL);
		NullCheck(L_492);
		ArrayElementTypeCheck (L_492, L_498);
		(L_492)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_498);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_499 = L_492;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_500 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_501 = L_500;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_502 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_502, ((int32_t)10), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_501);
		ArrayElementTypeCheck (L_501, L_502);
		(L_501)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_502);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_503 = L_501;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_504 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_504, 1, ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_503);
		ArrayElementTypeCheck (L_503, L_504);
		(L_503)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_504);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_505 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_505, ((int32_t)28), L_503, /*hidden argument*/NULL);
		NullCheck(L_499);
		ArrayElementTypeCheck (L_499, L_505);
		(L_499)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_505);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_506 = L_499;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_507 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_508 = L_507;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_509 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_509, 1, ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_508);
		ArrayElementTypeCheck (L_508, L_509);
		(L_508)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_509);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_510 = L_508;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_511 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_511, ((int32_t)15), ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_510);
		ArrayElementTypeCheck (L_510, L_511);
		(L_510)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_511);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_512 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_512, ((int32_t)28), L_510, /*hidden argument*/NULL);
		NullCheck(L_506);
		ArrayElementTypeCheck (L_506, L_512);
		(L_506)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_512);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_513 = L_506;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_514 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_515 = L_514;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_516 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_516, 2, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_515);
		ArrayElementTypeCheck (L_515, L_516);
		(L_515)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_516);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_517 = L_515;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_518 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_518, ((int32_t)17), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_517);
		ArrayElementTypeCheck (L_517, L_518);
		(L_517)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_518);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_519 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_519, ((int32_t)28), L_517, /*hidden argument*/NULL);
		NullCheck(L_513);
		ArrayElementTypeCheck (L_513, L_519);
		(L_513)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_519);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_520 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_520, ((int32_t)17), L_489, L_513, /*hidden argument*/NULL);
		NullCheck(L_487);
		ArrayElementTypeCheck (L_487, L_520);
		(L_487)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_520);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_521 = L_487;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_522 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_523 = L_522;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_524 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6DE89499E8F85867289E1690E176E1DE58EE6DD3_131_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_523, L_524, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_525 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_526 = L_525;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_527 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_528 = L_527;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_529 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_529, 5, ((int32_t)120), /*hidden argument*/NULL);
		NullCheck(L_528);
		ArrayElementTypeCheck (L_528, L_529);
		(L_528)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_529);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_530 = L_528;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_531 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_531, 1, ((int32_t)121), /*hidden argument*/NULL);
		NullCheck(L_530);
		ArrayElementTypeCheck (L_530, L_531);
		(L_530)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_531);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_532 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_532, ((int32_t)30), L_530, /*hidden argument*/NULL);
		NullCheck(L_526);
		ArrayElementTypeCheck (L_526, L_532);
		(L_526)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_532);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_533 = L_526;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_534 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_535 = L_534;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_536 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_536, ((int32_t)9), ((int32_t)43), /*hidden argument*/NULL);
		NullCheck(L_535);
		ArrayElementTypeCheck (L_535, L_536);
		(L_535)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_536);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_537 = L_535;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_538 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_538, 4, ((int32_t)44), /*hidden argument*/NULL);
		NullCheck(L_537);
		ArrayElementTypeCheck (L_537, L_538);
		(L_537)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_538);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_539 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_539, ((int32_t)26), L_537, /*hidden argument*/NULL);
		NullCheck(L_533);
		ArrayElementTypeCheck (L_533, L_539);
		(L_533)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_539);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_540 = L_533;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_541 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_542 = L_541;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_543 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_543, ((int32_t)17), ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_542);
		ArrayElementTypeCheck (L_542, L_543);
		(L_542)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_543);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_544 = L_542;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_545 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_545, 1, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_544);
		ArrayElementTypeCheck (L_544, L_545);
		(L_544)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_545);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_546 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_546, ((int32_t)28), L_544, /*hidden argument*/NULL);
		NullCheck(L_540);
		ArrayElementTypeCheck (L_540, L_546);
		(L_540)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_546);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_547 = L_540;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_548 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_549 = L_548;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_550 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_550, 2, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_549);
		ArrayElementTypeCheck (L_549, L_550);
		(L_549)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_550);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_551 = L_549;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_552 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_552, ((int32_t)19), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_551);
		ArrayElementTypeCheck (L_551, L_552);
		(L_551)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_552);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_553 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_553, ((int32_t)28), L_551, /*hidden argument*/NULL);
		NullCheck(L_547);
		ArrayElementTypeCheck (L_547, L_553);
		(L_547)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_553);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_554 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_554, ((int32_t)18), L_523, L_547, /*hidden argument*/NULL);
		NullCheck(L_521);
		ArrayElementTypeCheck (L_521, L_554);
		(L_521)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_554);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_555 = L_521;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_556 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_557 = L_556;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_558 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_315_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_557, L_558, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_559 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_560 = L_559;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_561 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_562 = L_561;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_563 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_563, 3, ((int32_t)113), /*hidden argument*/NULL);
		NullCheck(L_562);
		ArrayElementTypeCheck (L_562, L_563);
		(L_562)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_563);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_564 = L_562;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_565 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_565, 4, ((int32_t)114), /*hidden argument*/NULL);
		NullCheck(L_564);
		ArrayElementTypeCheck (L_564, L_565);
		(L_564)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_565);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_566 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_566, ((int32_t)28), L_564, /*hidden argument*/NULL);
		NullCheck(L_560);
		ArrayElementTypeCheck (L_560, L_566);
		(L_560)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_566);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_567 = L_560;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_568 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_569 = L_568;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_570 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_570, 3, ((int32_t)44), /*hidden argument*/NULL);
		NullCheck(L_569);
		ArrayElementTypeCheck (L_569, L_570);
		(L_569)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_570);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_571 = L_569;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_572 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_572, ((int32_t)11), ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_571);
		ArrayElementTypeCheck (L_571, L_572);
		(L_571)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_572);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_573 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_573, ((int32_t)26), L_571, /*hidden argument*/NULL);
		NullCheck(L_567);
		ArrayElementTypeCheck (L_567, L_573);
		(L_567)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_573);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_574 = L_567;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_575 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_576 = L_575;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_577 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_577, ((int32_t)17), ((int32_t)21), /*hidden argument*/NULL);
		NullCheck(L_576);
		ArrayElementTypeCheck (L_576, L_577);
		(L_576)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_577);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_578 = L_576;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_579 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_579, 4, ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_578);
		ArrayElementTypeCheck (L_578, L_579);
		(L_578)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_579);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_580 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_580, ((int32_t)26), L_578, /*hidden argument*/NULL);
		NullCheck(L_574);
		ArrayElementTypeCheck (L_574, L_580);
		(L_574)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_580);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_581 = L_574;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_582 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_583 = L_582;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_584 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_584, ((int32_t)9), ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_583);
		ArrayElementTypeCheck (L_583, L_584);
		(L_583)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_584);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_585 = L_583;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_586 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_586, ((int32_t)16), ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_585);
		ArrayElementTypeCheck (L_585, L_586);
		(L_585)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_586);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_587 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_587, ((int32_t)26), L_585, /*hidden argument*/NULL);
		NullCheck(L_581);
		ArrayElementTypeCheck (L_581, L_587);
		(L_581)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_587);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_588 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_588, ((int32_t)19), L_557, L_581, /*hidden argument*/NULL);
		NullCheck(L_555);
		ArrayElementTypeCheck (L_555, L_588);
		(L_555)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_588);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_589 = L_555;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_590 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_591 = L_590;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_592 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6882914B984B24C4880E3BEA646DC549B3C34ABC_121_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_591, L_592, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_593 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_594 = L_593;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_595 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_596 = L_595;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_597 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_597, 3, ((int32_t)107), /*hidden argument*/NULL);
		NullCheck(L_596);
		ArrayElementTypeCheck (L_596, L_597);
		(L_596)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_597);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_598 = L_596;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_599 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_599, 5, ((int32_t)108), /*hidden argument*/NULL);
		NullCheck(L_598);
		ArrayElementTypeCheck (L_598, L_599);
		(L_598)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_599);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_600 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_600, ((int32_t)28), L_598, /*hidden argument*/NULL);
		NullCheck(L_594);
		ArrayElementTypeCheck (L_594, L_600);
		(L_594)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_600);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_601 = L_594;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_602 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_603 = L_602;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_604 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_604, 3, ((int32_t)41), /*hidden argument*/NULL);
		NullCheck(L_603);
		ArrayElementTypeCheck (L_603, L_604);
		(L_603)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_604);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_605 = L_603;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_606 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_606, ((int32_t)13), ((int32_t)42), /*hidden argument*/NULL);
		NullCheck(L_605);
		ArrayElementTypeCheck (L_605, L_606);
		(L_605)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_606);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_607 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_607, ((int32_t)26), L_605, /*hidden argument*/NULL);
		NullCheck(L_601);
		ArrayElementTypeCheck (L_601, L_607);
		(L_601)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_607);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_608 = L_601;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_609 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_610 = L_609;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_611 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_611, ((int32_t)15), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_610);
		ArrayElementTypeCheck (L_610, L_611);
		(L_610)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_611);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_612 = L_610;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_613 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_613, 5, ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_612);
		ArrayElementTypeCheck (L_612, L_613);
		(L_612)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_613);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_614 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_614, ((int32_t)30), L_612, /*hidden argument*/NULL);
		NullCheck(L_608);
		ArrayElementTypeCheck (L_608, L_614);
		(L_608)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_614);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_615 = L_608;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_616 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_617 = L_616;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_618 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_618, ((int32_t)15), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_617);
		ArrayElementTypeCheck (L_617, L_618);
		(L_617)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_618);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_619 = L_617;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_620 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_620, ((int32_t)10), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_619);
		ArrayElementTypeCheck (L_619, L_620);
		(L_619)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_620);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_621 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_621, ((int32_t)28), L_619, /*hidden argument*/NULL);
		NullCheck(L_615);
		ArrayElementTypeCheck (L_615, L_621);
		(L_615)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_621);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_622 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_622, ((int32_t)20), L_591, L_615, /*hidden argument*/NULL);
		NullCheck(L_589);
		ArrayElementTypeCheck (L_589, L_622);
		(L_589)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_622);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_623 = L_589;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_624 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_625 = L_624;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_626 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____BB40482B7503DCF14BB6DAE300C2717C3021C7B2_225_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_625, L_626, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_627 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_628 = L_627;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_629 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_630 = L_629;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_631 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_631, 4, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_630);
		ArrayElementTypeCheck (L_630, L_631);
		(L_630)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_631);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_632 = L_630;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_633 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_633, 4, ((int32_t)117), /*hidden argument*/NULL);
		NullCheck(L_632);
		ArrayElementTypeCheck (L_632, L_633);
		(L_632)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_633);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_634 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_634, ((int32_t)28), L_632, /*hidden argument*/NULL);
		NullCheck(L_628);
		ArrayElementTypeCheck (L_628, L_634);
		(L_628)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_634);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_635 = L_628;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_636 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_637 = L_636;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_638 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_638, ((int32_t)17), ((int32_t)42), /*hidden argument*/NULL);
		NullCheck(L_637);
		ArrayElementTypeCheck (L_637, L_638);
		(L_637)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_638);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_639 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_639, ((int32_t)26), L_637, /*hidden argument*/NULL);
		NullCheck(L_635);
		ArrayElementTypeCheck (L_635, L_639);
		(L_635)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_639);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_640 = L_635;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_641 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_642 = L_641;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_643 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_643, ((int32_t)17), ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_642);
		ArrayElementTypeCheck (L_642, L_643);
		(L_642)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_643);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_644 = L_642;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_645 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_645, 6, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_644);
		ArrayElementTypeCheck (L_644, L_645);
		(L_644)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_645);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_646 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_646, ((int32_t)28), L_644, /*hidden argument*/NULL);
		NullCheck(L_640);
		ArrayElementTypeCheck (L_640, L_646);
		(L_640)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_646);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_647 = L_640;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_648 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_649 = L_648;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_650 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_650, ((int32_t)19), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_649);
		ArrayElementTypeCheck (L_649, L_650);
		(L_649)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_650);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_651 = L_649;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_652 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_652, 6, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_651);
		ArrayElementTypeCheck (L_651, L_652);
		(L_651)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_652);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_653 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_653, ((int32_t)30), L_651, /*hidden argument*/NULL);
		NullCheck(L_647);
		ArrayElementTypeCheck (L_647, L_653);
		(L_647)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_653);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_654 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_654, ((int32_t)21), L_625, L_647, /*hidden argument*/NULL);
		NullCheck(L_623);
		ArrayElementTypeCheck (L_623, L_654);
		(L_623)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_654);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_655 = L_623;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_656 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_657 = L_656;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_658 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_48_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_657, L_658, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_659 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_660 = L_659;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_661 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_662 = L_661;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_663 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_663, 2, ((int32_t)111), /*hidden argument*/NULL);
		NullCheck(L_662);
		ArrayElementTypeCheck (L_662, L_663);
		(L_662)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_663);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_664 = L_662;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_665 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_665, 7, ((int32_t)112), /*hidden argument*/NULL);
		NullCheck(L_664);
		ArrayElementTypeCheck (L_664, L_665);
		(L_664)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_665);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_666 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_666, ((int32_t)28), L_664, /*hidden argument*/NULL);
		NullCheck(L_660);
		ArrayElementTypeCheck (L_660, L_666);
		(L_660)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_666);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_667 = L_660;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_668 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_669 = L_668;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_670 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_670, ((int32_t)17), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_669);
		ArrayElementTypeCheck (L_669, L_670);
		(L_669)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_670);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_671 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_671, ((int32_t)28), L_669, /*hidden argument*/NULL);
		NullCheck(L_667);
		ArrayElementTypeCheck (L_667, L_671);
		(L_667)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_671);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_672 = L_667;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_673 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_674 = L_673;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_675 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_675, 7, ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_674);
		ArrayElementTypeCheck (L_674, L_675);
		(L_674)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_675);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_676 = L_674;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_677 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_677, ((int32_t)16), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_676);
		ArrayElementTypeCheck (L_676, L_677);
		(L_676)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_677);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_678 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_678, ((int32_t)30), L_676, /*hidden argument*/NULL);
		NullCheck(L_672);
		ArrayElementTypeCheck (L_672, L_678);
		(L_672)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_678);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_679 = L_672;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_680 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_681 = L_680;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_682 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_682, ((int32_t)34), ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_681);
		ArrayElementTypeCheck (L_681, L_682);
		(L_681)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_682);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_683 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_683, ((int32_t)24), L_681, /*hidden argument*/NULL);
		NullCheck(L_679);
		ArrayElementTypeCheck (L_679, L_683);
		(L_679)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_683);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_684 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_684, ((int32_t)22), L_657, L_679, /*hidden argument*/NULL);
		NullCheck(L_655);
		ArrayElementTypeCheck (L_655, L_684);
		(L_655)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_684);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_685 = L_655;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_686 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_687 = L_686;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_688 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____8D1369A0D8832C941BD6D09849525D65D807A1DD_180_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_687, L_688, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_689 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_690 = L_689;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_691 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_692 = L_691;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_693 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_693, 4, ((int32_t)121), /*hidden argument*/NULL);
		NullCheck(L_692);
		ArrayElementTypeCheck (L_692, L_693);
		(L_692)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_693);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_694 = L_692;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_695 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_695, 5, ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_694);
		ArrayElementTypeCheck (L_694, L_695);
		(L_694)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_695);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_696 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_696, ((int32_t)30), L_694, /*hidden argument*/NULL);
		NullCheck(L_690);
		ArrayElementTypeCheck (L_690, L_696);
		(L_690)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_696);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_697 = L_690;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_698 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_699 = L_698;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_700 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_700, 4, ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_699);
		ArrayElementTypeCheck (L_699, L_700);
		(L_699)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_700);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_701 = L_699;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_702 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_702, ((int32_t)14), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_701);
		ArrayElementTypeCheck (L_701, L_702);
		(L_701)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_702);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_703 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_703, ((int32_t)28), L_701, /*hidden argument*/NULL);
		NullCheck(L_697);
		ArrayElementTypeCheck (L_697, L_703);
		(L_697)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_703);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_704 = L_697;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_705 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_706 = L_705;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_707 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_707, ((int32_t)11), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_706);
		ArrayElementTypeCheck (L_706, L_707);
		(L_706)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_707);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_708 = L_706;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_709 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_709, ((int32_t)14), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_708);
		ArrayElementTypeCheck (L_708, L_709);
		(L_708)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_709);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_710 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_710, ((int32_t)30), L_708, /*hidden argument*/NULL);
		NullCheck(L_704);
		ArrayElementTypeCheck (L_704, L_710);
		(L_704)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_710);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_711 = L_704;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_712 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_713 = L_712;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_714 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_714, ((int32_t)16), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_713);
		ArrayElementTypeCheck (L_713, L_714);
		(L_713)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_714);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_715 = L_713;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_716 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_716, ((int32_t)14), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_715);
		ArrayElementTypeCheck (L_715, L_716);
		(L_715)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_716);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_717 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_717, ((int32_t)30), L_715, /*hidden argument*/NULL);
		NullCheck(L_711);
		ArrayElementTypeCheck (L_711, L_717);
		(L_711)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_717);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_718 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_718, ((int32_t)23), L_687, L_711, /*hidden argument*/NULL);
		NullCheck(L_685);
		ArrayElementTypeCheck (L_685, L_718);
		(L_685)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_718);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_719 = L_685;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_720 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_721 = L_720;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_722 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9BBAB86ACB95EF8C028708733458CDBAC4715197_198_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_721, L_722, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_723 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_724 = L_723;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_725 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_726 = L_725;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_727 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_727, 6, ((int32_t)117), /*hidden argument*/NULL);
		NullCheck(L_726);
		ArrayElementTypeCheck (L_726, L_727);
		(L_726)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_727);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_728 = L_726;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_729 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_729, 4, ((int32_t)118), /*hidden argument*/NULL);
		NullCheck(L_728);
		ArrayElementTypeCheck (L_728, L_729);
		(L_728)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_729);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_730 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_730, ((int32_t)30), L_728, /*hidden argument*/NULL);
		NullCheck(L_724);
		ArrayElementTypeCheck (L_724, L_730);
		(L_724)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_730);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_731 = L_724;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_732 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_733 = L_732;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_734 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_734, 6, ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_733);
		ArrayElementTypeCheck (L_733, L_734);
		(L_733)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_734);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_735 = L_733;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_736 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_736, ((int32_t)14), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_735);
		ArrayElementTypeCheck (L_735, L_736);
		(L_735)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_736);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_737 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_737, ((int32_t)28), L_735, /*hidden argument*/NULL);
		NullCheck(L_731);
		ArrayElementTypeCheck (L_731, L_737);
		(L_731)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_737);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_738 = L_731;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_739 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_740 = L_739;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_741 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_741, ((int32_t)11), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_740);
		ArrayElementTypeCheck (L_740, L_741);
		(L_740)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_741);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_742 = L_740;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_743 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_743, ((int32_t)16), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_742);
		ArrayElementTypeCheck (L_742, L_743);
		(L_742)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_743);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_744 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_744, ((int32_t)30), L_742, /*hidden argument*/NULL);
		NullCheck(L_738);
		ArrayElementTypeCheck (L_738, L_744);
		(L_738)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_744);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_745 = L_738;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_746 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_747 = L_746;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_748 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_748, ((int32_t)30), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_747);
		ArrayElementTypeCheck (L_747, L_748);
		(L_747)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_748);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_749 = L_747;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_750 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_750, 2, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_749);
		ArrayElementTypeCheck (L_749, L_750);
		(L_749)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_750);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_751 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_751, ((int32_t)30), L_749, /*hidden argument*/NULL);
		NullCheck(L_745);
		ArrayElementTypeCheck (L_745, L_751);
		(L_745)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_751);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_752 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_752, ((int32_t)24), L_721, L_745, /*hidden argument*/NULL);
		NullCheck(L_719);
		ArrayElementTypeCheck (L_719, L_752);
		(L_719)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_752);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_753 = L_719;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_754 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_755 = L_754;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_756 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_37_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_755, L_756, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_757 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_758 = L_757;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_759 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_760 = L_759;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_761 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_761, 8, ((int32_t)106), /*hidden argument*/NULL);
		NullCheck(L_760);
		ArrayElementTypeCheck (L_760, L_761);
		(L_760)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_761);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_762 = L_760;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_763 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_763, 4, ((int32_t)107), /*hidden argument*/NULL);
		NullCheck(L_762);
		ArrayElementTypeCheck (L_762, L_763);
		(L_762)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_763);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_764 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_764, ((int32_t)26), L_762, /*hidden argument*/NULL);
		NullCheck(L_758);
		ArrayElementTypeCheck (L_758, L_764);
		(L_758)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_764);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_765 = L_758;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_766 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_767 = L_766;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_768 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_768, 8, ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_767);
		ArrayElementTypeCheck (L_767, L_768);
		(L_767)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_768);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_769 = L_767;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_770 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_770, ((int32_t)13), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_769);
		ArrayElementTypeCheck (L_769, L_770);
		(L_769)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_770);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_771 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_771, ((int32_t)28), L_769, /*hidden argument*/NULL);
		NullCheck(L_765);
		ArrayElementTypeCheck (L_765, L_771);
		(L_765)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_771);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_772 = L_765;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_773 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_774 = L_773;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_775 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_775, 7, ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_774);
		ArrayElementTypeCheck (L_774, L_775);
		(L_774)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_775);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_776 = L_774;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_777 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_777, ((int32_t)22), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_776);
		ArrayElementTypeCheck (L_776, L_777);
		(L_776)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_777);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_778 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_778, ((int32_t)30), L_776, /*hidden argument*/NULL);
		NullCheck(L_772);
		ArrayElementTypeCheck (L_772, L_778);
		(L_772)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_778);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_779 = L_772;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_780 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_781 = L_780;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_782 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_782, ((int32_t)22), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_781);
		ArrayElementTypeCheck (L_781, L_782);
		(L_781)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_782);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_783 = L_781;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_784 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_784, ((int32_t)13), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_783);
		ArrayElementTypeCheck (L_783, L_784);
		(L_783)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_784);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_785 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_785, ((int32_t)30), L_783, /*hidden argument*/NULL);
		NullCheck(L_779);
		ArrayElementTypeCheck (L_779, L_785);
		(L_779)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_785);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_786 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_786, ((int32_t)25), L_755, L_779, /*hidden argument*/NULL);
		NullCheck(L_753);
		ArrayElementTypeCheck (L_753, L_786);
		(L_753)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_786);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_787 = L_753;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_788 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_789 = L_788;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_790 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____9B3E34C734351ED3DE1984119EAB9572C48FA15E_197_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_789, L_790, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_791 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_792 = L_791;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_793 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_794 = L_793;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_795 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_795, ((int32_t)10), ((int32_t)114), /*hidden argument*/NULL);
		NullCheck(L_794);
		ArrayElementTypeCheck (L_794, L_795);
		(L_794)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_795);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_796 = L_794;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_797 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_797, 2, ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_796);
		ArrayElementTypeCheck (L_796, L_797);
		(L_796)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_797);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_798 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_798, ((int32_t)28), L_796, /*hidden argument*/NULL);
		NullCheck(L_792);
		ArrayElementTypeCheck (L_792, L_798);
		(L_792)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_798);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_799 = L_792;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_800 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_801 = L_800;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_802 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_802, ((int32_t)19), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_801);
		ArrayElementTypeCheck (L_801, L_802);
		(L_801)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_802);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_803 = L_801;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_804 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_804, 4, ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_803);
		ArrayElementTypeCheck (L_803, L_804);
		(L_803)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_804);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_805 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_805, ((int32_t)28), L_803, /*hidden argument*/NULL);
		NullCheck(L_799);
		ArrayElementTypeCheck (L_799, L_805);
		(L_799)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_805);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_806 = L_799;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_807 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_808 = L_807;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_809 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_809, ((int32_t)28), ((int32_t)22), /*hidden argument*/NULL);
		NullCheck(L_808);
		ArrayElementTypeCheck (L_808, L_809);
		(L_808)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_809);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_810 = L_808;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_811 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_811, 6, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_810);
		ArrayElementTypeCheck (L_810, L_811);
		(L_810)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_811);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_812 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_812, ((int32_t)28), L_810, /*hidden argument*/NULL);
		NullCheck(L_806);
		ArrayElementTypeCheck (L_806, L_812);
		(L_806)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_812);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_813 = L_806;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_814 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_815 = L_814;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_816 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_816, ((int32_t)33), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_815);
		ArrayElementTypeCheck (L_815, L_816);
		(L_815)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_816);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_817 = L_815;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_818 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_818, 4, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_817);
		ArrayElementTypeCheck (L_817, L_818);
		(L_817)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_818);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_819 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_819, ((int32_t)30), L_817, /*hidden argument*/NULL);
		NullCheck(L_813);
		ArrayElementTypeCheck (L_813, L_819);
		(L_813)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_819);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_820 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_820, ((int32_t)26), L_789, L_813, /*hidden argument*/NULL);
		NullCheck(L_787);
		ArrayElementTypeCheck (L_787, L_820);
		(L_787)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_820);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_821 = L_787;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_822 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_823 = L_822;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_824 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____259B7EF54A535F34001B95480B4BB11245EA41B5_42_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_823, L_824, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_825 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_826 = L_825;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_827 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_828 = L_827;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_829 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_829, 8, ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_828);
		ArrayElementTypeCheck (L_828, L_829);
		(L_828)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_829);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_830 = L_828;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_831 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_831, 4, ((int32_t)123), /*hidden argument*/NULL);
		NullCheck(L_830);
		ArrayElementTypeCheck (L_830, L_831);
		(L_830)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_831);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_832 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_832, ((int32_t)30), L_830, /*hidden argument*/NULL);
		NullCheck(L_826);
		ArrayElementTypeCheck (L_826, L_832);
		(L_826)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_832);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_833 = L_826;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_834 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_835 = L_834;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_836 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_836, ((int32_t)22), ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_835);
		ArrayElementTypeCheck (L_835, L_836);
		(L_835)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_836);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_837 = L_835;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_838 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_838, 3, ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_837);
		ArrayElementTypeCheck (L_837, L_838);
		(L_837)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_838);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_839 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_839, ((int32_t)28), L_837, /*hidden argument*/NULL);
		NullCheck(L_833);
		ArrayElementTypeCheck (L_833, L_839);
		(L_833)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_839);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_840 = L_833;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_841 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_842 = L_841;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_843 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_843, 8, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_842);
		ArrayElementTypeCheck (L_842, L_843);
		(L_842)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_843);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_844 = L_842;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_845 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_845, ((int32_t)26), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_844);
		ArrayElementTypeCheck (L_844, L_845);
		(L_844)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_845);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_846 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_846, ((int32_t)30), L_844, /*hidden argument*/NULL);
		NullCheck(L_840);
		ArrayElementTypeCheck (L_840, L_846);
		(L_840)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_846);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_847 = L_840;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_848 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_849 = L_848;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_850 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_850, ((int32_t)12), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_849);
		ArrayElementTypeCheck (L_849, L_850);
		(L_849)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_850);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_851 = L_849;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_852 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_852, ((int32_t)28), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_851);
		ArrayElementTypeCheck (L_851, L_852);
		(L_851)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_852);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_853 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_853, ((int32_t)30), L_851, /*hidden argument*/NULL);
		NullCheck(L_847);
		ArrayElementTypeCheck (L_847, L_853);
		(L_847)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_853);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_854 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_854, ((int32_t)27), L_823, L_847, /*hidden argument*/NULL);
		NullCheck(L_821);
		ArrayElementTypeCheck (L_821, L_854);
		(L_821)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_854);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_855 = L_821;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_856 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_857 = L_856;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_858 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____188E7A480B871C554F3F805D3E1184673960EED5_21_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_857, L_858, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_859 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_860 = L_859;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_861 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_862 = L_861;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_863 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_863, 3, ((int32_t)117), /*hidden argument*/NULL);
		NullCheck(L_862);
		ArrayElementTypeCheck (L_862, L_863);
		(L_862)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_863);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_864 = L_862;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_865 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_865, ((int32_t)10), ((int32_t)118), /*hidden argument*/NULL);
		NullCheck(L_864);
		ArrayElementTypeCheck (L_864, L_865);
		(L_864)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_865);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_866 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_866, ((int32_t)30), L_864, /*hidden argument*/NULL);
		NullCheck(L_860);
		ArrayElementTypeCheck (L_860, L_866);
		(L_860)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_866);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_867 = L_860;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_868 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_869 = L_868;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_870 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_870, 3, ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_869);
		ArrayElementTypeCheck (L_869, L_870);
		(L_869)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_870);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_871 = L_869;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_872 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_872, ((int32_t)23), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_871);
		ArrayElementTypeCheck (L_871, L_872);
		(L_871)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_872);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_873 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_873, ((int32_t)28), L_871, /*hidden argument*/NULL);
		NullCheck(L_867);
		ArrayElementTypeCheck (L_867, L_873);
		(L_867)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_873);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_874 = L_867;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_875 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_876 = L_875;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_877 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_877, 4, ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_876);
		ArrayElementTypeCheck (L_876, L_877);
		(L_876)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_877);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_878 = L_876;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_879 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_879, ((int32_t)31), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_878);
		ArrayElementTypeCheck (L_878, L_879);
		(L_878)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_879);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_880 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_880, ((int32_t)30), L_878, /*hidden argument*/NULL);
		NullCheck(L_874);
		ArrayElementTypeCheck (L_874, L_880);
		(L_874)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_880);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_881 = L_874;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_882 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_883 = L_882;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_884 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_884, ((int32_t)11), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_883);
		ArrayElementTypeCheck (L_883, L_884);
		(L_883)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_884);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_885 = L_883;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_886 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_886, ((int32_t)31), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_885);
		ArrayElementTypeCheck (L_885, L_886);
		(L_885)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_886);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_887 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_887, ((int32_t)30), L_885, /*hidden argument*/NULL);
		NullCheck(L_881);
		ArrayElementTypeCheck (L_881, L_887);
		(L_881)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_887);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_888 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_888, ((int32_t)28), L_857, L_881, /*hidden argument*/NULL);
		NullCheck(L_855);
		ArrayElementTypeCheck (L_855, L_888);
		(L_855)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_888);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_889 = L_855;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_890 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_891 = L_890;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_892 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____AECEE3733C380177F0A39ACF3AF99BFE360C156F_216_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_891, L_892, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_893 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_894 = L_893;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_895 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_896 = L_895;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_897 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_897, 7, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_896);
		ArrayElementTypeCheck (L_896, L_897);
		(L_896)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_897);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_898 = L_896;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_899 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_899, 7, ((int32_t)117), /*hidden argument*/NULL);
		NullCheck(L_898);
		ArrayElementTypeCheck (L_898, L_899);
		(L_898)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_899);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_900 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_900, ((int32_t)30), L_898, /*hidden argument*/NULL);
		NullCheck(L_894);
		ArrayElementTypeCheck (L_894, L_900);
		(L_894)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_900);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_901 = L_894;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_902 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_903 = L_902;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_904 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_904, ((int32_t)21), ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_903);
		ArrayElementTypeCheck (L_903, L_904);
		(L_903)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_904);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_905 = L_903;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_906 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_906, 7, ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_905);
		ArrayElementTypeCheck (L_905, L_906);
		(L_905)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_906);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_907 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_907, ((int32_t)28), L_905, /*hidden argument*/NULL);
		NullCheck(L_901);
		ArrayElementTypeCheck (L_901, L_907);
		(L_901)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_907);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_908 = L_901;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_909 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_910 = L_909;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_911 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_911, 1, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_910);
		ArrayElementTypeCheck (L_910, L_911);
		(L_910)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_911);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_912 = L_910;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_913 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_913, ((int32_t)37), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_912);
		ArrayElementTypeCheck (L_912, L_913);
		(L_912)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_913);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_914 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_914, ((int32_t)30), L_912, /*hidden argument*/NULL);
		NullCheck(L_908);
		ArrayElementTypeCheck (L_908, L_914);
		(L_908)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_914);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_915 = L_908;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_916 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_917 = L_916;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_918 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_918, ((int32_t)19), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_917);
		ArrayElementTypeCheck (L_917, L_918);
		(L_917)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_918);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_919 = L_917;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_920 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_920, ((int32_t)26), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_919);
		ArrayElementTypeCheck (L_919, L_920);
		(L_919)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_920);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_921 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_921, ((int32_t)30), L_919, /*hidden argument*/NULL);
		NullCheck(L_915);
		ArrayElementTypeCheck (L_915, L_921);
		(L_915)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_921);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_922 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_922, ((int32_t)29), L_891, L_915, /*hidden argument*/NULL);
		NullCheck(L_889);
		ArrayElementTypeCheck (L_889, L_922);
		(L_889)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_922);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_923 = L_889;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_924 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_925 = L_924;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_926 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____29AB17251F5A730B539AB351FFCB45B9F2FA8027_46_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_925, L_926, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_927 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_928 = L_927;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_929 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_930 = L_929;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_931 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_931, 5, ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_930);
		ArrayElementTypeCheck (L_930, L_931);
		(L_930)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_931);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_932 = L_930;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_933 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_933, ((int32_t)10), ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_932);
		ArrayElementTypeCheck (L_932, L_933);
		(L_932)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_933);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_934 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_934, ((int32_t)30), L_932, /*hidden argument*/NULL);
		NullCheck(L_928);
		ArrayElementTypeCheck (L_928, L_934);
		(L_928)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_934);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_935 = L_928;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_936 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_937 = L_936;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_938 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_938, ((int32_t)19), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_937);
		ArrayElementTypeCheck (L_937, L_938);
		(L_937)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_938);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_939 = L_937;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_940 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_940, ((int32_t)10), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_939);
		ArrayElementTypeCheck (L_939, L_940);
		(L_939)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_940);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_941 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_941, ((int32_t)28), L_939, /*hidden argument*/NULL);
		NullCheck(L_935);
		ArrayElementTypeCheck (L_935, L_941);
		(L_935)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_941);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_942 = L_935;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_943 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_944 = L_943;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_945 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_945, ((int32_t)15), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_944);
		ArrayElementTypeCheck (L_944, L_945);
		(L_944)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_945);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_946 = L_944;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_947 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_947, ((int32_t)25), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_946);
		ArrayElementTypeCheck (L_946, L_947);
		(L_946)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_947);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_948 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_948, ((int32_t)30), L_946, /*hidden argument*/NULL);
		NullCheck(L_942);
		ArrayElementTypeCheck (L_942, L_948);
		(L_942)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_948);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_949 = L_942;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_950 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_951 = L_950;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_952 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_952, ((int32_t)23), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_951);
		ArrayElementTypeCheck (L_951, L_952);
		(L_951)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_952);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_953 = L_951;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_954 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_954, ((int32_t)25), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_953);
		ArrayElementTypeCheck (L_953, L_954);
		(L_953)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_954);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_955 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_955, ((int32_t)30), L_953, /*hidden argument*/NULL);
		NullCheck(L_949);
		ArrayElementTypeCheck (L_949, L_955);
		(L_949)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_955);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_956 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_956, ((int32_t)30), L_925, L_949, /*hidden argument*/NULL);
		NullCheck(L_923);
		ArrayElementTypeCheck (L_923, L_956);
		(L_923)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_956);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_957 = L_923;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_958 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_959 = L_958;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_960 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____6AA66657BB3292572C8F56AB3D015A7990C43606_126_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_959, L_960, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_961 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_962 = L_961;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_963 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_964 = L_963;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_965 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_965, ((int32_t)13), ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_964);
		ArrayElementTypeCheck (L_964, L_965);
		(L_964)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_965);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_966 = L_964;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_967 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_967, 3, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_966);
		ArrayElementTypeCheck (L_966, L_967);
		(L_966)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_967);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_968 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_968, ((int32_t)30), L_966, /*hidden argument*/NULL);
		NullCheck(L_962);
		ArrayElementTypeCheck (L_962, L_968);
		(L_962)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_968);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_969 = L_962;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_970 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_971 = L_970;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_972 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_972, 2, ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_971);
		ArrayElementTypeCheck (L_971, L_972);
		(L_971)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_972);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_973 = L_971;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_974 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_974, ((int32_t)29), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_973);
		ArrayElementTypeCheck (L_973, L_974);
		(L_973)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_974);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_975 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_975, ((int32_t)28), L_973, /*hidden argument*/NULL);
		NullCheck(L_969);
		ArrayElementTypeCheck (L_969, L_975);
		(L_969)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_975);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_976 = L_969;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_977 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_978 = L_977;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_979 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_979, ((int32_t)42), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_978);
		ArrayElementTypeCheck (L_978, L_979);
		(L_978)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_979);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_980 = L_978;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_981 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_981, 1, ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_980);
		ArrayElementTypeCheck (L_980, L_981);
		(L_980)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_981);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_982 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_982, ((int32_t)30), L_980, /*hidden argument*/NULL);
		NullCheck(L_976);
		ArrayElementTypeCheck (L_976, L_982);
		(L_976)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_982);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_983 = L_976;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_984 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_985 = L_984;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_986 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_986, ((int32_t)23), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_985);
		ArrayElementTypeCheck (L_985, L_986);
		(L_985)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_986);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_987 = L_985;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_988 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_988, ((int32_t)28), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_987);
		ArrayElementTypeCheck (L_987, L_988);
		(L_987)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_988);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_989 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_989, ((int32_t)30), L_987, /*hidden argument*/NULL);
		NullCheck(L_983);
		ArrayElementTypeCheck (L_983, L_989);
		(L_983)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_989);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_990 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_990, ((int32_t)31), L_959, L_983, /*hidden argument*/NULL);
		NullCheck(L_957);
		ArrayElementTypeCheck (L_957, L_990);
		(L_957)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_990);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_991 = L_957;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_992 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_993 = L_992;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_994 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_993, L_994, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_995 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_996 = L_995;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_997 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_998 = L_997;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_999 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_999, ((int32_t)17), ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_998);
		ArrayElementTypeCheck (L_998, L_999);
		(L_998)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_999);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1000 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1000, ((int32_t)30), L_998, /*hidden argument*/NULL);
		NullCheck(L_996);
		ArrayElementTypeCheck (L_996, L_1000);
		(L_996)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1000);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1001 = L_996;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1002 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1003 = L_1002;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1004 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1004, ((int32_t)10), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1003);
		ArrayElementTypeCheck (L_1003, L_1004);
		(L_1003)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1004);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1005 = L_1003;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1006 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1006, ((int32_t)23), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1005);
		ArrayElementTypeCheck (L_1005, L_1006);
		(L_1005)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1006);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1007 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1007, ((int32_t)28), L_1005, /*hidden argument*/NULL);
		NullCheck(L_1001);
		ArrayElementTypeCheck (L_1001, L_1007);
		(L_1001)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1007);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1008 = L_1001;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1009 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1010 = L_1009;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1011 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1011, ((int32_t)10), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1010);
		ArrayElementTypeCheck (L_1010, L_1011);
		(L_1010)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1011);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1012 = L_1010;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1013 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1013, ((int32_t)35), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1012);
		ArrayElementTypeCheck (L_1012, L_1013);
		(L_1012)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1013);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1014 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1014, ((int32_t)30), L_1012, /*hidden argument*/NULL);
		NullCheck(L_1008);
		ArrayElementTypeCheck (L_1008, L_1014);
		(L_1008)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1014);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1015 = L_1008;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1016 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1017 = L_1016;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1018 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1018, ((int32_t)19), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1017);
		ArrayElementTypeCheck (L_1017, L_1018);
		(L_1017)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1018);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1019 = L_1017;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1020 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1020, ((int32_t)35), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1019);
		ArrayElementTypeCheck (L_1019, L_1020);
		(L_1019)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1020);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1021 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1021, ((int32_t)30), L_1019, /*hidden argument*/NULL);
		NullCheck(L_1015);
		ArrayElementTypeCheck (L_1015, L_1021);
		(L_1015)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1021);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1022 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1022, ((int32_t)32), L_993, L_1015, /*hidden argument*/NULL);
		NullCheck(L_991);
		ArrayElementTypeCheck (L_991, L_1022);
		(L_991)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1022);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1023 = L_991;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1024 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1025 = L_1024;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1026 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____56F75F925774608B6BF88F69D6124CB44DDF42E1_98_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1025, L_1026, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1027 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1028 = L_1027;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1029 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1030 = L_1029;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1031 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1031, ((int32_t)17), ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_1030);
		ArrayElementTypeCheck (L_1030, L_1031);
		(L_1030)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1031);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1032 = L_1030;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1033 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1033, 1, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_1032);
		ArrayElementTypeCheck (L_1032, L_1033);
		(L_1032)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1033);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1034 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1034, ((int32_t)30), L_1032, /*hidden argument*/NULL);
		NullCheck(L_1028);
		ArrayElementTypeCheck (L_1028, L_1034);
		(L_1028)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1034);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1035 = L_1028;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1036 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1037 = L_1036;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1038 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1038, ((int32_t)14), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1037);
		ArrayElementTypeCheck (L_1037, L_1038);
		(L_1037)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1038);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1039 = L_1037;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1040 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1040, ((int32_t)21), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1039);
		ArrayElementTypeCheck (L_1039, L_1040);
		(L_1039)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1040);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1041 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1041, ((int32_t)28), L_1039, /*hidden argument*/NULL);
		NullCheck(L_1035);
		ArrayElementTypeCheck (L_1035, L_1041);
		(L_1035)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1041);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1042 = L_1035;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1043 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1044 = L_1043;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1045 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1045, ((int32_t)29), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1044);
		ArrayElementTypeCheck (L_1044, L_1045);
		(L_1044)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1045);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1046 = L_1044;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1047 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1047, ((int32_t)19), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1046);
		ArrayElementTypeCheck (L_1046, L_1047);
		(L_1046)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1047);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1048 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1048, ((int32_t)30), L_1046, /*hidden argument*/NULL);
		NullCheck(L_1042);
		ArrayElementTypeCheck (L_1042, L_1048);
		(L_1042)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1048);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1049 = L_1042;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1050 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1051 = L_1050;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1052 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1052, ((int32_t)11), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1051);
		ArrayElementTypeCheck (L_1051, L_1052);
		(L_1051)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1052);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1053 = L_1051;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1054 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1054, ((int32_t)46), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1053);
		ArrayElementTypeCheck (L_1053, L_1054);
		(L_1053)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1054);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1055 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1055, ((int32_t)30), L_1053, /*hidden argument*/NULL);
		NullCheck(L_1049);
		ArrayElementTypeCheck (L_1049, L_1055);
		(L_1049)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1055);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1056 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1056, ((int32_t)33), L_1025, L_1049, /*hidden argument*/NULL);
		NullCheck(L_1023);
		ArrayElementTypeCheck (L_1023, L_1056);
		(L_1023)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1056);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1057 = L_1023;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1058 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1059 = L_1058;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1060 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____28F991026781E1BC119F02498D75FB3A84F4ED37_44_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1059, L_1060, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1061 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1062 = L_1061;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1063 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1064 = L_1063;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1065 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1065, ((int32_t)13), ((int32_t)115), /*hidden argument*/NULL);
		NullCheck(L_1064);
		ArrayElementTypeCheck (L_1064, L_1065);
		(L_1064)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1065);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1066 = L_1064;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1067 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1067, 6, ((int32_t)116), /*hidden argument*/NULL);
		NullCheck(L_1066);
		ArrayElementTypeCheck (L_1066, L_1067);
		(L_1066)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1067);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1068 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1068, ((int32_t)30), L_1066, /*hidden argument*/NULL);
		NullCheck(L_1062);
		ArrayElementTypeCheck (L_1062, L_1068);
		(L_1062)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1068);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1069 = L_1062;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1070 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1071 = L_1070;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1072 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1072, ((int32_t)14), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1071);
		ArrayElementTypeCheck (L_1071, L_1072);
		(L_1071)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1072);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1073 = L_1071;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1074 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1074, ((int32_t)23), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1073);
		ArrayElementTypeCheck (L_1073, L_1074);
		(L_1073)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1074);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1075 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1075, ((int32_t)28), L_1073, /*hidden argument*/NULL);
		NullCheck(L_1069);
		ArrayElementTypeCheck (L_1069, L_1075);
		(L_1069)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1075);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1076 = L_1069;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1077 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1078 = L_1077;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1079 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1079, ((int32_t)44), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1078);
		ArrayElementTypeCheck (L_1078, L_1079);
		(L_1078)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1079);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1080 = L_1078;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1081 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1081, 7, ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1080);
		ArrayElementTypeCheck (L_1080, L_1081);
		(L_1080)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1081);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1082 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1082, ((int32_t)30), L_1080, /*hidden argument*/NULL);
		NullCheck(L_1076);
		ArrayElementTypeCheck (L_1076, L_1082);
		(L_1076)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1082);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1083 = L_1076;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1084 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1085 = L_1084;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1086 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1086, ((int32_t)59), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1085);
		ArrayElementTypeCheck (L_1085, L_1086);
		(L_1085)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1086);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1087 = L_1085;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1088 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1088, 1, ((int32_t)17), /*hidden argument*/NULL);
		NullCheck(L_1087);
		ArrayElementTypeCheck (L_1087, L_1088);
		(L_1087)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1088);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1089 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1089, ((int32_t)30), L_1087, /*hidden argument*/NULL);
		NullCheck(L_1083);
		ArrayElementTypeCheck (L_1083, L_1089);
		(L_1083)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1089);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1090 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1090, ((int32_t)34), L_1059, L_1083, /*hidden argument*/NULL);
		NullCheck(L_1057);
		ArrayElementTypeCheck (L_1057, L_1090);
		(L_1057)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1090);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1091 = L_1057;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1092 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1093 = L_1092;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1094 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_278_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1093, L_1094, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1095 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1096 = L_1095;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1097 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1098 = L_1097;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1099 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1099, ((int32_t)12), ((int32_t)121), /*hidden argument*/NULL);
		NullCheck(L_1098);
		ArrayElementTypeCheck (L_1098, L_1099);
		(L_1098)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1099);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1100 = L_1098;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1101 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1101, 7, ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_1100);
		ArrayElementTypeCheck (L_1100, L_1101);
		(L_1100)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1101);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1102 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1102, ((int32_t)30), L_1100, /*hidden argument*/NULL);
		NullCheck(L_1096);
		ArrayElementTypeCheck (L_1096, L_1102);
		(L_1096)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1102);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1103 = L_1096;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1104 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1105 = L_1104;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1106 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1106, ((int32_t)12), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1105);
		ArrayElementTypeCheck (L_1105, L_1106);
		(L_1105)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1106);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1107 = L_1105;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1108 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1108, ((int32_t)26), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_1107);
		ArrayElementTypeCheck (L_1107, L_1108);
		(L_1107)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1108);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1109 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1109, ((int32_t)28), L_1107, /*hidden argument*/NULL);
		NullCheck(L_1103);
		ArrayElementTypeCheck (L_1103, L_1109);
		(L_1103)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1109);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1110 = L_1103;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1111 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1112 = L_1111;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1113 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1113, ((int32_t)39), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1112);
		ArrayElementTypeCheck (L_1112, L_1113);
		(L_1112)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1113);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1114 = L_1112;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1115 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1115, ((int32_t)14), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1114);
		ArrayElementTypeCheck (L_1114, L_1115);
		(L_1114)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1115);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1116 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1116, ((int32_t)30), L_1114, /*hidden argument*/NULL);
		NullCheck(L_1110);
		ArrayElementTypeCheck (L_1110, L_1116);
		(L_1110)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1116);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1117 = L_1110;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1118 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1119 = L_1118;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1120 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1120, ((int32_t)22), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1119);
		ArrayElementTypeCheck (L_1119, L_1120);
		(L_1119)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1120);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1121 = L_1119;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1122 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1122, ((int32_t)41), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1121);
		ArrayElementTypeCheck (L_1121, L_1122);
		(L_1121)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1122);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1123 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1123, ((int32_t)30), L_1121, /*hidden argument*/NULL);
		NullCheck(L_1117);
		ArrayElementTypeCheck (L_1117, L_1123);
		(L_1117)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1123);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1124 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1124, ((int32_t)35), L_1093, L_1117, /*hidden argument*/NULL);
		NullCheck(L_1091);
		ArrayElementTypeCheck (L_1091, L_1124);
		(L_1091)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)34)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1124);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1125 = L_1091;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1126 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1127 = L_1126;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1128 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_235_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1127, L_1128, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1129 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1130 = L_1129;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1131 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1132 = L_1131;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1133 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1133, 6, ((int32_t)121), /*hidden argument*/NULL);
		NullCheck(L_1132);
		ArrayElementTypeCheck (L_1132, L_1133);
		(L_1132)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1133);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1134 = L_1132;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1135 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1135, ((int32_t)14), ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_1134);
		ArrayElementTypeCheck (L_1134, L_1135);
		(L_1134)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1135);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1136 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1136, ((int32_t)30), L_1134, /*hidden argument*/NULL);
		NullCheck(L_1130);
		ArrayElementTypeCheck (L_1130, L_1136);
		(L_1130)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1136);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1137 = L_1130;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1138 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1139 = L_1138;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1140 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1140, 6, ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1139);
		ArrayElementTypeCheck (L_1139, L_1140);
		(L_1139)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1140);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1141 = L_1139;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1142 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1142, ((int32_t)34), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_1141);
		ArrayElementTypeCheck (L_1141, L_1142);
		(L_1141)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1142);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1143 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1143, ((int32_t)28), L_1141, /*hidden argument*/NULL);
		NullCheck(L_1137);
		ArrayElementTypeCheck (L_1137, L_1143);
		(L_1137)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1143);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1144 = L_1137;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1145 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1146 = L_1145;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1147 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1147, ((int32_t)46), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1146);
		ArrayElementTypeCheck (L_1146, L_1147);
		(L_1146)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1147);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1148 = L_1146;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1149 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1149, ((int32_t)10), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1148);
		ArrayElementTypeCheck (L_1148, L_1149);
		(L_1148)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1149);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1150 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1150, ((int32_t)30), L_1148, /*hidden argument*/NULL);
		NullCheck(L_1144);
		ArrayElementTypeCheck (L_1144, L_1150);
		(L_1144)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1150);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1151 = L_1144;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1152 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1153 = L_1152;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1154 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1154, 2, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1153);
		ArrayElementTypeCheck (L_1153, L_1154);
		(L_1153)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1154);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1155 = L_1153;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1156 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1156, ((int32_t)64), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1155);
		ArrayElementTypeCheck (L_1155, L_1156);
		(L_1155)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1156);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1157 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1157, ((int32_t)30), L_1155, /*hidden argument*/NULL);
		NullCheck(L_1151);
		ArrayElementTypeCheck (L_1151, L_1157);
		(L_1151)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1157);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1158 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1158, ((int32_t)36), L_1127, L_1151, /*hidden argument*/NULL);
		NullCheck(L_1125);
		ArrayElementTypeCheck (L_1125, L_1158);
		(L_1125)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)35)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1158);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1159 = L_1125;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1160 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1161 = L_1160;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1162 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____00E14C77230AEF0974CFF3930481157AABDA07B4_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1161, L_1162, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1163 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1164 = L_1163;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1165 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1166 = L_1165;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1167 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1167, ((int32_t)17), ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_1166);
		ArrayElementTypeCheck (L_1166, L_1167);
		(L_1166)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1167);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1168 = L_1166;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1169 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1169, 4, ((int32_t)123), /*hidden argument*/NULL);
		NullCheck(L_1168);
		ArrayElementTypeCheck (L_1168, L_1169);
		(L_1168)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1169);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1170 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1170, ((int32_t)30), L_1168, /*hidden argument*/NULL);
		NullCheck(L_1164);
		ArrayElementTypeCheck (L_1164, L_1170);
		(L_1164)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1170);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1171 = L_1164;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1172 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1173 = L_1172;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1174 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1174, ((int32_t)29), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1173);
		ArrayElementTypeCheck (L_1173, L_1174);
		(L_1173)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1174);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1175 = L_1173;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1176 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1176, ((int32_t)14), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1175);
		ArrayElementTypeCheck (L_1175, L_1176);
		(L_1175)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1176);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1177 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1177, ((int32_t)28), L_1175, /*hidden argument*/NULL);
		NullCheck(L_1171);
		ArrayElementTypeCheck (L_1171, L_1177);
		(L_1171)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1177);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1178 = L_1171;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1179 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1180 = L_1179;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1181 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1181, ((int32_t)49), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1180);
		ArrayElementTypeCheck (L_1180, L_1181);
		(L_1180)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1181);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1182 = L_1180;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1183 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1183, ((int32_t)10), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1182);
		ArrayElementTypeCheck (L_1182, L_1183);
		(L_1182)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1183);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1184 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1184, ((int32_t)30), L_1182, /*hidden argument*/NULL);
		NullCheck(L_1178);
		ArrayElementTypeCheck (L_1178, L_1184);
		(L_1178)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1184);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1185 = L_1178;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1186 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1187 = L_1186;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1188 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1188, ((int32_t)24), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1187);
		ArrayElementTypeCheck (L_1187, L_1188);
		(L_1187)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1188);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1189 = L_1187;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1190 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1190, ((int32_t)46), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1189);
		ArrayElementTypeCheck (L_1189, L_1190);
		(L_1189)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1190);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1191 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1191, ((int32_t)30), L_1189, /*hidden argument*/NULL);
		NullCheck(L_1185);
		ArrayElementTypeCheck (L_1185, L_1191);
		(L_1185)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1191);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1192 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1192, ((int32_t)37), L_1161, L_1185, /*hidden argument*/NULL);
		NullCheck(L_1159);
		ArrayElementTypeCheck (L_1159, L_1192);
		(L_1159)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1192);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1193 = L_1159;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1194 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1195 = L_1194;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1196 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_84_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1195, L_1196, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1197 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1198 = L_1197;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1199 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1200 = L_1199;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1201 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1201, 4, ((int32_t)122), /*hidden argument*/NULL);
		NullCheck(L_1200);
		ArrayElementTypeCheck (L_1200, L_1201);
		(L_1200)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1201);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1202 = L_1200;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1203 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1203, ((int32_t)18), ((int32_t)123), /*hidden argument*/NULL);
		NullCheck(L_1202);
		ArrayElementTypeCheck (L_1202, L_1203);
		(L_1202)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1203);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1204 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1204, ((int32_t)30), L_1202, /*hidden argument*/NULL);
		NullCheck(L_1198);
		ArrayElementTypeCheck (L_1198, L_1204);
		(L_1198)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1204);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1205 = L_1198;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1206 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1207 = L_1206;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1208 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1208, ((int32_t)13), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1207);
		ArrayElementTypeCheck (L_1207, L_1208);
		(L_1207)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1208);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1209 = L_1207;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1210 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1210, ((int32_t)32), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1209);
		ArrayElementTypeCheck (L_1209, L_1210);
		(L_1209)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1210);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1211 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1211, ((int32_t)28), L_1209, /*hidden argument*/NULL);
		NullCheck(L_1205);
		ArrayElementTypeCheck (L_1205, L_1211);
		(L_1205)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1211);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1212 = L_1205;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1213 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1214 = L_1213;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1215 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1215, ((int32_t)48), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1214);
		ArrayElementTypeCheck (L_1214, L_1215);
		(L_1214)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1215);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1216 = L_1214;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1217 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1217, ((int32_t)14), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1216);
		ArrayElementTypeCheck (L_1216, L_1217);
		(L_1216)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1217);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1218 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1218, ((int32_t)30), L_1216, /*hidden argument*/NULL);
		NullCheck(L_1212);
		ArrayElementTypeCheck (L_1212, L_1218);
		(L_1212)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1218);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1219 = L_1212;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1220 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1221 = L_1220;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1222 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1222, ((int32_t)42), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1221);
		ArrayElementTypeCheck (L_1221, L_1222);
		(L_1221)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1222);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1223 = L_1221;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1224 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1224, ((int32_t)32), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1223);
		ArrayElementTypeCheck (L_1223, L_1224);
		(L_1223)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1224);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1225 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1225, ((int32_t)30), L_1223, /*hidden argument*/NULL);
		NullCheck(L_1219);
		ArrayElementTypeCheck (L_1219, L_1225);
		(L_1219)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1225);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1226 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1226, ((int32_t)38), L_1195, L_1219, /*hidden argument*/NULL);
		NullCheck(L_1193);
		ArrayElementTypeCheck (L_1193, L_1226);
		(L_1193)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1226);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1227 = L_1193;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1228 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1229 = L_1228;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1230 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____C0935C173C7A144FE24DA09584F4892153D01F3C_236_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1229, L_1230, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1231 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1232 = L_1231;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1233 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1234 = L_1233;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1235 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1235, ((int32_t)20), ((int32_t)117), /*hidden argument*/NULL);
		NullCheck(L_1234);
		ArrayElementTypeCheck (L_1234, L_1235);
		(L_1234)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1235);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1236 = L_1234;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1237 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1237, 4, ((int32_t)118), /*hidden argument*/NULL);
		NullCheck(L_1236);
		ArrayElementTypeCheck (L_1236, L_1237);
		(L_1236)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1237);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1238 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1238, ((int32_t)30), L_1236, /*hidden argument*/NULL);
		NullCheck(L_1232);
		ArrayElementTypeCheck (L_1232, L_1238);
		(L_1232)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1238);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1239 = L_1232;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1240 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1241 = L_1240;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1242 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1242, ((int32_t)40), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1241);
		ArrayElementTypeCheck (L_1241, L_1242);
		(L_1241)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1242);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1243 = L_1241;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1244 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1244, 7, ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_1243);
		ArrayElementTypeCheck (L_1243, L_1244);
		(L_1243)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1244);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1245 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1245, ((int32_t)28), L_1243, /*hidden argument*/NULL);
		NullCheck(L_1239);
		ArrayElementTypeCheck (L_1239, L_1245);
		(L_1239)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1245);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1246 = L_1239;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1247 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1248 = L_1247;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1249 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1249, ((int32_t)43), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1248);
		ArrayElementTypeCheck (L_1248, L_1249);
		(L_1248)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1249);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1250 = L_1248;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1251 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1251, ((int32_t)22), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1250);
		ArrayElementTypeCheck (L_1250, L_1251);
		(L_1250)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1251);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1252 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1252, ((int32_t)30), L_1250, /*hidden argument*/NULL);
		NullCheck(L_1246);
		ArrayElementTypeCheck (L_1246, L_1252);
		(L_1246)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1252);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1253 = L_1246;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1254 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1255 = L_1254;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1256 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1256, ((int32_t)10), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1255);
		ArrayElementTypeCheck (L_1255, L_1256);
		(L_1255)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1256);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1257 = L_1255;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1258 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1258, ((int32_t)67), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1257);
		ArrayElementTypeCheck (L_1257, L_1258);
		(L_1257)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1258);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1259 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1259, ((int32_t)30), L_1257, /*hidden argument*/NULL);
		NullCheck(L_1253);
		ArrayElementTypeCheck (L_1253, L_1259);
		(L_1253)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1259);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1260 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1260, ((int32_t)39), L_1229, L_1253, /*hidden argument*/NULL);
		NullCheck(L_1227);
		ArrayElementTypeCheck (L_1227, L_1260);
		(L_1227)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1260);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_1261 = L_1227;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1262 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1263 = L_1262;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_1264 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____11D4D9FC526D047E01891BEE9949EA42408A800E_17_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1263, L_1264, /*hidden argument*/NULL);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1265 = (ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22*)SZArrayNew(ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22_il2cpp_TypeInfo_var, (uint32_t)4);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1266 = L_1265;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1267 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1268 = L_1267;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1269 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1269, ((int32_t)19), ((int32_t)118), /*hidden argument*/NULL);
		NullCheck(L_1268);
		ArrayElementTypeCheck (L_1268, L_1269);
		(L_1268)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1269);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1270 = L_1268;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1271 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1271, 6, ((int32_t)119), /*hidden argument*/NULL);
		NullCheck(L_1270);
		ArrayElementTypeCheck (L_1270, L_1271);
		(L_1270)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1271);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1272 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1272, ((int32_t)30), L_1270, /*hidden argument*/NULL);
		NullCheck(L_1266);
		ArrayElementTypeCheck (L_1266, L_1272);
		(L_1266)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1272);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1273 = L_1266;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1274 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1275 = L_1274;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1276 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1276, ((int32_t)18), ((int32_t)47), /*hidden argument*/NULL);
		NullCheck(L_1275);
		ArrayElementTypeCheck (L_1275, L_1276);
		(L_1275)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1276);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1277 = L_1275;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1278 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1278, ((int32_t)31), ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_1277);
		ArrayElementTypeCheck (L_1277, L_1278);
		(L_1277)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1278);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1279 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1279, ((int32_t)28), L_1277, /*hidden argument*/NULL);
		NullCheck(L_1273);
		ArrayElementTypeCheck (L_1273, L_1279);
		(L_1273)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1279);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1280 = L_1273;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1281 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1282 = L_1281;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1283 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1283, ((int32_t)34), ((int32_t)24), /*hidden argument*/NULL);
		NullCheck(L_1282);
		ArrayElementTypeCheck (L_1282, L_1283);
		(L_1282)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1283);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1284 = L_1282;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1285 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1285, ((int32_t)34), ((int32_t)25), /*hidden argument*/NULL);
		NullCheck(L_1284);
		ArrayElementTypeCheck (L_1284, L_1285);
		(L_1284)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1285);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1286 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1286, ((int32_t)30), L_1284, /*hidden argument*/NULL);
		NullCheck(L_1280);
		ArrayElementTypeCheck (L_1280, L_1286);
		(L_1280)->SetAt(static_cast<il2cpp_array_size_t>(2), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1286);
		ECBlocksU5BU5D_tBC961C9969FD8010754BAEC0A591D117A7439B22* L_1287 = L_1280;
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1288 = (ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C*)SZArrayNew(ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1289 = L_1288;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1290 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1290, ((int32_t)20), ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_1289);
		ArrayElementTypeCheck (L_1289, L_1290);
		(L_1289)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1290);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1291 = L_1289;
		ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * L_1292 = (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)il2cpp_codegen_object_new(ECB_t21A54092AE86393FFF1F286B70E027F59ED07064_il2cpp_TypeInfo_var);
		ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784(L_1292, ((int32_t)61), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_1291);
		ArrayElementTypeCheck (L_1291, L_1292);
		(L_1291)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 *)L_1292);
		ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * L_1293 = (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)il2cpp_codegen_object_new(ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_il2cpp_TypeInfo_var);
		ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017(L_1293, ((int32_t)30), L_1291, /*hidden argument*/NULL);
		NullCheck(L_1287);
		ArrayElementTypeCheck (L_1287, L_1293);
		(L_1287)->SetAt(static_cast<il2cpp_array_size_t>(3), (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 *)L_1293);
		Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB * L_1294 = (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)il2cpp_codegen_object_new(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F(L_1294, ((int32_t)40), L_1263, L_1287, /*hidden argument*/NULL);
		NullCheck(L_1261);
		ArrayElementTypeCheck (L_1261, L_1294);
		(L_1261)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB *)L_1294);
		return L_1261;
	}
}
// System.Void ZXing.QrCode.Internal.Version::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Version__cctor_m8BC73054B966C5EF8D307E0BE69564C356A10561 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____40B82CCF037B966E2A6B5F4BAC86904796965A1C_73_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)34));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = L_0;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A____40B82CCF037B966E2A6B5F4BAC86904796965A1C_73_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields*)il2cpp_codegen_static_fields_for(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var))->set_VERSION_DECODE_INFO_0(L_1);
		VersionU5BU5D_tE89CA0F31CB1F98FEAEF419C03D521E7D94F237F* L_3;
		L_3 = Version_buildVersions_m4FF9D42DFED8563CAE3AF70AB90A30046EC1026E(/*hidden argument*/NULL);
		((Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_StaticFields*)il2cpp_codegen_static_fields_for(Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_il2cpp_TypeInfo_var))->set_VERSIONS_1(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * WhiteRectangleDetector_Create_mB7E5C4CCD1DD3D79EB0581A45071CE02B0DECFDF (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * V_0 = NULL;
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_0 = ___image0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 *)NULL;
	}

IL_0005:
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_1 = ___image0;
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_2 = (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 *)il2cpp_codegen_object_new(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8_il2cpp_TypeInfo_var);
		WhiteRectangleDetector__ctor_m65D6591012DC7B1053F586C40156AE32937F67DE(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_upInit_6();
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_leftInit_3();
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_downInit_5();
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_height_1();
		if ((((int32_t)L_8) >= ((int32_t)L_10)))
		{
			goto IL_003a;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_rightInit_4();
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_width_2();
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_003c;
		}
	}

IL_003a:
	{
		return (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 *)NULL;
	}

IL_003c:
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_15 = V_0;
		return L_15;
	}
}
// ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * WhiteRectangleDetector_Create_mCF22DCF654C8C8B88AB289C3A1C9C14D58253658 (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, int32_t ___initSize1, int32_t ___x2, int32_t ___y3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * V_0 = NULL;
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_0 = ___image0;
		int32_t L_1 = ___initSize1;
		int32_t L_2 = ___x2;
		int32_t L_3 = ___y3;
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_4 = (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 *)il2cpp_codegen_object_new(WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8_il2cpp_TypeInfo_var);
		WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_upInit_6();
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_leftInit_3();
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_downInit_5();
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_height_1();
		if ((((int32_t)L_10) >= ((int32_t)L_12)))
		{
			goto IL_0038;
		}
	}
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_rightInit_4();
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_width_2();
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_003a;
		}
	}

IL_0038:
	{
		return (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 *)NULL;
	}

IL_003a:
	{
		WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * L_17 = V_0;
		return L_17;
	}
}
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteRectangleDetector__ctor_m65D6591012DC7B1053F586C40156AE32937F67DE (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, const RuntimeMethod* method)
{
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_0 = ___image0;
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_1 = ___image0;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8_inline(L_1, /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_3 = ___image0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433_inline(L_3, /*hidden argument*/NULL);
		WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8(__this, L_0, ((int32_t)10), ((int32_t)((int32_t)L_2/(int32_t)2)), ((int32_t)((int32_t)L_4/(int32_t)2)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * ___image0, int32_t ___initSize1, int32_t ___x2, int32_t ___y3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_0 = ___image0;
		__this->set_image_0(L_0);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_1 = ___image0;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433_inline(L_1, /*hidden argument*/NULL);
		__this->set_height_1(L_2);
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_3 = ___image0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8_inline(L_3, /*hidden argument*/NULL);
		__this->set_width_2(L_4);
		int32_t L_5 = ___initSize1;
		V_0 = ((int32_t)((int32_t)L_5/(int32_t)2));
		int32_t L_6 = ___x2;
		int32_t L_7 = V_0;
		__this->set_leftInit_3(((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)L_7)));
		int32_t L_8 = ___x2;
		int32_t L_9 = V_0;
		__this->set_rightInit_4(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_9)));
		int32_t L_10 = ___y3;
		int32_t L_11 = V_0;
		__this->set_upInit_6(((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)L_11)));
		int32_t L_12 = ___y3;
		int32_t L_13 = V_0;
		__this->set_downInit_5(((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_13)));
		return;
	}
}
// ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::detect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* WhiteRectangleDetector_detect_mC702724840AE187F538791BAB7DFE6DFAFBE9110 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	int32_t V_15 = 0;
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * V_16 = NULL;
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * V_17 = NULL;
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * V_18 = NULL;
	ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * V_19 = NULL;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	{
		int32_t L_0 = __this->get_leftInit_3();
		V_0 = L_0;
		int32_t L_1 = __this->get_rightInit_4();
		V_1 = L_1;
		int32_t L_2 = __this->get_upInit_6();
		V_2 = L_2;
		int32_t L_3 = __this->get_downInit_5();
		V_3 = L_3;
		V_4 = (bool)0;
		V_5 = (bool)1;
		V_6 = (bool)0;
		V_7 = (bool)0;
		V_8 = (bool)0;
		V_9 = (bool)0;
		V_10 = (bool)0;
		goto IL_0152;
	}

IL_0036:
	{
		V_5 = (bool)0;
		V_11 = (bool)1;
		goto IL_0062;
	}

IL_003e:
	{
		int32_t L_4 = V_2;
		int32_t L_5 = V_3;
		int32_t L_6 = V_1;
		bool L_7;
		L_7 = WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482(__this, L_4, L_5, L_6, (bool)0, /*hidden argument*/NULL);
		V_11 = L_7;
		bool L_8 = V_11;
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		V_5 = (bool)1;
		V_7 = (bool)1;
		goto IL_0062;
	}

IL_005a:
	{
		bool L_10 = V_7;
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0062:
	{
		bool L_12 = V_11;
		if (L_12)
		{
			goto IL_006a;
		}
	}
	{
		bool L_13 = V_7;
		if (L_13)
		{
			goto IL_0073;
		}
	}

IL_006a:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = __this->get_width_2();
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_003e;
		}
	}

IL_0073:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = __this->get_width_2();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0084;
		}
	}
	{
		V_4 = (bool)1;
		goto IL_0159;
	}

IL_0084:
	{
		V_12 = (bool)1;
		goto IL_00ad;
	}

IL_0089:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = V_1;
		int32_t L_20 = V_3;
		bool L_21;
		L_21 = WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482(__this, L_18, L_19, L_20, (bool)1, /*hidden argument*/NULL);
		V_12 = L_21;
		bool L_22 = V_12;
		if (!L_22)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
		V_5 = (bool)1;
		V_8 = (bool)1;
		goto IL_00ad;
	}

IL_00a5:
	{
		bool L_24 = V_8;
		if (L_24)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_25 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_00ad:
	{
		bool L_26 = V_12;
		if (L_26)
		{
			goto IL_00b5;
		}
	}
	{
		bool L_27 = V_8;
		if (L_27)
		{
			goto IL_00be;
		}
	}

IL_00b5:
	{
		int32_t L_28 = V_3;
		int32_t L_29 = __this->get_height_1();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0089;
		}
	}

IL_00be:
	{
		int32_t L_30 = V_3;
		int32_t L_31 = __this->get_height_1();
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_00cf;
		}
	}
	{
		V_4 = (bool)1;
		goto IL_0159;
	}

IL_00cf:
	{
		V_13 = (bool)1;
		goto IL_00f8;
	}

IL_00d4:
	{
		int32_t L_32 = V_2;
		int32_t L_33 = V_3;
		int32_t L_34 = V_0;
		bool L_35;
		L_35 = WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482(__this, L_32, L_33, L_34, (bool)0, /*hidden argument*/NULL);
		V_13 = L_35;
		bool L_36 = V_13;
		if (!L_36)
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_37 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_37, (int32_t)1));
		V_5 = (bool)1;
		V_9 = (bool)1;
		goto IL_00f8;
	}

IL_00f0:
	{
		bool L_38 = V_9;
		if (L_38)
		{
			goto IL_00f8;
		}
	}
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_39, (int32_t)1));
	}

IL_00f8:
	{
		bool L_40 = V_13;
		if (L_40)
		{
			goto IL_0100;
		}
	}
	{
		bool L_41 = V_9;
		if (L_41)
		{
			goto IL_0104;
		}
	}

IL_0100:
	{
		int32_t L_42 = V_0;
		if ((((int32_t)L_42) >= ((int32_t)0)))
		{
			goto IL_00d4;
		}
	}

IL_0104:
	{
		int32_t L_43 = V_0;
		if ((((int32_t)L_43) >= ((int32_t)0)))
		{
			goto IL_010d;
		}
	}
	{
		V_4 = (bool)1;
		goto IL_0159;
	}

IL_010d:
	{
		V_14 = (bool)1;
		goto IL_0136;
	}

IL_0112:
	{
		int32_t L_44 = V_0;
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		bool L_47;
		L_47 = WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482(__this, L_44, L_45, L_46, (bool)1, /*hidden argument*/NULL);
		V_14 = L_47;
		bool L_48 = V_14;
		if (!L_48)
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_49 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_49, (int32_t)1));
		V_5 = (bool)1;
		V_10 = (bool)1;
		goto IL_0136;
	}

IL_012e:
	{
		bool L_50 = V_10;
		if (L_50)
		{
			goto IL_0136;
		}
	}
	{
		int32_t L_51 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_51, (int32_t)1));
	}

IL_0136:
	{
		bool L_52 = V_14;
		if (L_52)
		{
			goto IL_013e;
		}
	}
	{
		bool L_53 = V_10;
		if (L_53)
		{
			goto IL_0142;
		}
	}

IL_013e:
	{
		int32_t L_54 = V_2;
		if ((((int32_t)L_54) >= ((int32_t)0)))
		{
			goto IL_0112;
		}
	}

IL_0142:
	{
		int32_t L_55 = V_2;
		if ((((int32_t)L_55) >= ((int32_t)0)))
		{
			goto IL_014b;
		}
	}
	{
		V_4 = (bool)1;
		goto IL_0159;
	}

IL_014b:
	{
		bool L_56 = V_5;
		if (!L_56)
		{
			goto IL_0152;
		}
	}
	{
		V_6 = (bool)1;
	}

IL_0152:
	{
		bool L_57 = V_5;
		if (L_57)
		{
			goto IL_0036;
		}
	}

IL_0159:
	{
		bool L_58 = V_4;
		bool L_59 = V_6;
		if (!((int32_t)((int32_t)((((int32_t)L_58) == ((int32_t)0))? 1 : 0)&(int32_t)L_59)))
		{
			goto IL_024a;
		}
	}
	{
		int32_t L_60 = V_1;
		int32_t L_61 = V_0;
		V_15 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_60, (int32_t)L_61));
		V_16 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)NULL;
		V_20 = 1;
		goto IL_018f;
	}

IL_0173:
	{
		int32_t L_62 = V_0;
		int32_t L_63 = V_3;
		int32_t L_64 = V_20;
		int32_t L_65 = V_0;
		int32_t L_66 = V_20;
		int32_t L_67 = V_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_68;
		L_68 = WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A(__this, ((float)((float)L_62)), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_63, (int32_t)L_64)))), ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_65, (int32_t)L_66)))), ((float)((float)L_67)), /*hidden argument*/NULL);
		V_16 = L_68;
		int32_t L_69 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add((int32_t)L_69, (int32_t)1));
	}

IL_018f:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_70 = V_16;
		if (L_70)
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_71 = V_20;
		int32_t L_72 = V_15;
		if ((((int32_t)L_71) < ((int32_t)L_72)))
		{
			goto IL_0173;
		}
	}

IL_0199:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_73 = V_16;
		if (L_73)
		{
			goto IL_019f;
		}
	}
	{
		return (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)NULL;
	}

IL_019f:
	{
		V_17 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)NULL;
		V_21 = 1;
		goto IL_01c3;
	}

IL_01a7:
	{
		int32_t L_74 = V_0;
		int32_t L_75 = V_2;
		int32_t L_76 = V_21;
		int32_t L_77 = V_0;
		int32_t L_78 = V_21;
		int32_t L_79 = V_2;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_80;
		L_80 = WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A(__this, ((float)((float)L_74)), ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_75, (int32_t)L_76)))), ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_77, (int32_t)L_78)))), ((float)((float)L_79)), /*hidden argument*/NULL);
		V_17 = L_80;
		int32_t L_81 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_81, (int32_t)1));
	}

IL_01c3:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_82 = V_17;
		if (L_82)
		{
			goto IL_01cd;
		}
	}
	{
		int32_t L_83 = V_21;
		int32_t L_84 = V_15;
		if ((((int32_t)L_83) < ((int32_t)L_84)))
		{
			goto IL_01a7;
		}
	}

IL_01cd:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_85 = V_17;
		if (L_85)
		{
			goto IL_01d3;
		}
	}
	{
		return (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)NULL;
	}

IL_01d3:
	{
		V_18 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)NULL;
		V_22 = 1;
		goto IL_01f7;
	}

IL_01db:
	{
		int32_t L_86 = V_1;
		int32_t L_87 = V_2;
		int32_t L_88 = V_22;
		int32_t L_89 = V_1;
		int32_t L_90 = V_22;
		int32_t L_91 = V_2;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_92;
		L_92 = WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A(__this, ((float)((float)L_86)), ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_87, (int32_t)L_88)))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_89, (int32_t)L_90)))), ((float)((float)L_91)), /*hidden argument*/NULL);
		V_18 = L_92;
		int32_t L_93 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_93, (int32_t)1));
	}

IL_01f7:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_94 = V_18;
		if (L_94)
		{
			goto IL_0201;
		}
	}
	{
		int32_t L_95 = V_22;
		int32_t L_96 = V_15;
		if ((((int32_t)L_95) < ((int32_t)L_96)))
		{
			goto IL_01db;
		}
	}

IL_0201:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_97 = V_18;
		if (L_97)
		{
			goto IL_0207;
		}
	}
	{
		return (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)NULL;
	}

IL_0207:
	{
		V_19 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)NULL;
		V_23 = 1;
		goto IL_022b;
	}

IL_020f:
	{
		int32_t L_98 = V_1;
		int32_t L_99 = V_3;
		int32_t L_100 = V_23;
		int32_t L_101 = V_1;
		int32_t L_102 = V_23;
		int32_t L_103 = V_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_104;
		L_104 = WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A(__this, ((float)((float)L_98)), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_99, (int32_t)L_100)))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_101, (int32_t)L_102)))), ((float)((float)L_103)), /*hidden argument*/NULL);
		V_19 = L_104;
		int32_t L_105 = V_23;
		V_23 = ((int32_t)il2cpp_codegen_add((int32_t)L_105, (int32_t)1));
	}

IL_022b:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_106 = V_19;
		if (L_106)
		{
			goto IL_0235;
		}
	}
	{
		int32_t L_107 = V_23;
		int32_t L_108 = V_15;
		if ((((int32_t)L_107) < ((int32_t)L_108)))
		{
			goto IL_020f;
		}
	}

IL_0235:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_109 = V_19;
		if (L_109)
		{
			goto IL_023b;
		}
	}
	{
		return (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)NULL;
	}

IL_023b:
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_110 = V_19;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_111 = V_16;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_112 = V_18;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_113 = V_17;
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_114;
		L_114 = WhiteRectangleDetector_centerEdges_m627FE076E322023CD2E80416A1D2AE41CEBAA6D3(__this, L_110, L_111, L_112, L_113, /*hidden argument*/NULL);
		return L_114;
	}

IL_024a:
	{
		return (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)NULL;
	}
}
// ZXing.ResultPoint ZXing.Common.Detector.WhiteRectangleDetector::getBlackPointOnSegment(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, float ___aX0, float ___aY1, float ___bX2, float ___bY3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		float L_0 = ___aX0;
		float L_1 = ___aY1;
		float L_2 = ___bX2;
		float L_3 = ___bY3;
		float L_4;
		L_4 = MathUtils_distance_mFD733773EEBEB9B2DE497DB56F877DE8241841EF(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_5;
		L_5 = MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = ___bX2;
		float L_7 = ___aX0;
		int32_t L_8 = V_0;
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_6, (float)L_7))/(float)((float)((float)L_8))));
		float L_9 = ___bY3;
		float L_10 = ___aY1;
		int32_t L_11 = V_0;
		V_2 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_9, (float)L_10))/(float)((float)((float)L_11))));
		V_3 = 0;
		goto IL_005e;
	}

IL_0023:
	{
		float L_12 = ___aX0;
		int32_t L_13 = V_3;
		float L_14 = V_1;
		int32_t L_15;
		L_15 = MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60(((float)il2cpp_codegen_add((float)L_12, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_13)), (float)L_14)))), /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = ___aY1;
		int32_t L_17 = V_3;
		float L_18 = V_2;
		int32_t L_19;
		L_19 = MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60(((float)il2cpp_codegen_add((float)L_16, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_17)), (float)L_18)))), /*hidden argument*/NULL);
		V_5 = L_19;
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_20 = __this->get_image_0();
		int32_t L_21 = V_4;
		int32_t L_22 = V_5;
		NullCheck(L_20);
		bool L_23;
		L_23 = BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475(L_20, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_24 = V_4;
		int32_t L_25 = V_5;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_26 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_26, ((float)((float)L_24)), ((float)((float)L_25)), /*hidden argument*/NULL);
		return L_26;
	}

IL_005a:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_005e:
	{
		int32_t L_28 = V_3;
		int32_t L_29 = V_0;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0023;
		}
	}
	{
		return (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)NULL;
	}
}
// ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::centerEdges(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* WhiteRectangleDetector_centerEdges_m627FE076E322023CD2E80416A1D2AE41CEBAA6D3 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___y0, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___z1, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___x2, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___t3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___y0;
		NullCheck(L_0);
		float L_1;
		L_1 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single ZXing.ResultPoint::get_X() */, L_0);
		V_0 = L_1;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_2 = ___y0;
		NullCheck(L_2);
		float L_3;
		L_3 = VirtFuncInvoker0< float >::Invoke(5 /* System.Single ZXing.ResultPoint::get_Y() */, L_2);
		V_1 = L_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_4 = ___z1;
		NullCheck(L_4);
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single ZXing.ResultPoint::get_X() */, L_4);
		V_2 = L_5;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_6 = ___z1;
		NullCheck(L_6);
		float L_7;
		L_7 = VirtFuncInvoker0< float >::Invoke(5 /* System.Single ZXing.ResultPoint::get_Y() */, L_6);
		V_3 = L_7;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_8 = ___x2;
		NullCheck(L_8);
		float L_9;
		L_9 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single ZXing.ResultPoint::get_X() */, L_8);
		V_4 = L_9;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_10 = ___x2;
		NullCheck(L_10);
		float L_11;
		L_11 = VirtFuncInvoker0< float >::Invoke(5 /* System.Single ZXing.ResultPoint::get_Y() */, L_10);
		V_5 = L_11;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_12 = ___t3;
		NullCheck(L_12);
		float L_13;
		L_13 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single ZXing.ResultPoint::get_X() */, L_12);
		V_6 = L_13;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_14 = ___t3;
		NullCheck(L_14);
		float L_15;
		L_15 = VirtFuncInvoker0< float >::Invoke(5 /* System.Single ZXing.ResultPoint::get_Y() */, L_14);
		V_7 = L_15;
		float L_16 = V_0;
		int32_t L_17 = __this->get_width_2();
		if ((!(((float)L_16) < ((float)((float)((float)((float)((float)L_17))/(float)(2.0f)))))))
		{
			goto IL_00b1;
		}
	}
	{
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_18 = (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)(ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)SZArrayNew(ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E_il2cpp_TypeInfo_var, (uint32_t)4);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_19 = L_18;
		float L_20 = V_6;
		float L_21 = V_7;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_22 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_22, ((float)il2cpp_codegen_subtract((float)L_20, (float)(1.0f))), ((float)il2cpp_codegen_add((float)L_21, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_22);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_23 = L_19;
		float L_24 = V_2;
		float L_25 = V_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_26 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_26, ((float)il2cpp_codegen_add((float)L_24, (float)(1.0f))), ((float)il2cpp_codegen_add((float)L_25, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_26);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_27 = L_23;
		float L_28 = V_4;
		float L_29 = V_5;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_30 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_30, ((float)il2cpp_codegen_subtract((float)L_28, (float)(1.0f))), ((float)il2cpp_codegen_subtract((float)L_29, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_30);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_30);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_31 = L_27;
		float L_32 = V_0;
		float L_33 = V_1;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_34 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_34, ((float)il2cpp_codegen_add((float)L_32, (float)(1.0f))), ((float)il2cpp_codegen_subtract((float)L_33, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_34);
		return L_31;
	}

IL_00b1:
	{
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_35 = (ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)(ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E*)SZArrayNew(ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E_il2cpp_TypeInfo_var, (uint32_t)4);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_36 = L_35;
		float L_37 = V_6;
		float L_38 = V_7;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_39 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_39, ((float)il2cpp_codegen_add((float)L_37, (float)(1.0f))), ((float)il2cpp_codegen_add((float)L_38, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_39);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_40 = L_36;
		float L_41 = V_2;
		float L_42 = V_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_43 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_43, ((float)il2cpp_codegen_add((float)L_41, (float)(1.0f))), ((float)il2cpp_codegen_subtract((float)L_42, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(1), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_43);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_44 = L_40;
		float L_45 = V_4;
		float L_46 = V_5;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_47 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_47, ((float)il2cpp_codegen_subtract((float)L_45, (float)(1.0f))), ((float)il2cpp_codegen_add((float)L_46, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(2), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_47);
		ResultPointU5BU5D_tAA113C29B671C3535E4369C900DE9CC0F9E6C31E* L_48 = L_44;
		float L_49 = V_0;
		float L_50 = V_1;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_51 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_51, ((float)il2cpp_codegen_subtract((float)L_49, (float)(1.0f))), ((float)il2cpp_codegen_subtract((float)L_50, (float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(3), (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)L_51);
		return L_48;
	}
}
// System.Boolean ZXing.Common.Detector.WhiteRectangleDetector::containsBlackPoint(System.Int32,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482 (WhiteRectangleDetector_tB0C9FBB38D5127447FFE29420BF48D447F594AB8 * __this, int32_t ___a0, int32_t ___b1, int32_t ___fixed2, bool ___horizontal3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___horizontal3;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_1 = ___a0;
		V_0 = L_1;
		goto IL_001d;
	}

IL_0008:
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_2 = __this->get_image_0();
		int32_t L_3 = V_0;
		int32_t L_4 = ___fixed2;
		NullCheck(L_2);
		bool L_5;
		L_5 = BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475(L_2, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}

IL_0019:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_001d:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = ___b1;
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_0008;
		}
	}
	{
		goto IL_0040;
	}

IL_0023:
	{
		int32_t L_9 = ___a0;
		V_1 = L_9;
		goto IL_003c;
	}

IL_0027:
	{
		BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * L_10 = __this->get_image_0();
		int32_t L_11 = ___fixed2;
		int32_t L_12 = V_1;
		NullCheck(L_10);
		bool L_13;
		L_13 = BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475(L_10, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = ___b1;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0027;
		}
	}

IL_0040:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.BarcodeReader/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mECF73754D3E659397FBAC0C5B1ED9ADC8D5F5B91 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * L_0 = (U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE *)il2cpp_codegen_object_new(U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mC582116316DB1F8235AB27A5BE58C6B17EA9F24D(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void ZXing.BarcodeReader/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC582116316DB1F8235AB27A5BE58C6B17EA9F24D (U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// ZXing.LuminanceSource ZXing.BarcodeReader/<>c::<.cctor>b__9_0(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * U3CU3Ec_U3C_cctorU3Eb__9_0_m3A4863AE47CD642B2E628E5ACC01C1E566FF9949 (U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE * __this, Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___rawColor320, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_0 = ___rawColor320;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB * L_3 = (Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB *)il2cpp_codegen_object_new(Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_il2cpp_TypeInfo_var);
		Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.BarcodeReaderGeneric/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m12E9FA4BF9DE2A8179EEC86554CE1050DAE4D962 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * L_0 = (U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 *)il2cpp_codegen_object_new(U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m0EAA14E42607DB22B6B1DBA9E1EEBDD21948DB20(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void ZXing.BarcodeReaderGeneric/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0EAA14E42607DB22B6B1DBA9E1EEBDD21948DB20 (U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// ZXing.Binarizer ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_0(ZXing.LuminanceSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Binarizer_t9B50BEB9FC790D2DE58CB4F5757F97F8F26CD6F0 * U3CU3Ec_U3C_cctorU3Eb__40_0_mEED660C1CC3A9D0ECB0F0AA6B49C72FB2A0A6154 (U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * __this, LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * ___luminanceSource0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * L_0 = ___luminanceSource0;
		HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016 * L_1 = (HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016 *)il2cpp_codegen_object_new(HybridBinarizer_tE72FF3214B01E68A7BB7242E54E4287FEBA58016_il2cpp_TypeInfo_var);
		HybridBinarizer__ctor_mAFDF1DB99F02C26A219AFF9F31A442BBD6C1C1B2(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ZXing.LuminanceSource ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LuminanceSource_t010B8B8FDAF8E844D91CBEAFB0DF8E4A428C6167 * U3CU3Ec_U3C_cctorU3Eb__40_1_m68137642FE973A8B9853D6F468F4B46D3FE03F3B (U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___rawBytes0, int32_t ___width1, int32_t ___height2, int32_t ___format3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___rawBytes0;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		int32_t L_3 = ___format3;
		RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493 * L_4 = (RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493 *)il2cpp_codegen_object_new(RGBLuminanceSource_t3FC524096E2E26F95BAB6DD347E3497BC48AC493_il2cpp_TypeInfo_var);
		RGBLuminanceSource__ctor_m3521EC26C240CFA8FA8AF04A3AC09B97ABDB6CB3(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DigitContainer__ctor_m5593EDFE34483051D6AC430CDF0EB4778B0BD712 (DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_0 = (Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC*)(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC*)SZArrayNew(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC_il2cpp_TypeInfo_var, (uint32_t)((int32_t)200));
		__this->set_digits_0(L_0);
		return;
	}
}
// System.Int64 BigIntegerLibrary.Base10BigInteger/DigitContainer::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DigitContainer_get_Item_m25983F3EE4061CD2280C4B1B02281E04AD9728AC (DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_1 = NULL;
	{
		int32_t L_0 = ___index0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)5));
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_1 = __this->get_digits_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_4 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_5 = V_1;
		if (!L_5)
		{
			goto IL_0017;
		}
	}
	{
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_6 = V_1;
		int32_t L_7 = ___index0;
		NullCheck(L_6);
		int32_t L_8 = ((int32_t)((int32_t)L_7%(int32_t)((int32_t)32)));
		int64_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		return L_9;
	}

IL_0017:
	{
		return ((int64_t)((int64_t)0));
	}
}
// System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DigitContainer_set_Item_mD0206461D4866D3E4A36C686CFC3E07AD8343137 (DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F * __this, int32_t ___index0, int64_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_1 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B2_0 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B1_0 = NULL;
	{
		int32_t L_0 = ___index0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)5));
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_1 = __this->get_digits_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_4 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_5 = L_4;
		G_B1_0 = L_5;
		if (L_5)
		{
			G_B2_0 = L_5;
			goto IL_0022;
		}
	}
	{
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_6 = __this->get_digits_0();
		int32_t L_7 = V_0;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_8 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)SZArrayNew(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32));
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_9 = L_8;
		V_1 = L_9;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)L_9);
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_10 = V_1;
		G_B2_0 = L_10;
	}

IL_0022:
	{
		int32_t L_11 = ___index0;
		int64_t L_12 = ___value1;
		NullCheck(G_B2_0);
		(G_B2_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_11%(int32_t)((int32_t)32)))), (int64_t)L_12);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BigIntegerLibrary.BigInteger/DigitContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DigitContainer__ctor_m173ECCA1CA08D537D379FE12A8A87C6A8211ADE9 (DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_0 = (Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC*)(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC*)SZArrayNew(Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC_il2cpp_TypeInfo_var, (uint32_t)((int32_t)80));
		__this->set_digits_0(L_0);
		return;
	}
}
// System.Int64 BigIntegerLibrary.BigInteger/DigitContainer::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DigitContainer_get_Item_m9AC7D74C5F872C9ECD3C20A864B51E4DDE03302F (DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_1 = NULL;
	{
		int32_t L_0 = ___index0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)4));
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_1 = __this->get_digits_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_4 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_5 = V_1;
		if (!L_5)
		{
			goto IL_0017;
		}
	}
	{
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_6 = V_1;
		int32_t L_7 = ___index0;
		NullCheck(L_6);
		int32_t L_8 = ((int32_t)((int32_t)L_7%(int32_t)((int32_t)16)));
		int64_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		return L_9;
	}

IL_0017:
	{
		return ((int64_t)((int64_t)0));
	}
}
// System.Void BigIntegerLibrary.BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DigitContainer_set_Item_m19E1AB570C478C138EAD30A79AFAA8D5DDC2C86C (DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107 * __this, int32_t ___index0, int64_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_1 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B2_0 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B1_0 = NULL;
	{
		int32_t L_0 = ___index0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)4));
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_1 = __this->get_digits_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_4 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_5 = L_4;
		G_B1_0 = L_5;
		if (L_5)
		{
			G_B2_0 = L_5;
			goto IL_0022;
		}
	}
	{
		Int64U5BU5DU5BU5D_t5237BA0F53E06948ADC63C3B2D68D7EEC8CBD2AC* L_6 = __this->get_digits_0();
		int32_t L_7 = V_0;
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_8 = (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)SZArrayNew(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_9 = L_8;
		V_1 = L_9;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)L_9);
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_10 = V_1;
		G_B2_0 = L_10;
	}

IL_0022:
	{
		int32_t L_11 = ___index0;
		int64_t L_12 = ___value1;
		NullCheck(G_B2_0);
		(G_B2_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_11%(int32_t)((int32_t)16)))), (int64_t)L_12);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.DataMask/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m5C99D6D8DF32E075FFEFEA62D5D8BC0B7794FB0F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * L_0 = (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D *)il2cpp_codegen_object_new(U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m8EBA2F4F5936544E00D4494EBF428E326B8614DC(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void ZXing.QrCode.Internal.DataMask/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8EBA2F4F5936544E00D4494EBF428E326B8614DC (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_0(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_0_m9276BBEA1A2866654015CF183BA1EA34634C2C53 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1))&(int32_t)1))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_1(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_1_mC6827C7C353AFD1A41149FB722EC788073198819 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_2(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_2_mF88732B03F67C94E33EF8EC66053FF16275BE019 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)L_0%(int32_t)3))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_3(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_3_m518AF36E4A955D6AC2AC63AD6C62148136489428 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1))%(int32_t)3))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_4(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_4_mDE0A722B0F44EC9EA4960179BB791ED07E9344C0 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((uint32_t)L_0>>1)), (int32_t)((int32_t)((int32_t)L_1/(int32_t)3))))&(int32_t)1))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_5(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_5_m4B2C1B6AD542F0CD515AE18E5BDFB5355566C7EF (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_1))%(int32_t)6))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_6(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_6_mCD855E2C5E74C424E0E278B509172854DA829CB1 (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_1))%(int32_t)6))) < ((int32_t)3))? 1 : 0);
	}
}
// System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_7(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3C_cctorU3Eb__2_7_mC3E1E3D988091B923B9BD0656968C29A7627142B (U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		int32_t L_2 = ___i0;
		int32_t L_3 = ___j1;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)), (int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_2, (int32_t)L_3))%(int32_t)3))))&(int32_t)1))) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_X()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CXU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void ZXing.Aztec.Internal.Detector/Point::set_X(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CXU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_Y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CYU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void ZXing.Aztec.Internal.Detector/Point::set_Y(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CYU3Ek__BackingField_1(L_0);
		return;
	}
}
// ZXing.ResultPoint ZXing.Aztec.Internal.Detector/Point::toResultPoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * Point_toResultPoint_m01A8B2007545A98090BD3DC9892954E6F0DD5E7E (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0;
		L_0 = Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660_inline(__this, /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605_inline(__this, /*hidden argument*/NULL);
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_2 = (ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 *)il2cpp_codegen_object_new(ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636_il2cpp_TypeInfo_var);
		ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765(L_2, ((float)((float)L_0)), ((float)((float)L_1)), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ZXing.Aztec.Internal.Detector/Point::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Point__ctor_m3D624C53D3AE1A834D9F70F586FB0F4C025E3974 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___x0;
		Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408_inline(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___y1;
		Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String ZXing.Aztec.Internal.Detector/Point::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Point_ToString_m8D03498487474567D310763DA959A40D675373B7 (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = L_1;
		int32_t L_3;
		L_3 = Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660_inline(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_2;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_6;
		int32_t L_8;
		L_8 = Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605_inline(__this, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_7;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5);
		String_t* L_12;
		L_12 = String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306(L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_From()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = __this->get_U3CFromU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_From(ZXing.ResultPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___value0;
		__this->set_U3CFromU3Ek__BackingField_0(L_0);
		return;
	}
}
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_To()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = __this->get_U3CToU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_To(ZXing.ResultPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___value0;
		__this->set_U3CToU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_Transitions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTransitionsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_Transitions(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTransitionsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::.ctor(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPointsAndTransitions__ctor_m93CA3CE4208D5880629A3B07C230100E1CC0A408 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___from0, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___to1, int32_t ___transitions2, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___from0;
		ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985_inline(__this, L_0, /*hidden argument*/NULL);
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_1 = ___to1;
		ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D_inline(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___transitions2;
		ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927_inline(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ResultPointsAndTransitions_ToString_m6EAA6FB9BB8C6BE81D5BE8D61E6376D6238F1735 (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_2;
		L_2 = ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4 = L_3;
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_5;
		L_5 = ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_6;
		int32_t L_8;
		L_8 = ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172_inline(__this, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_10);
		String_t* L_11;
		L_11 = String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306(L_7, /*hidden argument*/NULL);
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::Compare(ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions,ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ResultPointsAndTransitionsComparator_Compare_m9DCE83B6D7A89A8E5F28AB71AB32820E5AFC45E4 (ResultPointsAndTransitionsComparator_tDC383B70BD00047FA5D5D536E6799F1F4F529561 * __this, ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * ___o10, ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * ___o21, const RuntimeMethod* method)
{
	{
		ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * L_0 = ___o10;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172_inline(L_0, /*hidden argument*/NULL);
		ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * L_2 = ___o21;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_3));
	}
}
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResultPointsAndTransitionsComparator__ctor_mEA8E6FA2CD0BD25E539D9E248043981B9E93A4CB (ResultPointsAndTransitionsComparator_tDC383B70BD00047FA5D5D536E6799F1F4F529561 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CenterComparator__ctor_mECCDC684AC2E584356017B90C371F036F4EFEA1B (CenterComparator_t95A3068152BE5AADC25E56F759AD55997F96B5CE * __this, float ___f0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		float L_0 = ___f0;
		__this->set_average_0(L_0);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CenterComparator_Compare_m813791C4BB6556E6BA2628F3CF6FA3DE5C63364C (CenterComparator_t95A3068152BE5AADC25E56F759AD55997F96B5CE * __this, FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * ___x0, FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_0 = ___y1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline(L_0, /*hidden argument*/NULL);
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_2 = ___x0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0042;
		}
	}
	{
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_4 = ___y1;
		NullCheck(L_4);
		float L_5;
		L_5 = FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_average_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_7;
		L_7 = fabsf(((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)));
		V_0 = L_7;
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_8 = ___x0;
		NullCheck(L_8);
		float L_9;
		L_9 = FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_average_0();
		float L_11;
		L_11 = fabsf(((float)il2cpp_codegen_subtract((float)L_9, (float)L_10)));
		V_1 = L_11;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((((float)L_12) < ((float)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		float L_14 = V_0;
		float L_15 = V_1;
		if ((((float)L_14) > ((float)L_15)))
		{
			goto IL_003e;
		}
	}
	{
		return 0;
	}

IL_003e:
	{
		return (-1);
	}

IL_0040:
	{
		return 1;
	}

IL_0042:
	{
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_16 = ___y1;
		NullCheck(L_16);
		int32_t L_17;
		L_17 = FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline(L_16, /*hidden argument*/NULL);
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_18 = ___x0;
		NullCheck(L_18);
		int32_t L_19;
		L_19 = FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline(L_18, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)L_19));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FurthestFromAverageComparator__ctor_m4AE0185AED1653F0E0F2B6B48A79202F3D7428C3 (FurthestFromAverageComparator_t5EA62ECFC96D9750AD52441A424D004C060208E0 * __this, float ___f0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		float L_0 = ___f0;
		__this->set_average_0(L_0);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FurthestFromAverageComparator_Compare_m30841DD8A686609796601BCBD6AFD4FD0FECE93F (FurthestFromAverageComparator_t5EA62ECFC96D9750AD52441A424D004C060208E0 * __this, FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * ___x0, FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_0 = ___y1;
		NullCheck(L_0);
		float L_1;
		L_1 = FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline(L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_average_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = fabsf(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		V_0 = L_3;
		FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * L_4 = ___x0;
		NullCheck(L_4);
		float L_5;
		L_5 = FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_average_0();
		float L_7;
		L_7 = fabsf(((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)));
		V_1 = L_7;
		float L_8 = V_0;
		float L_9 = V_1;
		if ((((float)L_8) < ((float)L_9)))
		{
			goto IL_0032;
		}
	}
	{
		float L_10 = V_0;
		float L_11 = V_1;
		if ((((float)L_10) > ((float)L_11)))
		{
			goto IL_0030;
		}
	}
	{
		return 0;
	}

IL_0030:
	{
		return 1;
	}

IL_0032:
	{
		return (-1);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.Datamatrix.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECB__ctor_mFB2892A387586B52357CB5496BC67ABD357DED1B (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * __this, int32_t ___count0, int32_t ___dataCodewords1, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		__this->set_count_0(L_0);
		int32_t L_1 = ___dataCodewords1;
		__this->set_dataCodewords_1(L_1);
		return;
	}
}
// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_Count_m36046552D97F93739FF93F152833AAEA321D920A (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_DataCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_DataCodewords_m93C7355C18323CC41766125F732C858A0725FB1A (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_dataCodewords_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECBlocks__ctor_m5A1EC8BBA032650CDF7599CCA6C47E433E4ACB6B (ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4 * __this, int32_t ___ecCodewords0, ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * ___ecBlocks1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___ecCodewords0;
		__this->set_ecCodewords_0(L_0);
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_1 = (ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B*)(ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B*)SZArrayNew(ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B_il2cpp_TypeInfo_var, (uint32_t)1);
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_2 = L_1;
		ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * L_3 = ___ecBlocks1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A *)L_3);
		__this->set__ecBlocksValue_1(L_2);
		return;
	}
}
// System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB,ZXing.Datamatrix.Internal.Version/ECB)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECBlocks__ctor_m36FA0B4968370547116ECD37E8CD4723277379AE (ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4 * __this, int32_t ___ecCodewords0, ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * ___ecBlocks11, ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * ___ecBlocks22, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___ecCodewords0;
		__this->set_ecCodewords_0(L_0);
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_1 = (ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B*)(ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B*)SZArrayNew(ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B_il2cpp_TypeInfo_var, (uint32_t)2);
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_2 = L_1;
		ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * L_3 = ___ecBlocks11;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A *)L_3);
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_4 = L_2;
		ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A * L_5 = ___ecBlocks22;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (ECB_tA01BCDD5399245FD7873FEDC3A316D9741185B8A *)L_5);
		__this->set__ecBlocksValue_1(L_4);
		return;
	}
}
// System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_ECCodewords_mA046C3872AF9D771F6679BC80E18D4DB07927BAA (ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ecCodewords_0();
		return L_0;
	}
}
// ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECBlocksValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* ECBlocks_get_ECBlocksValue_m2932E2682570E3954B2C9DAC28850342777821A8 (ECBlocks_t4D7C9E393C7DF5484FAE15BC22704E20D122D7F4 * __this, const RuntimeMethod* method)
{
	{
		ECBU5BU5D_t4281730425E34BCD3B8E7768794A8CFA68A5557B* L_0 = __this->get__ecBlocksValue_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784 (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, int32_t ___count0, int32_t ___dataCodewords1, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		__this->set_count_0(L_0);
		int32_t L_1 = ___dataCodewords1;
		__this->set_dataCodewords_1(L_1);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version/ECB::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567 (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version/ECB::get_DataCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_dataCodewords_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version/ECB[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017 (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, int32_t ___ecCodewordsPerBlock0, ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ___ecBlocks1, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___ecCodewordsPerBlock0;
		__this->set_ecCodewordsPerBlock_0(L_0);
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_1 = ___ecBlocks1;
		__this->set_ecBlocks_1(L_1);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_ECCodewordsPerBlock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ecCodewordsPerBlock_0();
		return L_0;
	}
}
// ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::getECBlocks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3 (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method)
{
	{
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_0 = __this->get_ecBlocks_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA_inline (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ecCodewordsPerBlock_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3_inline (ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0 * __this, const RuntimeMethod* method)
{
	{
		ECBU5BU5D_t9BBE9D248176D164B0E89E687E1EB508AD93C73C* L_0 = __this->get_ecBlocks_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567_inline (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F_inline (ECB_t21A54092AE86393FFF1F286B70E027F59ED07064 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_dataCodewords_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorCorrectionLevel_ordinal_mACCE779D514AF2084CA531514F85028357ABE219_inline (ErrorCorrectionLevel_tD286C5D1C475E10A4D6156DC554E2FB3A2AD8045 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ordinal_Renamed_Field_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8_inline (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_width_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433_inline (BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_height_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CXU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CYU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CXU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5_inline (Point_t671952EEFD2B77F47039C983FD785221F65BC8F2 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CYU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___value0;
		__this->set_U3CFromU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ___value0, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = ___value0;
		__this->set_U3CToU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTransitionsU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = __this->get_U3CFromU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		ResultPoint_t7E0915794AA6F08842E0A1B2DFAA36B36A24E636 * L_0 = __this->get_U3CToU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172_inline (ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTransitionsU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD_inline (FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB_inline (FinderPattern_t6935D9A8314D60E68277E3FFDA01FE6CE9150134 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_estimatedModuleSize_5();
		return L_0;
	}
}
