﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct  AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct  FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.ParamArrayAttribute
struct  ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void zxing_unity_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x7A\x78\x69\x6E\x67\x2E\x73\x6C\x34\x2E\x74\x65\x73\x74\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x39\x34\x30\x30\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x34\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x34\x35\x66\x62\x34\x61\x63\x62\x39\x35\x31\x63\x64\x35\x65\x33\x32\x61\x65\x39\x65\x35\x33\x61\x38\x32\x62\x31\x65\x36\x32\x62\x30\x32\x39\x34\x39\x35\x37\x62\x37\x33\x37\x34\x36\x31\x31\x38\x65\x61\x62\x30\x64\x39\x34\x65\x34\x39\x61\x35\x34\x36\x33\x66\x34\x39\x39\x34\x62\x30\x36\x30\x34\x33\x31\x62\x36\x33\x38\x39\x64\x66\x66\x31\x33\x38\x64\x63\x31\x62\x36\x39\x33\x32\x30\x30\x36\x62\x65\x32\x66\x65\x30\x30\x37\x61\x38\x32\x31\x37\x36\x32\x36\x31\x65\x64\x38\x38\x64\x33\x64\x39\x64\x64\x64\x64\x30\x65\x65\x35\x36\x38\x39\x38\x38\x62\x62\x39\x36\x66\x33\x36\x36\x37\x61\x61\x64\x38\x34\x63\x32\x62\x65\x65\x31\x33\x39\x35\x66\x39\x65\x63\x32\x33\x63\x66\x36\x31\x64\x64\x66\x35\x61\x37\x63\x61\x35\x34\x30\x65\x33\x34\x62\x34\x61\x32\x65\x34\x31\x39\x32\x30\x63\x36\x61\x32\x30\x34\x64\x33\x39\x63\x66\x37\x38\x65\x62\x66\x37\x64\x34\x33\x33\x31\x63\x38\x32\x33\x61\x39\x36\x63\x62\x33\x36\x61\x31\x64\x35\x62\x63\x31\x61\x38\x63\x36\x36\x36\x64\x62\x31\x65\x64\x65\x66\x34\x61\x62\x36\x37\x63\x31\x36\x37\x64\x63"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x7A\x78\x69\x6E\x67\x2E\x73\x6C\x34\x2E\x74\x65\x73\x74\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x31\x34\x30\x31\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x38\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x30\x31\x34\x63\x39\x61\x30\x31\x39\x35\x36\x66\x31\x33\x61\x33\x33\x39\x31\x33\x30\x36\x31\x36\x34\x37\x33\x66\x36\x39\x66\x39\x37\x35\x65\x30\x38\x36\x64\x39\x61\x33\x61\x35\x36\x32\x37\x38\x39\x33\x36\x62\x31\x32\x63\x34\x38\x63\x61\x34\x35\x61\x34\x64\x64\x66\x65\x65\x30\x35\x63\x32\x31\x63\x64\x63\x32\x32\x61\x65\x64\x64\x38\x34\x65\x39\x34\x36\x38\x32\x38\x33\x31\x32\x37\x61\x32\x30\x62\x62\x61\x34\x37\x36\x31\x63\x34\x65\x30\x64\x39\x38\x33\x36\x36\x32\x33\x66\x63\x39\x39\x31\x64\x35\x36\x32\x61\x35\x30\x38\x38\x34\x35\x66\x65\x33\x31\x34\x61\x34\x33\x35\x62\x64\x36\x63\x36\x66\x66\x34\x62\x30\x62\x31\x64\x37\x61\x31\x34\x31\x65\x66\x39\x33\x64\x63\x31\x63\x36\x32\x32\x35\x32\x34\x33\x38\x37\x32\x33\x66\x30\x66\x39\x33\x36\x36\x38\x32\x38\x38\x36\x37\x33\x65\x61\x36\x30\x34\x32\x65\x35\x38\x33\x62\x30\x65\x65\x64\x30\x34\x30\x65\x33\x36\x37\x33\x61\x63\x61\x35\x38\x34\x66\x39\x36\x64\x34\x64\x63\x61\x31\x39\x39\x33\x37\x66\x62\x65\x64\x33\x30\x65\x36\x63\x64\x33\x63\x30\x34\x30\x39\x64\x62\x38\x32\x64\x35\x63\x35\x64\x32\x30\x36\x37\x37\x31\x30\x64\x38\x64\x38\x36\x65\x30\x30\x38\x34\x34\x37\x32\x30\x31\x64\x39\x39\x32\x33\x38\x62\x39\x34\x64\x39\x31\x31\x37\x31\x62\x62\x30\x65\x64\x66\x33\x65\x38\x35\x34\x39\x38\x35\x36\x39\x33\x30\x35\x31\x62\x61\x35\x31\x36\x37\x63\x61\x36\x61\x65\x36\x35\x30\x61\x63\x61\x35\x64\x64\x36\x35\x34\x37\x31\x64\x36\x38\x38\x33\x35\x64\x62\x30\x30\x63\x65\x31\x37\x32\x38\x63\x35\x38\x63\x37\x62\x62\x66\x39\x61\x35\x64\x31\x35\x32\x66\x34\x39\x31\x31\x32\x33\x63\x61\x66\x39\x63\x30\x66\x36\x38\x36\x64\x63\x34\x65\x34\x38\x65\x31\x65\x66\x36\x33\x65\x61\x66\x37\x33\x38\x61\x31\x32\x62\x33\x37\x37\x31\x63\x32\x34\x64\x35\x39\x35\x63\x63\x35\x61\x35\x62\x35\x64\x61\x66\x32\x63\x63\x37\x36\x31\x31\x37\x35\x36\x65\x39\x62\x61\x33\x63\x63\x38\x39\x66\x30\x38\x66\x62\x39\x61\x64\x66\x33\x39\x36\x38\x35\x62\x64\x35\x33\x35\x36\x38\x35\x38\x63\x30\x31\x30\x65\x62\x39\x61\x61\x38\x61\x37\x36\x37\x65\x35\x65\x66\x30\x32\x30\x34\x30\x38\x65\x30\x63\x39\x37\x34\x36\x63\x62\x62\x35\x61\x38"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x7A\x78\x69\x6E\x67\x2E\x74\x65\x73\x74\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x31\x34\x30\x31\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x38\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x30\x31\x34\x63\x39\x61\x30\x31\x39\x35\x36\x66\x31\x33\x61\x33\x33\x39\x31\x33\x30\x36\x31\x36\x34\x37\x33\x66\x36\x39\x66\x39\x37\x35\x65\x30\x38\x36\x64\x39\x61\x33\x61\x35\x36\x32\x37\x38\x39\x33\x36\x62\x31\x32\x63\x34\x38\x63\x61\x34\x35\x61\x34\x64\x64\x66\x65\x65\x30\x35\x63\x32\x31\x63\x64\x63\x32\x32\x61\x65\x64\x64\x38\x34\x65\x39\x34\x36\x38\x32\x38\x33\x31\x32\x37\x61\x32\x30\x62\x62\x61\x34\x37\x36\x31\x63\x34\x65\x30\x64\x39\x38\x33\x36\x36\x32\x33\x66\x63\x39\x39\x31\x64\x35\x36\x32\x61\x35\x30\x38\x38\x34\x35\x66\x65\x33\x31\x34\x61\x34\x33\x35\x62\x64\x36\x63\x36\x66\x66\x34\x62\x30\x62\x31\x64\x37\x61\x31\x34\x31\x65\x66\x39\x33\x64\x63\x31\x63\x36\x32\x32\x35\x32\x34\x33\x38\x37\x32\x33\x66\x30\x66\x39\x33\x36\x36\x38\x32\x38\x38\x36\x37\x33\x65\x61\x36\x30\x34\x32\x65\x35\x38\x33\x62\x30\x65\x65\x64\x30\x34\x30\x65\x33\x36\x37\x33\x61\x63\x61\x35\x38\x34\x66\x39\x36\x64\x34\x64\x63\x61\x31\x39\x39\x33\x37\x66\x62\x65\x64\x33\x30\x65\x36\x63\x64\x33\x63\x30\x34\x30\x39\x64\x62\x38\x32\x64\x35\x63\x35\x64\x32\x30\x36\x37\x37\x31\x30\x64\x38\x64\x38\x36\x65\x30\x30\x38\x34\x34\x37\x32\x30\x31\x64\x39\x39\x32\x33\x38\x62\x39\x34\x64\x39\x31\x31\x37\x31\x62\x62\x30\x65\x64\x66\x33\x65\x38\x35\x34\x39\x38\x35\x36\x39\x33\x30\x35\x31\x62\x61\x35\x31\x36\x37\x63\x61\x36\x61\x65\x36\x35\x30\x61\x63\x61\x35\x64\x64\x36\x35\x34\x37\x31\x64\x36\x38\x38\x33\x35\x64\x62\x30\x30\x63\x65\x31\x37\x32\x38\x63\x35\x38\x63\x37\x62\x62\x66\x39\x61\x35\x64\x31\x35\x32\x66\x34\x39\x31\x31\x32\x33\x63\x61\x66\x39\x63\x30\x66\x36\x38\x36\x64\x63\x34\x65\x34\x38\x65\x31\x65\x66\x36\x33\x65\x61\x66\x37\x33\x38\x61\x31\x32\x62\x33\x37\x37\x31\x63\x32\x34\x64\x35\x39\x35\x63\x63\x35\x61\x35\x62\x35\x64\x61\x66\x32\x63\x63\x37\x36\x31\x31\x37\x35\x36\x65\x39\x62\x61\x33\x63\x63\x38\x39\x66\x30\x38\x66\x62\x39\x61\x64\x66\x33\x39\x36\x38\x35\x62\x64\x35\x33\x35\x36\x38\x35\x38\x63\x30\x31\x30\x65\x62\x39\x61\x61\x38\x61\x37\x36\x37\x65\x35\x65\x66\x30\x32\x30\x34\x30\x38\x65\x30\x63\x39\x37\x34\x36\x63\x62\x62\x35\x61\x38"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[3];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x30\x2E\x31\x36\x2E\x32\x2E\x30"), NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[5];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x45\x43\x45\x33\x41\x42\x37\x34\x2D\x39\x44\x44\x31\x2D\x34\x43\x46\x42\x2D\x39\x44\x34\x38\x2D\x46\x43\x42\x46\x42\x33\x30\x45\x30\x36\x44\x36"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[6];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[7];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x32\x30\x31\x32"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[8];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x58\x69\x6E\x67\x2E\x4E\x65\x74"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[9];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x58\x69\x6E\x67\x2E\x4E\x65\x74\x20\x44\x65\x76\x65\x6C\x6F\x70\x6D\x65\x6E\x74"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[10];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x70\x6F\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x6A\x61\x76\x61\x20\x62\x61\x73\x65\x64\x20\x62\x61\x72\x63\x6F\x64\x65\x20\x73\x63\x61\x6E\x6E\x69\x6E\x67\x20\x6C\x69\x62\x72\x61\x72\x79\x20\x66\x6F\x72\x20\x2E\x6E\x65\x74\x20\x28\x6A\x61\x76\x61\x20\x7A\x78\x69\x6E\x67\x20\x31\x36\x2E\x30\x36\x2E\x32\x30\x31\x37\x20\x31\x34\x3A\x35\x31\x3A\x30\x32\x29"), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[11];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x7A\x78\x69\x6E\x67\x2E\x6E\x65\x74\x20\x66\x6F\x72\x20\x2E\x6E\x65\x74\x20\x33\x2E\x35\x20\x61\x6E\x64\x20\x75\x6E\x69\x74\x79\x20\x28\x77\x2F\x6F\x20\x53\x79\x73\x74\x65\x6D\x2E\x44\x72\x61\x77\x69\x6E\x67\x29"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[12];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[13];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[14];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x7A\x78\x69\x6E\x67\x2E\x74\x65\x73\x74\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x39\x34\x30\x30\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x34\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x34\x35\x66\x62\x34\x61\x63\x62\x39\x35\x31\x63\x64\x35\x65\x33\x32\x61\x65\x39\x65\x35\x33\x61\x38\x32\x62\x31\x65\x36\x32\x62\x30\x32\x39\x34\x39\x35\x37\x62\x37\x33\x37\x34\x36\x31\x31\x38\x65\x61\x62\x30\x64\x39\x34\x65\x34\x39\x61\x35\x34\x36\x33\x66\x34\x39\x39\x34\x62\x30\x36\x30\x34\x33\x31\x62\x36\x33\x38\x39\x64\x66\x66\x31\x33\x38\x64\x63\x31\x62\x36\x39\x33\x32\x30\x30\x36\x62\x65\x32\x66\x65\x30\x30\x37\x61\x38\x32\x31\x37\x36\x32\x36\x31\x65\x64\x38\x38\x64\x33\x64\x39\x64\x64\x64\x64\x30\x65\x65\x35\x36\x38\x39\x38\x38\x62\x62\x39\x36\x66\x33\x36\x36\x37\x61\x61\x64\x38\x34\x63\x32\x62\x65\x65\x31\x33\x39\x35\x66\x39\x65\x63\x32\x33\x63\x66\x36\x31\x64\x64\x66\x35\x61\x37\x63\x61\x35\x34\x30\x65\x33\x34\x62\x34\x61\x32\x65\x34\x31\x39\x32\x30\x63\x36\x61\x32\x30\x34\x64\x33\x39\x63\x66\x37\x38\x65\x62\x66\x37\x64\x34\x33\x33\x31\x63\x38\x32\x33\x61\x39\x36\x63\x62\x33\x36\x61\x31\x64\x35\x62\x63\x31\x61\x38\x63\x36\x36\x36\x64\x62\x31\x65\x64\x65\x66\x34\x61\x62\x36\x37\x63\x31\x36\x37\x64\x63"), NULL);
	}
}
static void DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void BarcodeFormat_t2DD630001207452C27CD9C9C5B8D9C09C96C1B3E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void BarcodeReader_t1234797046EE86EBB37309A73484BAA2F45A3012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_ResultFound(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_U3CAutoRotateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_U3CTryInvertedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_get_AutoRotate_mA7031123B8C71BC186B614012ED67D719F5BF2B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_set_AutoRotate_mA3F321343B91733403B13B78DB93FA7B5C09EFEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_get_TryInverted_m7CEAFFF9350F50E5EDA598D6CABE2129ECF32AEA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_set_TryInverted_m9094235165E632A4E8F4819B8149057B3B53CD5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m5F25CC854255C19BB7387C999AFF6F72224D9641(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CRawBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CResultPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CBarcodeFormatU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CResultMetadataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CNumBitsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_Text_m855527BB9C7057F702BDABD42E7DECC1572721EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_Text_mCE77595E7629B6DC028B96FC6B1001D3553B4827(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_RawBytes_m37EADC349C16DD0F5A01A9153DFFBAE55BC6D708(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_RawBytes_mAE2D1DC82194151BE42AB1B9D3A9420880111142(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_ResultPoints_mB6717EF744141D983630A17EDACB39C11BE452FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_ResultPoints_mDC64CD4147E895472D1C2417141086D9F8EC15E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_BarcodeFormat_m446306E9DC37F14EB447E0C711A55A45D5514AA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_BarcodeFormat_m66740266B609E88E7C1A8496F4369AA9B17F3733(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_ResultMetadata_m82489B9ED570D7F50058B680DCEF0D126448129C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_ResultMetadata_mD48ACD9D2F3A263E6E705391CDCA38E9B2958CA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_Timestamp_m9399B76A449D904D5773C5463656BE35040F70D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_NumBits_m0EB6FB28263F1E58FB521CB0E151B21A344D74DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_CustomAttributesCacheGenerator_Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_CustomAttributesCacheGenerator_Color32LuminanceSource_SetPixels_m9BCC155B50914AA2DD6A19F3DDEFF89101FE21E4(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_get_Name_mDD10EE936C24F81A4E3C5BB7A483853C5F22E448(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_set_Name_m75EFECA9B152C3114257443AD3CDD16A9FD5A789(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_set_Bits_m050E659D3B05D670BCE285DE9125B69816015542(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_CustomAttributesCacheGenerator_Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F____ecBlocks2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_CustomAttributesCacheGenerator_ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017____ecBlocks1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CSegmentIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CFileIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3COptionalDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CIsLastSegmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_SegmentIndex_m6792A85EE271BFA6063BE4B2A3990743B73C1093(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_FileId_m6A79CC9F4624FE95AB7A870D742357FE0011F520(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_get_OptionalData_mEFBB80130FA8AE0C7E7657790368E3646220FD7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_OptionalData_m0D81D688400517D1D65B3E36CF5B9F996B70BC6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_IsLastSegment_m0FC2FB8C14FA37900B7B57397602F5D9DA1E64B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CColumnCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CErrorCorrectionLevelU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountUpperU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountLowerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_ColumnCount_m4A9D3AD55F9D68456B8E2DB771835EA55CCDE6C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_ColumnCount_m176BF152AEF28C1A672DAB8A5BD8278AB083A0DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_ErrorCorrectionLevel_m84BB571AFA2AE2423ED8042DF14BC1DB78EF538C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_ErrorCorrectionLevel_m815B1DAB46579F73F17ACBF31E6E39703FD05627(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCountUpper_m03D2B4FC5647BF341DF22E2208586CFE20140301(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCountUpper_m9864EFB99D21684C54F44F9987DB9E1A67A45756(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCountLower_m01D867E4FAF15591DB9ADCA2681A4F524909528D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCountLower_m8691F9A95CC8158F00B4D521E9A72E965081599E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCount_mACD2C5E04E6FE23404844F1C4B24B33642C9E93F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCount_m757D5A46753DA91F51B97CA777A2A30826FCDE85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CTopLeftU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CTopRightU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CBottomLeftU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CBottomRightU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMinXU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMaxXU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMinYU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMaxYU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_TopLeft_m5CB4B6EC44B499CD0BA203C9B3B6F8933121724C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_TopLeft_mCCCE09F3624FA382B74AC8E0A199AA6D8264FD35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_TopRight_m5A6F9AD58CD8CA92AEA0C76D84C763EECFA40CD3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_TopRight_m10405141D9546413D3A065768C78A4DAC520929A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_BottomLeft_m9A8E6255BF210761723976D7C38CF7D118EDFE8F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_BottomLeft_mB4C5B675177B796E7E681F59DB76A625A5620B68(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_BottomRight_m2035AC5736DC155C43192B4ECDAB8A4F57EEBB94(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_BottomRight_m8C954B0A293AC7B39084CA049E9B386F712A079B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MinX_m5F83E51CACD2BBA44036A99706C101CF6016840B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MinX_mBA6A3362975DA7BD7D6A0F04DD13AE8FE75931C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MaxX_m3AE590ECC9CEA0B6DF5517CD8C3D182D95511712(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MaxX_m3119F44A66FDB33CF14D4B1228679C148B4D82D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MinY_m1FFEDA362F3B77F1DFE9385F7C3EF419DF0EC394(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MinY_m595E30BC39CD5ADA2D82C0994B0923BB4C4FF8F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MaxY_m5CFBC7ECF98C8659323F1A69392E297A82AB285B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MaxY_m4F13753AEAEBD5CF67FA851DDCD4D2C586423D0F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CStartXU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CEndXU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CBucketU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CRowNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_StartX_m7142A4213335F1F993CA7802368A5AAD32672949(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_StartX_mEFB95587ADF18A1371DA69054C6D5600B61B8AD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_EndX_m76D22FBE3E14FCC8FA93068BE2CE2CB08E4E8998(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_EndX_m86CF8AFE1193EC119585AFBA4A29B1B80237983C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_Bucket_mDC1B59D5B2EF3B9C57825CFA4209B3D20E3D6FB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_Bucket_m4ACB8A7E1675CCDAA8AB38072CB91E78BA8C01E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_Value_m6E938EB77D32B6E926D4A1DE4D4A14308EBDCEA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_Value_m2704DED6B54EC01D56145587EE2FFEEE3704A85E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_RowNumber_mE9ABCE1EC3AF7CF50363CD1C02C5703C6498F2D0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_RowNumber_m099810CF7CB72780C60AA7C1ABA4FA0C8879F935(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CMetadataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CDetectionResultColumnsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CBoxU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CColumnCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_Metadata_m28222ACC510CDDE151005C522558517F54F11E31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_Metadata_mE75F8B8FF73BE8E6949821C378222EA306F929AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_DetectionResultColumns_mE527F5B2DC9F258C3A2FDDF03AF1E6C571E3DB68(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_DetectionResultColumns_m65CE606CF83520C680E1CFEC8AB98D8147807C9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_Box_m787AE2F48FF618EAD3AA5AC69F0E5CBA0557C03D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_Box_m151695A38E2C002A537D09CCB87DAC7DDAF08E13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_ColumnCount_mB43FEE1A3282C41D121B3A5C8220F6684F0266B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_ColumnCount_mE0107ECCF65B7CAA5B790C6F8BF4FE68F330DE61(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_U3CBoxU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_U3CCodewordsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_get_Box_m28DE039B3B068F05F1043D74471A84948310A5D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_set_Box_m3949C81CBD20F30155D8B7A5B720A809AABCA29B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_get_Codewords_m8C5E2EDFE024EBB4833DAB8E33725D2E2906790A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_set_Codewords_m507D740FE136F6E201194B24CF7F7BD80CE0CE13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_U3CIsLeftU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_DetectionResultRowIndicatorColumn_get_IsLeft_mC2C89A027BF107A96DDC704A5C7F5CD9998B5450(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_DetectionResultRowIndicatorColumn_set_IsLeft_m0A5451068AE892E9E3180FD80840869545C3DEB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_get_Bits_mF582F132EF48BC1E687742C76A1004054C51BCF8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_set_Bits_mBF16DD37F7407142F2C31B184AC52A326E630B3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_get_Points_mE94F3CA82AE031E694E202F8C6151AEF086E5BDA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_set_Points_mA06AC47807901C9A6A34C2CFFD32CF3B3E22B9F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_U3CZeroU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_U3COneU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_get_Zero_m65338B601D7118B1864EBA357548AD7DBB3901B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_set_Zero_m8AD743F6D1B4E3B5C2369DD6DB62FC227C686C5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_get_One_m8DE4FAEE75B473FC0E62FDC6232A4D7EAB7DF90C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_set_One_m377CF840C61DCD154038F6398B92CA9610250DD8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_U3CChecksumPortionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_get_Value_m989F7EFF27F7D5857631A9E6EF3491524F923CB7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_set_Value_m4877B2E2E40BBF5855567B1E23B0AFA76DB7ACA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_get_ChecksumPortion_m6798F3EDC1B1BC40E7722823E6E7AC8D113C70FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_set_ChecksumPortion_mD9394227B2D3A90D5C99BFE4A398B6F1F82504DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CStartEndU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CResultPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_Value_m8014DA11325AD1E1E08646912E56C24807170F9F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_Value_mF2E69AB10949ADD73E4762CDD2060603FA16F1BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_StartEnd_mE939A50F1B0D50B4A54D0A78BF100A8D013AF2D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_StartEnd_m4B827D1750AF31559D0B1AD309FA330BB59768A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_ResultPoints_m31BDCA7CA9929339A3131636EF96816A8F857828(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_ResultPoints_m6E89079AEB0D1CB82EA4184C9FC8B3E3817F66B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_U3CFinderPatternU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_U3CCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_get_FinderPattern_m96B759DC05F52541A4D5323517F7B2CC5F754A2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_set_FinderPattern_m5728E8A721DEBE405B277038A44CABE83320C038(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_get_Count_m987093D75472F4272235F675E04B92DB38E33F51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_set_Count_m32CD3B8130984363CB154A45B947A567E950F7A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CMayBeLastU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CLeftCharU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CRightCharU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CFinderPatternU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_MayBeLast_mF8ED068D7464BB2189835D13E2DAB710A7E534A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_LeftChar_mBB6B374F16E0D99A4FBE539CFC986AD770C29754(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_LeftChar_m88021F4C7B0A02E06B313F69D82030673388AA6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_RightChar_m04B44D7E072945EB23BD0F9BCF899982F7EC5955(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_RightChar_m21F9E5A538C4CE938EB4C66EF175C9040D651C15(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_FinderPattern_mD3E9E768D8CB35A730ABBBDC5D88475D5A02D4DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_FinderPattern_m339DD5FC48B79D5D28F185DF8B4CF37B84C23961(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CPairsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CRowNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CIsReversedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_Pairs_m50450B2009FE8FD7118F149A6E309560463777D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_Pairs_m302A4E5EE79058640E1744CD6FF68AF6FA0F64F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_RowNumber_mB3C677AF04C8D715F41914B552FFCEE5B27FC2A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_RowNumber_m5D9E523450193C6945F959DFA191509F771D6C91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_IsReversed_m469685004873C73FC4EFB292A3266BD9D72EA4FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_IsReversed_m3D5EEFD4BE1AF4085620F15BABBA06B4F6FC7FFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_U3CNewPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_DecodedObject_get_NewPosition_m995F3D4A19B254CF37BB255961914C45CBDB9D99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_DecodedObject_set_NewPosition_mC16722AB1F55B5ABB4C3CDD32D251A20DBB0B3EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CFromU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CTransitionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BitArray_t123CB0BFB059449B078BEA6C79320904A4FC5BC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CRawBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CNumBitsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CByteSegmentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CECLevelU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CErrorsCorrectedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CStructuredAppendSequenceNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CErasuresU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CStructuredAppendParityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3COtherU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_RawBytes_m43CA9502BF6A26C8D70D0BFDAB51113CC353E9D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_RawBytes_m987DFC589F07D732CBB655528686CE23A23A9C53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_NumBits_mD3D9C73C3F9DC29C45E2DABB5E96101829E4FD17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_NumBits_mCA6CD50AF39849DF204C1AF96DF35E45C35B1E95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_Text_mC0B1F01D2776183DA406FF6726629B46F905EE7C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Text_m378794910F12A57EFE7501C00B5FD5E7D9C8238C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_ByteSegments_m32CE56F80C15164051C7C3AF3619C62DEDECF6D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ByteSegments_m7A822E31AEDAB0567A23BEB7E853148AF97CAC2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_ECLevel_m29BD97206A9E780E851942193D1247119284FBD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ECLevel_m3FCA51695B6A6294FF8838F64979774A0F6CE971(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ErrorsCorrected_m124A1F030FEB55F836070E8536E938BF3E4D49F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_StructuredAppendSequenceNumber_m96111256F7098707D35BE947261A1692F22788E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_StructuredAppendSequenceNumber_m0B2B6C0023B77A8A777FF06BB2EDFE35CDC8880C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Erasures_m08B47141BC1467C774335C8BC6E150C263A40E5D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_StructuredAppendParity_m66FEE817E38BE7875BA5F0B0344C10401CB429AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_StructuredAppendParity_mFB96734D5857717C16310E554B3B4BCBC4FD82C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_Other_mBA4D7C335AAA256D1D0CBE916AF94439125B980D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Other_m090AF96413E60D86B7819D00AD305EABC160489A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_U3CHintsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_ValueChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_get_Hints_m49D5EDE8C24D2C69569F27E6B07CCE1A35EB578F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_set_Hints_m7713D3EAF7335F7964F81FF3DBA36D64E4173716(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_add_ValueChanged_m473222ED91D5FCEE2C1FFC48C88242408BB24C33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_remove_ValueChanged_mE667F8E7EA37EA3F6410AB0004C53780A5D4806B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_U3C_ctorU3Eb__43_0_m1588EBB00CA9EB20A446A3776CC4393F0CFE4331(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ValueChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ChangeNotifyDictionary_2_add_ValueChanged_m2ADF7745E222C631FF96B173CB2E828A12811BB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ChangeNotifyDictionary_2_remove_ValueChanged_m9204552E56F2E4CFD659D04E6DAAE2B0C0D5CE28(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_get_Bits_m848D10FED0407284D761433AA00A4CB7FB442D1A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_set_Bits_mD70104166B615ACA1AD715C218767A4527906945(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_get_Points_m15DF794C156FD71EEAC5113F7974F991368DDA49(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_set_Points_mF5A0B8D443D25DE03EFE0962F2FAF1C2FA62DA36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ECI_tBC3CB256D3BAE314AAFC10418104F4D3FBC82FDB_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ECI_tBC3CB256D3BAE314AAFC10418104F4D3FBC82FDB_CustomAttributesCacheGenerator_ECI_set_Value_m9F9CA06BCDBAB6747CDEE9A4598D6B9BAF13443E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CCompactU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CDatablocksU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CLayersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Compact_m72E8B973DA813D4E082C976D8A0CE6894D134914(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Datablocks_m62A367370AA07C9FC6423F8765F9EFA9781A6904(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Layers_mED123A0C8E0BE7D7DF2CE6E16000B903A4191009(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CCompactU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CNbDatablocksU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CNbLayersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_Compact_mF9786EF4B4AE2046D4EBC1ACE4421CAF0D8D635C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_Compact_m5746B4844627342B9D01FE94335BBCB3CC2F27D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_NbDatablocks_m8C8449F959B74EB58C27F13F685D657A6350EC83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_NbDatablocks_mAF024D9BF8B752D63183CB8A9DC176B3C2231D4C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_NbLayers_m1F624B4B6E994D0DF191D19CAD9285938D14D648(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_NbLayers_m724AC660E7B21D40509ECFC080450022B738C1A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_U3CXU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_U3CYU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_zxing_unity_AttributeGenerators[];
const CustomAttributesCacheGenerator g_zxing_unity_AttributeGenerators[264] = 
{
	DigitContainer_t7D03AF6CFB49CF97C412D8910B7F912DF562930F_CustomAttributesCacheGenerator,
	DigitContainer_t6B47E9A77123FE36897FC583291FAF29881D7107_CustomAttributesCacheGenerator,
	BarcodeFormat_t2DD630001207452C27CD9C9C5B8D9C09C96C1B3E_CustomAttributesCacheGenerator,
	BarcodeReader_t1234797046EE86EBB37309A73484BAA2F45A3012_CustomAttributesCacheGenerator,
	U3CU3Ec_t3D5CB6F246ECC521FD6BA5F198686B75F01CBEDE_CustomAttributesCacheGenerator,
	U3CU3Ec_tC499AC391CD772B4FBC8A1A7D3EAF0E6ACDFEC30_CustomAttributesCacheGenerator,
	U3CU3Ec_t67A8F0BFB8D0B40540D0125434B10E6D9489EA1D_CustomAttributesCacheGenerator,
	BitArray_t123CB0BFB059449B078BEA6C79320904A4FC5BC5_CustomAttributesCacheGenerator,
	BitMatrix_tAF3E5E2B3A86E9E453455277B9B96DC8222E4BDB_CustomAttributesCacheGenerator,
	ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tC74E1EBE4D2600833307847A0B588423F73A184A_CustomAttributesCacheGenerator,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_ResultFound,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_U3CAutoRotateU3Ek__BackingField,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_U3CTryInvertedU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CRawBytesU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CResultPointsU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CBarcodeFormatU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CResultMetadataU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_U3CNumBitsU3Ek__BackingField,
	Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CSegmentIndexU3Ek__BackingField,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CFileIdU3Ek__BackingField,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3COptionalDataU3Ek__BackingField,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_U3CIsLastSegmentU3Ek__BackingField,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CColumnCountU3Ek__BackingField,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CErrorCorrectionLevelU3Ek__BackingField,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountUpperU3Ek__BackingField,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountLowerU3Ek__BackingField,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_U3CRowCountU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CTopLeftU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CTopRightU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CBottomLeftU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CBottomRightU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMinXU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMaxXU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMinYU3Ek__BackingField,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_U3CMaxYU3Ek__BackingField,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CStartXU3Ek__BackingField,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CEndXU3Ek__BackingField,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CBucketU3Ek__BackingField,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_U3CRowNumberU3Ek__BackingField,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CMetadataU3Ek__BackingField,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CDetectionResultColumnsU3Ek__BackingField,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CBoxU3Ek__BackingField,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_U3CColumnCountU3Ek__BackingField,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_U3CBoxU3Ek__BackingField,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_U3CCodewordsU3Ek__BackingField,
	DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_U3CIsLeftU3Ek__BackingField,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_U3CZeroU3Ek__BackingField,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_U3COneU3Ek__BackingField,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_U3CChecksumPortionU3Ek__BackingField,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CStartEndU3Ek__BackingField,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_U3CResultPointsU3Ek__BackingField,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_U3CFinderPatternU3Ek__BackingField,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_U3CCountU3Ek__BackingField,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CMayBeLastU3Ek__BackingField,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CLeftCharU3Ek__BackingField,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CRightCharU3Ek__BackingField,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_U3CFinderPatternU3Ek__BackingField,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CPairsU3Ek__BackingField,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CRowNumberU3Ek__BackingField,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_U3CIsReversedU3Ek__BackingField,
	DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_U3CNewPositionU3Ek__BackingField,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CFromU3Ek__BackingField,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_U3CTransitionsU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CRawBytesU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CNumBitsU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CByteSegmentsU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CECLevelU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CErrorsCorrectedU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CStructuredAppendSequenceNumberU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CErasuresU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3CStructuredAppendParityU3Ek__BackingField,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_U3COtherU3Ek__BackingField,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_U3CHintsU3Ek__BackingField,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_ValueChanged,
	ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ValueChanged,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_U3CBitsU3Ek__BackingField,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField,
	ECI_tBC3CB256D3BAE314AAFC10418104F4D3FBC82FDB_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CCompactU3Ek__BackingField,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CDatablocksU3Ek__BackingField,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_U3CLayersU3Ek__BackingField,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CCompactU3Ek__BackingField,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CNbDatablocksU3Ek__BackingField,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_U3CNbLayersU3Ek__BackingField,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_U3CXU3Ek__BackingField,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_U3CYU3Ek__BackingField,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_get_AutoRotate_mA7031123B8C71BC186B614012ED67D719F5BF2B9,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_set_AutoRotate_mA3F321343B91733403B13B78DB93FA7B5C09EFEE,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_get_TryInverted_m7CEAFFF9350F50E5EDA598D6CABE2129ECF32AEA,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_set_TryInverted_m9094235165E632A4E8F4819B8149057B3B53CD5F,
	BarcodeReaderGeneric_t7A700D55253DA795E16B2914EA6FB4F767E053F7_CustomAttributesCacheGenerator_BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m5F25CC854255C19BB7387C999AFF6F72224D9641,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_Text_m855527BB9C7057F702BDABD42E7DECC1572721EB,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_Text_mCE77595E7629B6DC028B96FC6B1001D3553B4827,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_RawBytes_m37EADC349C16DD0F5A01A9153DFFBAE55BC6D708,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_RawBytes_mAE2D1DC82194151BE42AB1B9D3A9420880111142,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_ResultPoints_mB6717EF744141D983630A17EDACB39C11BE452FC,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_ResultPoints_mDC64CD4147E895472D1C2417141086D9F8EC15E2,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_BarcodeFormat_m446306E9DC37F14EB447E0C711A55A45D5514AA7,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_BarcodeFormat_m66740266B609E88E7C1A8496F4369AA9B17F3733,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_get_ResultMetadata_m82489B9ED570D7F50058B680DCEF0D126448129C,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_ResultMetadata_mD48ACD9D2F3A263E6E705391CDCA38E9B2958CA4,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_Timestamp_m9399B76A449D904D5773C5463656BE35040F70D8,
	Result_tE3BECD92BFFB0320CCCF6A64938A5065FAA6513D_CustomAttributesCacheGenerator_Result_set_NumBits_m0EB6FB28263F1E58FB521CB0E151B21A344D74DB,
	Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_CustomAttributesCacheGenerator_Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677,
	Color32LuminanceSource_tD3E70D2C99BD52886D5AAB01A12DF38E49DA8FCB_CustomAttributesCacheGenerator_Color32LuminanceSource_SetPixels_m9BCC155B50914AA2DD6A19F3DDEFF89101FE21E4,
	Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_get_Name_mDD10EE936C24F81A4E3C5BB7A483853C5F22E448,
	Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_set_Name_m75EFECA9B152C3114257443AD3CDD16A9FD5A789,
	Mode_tF8350BEF5D8F76577AF9CE42B3B9A41EA9A3354E_CustomAttributesCacheGenerator_Mode_set_Bits_m050E659D3B05D670BCE285DE9125B69816015542,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_SegmentIndex_m6792A85EE271BFA6063BE4B2A3990743B73C1093,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_FileId_m6A79CC9F4624FE95AB7A870D742357FE0011F520,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_get_OptionalData_mEFBB80130FA8AE0C7E7657790368E3646220FD7E,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_OptionalData_m0D81D688400517D1D65B3E36CF5B9F996B70BC6F,
	PDF417ResultMetadata_tF79E9B9A1C45EFB75D1801E6B4D213C09B1E7032_CustomAttributesCacheGenerator_PDF417ResultMetadata_set_IsLastSegment_m0FC2FB8C14FA37900B7B57397602F5D9DA1E64B8,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_ColumnCount_m4A9D3AD55F9D68456B8E2DB771835EA55CCDE6C9,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_ColumnCount_m176BF152AEF28C1A672DAB8A5BD8278AB083A0DD,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_ErrorCorrectionLevel_m84BB571AFA2AE2423ED8042DF14BC1DB78EF538C,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_ErrorCorrectionLevel_m815B1DAB46579F73F17ACBF31E6E39703FD05627,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCountUpper_m03D2B4FC5647BF341DF22E2208586CFE20140301,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCountUpper_m9864EFB99D21684C54F44F9987DB9E1A67A45756,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCountLower_m01D867E4FAF15591DB9ADCA2681A4F524909528D,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCountLower_m8691F9A95CC8158F00B4D521E9A72E965081599E,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_get_RowCount_mACD2C5E04E6FE23404844F1C4B24B33642C9E93F,
	BarcodeMetadata_tE9116DB9BC7BA34451040A4DA447A934D2BFC44F_CustomAttributesCacheGenerator_BarcodeMetadata_set_RowCount_m757D5A46753DA91F51B97CA777A2A30826FCDE85,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_TopLeft_m5CB4B6EC44B499CD0BA203C9B3B6F8933121724C,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_TopLeft_mCCCE09F3624FA382B74AC8E0A199AA6D8264FD35,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_TopRight_m5A6F9AD58CD8CA92AEA0C76D84C763EECFA40CD3,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_TopRight_m10405141D9546413D3A065768C78A4DAC520929A,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_BottomLeft_m9A8E6255BF210761723976D7C38CF7D118EDFE8F,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_BottomLeft_mB4C5B675177B796E7E681F59DB76A625A5620B68,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_BottomRight_m2035AC5736DC155C43192B4ECDAB8A4F57EEBB94,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_BottomRight_m8C954B0A293AC7B39084CA049E9B386F712A079B,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MinX_m5F83E51CACD2BBA44036A99706C101CF6016840B,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MinX_mBA6A3362975DA7BD7D6A0F04DD13AE8FE75931C8,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MaxX_m3AE590ECC9CEA0B6DF5517CD8C3D182D95511712,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MaxX_m3119F44A66FDB33CF14D4B1228679C148B4D82D8,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MinY_m1FFEDA362F3B77F1DFE9385F7C3EF419DF0EC394,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MinY_m595E30BC39CD5ADA2D82C0994B0923BB4C4FF8F0,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_get_MaxY_m5CFBC7ECF98C8659323F1A69392E297A82AB285B,
	BoundingBox_tE1AC25CC4B1DF0E7E0B2269D070AE50CEA4B40EB_CustomAttributesCacheGenerator_BoundingBox_set_MaxY_m4F13753AEAEBD5CF67FA851DDCD4D2C586423D0F,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_StartX_m7142A4213335F1F993CA7802368A5AAD32672949,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_StartX_mEFB95587ADF18A1371DA69054C6D5600B61B8AD2,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_EndX_m76D22FBE3E14FCC8FA93068BE2CE2CB08E4E8998,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_EndX_m86CF8AFE1193EC119585AFBA4A29B1B80237983C,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_Bucket_mDC1B59D5B2EF3B9C57825CFA4209B3D20E3D6FB3,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_Bucket_m4ACB8A7E1675CCDAA8AB38072CB91E78BA8C01E7,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_Value_m6E938EB77D32B6E926D4A1DE4D4A14308EBDCEA9,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_Value_m2704DED6B54EC01D56145587EE2FFEEE3704A85E,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_get_RowNumber_mE9ABCE1EC3AF7CF50363CD1C02C5703C6498F2D0,
	Codeword_t531F944C9E0993C6C67CB8A9937EA5FA0E2387B2_CustomAttributesCacheGenerator_Codeword_set_RowNumber_m099810CF7CB72780C60AA7C1ABA4FA0C8879F935,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_Metadata_m28222ACC510CDDE151005C522558517F54F11E31,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_Metadata_mE75F8B8FF73BE8E6949821C378222EA306F929AF,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_DetectionResultColumns_mE527F5B2DC9F258C3A2FDDF03AF1E6C571E3DB68,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_DetectionResultColumns_m65CE606CF83520C680E1CFEC8AB98D8147807C9A,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_Box_m787AE2F48FF618EAD3AA5AC69F0E5CBA0557C03D,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_Box_m151695A38E2C002A537D09CCB87DAC7DDAF08E13,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_get_ColumnCount_mB43FEE1A3282C41D121B3A5C8220F6684F0266B8,
	DetectionResult_t6D32E237D42625163409E6000E2B2885F332C1FE_CustomAttributesCacheGenerator_DetectionResult_set_ColumnCount_mE0107ECCF65B7CAA5B790C6F8BF4FE68F330DE61,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_get_Box_m28DE039B3B068F05F1043D74471A84948310A5D4,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_set_Box_m3949C81CBD20F30155D8B7A5B720A809AABCA29B,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_get_Codewords_m8C5E2EDFE024EBB4833DAB8E33725D2E2906790A,
	DetectionResultColumn_tA117FFF247410059B9172C03566E5644A10394DF_CustomAttributesCacheGenerator_DetectionResultColumn_set_Codewords_m507D740FE136F6E201194B24CF7F7BD80CE0CE13,
	DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_DetectionResultRowIndicatorColumn_get_IsLeft_mC2C89A027BF107A96DDC704A5C7F5CD9998B5450,
	DetectionResultRowIndicatorColumn_t64953D37346E899F3A5CB124DB1FC12E075715AB_CustomAttributesCacheGenerator_DetectionResultRowIndicatorColumn_set_IsLeft_m0A5451068AE892E9E3180FD80840869545C3DEB1,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_get_Bits_mF582F132EF48BC1E687742C76A1004054C51BCF8,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_set_Bits_mBF16DD37F7407142F2C31B184AC52A326E630B3E,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_get_Points_mE94F3CA82AE031E694E202F8C6151AEF086E5BDA,
	PDF417DetectorResult_tF40526A070985489BE8CB7625258900E8450D41A_CustomAttributesCacheGenerator_PDF417DetectorResult_set_Points_mA06AC47807901C9A6A34C2CFFD32CF3B3E22B9F6,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_get_Zero_m65338B601D7118B1864EBA357548AD7DBB3901B0,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_set_Zero_m8AD743F6D1B4E3B5C2369DD6DB62FC227C686C5F,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_get_One_m8DE4FAEE75B473FC0E62FDC6232A4D7EAB7DF90C,
	ModulusGF_t8D7B55A18AC2ED6FCA51FC6B14F6A82280D57583_CustomAttributesCacheGenerator_ModulusGF_set_One_m377CF840C61DCD154038F6398B92CA9610250DD8,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_get_Value_m989F7EFF27F7D5857631A9E6EF3491524F923CB7,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_set_Value_m4877B2E2E40BBF5855567B1E23B0AFA76DB7ACA7,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_get_ChecksumPortion_m6798F3EDC1B1BC40E7722823E6E7AC8D113C70FC,
	DataCharacter_t153BBCC5173EDB30DCA6F97B854BF60DADE289C4_CustomAttributesCacheGenerator_DataCharacter_set_ChecksumPortion_mD9394227B2D3A90D5C99BFE4A398B6F1F82504DD,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_Value_m8014DA11325AD1E1E08646912E56C24807170F9F,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_Value_mF2E69AB10949ADD73E4762CDD2060603FA16F1BA,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_StartEnd_mE939A50F1B0D50B4A54D0A78BF100A8D013AF2D2,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_StartEnd_m4B827D1750AF31559D0B1AD309FA330BB59768A8,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_get_ResultPoints_m31BDCA7CA9929339A3131636EF96816A8F857828,
	FinderPattern_t90E012089F79368DEC473EA327DD42B88BFBA29B_CustomAttributesCacheGenerator_FinderPattern_set_ResultPoints_m6E89079AEB0D1CB82EA4184C9FC8B3E3817F66B6,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_get_FinderPattern_m96B759DC05F52541A4D5323517F7B2CC5F754A2A,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_set_FinderPattern_m5728E8A721DEBE405B277038A44CABE83320C038,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_get_Count_m987093D75472F4272235F675E04B92DB38E33F51,
	Pair_tAEDC5855354D987E063CCAF0DDA8C55E0254F264_CustomAttributesCacheGenerator_Pair_set_Count_m32CD3B8130984363CB154A45B947A567E950F7A0,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_MayBeLast_mF8ED068D7464BB2189835D13E2DAB710A7E534A7,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_LeftChar_mBB6B374F16E0D99A4FBE539CFC986AD770C29754,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_LeftChar_m88021F4C7B0A02E06B313F69D82030673388AA6F,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_RightChar_m04B44D7E072945EB23BD0F9BCF899982F7EC5955,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_RightChar_m21F9E5A538C4CE938EB4C66EF175C9040D651C15,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_get_FinderPattern_mD3E9E768D8CB35A730ABBBDC5D88475D5A02D4DC,
	ExpandedPair_t32551CD1F8B215BECEB5DB0975B87BAE69D884AA_CustomAttributesCacheGenerator_ExpandedPair_set_FinderPattern_m339DD5FC48B79D5D28F185DF8B4CF37B84C23961,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_Pairs_m50450B2009FE8FD7118F149A6E309560463777D7,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_Pairs_m302A4E5EE79058640E1744CD6FF68AF6FA0F64F7,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_RowNumber_mB3C677AF04C8D715F41914B552FFCEE5B27FC2A1,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_RowNumber_m5D9E523450193C6945F959DFA191509F771D6C91,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_get_IsReversed_m469685004873C73FC4EFB292A3266BD9D72EA4FD,
	ExpandedRow_t16FFB38467A41C13A02507AFA8B00E48C048454A_CustomAttributesCacheGenerator_ExpandedRow_set_IsReversed_m3D5EEFD4BE1AF4085620F15BABBA06B4F6FC7FFF,
	DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_DecodedObject_get_NewPosition_m995F3D4A19B254CF37BB255961914C45CBDB9D99,
	DecodedObject_t32F5850757A8462900482853B95640C8EC79E70F_CustomAttributesCacheGenerator_DecodedObject_set_NewPosition_mC16722AB1F55B5ABB4C3CDD32D251A20DBB0B3EB,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172,
	ResultPointsAndTransitions_t8C716D49569D3F44F8AF3FCBAA5AA5A8C54556C1_CustomAttributesCacheGenerator_ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_RawBytes_m43CA9502BF6A26C8D70D0BFDAB51113CC353E9D5,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_RawBytes_m987DFC589F07D732CBB655528686CE23A23A9C53,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_NumBits_mD3D9C73C3F9DC29C45E2DABB5E96101829E4FD17,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_NumBits_mCA6CD50AF39849DF204C1AF96DF35E45C35B1E95,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_Text_mC0B1F01D2776183DA406FF6726629B46F905EE7C,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Text_m378794910F12A57EFE7501C00B5FD5E7D9C8238C,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_ByteSegments_m32CE56F80C15164051C7C3AF3619C62DEDECF6D8,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ByteSegments_m7A822E31AEDAB0567A23BEB7E853148AF97CAC2C,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_ECLevel_m29BD97206A9E780E851942193D1247119284FBD2,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ECLevel_m3FCA51695B6A6294FF8838F64979774A0F6CE971,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_ErrorsCorrected_m124A1F030FEB55F836070E8536E938BF3E4D49F9,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_StructuredAppendSequenceNumber_m96111256F7098707D35BE947261A1692F22788E0,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_StructuredAppendSequenceNumber_m0B2B6C0023B77A8A777FF06BB2EDFE35CDC8880C,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Erasures_m08B47141BC1467C774335C8BC6E150C263A40E5D,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_StructuredAppendParity_m66FEE817E38BE7875BA5F0B0344C10401CB429AC,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_StructuredAppendParity_mFB96734D5857717C16310E554B3B4BCBC4FD82C0,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_get_Other_mBA4D7C335AAA256D1D0CBE916AF94439125B980D,
	DecoderResult_t2A3AE157A359D5A63731E2D131FE5B7424AD0491_CustomAttributesCacheGenerator_DecoderResult_set_Other_m090AF96413E60D86B7819D00AD305EABC160489A,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_get_Hints_m49D5EDE8C24D2C69569F27E6B07CCE1A35EB578F,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_set_Hints_m7713D3EAF7335F7964F81FF3DBA36D64E4173716,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_add_ValueChanged_m473222ED91D5FCEE2C1FFC48C88242408BB24C33,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_remove_ValueChanged_mE667F8E7EA37EA3F6410AB0004C53780A5D4806B,
	DecodingOptions_tA68CAC1ABF1D1F46E20A057985FAA541037ED21C_CustomAttributesCacheGenerator_DecodingOptions_U3C_ctorU3Eb__43_0_m1588EBB00CA9EB20A446A3776CC4393F0CFE4331,
	ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ChangeNotifyDictionary_2_add_ValueChanged_m2ADF7745E222C631FF96B173CB2E828A12811BB5,
	ChangeNotifyDictionary_2_t36ACD206AAF7DAA35544BA5F7FCD4E178AAC21F8_CustomAttributesCacheGenerator_ChangeNotifyDictionary_2_remove_ValueChanged_m9204552E56F2E4CFD659D04E6DAAE2B0C0D5CE28,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_get_Bits_m848D10FED0407284D761433AA00A4CB7FB442D1A,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_set_Bits_mD70104166B615ACA1AD715C218767A4527906945,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_get_Points_m15DF794C156FD71EEAC5113F7974F991368DDA49,
	DetectorResult_t1E21070D6F2557E57C2766BDB76EE186BB5F4A65_CustomAttributesCacheGenerator_DetectorResult_set_Points_mF5A0B8D443D25DE03EFE0962F2FAF1C2FA62DA36,
	ECI_tBC3CB256D3BAE314AAFC10418104F4D3FBC82FDB_CustomAttributesCacheGenerator_ECI_set_Value_m9F9CA06BCDBAB6747CDEE9A4598D6B9BAF13443E,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Compact_m72E8B973DA813D4E082C976D8A0CE6894D134914,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Datablocks_m62A367370AA07C9FC6423F8765F9EFA9781A6904,
	AztecResultMetadata_t49A2C92EE61DC4FDEBEF319B9A3BC5D84456A840_CustomAttributesCacheGenerator_AztecResultMetadata_set_Layers_mED123A0C8E0BE7D7DF2CE6E16000B903A4191009,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_Compact_mF9786EF4B4AE2046D4EBC1ACE4421CAF0D8D635C,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_Compact_m5746B4844627342B9D01FE94335BBCB3CC2F27D3,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_NbDatablocks_m8C8449F959B74EB58C27F13F685D657A6350EC83,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_NbDatablocks_mAF024D9BF8B752D63183CB8A9DC176B3C2231D4C,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_get_NbLayers_m1F624B4B6E994D0DF191D19CAD9285938D14D648,
	AztecDetectorResult_t9D05FC9B855F8FF331AA69FA5DA254ECA9D2ACC3_CustomAttributesCacheGenerator_AztecDetectorResult_set_NbLayers_m724AC660E7B21D40509ECFC080450022B738C1A8,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605,
	Point_t671952EEFD2B77F47039C983FD785221F65BC8F2_CustomAttributesCacheGenerator_Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5,
	Version_t7946E266ADEDBC8E59B285435F3DC3DE0198FEEB_CustomAttributesCacheGenerator_Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F____ecBlocks2,
	ECBlocks_t0DB4C13D3FF18F540748388FE3DA93EF141D3BC0_CustomAttributesCacheGenerator_ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017____ecBlocks1,
	zxing_unity_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
