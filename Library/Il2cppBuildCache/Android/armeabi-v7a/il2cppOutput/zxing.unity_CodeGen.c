﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BigIntegerLibrary.Base10BigInteger::set_NumberSign(BigIntegerLibrary.Sign)
extern void Base10BigInteger_set_NumberSign_m318126DE9F52340DD6DE456501EC94FF35210B22 (void);
// 0x00000002 System.Void BigIntegerLibrary.Base10BigInteger::.ctor()
extern void Base10BigInteger__ctor_m1EACDEE3EB7FE4EED9A62A61967D37F95C8BF61E (void);
// 0x00000003 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(System.Int64)
extern void Base10BigInteger__ctor_m5116F59B394162C5E93BC7025F74338A1225F407 (void);
// 0x00000004 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger__ctor_m1F541EB5E39C1D1EBE5D0361886C3B8417B6DE1C (void);
// 0x00000005 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Equals_mA0A8A0A9E01514A9DA511C8666564E4BCCCD1A0F (void);
// 0x00000006 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(System.Object)
extern void Base10BigInteger_Equals_mF3183754DA871F82B5D9883223C8EE0C6D5B1622 (void);
// 0x00000007 System.Int32 BigIntegerLibrary.Base10BigInteger::GetHashCode()
extern void Base10BigInteger_GetHashCode_m677E9663A6EEFB10FD85331936580F44103BC42C (void);
// 0x00000008 System.String BigIntegerLibrary.Base10BigInteger::ToString()
extern void Base10BigInteger_ToString_mE2763B323015C0BEB3C902555D0439F8B1F3AB59 (void);
// 0x00000009 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Opposite(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Opposite_mF9A574FA9845236F48B4180C347D150E0AA88C8F (void);
// 0x0000000A System.Boolean BigIntegerLibrary.Base10BigInteger::Greater(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Greater_mD738D8DA810F8AF38CD73B444B162B944DAA28F4 (void);
// 0x0000000B System.Boolean BigIntegerLibrary.Base10BigInteger::GreaterOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_GreaterOrEqual_mAF734A4EC6D5EF155A9A74EF523EA12053C52E3B (void);
// 0x0000000C System.Boolean BigIntegerLibrary.Base10BigInteger::SmallerOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_SmallerOrEqual_m8451318F61131699607B4E3DA1C13861132EB07C (void);
// 0x0000000D BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Abs(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Abs_m13DDD583C13BCD6612B0EE81181A9C6A59E7B8B9 (void);
// 0x0000000E BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Addition_mAA0A8F747456B77BA1039B4DF0ED989E14292B04 (void);
// 0x0000000F BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiplication(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiplication_mC88410FE58D7DC2AA935C2207513038A72DC915F (void);
// 0x00000010 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Implicit(System.Int64)
extern void Base10BigInteger_op_Implicit_m0B166017A7F4032C0336666AD605F928670CA5C1 (void);
// 0x00000011 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Equality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Equality_m0A2846B4B495CC070850A807A89ABF5F3827604B (void);
// 0x00000012 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Inequality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Inequality_m25348FCB953E3068635090D912AD03636B8296A7 (void);
// 0x00000013 System.Boolean BigIntegerLibrary.Base10BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_GreaterThanOrEqual_mC0DCBC9AC4F6AD86CF7B31CE1B2D80844918B351 (void);
// 0x00000014 System.Boolean BigIntegerLibrary.Base10BigInteger::op_LessThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_LessThanOrEqual_m272737DC7C9E5B3C631011D8315844E02D221A68 (void);
// 0x00000015 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_UnaryNegation(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_UnaryNegation_mF3A4F0A82F8D2343019384B6DDF01CAC4E279E1A (void);
// 0x00000016 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Addition_m7835CAB2D25921D21E136C0ACED7FEF3072812D9 (void);
// 0x00000017 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Multiply_m6A5D377E80138660C947E4E744EFCBD3E5F3615B (void);
// 0x00000018 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Add(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Add_m43244C7158D9CA9D19521639D9AD77FE9AB8D532 (void);
// 0x00000019 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Subtract(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Subtract_m2444C6C00B26D2BFAADAB65E7652733F741C9DCE (void);
// 0x0000001A BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiply_m1711C6FE920A26D1A185EDDB21734BE3570276BA (void);
// 0x0000001B System.Void BigIntegerLibrary.Base10BigInteger::.cctor()
extern void Base10BigInteger__cctor_mCC0EB61B92F3A8EA71C241E0DFCCF3A7D137035C (void);
// 0x0000001C System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::.ctor()
extern void DigitContainer__ctor_m5593EDFE34483051D6AC430CDF0EB4778B0BD712 (void);
// 0x0000001D System.Int64 BigIntegerLibrary.Base10BigInteger/DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_m25983F3EE4061CD2280C4B1B02281E04AD9728AC (void);
// 0x0000001E System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_mD0206461D4866D3E4A36C686CFC3E07AD8343137 (void);
// 0x0000001F System.Void BigIntegerLibrary.BigInteger::.ctor()
extern void BigInteger__ctor_mE585575A61DFBB95EE746E71A63DF3D2B9BECB0F (void);
// 0x00000020 System.Void BigIntegerLibrary.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m1D57159256C1D12339C8750BBAA75A0416B68890 (void);
// 0x00000021 System.Void BigIntegerLibrary.BigInteger::.ctor(BigIntegerLibrary.BigInteger)
extern void BigInteger__ctor_m49A47699C6B5C617A47E09096BB5D0D8D115F399 (void);
// 0x00000022 System.Boolean BigIntegerLibrary.BigInteger::Equals(BigIntegerLibrary.BigInteger)
extern void BigInteger_Equals_m381F80F7197D4EB806F654B85B7EBACFD8757080 (void);
// 0x00000023 System.Boolean BigIntegerLibrary.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_mBA2F863D8E14E8F5A9AF4E444A2DE92BA444FC6B (void);
// 0x00000024 System.Int32 BigIntegerLibrary.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_mBACE4F6815D6FD001F6B703BD012EA7E4022BB56 (void);
// 0x00000025 System.String BigIntegerLibrary.BigInteger::ToString()
extern void BigInteger_ToString_mD74459FFA1B41C651A4FE687BDDFB60E5B88F8C7 (void);
// 0x00000026 System.Int32 BigIntegerLibrary.BigInteger::CompareTo(BigIntegerLibrary.BigInteger)
extern void BigInteger_CompareTo_m093FA3B527B18E0293BA132C5C411323F562EE5A (void);
// 0x00000027 System.Int32 BigIntegerLibrary.BigInteger::CompareTo(System.Object)
extern void BigInteger_CompareTo_mEAFF995FAC40B030E4A68B3C01A5951039D5FF02 (void);
// 0x00000028 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Opposite(BigIntegerLibrary.BigInteger)
extern void BigInteger_Opposite_mC4439D12CC2C94755C39A47285D00A3745D3C4E0 (void);
// 0x00000029 System.Boolean BigIntegerLibrary.BigInteger::Greater(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Greater_mA0FBB2DC14AC997F3B645CB1CF7EDCD1F6B9C54D (void);
// 0x0000002A System.Boolean BigIntegerLibrary.BigInteger::GreaterOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_GreaterOrEqual_m4F8B533BAD98285C0ED60BFC71DA14133D4262CA (void);
// 0x0000002B System.Boolean BigIntegerLibrary.BigInteger::Smaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Smaller_m6676EB8CACD9E43E7B9CC6246BC5CDD5E71A6731 (void);
// 0x0000002C System.Boolean BigIntegerLibrary.BigInteger::SmallerOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_SmallerOrEqual_m8ED8D8A2356849FF4CFB830781AA8A4B48FCC894 (void);
// 0x0000002D BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Abs(BigIntegerLibrary.BigInteger)
extern void BigInteger_Abs_m9811944203BD90CD88DC9040729FA86F062D16DC (void);
// 0x0000002E BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Addition_m7B30E44292C57CEAF2BC94E5B9ED4725072182C5 (void);
// 0x0000002F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtraction_m83F5F4840253C3EE5DEED372A7311325812DCE73 (void);
// 0x00000030 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiplication(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiplication_mF2EB37ABD84AE7D3C79B584DC822C85791E1D8CB (void);
// 0x00000031 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Division_m2453413637C035B66C36A130D54131DBCC76150F (void);
// 0x00000032 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Modulo(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Modulo_m68F951782F89D9FE17791447837B2E43E26F1869 (void);
// 0x00000033 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int64)
extern void BigInteger_op_Implicit_m354439251A263BFF4AF79A6823AAC58342CB7A09 (void);
// 0x00000034 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int32)
extern void BigInteger_op_Implicit_m997AF7B456DDA0CA1B0C53AD70AC8497FD99F807 (void);
// 0x00000035 System.Int32 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Explicit_m45B18F87798C8AF3B8C9991ED3B60A2E082461C1 (void);
// 0x00000036 System.UInt64 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Explicit_m5E9F820D69C3058EFAFC54C79BEDF8688D2EADFE (void);
// 0x00000037 System.Boolean BigIntegerLibrary.BigInteger::op_Equality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Equality_m82BD5FC8C50E191A5473B61B304370C57F39F948 (void);
// 0x00000038 System.Boolean BigIntegerLibrary.BigInteger::op_Inequality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Inequality_m8777CAB30B7852430D8BA019E0E351F8216C8D32 (void);
// 0x00000039 System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_GreaterThan_m614A102E33649450C7C755ED620630A49A7963CB (void);
// 0x0000003A System.Boolean BigIntegerLibrary.BigInteger::op_LessThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThan_mBBD0A175B1A0CE5B88908563FCC011E4C1C244B2 (void);
// 0x0000003B System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_GreaterThanOrEqual_mE2967F43D21ADDB0626C24475B0EE1FE1F630BA8 (void);
// 0x0000003C System.Boolean BigIntegerLibrary.BigInteger::op_LessThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThanOrEqual_m84C0E39990D6F9EB1AD04C9B0B00B28631F88EE6 (void);
// 0x0000003D BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_UnaryNegation(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_UnaryNegation_mA43B696B3AFF4E8313A0DBC28D1C212AFF258EFD (void);
// 0x0000003E BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Addition_m8E34B4E094DBC7C8C491F11EF9D36823A07F43CF (void);
// 0x0000003F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Subtraction_m16A9075CD7BC7DDEA0B8DB49AE537B8A411D2D3A (void);
// 0x00000040 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Multiply_m0D548C1654E3B10385F6A1BDF2CB4F2A5E9F623A (void);
// 0x00000041 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Division_mF2F4079FAF5FB32959B2B518ABC004D2169E1571 (void);
// 0x00000042 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Modulus(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Modulus_m36DCE0EAD5AB54B5B75ECE9BF4F7CCAA5106C69A (void);
// 0x00000043 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Add(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Add_m0F55BF603F40FCD911D02A55E88F02762E1BC0D7 (void);
// 0x00000044 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtract(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtract_m0567F5539BB2EAB395191096087B5C02CE229AB0 (void);
// 0x00000045 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiply_m866DE3938B29F970041CFFB62569B8AB9F6F5A1F (void);
// 0x00000046 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByOneDigitNumber(BigIntegerLibrary.BigInteger,System.Int64)
extern void BigInteger_DivideByOneDigitNumber_mD4BE7852E0B181E33295918FDAB75ADF8ABFC1F6 (void);
// 0x00000047 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByBigNumber(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_DivideByBigNumber_mC1DC468DBFAC09E7E1BCEA951FAB6778D780B43E (void);
// 0x00000048 System.Boolean BigIntegerLibrary.BigInteger::DivideByBigNumberSmaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_DivideByBigNumberSmaller_mDA020A4757FC5FDCD14631F344F6F4087E97C38A (void);
// 0x00000049 System.Void BigIntegerLibrary.BigInteger::Difference(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Difference_mF4C14ABF636DC4401238FECAE1A9F1B23008B535 (void);
// 0x0000004A System.Int64 BigIntegerLibrary.BigInteger::Trial(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Trial_m36DB99C930B0AE7933BFB782EB7028DB345C8622 (void);
// 0x0000004B System.Void BigIntegerLibrary.BigInteger::.cctor()
extern void BigInteger__cctor_mA0A0D687BAA037C95CA5474D1E27EB441A3E288E (void);
// 0x0000004C System.Void BigIntegerLibrary.BigInteger/DigitContainer::.ctor()
extern void DigitContainer__ctor_m173ECCA1CA08D537D379FE12A8A87C6A8211ADE9 (void);
// 0x0000004D System.Int64 BigIntegerLibrary.BigInteger/DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_m9AC7D74C5F872C9ECD3C20A864B51E4DDE03302F (void);
// 0x0000004E System.Void BigIntegerLibrary.BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_m19E1AB570C478C138EAD30A79AFAA8D5DDC2C86C (void);
// 0x0000004F System.Void BigIntegerLibrary.BigIntegerException::.ctor(System.String,System.Exception)
extern void BigIntegerException__ctor_m91613E6288DAD4D5C7ED02CC69740D5D3E74979A (void);
// 0x00000050 System.Void ZXing.BarcodeReader::.ctor()
extern void BarcodeReader__ctor_m114048B1A41B44644752C421FC901A649F6B940B (void);
// 0x00000051 System.Void ZXing.BarcodeReader::.ctor(ZXing.Reader,System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
extern void BarcodeReader__ctor_m5214CF12EAAE38C3394F2E16FB822AC94C93A844 (void);
// 0x00000052 System.Void ZXing.BarcodeReader::.ctor(ZXing.Reader,System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern void BarcodeReader__ctor_m0928B1ADFD9BB574EB50B94179DF79BE72328D16 (void);
// 0x00000053 System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::get_CreateLuminanceSource()
extern void BarcodeReader_get_CreateLuminanceSource_mE01B7DE98E22F02E3EA02E84D4975B27BC185EB0 (void);
// 0x00000054 ZXing.Result ZXing.BarcodeReader::Decode(UnityEngine.Color32[],System.Int32,System.Int32)
extern void BarcodeReader_Decode_m501C41B855C0A6EE102A22D992AECB963EE53D5B (void);
// 0x00000055 System.Void ZXing.BarcodeReader::.cctor()
extern void BarcodeReader__cctor_m080759F6071EFB12C9C05883243C647B2C0D093E (void);
// 0x00000056 System.Void ZXing.BarcodeReader/<>c::.cctor()
extern void U3CU3Ec__cctor_mECF73754D3E659397FBAC0C5B1ED9ADC8D5F5B91 (void);
// 0x00000057 System.Void ZXing.BarcodeReader/<>c::.ctor()
extern void U3CU3Ec__ctor_mC582116316DB1F8235AB27A5BE58C6B17EA9F24D (void);
// 0x00000058 ZXing.LuminanceSource ZXing.BarcodeReader/<>c::<.cctor>b__9_0(UnityEngine.Color32[],System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__9_0_m3A4863AE47CD642B2E628E5ACC01C1E566FF9949 (void);
// 0x00000059 ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric::get_Options()
extern void BarcodeReaderGeneric_get_Options_mEE13C157F091AF9C1CDEDD600045018AE9CC106E (void);
// 0x0000005A ZXing.Reader ZXing.BarcodeReaderGeneric::get_Reader()
extern void BarcodeReaderGeneric_get_Reader_mC6BC0F8670FC382906E90EC1FFCBB672F169AA03 (void);
// 0x0000005B System.Boolean ZXing.BarcodeReaderGeneric::get_AutoRotate()
extern void BarcodeReaderGeneric_get_AutoRotate_mA7031123B8C71BC186B614012ED67D719F5BF2B9 (void);
// 0x0000005C System.Void ZXing.BarcodeReaderGeneric::set_AutoRotate(System.Boolean)
extern void BarcodeReaderGeneric_set_AutoRotate_mA3F321343B91733403B13B78DB93FA7B5C09EFEE (void);
// 0x0000005D System.Boolean ZXing.BarcodeReaderGeneric::get_TryInverted()
extern void BarcodeReaderGeneric_get_TryInverted_m7CEAFFF9350F50E5EDA598D6CABE2129ECF32AEA (void);
// 0x0000005E System.Void ZXing.BarcodeReaderGeneric::set_TryInverted(System.Boolean)
extern void BarcodeReaderGeneric_set_TryInverted_m9094235165E632A4E8F4819B8149057B3B53CD5F (void);
// 0x0000005F System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric::get_CreateBinarizer()
extern void BarcodeReaderGeneric_get_CreateBinarizer_m39A8C1DD65ACAB46BAC55615B6CD68C497230F4F (void);
// 0x00000060 System.Void ZXing.BarcodeReaderGeneric::.ctor(ZXing.Reader,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern void BarcodeReaderGeneric__ctor_m19DAD94E50637D482E6C9E9DCCFB21B3469BC4E0 (void);
// 0x00000061 ZXing.Result ZXing.BarcodeReaderGeneric::Decode(ZXing.LuminanceSource)
extern void BarcodeReaderGeneric_Decode_m2AA234799697ECB3D2726038BC8AD8835CA0E280 (void);
// 0x00000062 System.Void ZXing.BarcodeReaderGeneric::OnResultFound(ZXing.Result)
extern void BarcodeReaderGeneric_OnResultFound_mB4AD7EEEA57A464BDEC33E15017779BEAEDD0465 (void);
// 0x00000063 System.Void ZXing.BarcodeReaderGeneric::.cctor()
extern void BarcodeReaderGeneric__cctor_m0966AB37FB7169FA63D13DF7731451EE0CBFF941 (void);
// 0x00000064 System.Void ZXing.BarcodeReaderGeneric::<get_Options>b__8_0(System.Object,System.EventArgs)
extern void BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m5F25CC854255C19BB7387C999AFF6F72224D9641 (void);
// 0x00000065 System.Void ZXing.BarcodeReaderGeneric/<>c::.cctor()
extern void U3CU3Ec__cctor_m12E9FA4BF9DE2A8179EEC86554CE1050DAE4D962 (void);
// 0x00000066 System.Void ZXing.BarcodeReaderGeneric/<>c::.ctor()
extern void U3CU3Ec__ctor_m0EAA14E42607DB22B6B1DBA9E1EEBDD21948DB20 (void);
// 0x00000067 ZXing.Binarizer ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_0(ZXing.LuminanceSource)
extern void U3CU3Ec_U3C_cctorU3Eb__40_0_mEED660C1CC3A9D0ECB0F0AA6B49C72FB2A0A6154 (void);
// 0x00000068 ZXing.LuminanceSource ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void U3CU3Ec_U3C_cctorU3Eb__40_1_m68137642FE973A8B9853D6F468F4B46D3FE03F3B (void);
// 0x00000069 System.Void ZXing.BaseLuminanceSource::.ctor(System.Int32,System.Int32)
extern void BaseLuminanceSource__ctor_m3C977BDA7616746CB83FD6F24897D9D2A3AA2995 (void);
// 0x0000006A System.Byte[] ZXing.BaseLuminanceSource::getRow(System.Int32,System.Byte[])
extern void BaseLuminanceSource_getRow_m39278E1D3319FB64B6949D137FC5CE683A03A306 (void);
// 0x0000006B System.Byte[] ZXing.BaseLuminanceSource::get_Matrix()
extern void BaseLuminanceSource_get_Matrix_mDB62ED37630CAE1A900EEDFD572B3B641221200C (void);
// 0x0000006C ZXing.LuminanceSource ZXing.BaseLuminanceSource::rotateCounterClockwise()
extern void BaseLuminanceSource_rotateCounterClockwise_mA08C035544611E16F47C4B5FE90F61870FFB64F7 (void);
// 0x0000006D System.Boolean ZXing.BaseLuminanceSource::get_RotateSupported()
extern void BaseLuminanceSource_get_RotateSupported_m1DFC1C677FABBAB1F5B6F6AC20D02924ECA73559 (void);
// 0x0000006E System.Boolean ZXing.BaseLuminanceSource::get_InversionSupported()
extern void BaseLuminanceSource_get_InversionSupported_mA66F674120F721E2629C754E09EE849B3C4BBB8F (void);
// 0x0000006F ZXing.LuminanceSource ZXing.BaseLuminanceSource::invert()
extern void BaseLuminanceSource_invert_mAF6BBA3BCBAE3DEEFB1C6050C3F68C433BAB887D (void);
// 0x00000070 ZXing.LuminanceSource ZXing.BaseLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
// 0x00000071 System.Void ZXing.Binarizer::.ctor(ZXing.LuminanceSource)
extern void Binarizer__ctor_m0F52AE2DBB19E1285BAFA1FA87A49B829DEE80A2 (void);
// 0x00000072 ZXing.LuminanceSource ZXing.Binarizer::get_LuminanceSource()
extern void Binarizer_get_LuminanceSource_m96D1FE729022B03C18348C2169311D2F37B7D4CF (void);
// 0x00000073 ZXing.Common.BitArray ZXing.Binarizer::getBlackRow(System.Int32,ZXing.Common.BitArray)
// 0x00000074 ZXing.Common.BitMatrix ZXing.Binarizer::get_BlackMatrix()
// 0x00000075 ZXing.Binarizer ZXing.Binarizer::createBinarizer(ZXing.LuminanceSource)
// 0x00000076 System.Int32 ZXing.Binarizer::get_Width()
extern void Binarizer_get_Width_m67E9FE03694646DB8202EA3B1BC4CCFA4BE04B73 (void);
// 0x00000077 System.Int32 ZXing.Binarizer::get_Height()
extern void Binarizer_get_Height_mC1BF6C86B03A8093999E316AB4D8B18C935CE433 (void);
// 0x00000078 System.Void ZXing.BinaryBitmap::.ctor(ZXing.Binarizer)
extern void BinaryBitmap__ctor_m88F06DDDBE0AECE462A72A417A16F7A27675D81D (void);
// 0x00000079 System.Int32 ZXing.BinaryBitmap::get_Width()
extern void BinaryBitmap_get_Width_m2A94EDDCD504C85C062E9C2E07983DBBE9C017D4 (void);
// 0x0000007A System.Int32 ZXing.BinaryBitmap::get_Height()
extern void BinaryBitmap_get_Height_mF40A3E716EA43DE8D0EF07624B600DF02075C264 (void);
// 0x0000007B ZXing.Common.BitArray ZXing.BinaryBitmap::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern void BinaryBitmap_getBlackRow_m77B2DEE934886812C456E4ACB6E11290EB11D6D4 (void);
// 0x0000007C ZXing.Common.BitMatrix ZXing.BinaryBitmap::get_BlackMatrix()
extern void BinaryBitmap_get_BlackMatrix_mE039C6FA6553C4DF01CEBC1F41B246478874A371 (void);
// 0x0000007D System.Boolean ZXing.BinaryBitmap::get_RotateSupported()
extern void BinaryBitmap_get_RotateSupported_m311B6F02BD37CD2DEFB5BD0BE3536A5079B39810 (void);
// 0x0000007E ZXing.BinaryBitmap ZXing.BinaryBitmap::rotateCounterClockwise()
extern void BinaryBitmap_rotateCounterClockwise_mA0E65FCAD3227B4D14C8C3D99524846E3CA90034 (void);
// 0x0000007F System.String ZXing.BinaryBitmap::ToString()
extern void BinaryBitmap_ToString_mD1D0EA7561E739ACEF307E158E7F39DE21E6AFF3 (void);
// 0x00000080 System.Void ZXing.FormatException::.ctor()
extern void FormatException__ctor_m647F5EE083E5A609EAECBE9FEBC4227FD4354617 (void);
// 0x00000081 System.Void ZXing.FormatException::.ctor(System.String)
extern void FormatException__ctor_m1A8EAA0C4127CB67D646E9CF8074C5441D706316 (void);
// 0x00000082 System.Void ZXing.InvertedLuminanceSource::.ctor(ZXing.LuminanceSource)
extern void InvertedLuminanceSource__ctor_m22EAC127CBFBC15128C37085F9243F50E3656641 (void);
// 0x00000083 System.Byte[] ZXing.InvertedLuminanceSource::getRow(System.Int32,System.Byte[])
extern void InvertedLuminanceSource_getRow_m1B3E682D322C8B6C0CF0367029D0F68C8690DC96 (void);
// 0x00000084 System.Byte[] ZXing.InvertedLuminanceSource::get_Matrix()
extern void InvertedLuminanceSource_get_Matrix_m77A11D0929DD94654C64F3063EF3146C2966D023 (void);
// 0x00000085 System.Boolean ZXing.InvertedLuminanceSource::get_RotateSupported()
extern void InvertedLuminanceSource_get_RotateSupported_m31B260FB0294A5BE543F171C059210B4E755031C (void);
// 0x00000086 ZXing.LuminanceSource ZXing.InvertedLuminanceSource::invert()
extern void InvertedLuminanceSource_invert_m6CC3E636C0FC2982AE55193C5A92900096B7F860 (void);
// 0x00000087 ZXing.LuminanceSource ZXing.InvertedLuminanceSource::rotateCounterClockwise()
extern void InvertedLuminanceSource_rotateCounterClockwise_m5E97F77ED08238B267321AA53793256357172081 (void);
// 0x00000088 System.Void ZXing.LuminanceSource::.ctor(System.Int32,System.Int32)
extern void LuminanceSource__ctor_m04CAB451BD316FE4955459E1951E42A8A5592250 (void);
// 0x00000089 System.Byte[] ZXing.LuminanceSource::getRow(System.Int32,System.Byte[])
// 0x0000008A System.Byte[] ZXing.LuminanceSource::get_Matrix()
// 0x0000008B System.Int32 ZXing.LuminanceSource::get_Width()
extern void LuminanceSource_get_Width_m329DEE06AE3A28E458AB338395EAC9C82A1445BB (void);
// 0x0000008C System.Int32 ZXing.LuminanceSource::get_Height()
extern void LuminanceSource_get_Height_m7E254CE85D5991BB54E93F97F9952932C08D5F41 (void);
// 0x0000008D System.Boolean ZXing.LuminanceSource::get_RotateSupported()
extern void LuminanceSource_get_RotateSupported_mA29BC600777086CBC16872FA42F4AD98F2536174 (void);
// 0x0000008E ZXing.LuminanceSource ZXing.LuminanceSource::rotateCounterClockwise()
extern void LuminanceSource_rotateCounterClockwise_mAF9DB3D552B78C9F15DBB6C1CBF67C64DB56954B (void);
// 0x0000008F System.Boolean ZXing.LuminanceSource::get_InversionSupported()
extern void LuminanceSource_get_InversionSupported_m65DABD8C907996CCCA249F98ACB648D62F03FDCF (void);
// 0x00000090 ZXing.LuminanceSource ZXing.LuminanceSource::invert()
extern void LuminanceSource_invert_m4FF1F17ECB48E4C17CBDAF77BC64CD93D3186025 (void);
// 0x00000091 System.String ZXing.LuminanceSource::ToString()
extern void LuminanceSource_ToString_m32BD816245B5553972092A0BF8211A6D9D586DE7 (void);
// 0x00000092 ZXing.Result ZXing.MultiFormatReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatReader_decode_mC245093E036662C897AE475475B55A056B2D54B5 (void);
// 0x00000093 ZXing.Result ZXing.MultiFormatReader::decodeWithState(ZXing.BinaryBitmap)
extern void MultiFormatReader_decodeWithState_mBA73F14533546926E09AD93E8E4513DD8884634C (void);
// 0x00000094 System.Void ZXing.MultiFormatReader::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatReader_set_Hints_m871E32EB5212D66C01509F84D27C03E5FAF07D08 (void);
// 0x00000095 System.Void ZXing.MultiFormatReader::reset()
extern void MultiFormatReader_reset_m2D2ABEBC56020D7F866DCACA9735C9EF54EB1888 (void);
// 0x00000096 ZXing.Result ZXing.MultiFormatReader::decodeInternal(ZXing.BinaryBitmap)
extern void MultiFormatReader_decodeInternal_mDDB723455EB58FA2585EE80CEDCC4FAE57DF6161 (void);
// 0x00000097 System.Void ZXing.MultiFormatReader::.ctor()
extern void MultiFormatReader__ctor_m4CAEE21C0DD38926FE801C39E4223BB5666BCFAE (void);
// 0x00000098 ZXing.Result ZXing.Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
// 0x00000099 System.Void ZXing.Reader::reset()
// 0x0000009A System.Void ZXing.ReaderException::.ctor()
extern void ReaderException__ctor_m24690C0D9D85BCCB26ED6D33C2452AD4BF66D2B7 (void);
// 0x0000009B System.Void ZXing.ReaderException::.ctor(System.String)
extern void ReaderException__ctor_mAE3743148B371202AFBE5115E3B0C868A534DC58 (void);
// 0x0000009C System.String ZXing.Result::get_Text()
extern void Result_get_Text_m855527BB9C7057F702BDABD42E7DECC1572721EB (void);
// 0x0000009D System.Void ZXing.Result::set_Text(System.String)
extern void Result_set_Text_mCE77595E7629B6DC028B96FC6B1001D3553B4827 (void);
// 0x0000009E System.Byte[] ZXing.Result::get_RawBytes()
extern void Result_get_RawBytes_m37EADC349C16DD0F5A01A9153DFFBAE55BC6D708 (void);
// 0x0000009F System.Void ZXing.Result::set_RawBytes(System.Byte[])
extern void Result_set_RawBytes_mAE2D1DC82194151BE42AB1B9D3A9420880111142 (void);
// 0x000000A0 ZXing.ResultPoint[] ZXing.Result::get_ResultPoints()
extern void Result_get_ResultPoints_mB6717EF744141D983630A17EDACB39C11BE452FC (void);
// 0x000000A1 System.Void ZXing.Result::set_ResultPoints(ZXing.ResultPoint[])
extern void Result_set_ResultPoints_mDC64CD4147E895472D1C2417141086D9F8EC15E2 (void);
// 0x000000A2 ZXing.BarcodeFormat ZXing.Result::get_BarcodeFormat()
extern void Result_get_BarcodeFormat_m446306E9DC37F14EB447E0C711A55A45D5514AA7 (void);
// 0x000000A3 System.Void ZXing.Result::set_BarcodeFormat(ZXing.BarcodeFormat)
extern void Result_set_BarcodeFormat_m66740266B609E88E7C1A8496F4369AA9B17F3733 (void);
// 0x000000A4 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.Result::get_ResultMetadata()
extern void Result_get_ResultMetadata_m82489B9ED570D7F50058B680DCEF0D126448129C (void);
// 0x000000A5 System.Void ZXing.Result::set_ResultMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern void Result_set_ResultMetadata_mD48ACD9D2F3A263E6E705391CDCA38E9B2958CA4 (void);
// 0x000000A6 System.Void ZXing.Result::set_Timestamp(System.Int64)
extern void Result_set_Timestamp_m9399B76A449D904D5773C5463656BE35040F70D8 (void);
// 0x000000A7 System.Void ZXing.Result::set_NumBits(System.Int32)
extern void Result_set_NumBits_m0EB6FB28263F1E58FB521CB0E151B21A344D74DB (void);
// 0x000000A8 System.Void ZXing.Result::.ctor(System.String,System.Byte[],ZXing.ResultPoint[],ZXing.BarcodeFormat)
extern void Result__ctor_m2E5656F74156AA6A52C41278F52EB2D9A99053B8 (void);
// 0x000000A9 System.Void ZXing.Result::.ctor(System.String,System.Byte[],System.Int32,ZXing.ResultPoint[],ZXing.BarcodeFormat)
extern void Result__ctor_mE029B6841F8B010A41DAD6A7D84EEAF5B43387C7 (void);
// 0x000000AA System.Void ZXing.Result::.ctor(System.String,System.Byte[],System.Int32,ZXing.ResultPoint[],ZXing.BarcodeFormat,System.Int64)
extern void Result__ctor_m64289D6F7B1CB69C95F8413B68EE440FA8ADE851 (void);
// 0x000000AB System.Void ZXing.Result::putMetadata(ZXing.ResultMetadataType,System.Object)
extern void Result_putMetadata_mA112D7FE7ED706B541956298BF9F0C9820655980 (void);
// 0x000000AC System.Void ZXing.Result::putAllMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern void Result_putAllMetadata_mD4A8E3866D1F651A37C0DF12CC87110224AA1FF6 (void);
// 0x000000AD System.Void ZXing.Result::addResultPoints(ZXing.ResultPoint[])
extern void Result_addResultPoints_m8D98B36BACD22D5F930B24C2007F93083822FD66 (void);
// 0x000000AE System.String ZXing.Result::ToString()
extern void Result_ToString_m96C5B816773242D4D1423ABBB7DC4F24D57386F6 (void);
// 0x000000AF System.Void ZXing.ResultPoint::.ctor(System.Single,System.Single)
extern void ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765 (void);
// 0x000000B0 System.Single ZXing.ResultPoint::get_X()
extern void ResultPoint_get_X_m5B20F6C7C7F0BC0D44AC0C33CFE70CA65832167D (void);
// 0x000000B1 System.Single ZXing.ResultPoint::get_Y()
extern void ResultPoint_get_Y_m261B83F542DE7D81B20CB743E630090963D322D2 (void);
// 0x000000B2 System.Boolean ZXing.ResultPoint::Equals(System.Object)
extern void ResultPoint_Equals_m6CE5C8EC1E965B3F620BDF0B986B06CE4E4E55A2 (void);
// 0x000000B3 System.Int32 ZXing.ResultPoint::GetHashCode()
extern void ResultPoint_GetHashCode_m880A744140B803242B7E4208E5B7CFF58DE75241 (void);
// 0x000000B4 System.String ZXing.ResultPoint::ToString()
extern void ResultPoint_ToString_mAD26D2C86C2F4EA7E2D6460D7920B718B5A56812 (void);
// 0x000000B5 System.Void ZXing.ResultPoint::orderBestPatterns(ZXing.ResultPoint[])
extern void ResultPoint_orderBestPatterns_mE5F97B8CC95CD483C6473EF47BF778449D82F0EE (void);
// 0x000000B6 System.Single ZXing.ResultPoint::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern void ResultPoint_distance_mA230ADB22766B14962659AC7C60BFA148E7C58F7 (void);
// 0x000000B7 System.Single ZXing.ResultPoint::crossProductZ(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void ResultPoint_crossProductZ_mFC376C0601939F03EB399524E60E40B1268A5A7D (void);
// 0x000000B8 System.Void ZXing.ResultPointCallback::.ctor(System.Object,System.IntPtr)
extern void ResultPointCallback__ctor_m83C444A8F9C641C1BF6224C349BB14A280B85B46 (void);
// 0x000000B9 System.Void ZXing.ResultPointCallback::Invoke(ZXing.ResultPoint)
extern void ResultPointCallback_Invoke_m70AE55890223412EFFEFA10329D3873820BE81D7 (void);
// 0x000000BA System.IAsyncResult ZXing.ResultPointCallback::BeginInvoke(ZXing.ResultPoint,System.AsyncCallback,System.Object)
extern void ResultPointCallback_BeginInvoke_m10230E7037C140B1B64B0D014C920868D7308153 (void);
// 0x000000BB System.Void ZXing.ResultPointCallback::EndInvoke(System.IAsyncResult)
extern void ResultPointCallback_EndInvoke_m7B03D332DC677EBD3478CF875EBB45D24911133E (void);
// 0x000000BC System.Void ZXing.RGBLuminanceSource::.ctor(System.Int32,System.Int32)
extern void RGBLuminanceSource__ctor_m5C386B71A0E7B462A3A47DD13C41A922913C8B71 (void);
// 0x000000BD System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void RGBLuminanceSource__ctor_m3521EC26C240CFA8FA8AF04A3AC09B97ABDB6CB3 (void);
// 0x000000BE ZXing.LuminanceSource ZXing.RGBLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern void RGBLuminanceSource_CreateLuminanceSource_mCA8AB621F0FE47E380F0DFC0BFCD44456E9C2804 (void);
// 0x000000BF ZXing.RGBLuminanceSource/BitmapFormat ZXing.RGBLuminanceSource::DetermineBitmapFormat(System.Byte[],System.Int32,System.Int32)
extern void RGBLuminanceSource_DetermineBitmapFormat_mB815B701CD9F38EEACD36DD79111DF0C09AD0B13 (void);
// 0x000000C0 System.Void ZXing.RGBLuminanceSource::CalculateLuminance(System.Byte[],ZXing.RGBLuminanceSource/BitmapFormat)
extern void RGBLuminanceSource_CalculateLuminance_m55FA65667B7EA9296D2E47068DF8EB9CE21B70A9 (void);
// 0x000000C1 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB565(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB565_m2430B7C22B8A3515EC659CA247DEB0E2669025E5 (void);
// 0x000000C2 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB24(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB24_mBDF513DD2E6823159600CDCF3AC657A6C0515D00 (void);
// 0x000000C3 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR24(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGR24_m621BA127E825418417A3762C5D59D2EBC72A7ABD (void);
// 0x000000C4 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB32_m103C2A4B5F096B56ECC4AFCA30B06121A53C8AF5 (void);
// 0x000000C5 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGR32_mC0FE59A71EA79BF88A6AC9F90E62A5CAD7661AAF (void);
// 0x000000C6 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGRA32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGRA32_m7627AC1D7D91F8C6FBC56EC8DAAE275F7CF1FD9C (void);
// 0x000000C7 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGBA32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGBA32_m49E384F6BA76FC2D067CDF9E34B77583AA6C7B4E (void);
// 0x000000C8 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceARGB32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceARGB32_mA22CFFA0B209F2749BE02338CB67AFD45413A1D3 (void);
// 0x000000C9 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceUYVY(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceUYVY_m6F0893D78E61921B29640B7724607FAF07772232 (void);
// 0x000000CA System.Void ZXing.RGBLuminanceSource::CalculateLuminanceYUYV(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceYUYV_m1AEC33F069050D448D2D68BFF2F41E43883CBC92 (void);
// 0x000000CB System.Void ZXing.RGBLuminanceSource::CalculateLuminanceGray16(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceGray16_m9B8868DDBCE9C24972D25DE9922B2A3C3ADE310A (void);
// 0x000000CC System.Void ZXing.SupportClass::Fill(T[],T)
// 0x000000CD System.Void ZXing.SupportClass::Fill(T[],System.Int32,System.Int32,T)
// 0x000000CE System.Int32 ZXing.SupportClass::bitCount(System.Int32)
extern void SupportClass_bitCount_m3C9E2E53F2C5AD771F17EDFD0E0E383F1DA72D9D (void);
// 0x000000CF T ZXing.SupportClass::GetValue(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
// 0x000000D0 System.Void ZXing.Color32LuminanceSource::.ctor(System.Int32,System.Int32)
extern void Color32LuminanceSource__ctor_m1FFB91A7BCE26C80BF887FD86EEB4F9F410B1A1C (void);
// 0x000000D1 System.Void ZXing.Color32LuminanceSource::.ctor(UnityEngine.Color32[],System.Int32,System.Int32)
extern void Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677 (void);
// 0x000000D2 System.Void ZXing.Color32LuminanceSource::SetPixels(UnityEngine.Color32[])
extern void Color32LuminanceSource_SetPixels_m9BCC155B50914AA2DD6A19F3DDEFF89101FE21E4 (void);
// 0x000000D3 ZXing.LuminanceSource ZXing.Color32LuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern void Color32LuminanceSource_CreateLuminanceSource_m0B591ED16CF4AC52667E888F44FB53DEED789043 (void);
// 0x000000D4 ZXing.Result ZXing.QrCode.QRCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void QRCodeReader_decode_mE6AAD8A5595C3052903C397935D9972B8DEC55C4 (void);
// 0x000000D5 System.Void ZXing.QrCode.QRCodeReader::reset()
extern void QRCodeReader_reset_m6A716A221BA9E7260E7A260CAF569B369876DE4B (void);
// 0x000000D6 ZXing.Common.BitMatrix ZXing.QrCode.QRCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern void QRCodeReader_extractPureBits_mDD6DA398EB59E894B6086D458FEBA2982764CA7E (void);
// 0x000000D7 System.Boolean ZXing.QrCode.QRCodeReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Single&)
extern void QRCodeReader_moduleSize_m184DA683EA13B7E7537F596E1D5CBF941E2A7D65 (void);
// 0x000000D8 System.Void ZXing.QrCode.QRCodeReader::.ctor()
extern void QRCodeReader__ctor_m8ED215084135AA81D2674D6C3C7315D534A21EEE (void);
// 0x000000D9 System.Void ZXing.QrCode.QRCodeReader::.cctor()
extern void QRCodeReader__cctor_m2C6853D3F01D8702770EE09D6221720F91AE7824 (void);
// 0x000000DA ZXing.QrCode.Internal.BitMatrixParser ZXing.QrCode.Internal.BitMatrixParser::createBitMatrixParser(ZXing.Common.BitMatrix)
extern void BitMatrixParser_createBitMatrixParser_m4BD4DBD284167504535F88B8EEB40A28BEB3AB27 (void);
// 0x000000DB System.Void ZXing.QrCode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_m488B886FDE1F58840C88B743269CD00D0EA241D0 (void);
// 0x000000DC ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.BitMatrixParser::readFormatInformation()
extern void BitMatrixParser_readFormatInformation_m293370F149F582DC92BC08C9468B151114B8003B (void);
// 0x000000DD ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.BitMatrixParser::readVersion()
extern void BitMatrixParser_readVersion_m66CB3B68B791E7AB65E5555449D050E98CB6160C (void);
// 0x000000DE System.Int32 ZXing.QrCode.Internal.BitMatrixParser::copyBit(System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_copyBit_m4666E901BBC1CD2021A26B1BECA67A93198B7A26 (void);
// 0x000000DF System.Byte[] ZXing.QrCode.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_mC91E8CA4FD5EFAB072C3BE7E63ED1D297434A866 (void);
// 0x000000E0 System.Void ZXing.QrCode.Internal.BitMatrixParser::remask()
extern void BitMatrixParser_remask_m67CAA2E1A31FA3BEF1FF9FA826F06439B1D3DFE6 (void);
// 0x000000E1 System.Void ZXing.QrCode.Internal.BitMatrixParser::setMirror(System.Boolean)
extern void BitMatrixParser_setMirror_m7C1DF63BEF6CFB0F2439432163D2D6163E7D89A4 (void);
// 0x000000E2 System.Void ZXing.QrCode.Internal.BitMatrixParser::mirror()
extern void BitMatrixParser_mirror_m9C9AF9F0EDD3B7FF6BC20975676BE6B4955C9C64 (void);
// 0x000000E3 System.Void ZXing.QrCode.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern void DataBlock__ctor_mD28EB5FD282FB58109B1BA57DDE9A82EDA8995E1 (void);
// 0x000000E4 ZXing.QrCode.Internal.DataBlock[] ZXing.QrCode.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void DataBlock_getDataBlocks_m0D5783F85BE7E49F8F4CFE3A46E522C0055E4A86 (void);
// 0x000000E5 System.Int32 ZXing.QrCode.Internal.DataBlock::get_NumDataCodewords()
extern void DataBlock_get_NumDataCodewords_mEE8E4881D502BC9C28DDD16073038D94A4A15530 (void);
// 0x000000E6 System.Byte[] ZXing.QrCode.Internal.DataBlock::get_Codewords()
extern void DataBlock_get_Codewords_mD329189BB8471B482C4710F3E42687B421832EFC (void);
// 0x000000E7 System.Void ZXing.QrCode.Internal.DataMask::unmaskBitMatrix(System.Int32,ZXing.Common.BitMatrix,System.Int32)
extern void DataMask_unmaskBitMatrix_m58082B0C86D433F96B76CF89D85B2123327581AF (void);
// 0x000000E8 System.Void ZXing.QrCode.Internal.DataMask::.cctor()
extern void DataMask__cctor_m627DDE9E6011A96521E6FBDB2F0B5CB23FD38262 (void);
// 0x000000E9 System.Void ZXing.QrCode.Internal.DataMask/<>c::.cctor()
extern void U3CU3Ec__cctor_m5C99D6D8DF32E075FFEFEA62D5D8BC0B7794FB0F (void);
// 0x000000EA System.Void ZXing.QrCode.Internal.DataMask/<>c::.ctor()
extern void U3CU3Ec__ctor_m8EBA2F4F5936544E00D4494EBF428E326B8614DC (void);
// 0x000000EB System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_0(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_0_m9276BBEA1A2866654015CF183BA1EA34634C2C53 (void);
// 0x000000EC System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_1(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_1_mC6827C7C353AFD1A41149FB722EC788073198819 (void);
// 0x000000ED System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_2(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_2_mF88732B03F67C94E33EF8EC66053FF16275BE019 (void);
// 0x000000EE System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_3(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_3_m518AF36E4A955D6AC2AC63AD6C62148136489428 (void);
// 0x000000EF System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_4(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_4_mDE0A722B0F44EC9EA4960179BB791ED07E9344C0 (void);
// 0x000000F0 System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_5(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_5_m4B2C1B6AD542F0CD515AE18E5BDFB5355566C7EF (void);
// 0x000000F1 System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_6(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_6_mCD855E2C5E74C424E0E278B509172854DA829CB1 (void);
// 0x000000F2 System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_7(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_7_mC3E1E3D988091B923B9BD0656968C29A7627142B (void);
// 0x000000F3 ZXing.Common.DecoderResult ZXing.QrCode.Internal.DecodedBitStreamParser::decode(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodedBitStreamParser_decode_m0717755E012876AE8DA19D7C4DD368CA219E6777 (void);
// 0x000000F4 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeHanziSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeHanziSegment_m7D5E2CAEFEF61FA35B8C25B74E5E9981D90C67BC (void);
// 0x000000F5 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeKanjiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeKanjiSegment_m839D217CA235F452F1ACCC204DF3673A8E4F6DA2 (void);
// 0x000000F6 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeByteSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,ZXing.Common.CharacterSetECI,System.Collections.Generic.IList`1<System.Byte[]>,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodedBitStreamParser_decodeByteSegment_mB3F60B838C11286B72424E04A1577219E688E3B5 (void);
// 0x000000F7 System.Char ZXing.QrCode.Internal.DecodedBitStreamParser::toAlphaNumericChar(System.Int32)
extern void DecodedBitStreamParser_toAlphaNumericChar_mFC19A72906808C99E821F618DDFF9C361D37935A (void);
// 0x000000F8 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeAlphanumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,System.Boolean)
extern void DecodedBitStreamParser_decodeAlphanumericSegment_mF42F9CD847AF5E3A0E29B71F9014DE23A3273BE2 (void);
// 0x000000F9 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeNumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeNumericSegment_m10D0F971E8655CBC641C240E21F5827876B71695 (void);
// 0x000000FA System.Int32 ZXing.QrCode.Internal.DecodedBitStreamParser::parseECIValue(ZXing.Common.BitSource)
extern void DecodedBitStreamParser_parseECIValue_mE66AFA3A1F395A3EB2CB0C6DA170AF90268E95BB (void);
// 0x000000FB System.Void ZXing.QrCode.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_m2650820A9EB13A483FDCA4CDD64189073D19563C (void);
// 0x000000FC System.Void ZXing.QrCode.Internal.Decoder::.ctor()
extern void Decoder__ctor_m3EDBF4259494302C6BC25F1C7C1BFB852D1E6A83 (void);
// 0x000000FD ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_mD106DE4AFF03ABC728700F9AA3AD16D3D1583828 (void);
// 0x000000FE ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.QrCode.Internal.BitMatrixParser,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_mD7AFA763EDDF7C72262558330466BEA4EF3F912F (void);
// 0x000000FF System.Boolean ZXing.QrCode.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern void Decoder_correctErrors_m7DB9DD42FCEE9EB439BCAA935973376863B71D3B (void);
// 0x00000100 System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.ctor(System.Int32,System.Int32,System.String)
extern void ErrorCorrectionLevel__ctor_m6CC38238785F128FE93C28D42B10D97F5EE4A456 (void);
// 0x00000101 System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal()
extern void ErrorCorrectionLevel_ordinal_mACCE779D514AF2084CA531514F85028357ABE219 (void);
// 0x00000102 System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::ToString()
extern void ErrorCorrectionLevel_ToString_m4AB1D868FF8EBC59C071ED87963ECD11676C5DC7 (void);
// 0x00000103 ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::forBits(System.Int32)
extern void ErrorCorrectionLevel_forBits_m660A8D4F1F8BC5C374511AB8EE04DB3EAFE28A7B (void);
// 0x00000104 System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.cctor()
extern void ErrorCorrectionLevel__cctor_mAF208B3752A00FA5783DA9DE4DEE16B5C53B112F (void);
// 0x00000105 System.Void ZXing.QrCode.Internal.FormatInformation::.ctor(System.Int32)
extern void FormatInformation__ctor_m1AF28518CFD7FD886D43069BE3EC4D7336F436E1 (void);
// 0x00000106 System.Int32 ZXing.QrCode.Internal.FormatInformation::numBitsDiffering(System.Int32,System.Int32)
extern void FormatInformation_numBitsDiffering_mD6BF158CF9F2D2AE0A41801B858CBECFBC2FBC17 (void);
// 0x00000107 ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::decodeFormatInformation(System.Int32,System.Int32)
extern void FormatInformation_decodeFormatInformation_m58EA6CD596C7C945CEC945A4F47A06C9362DBC52 (void);
// 0x00000108 ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::doDecodeFormatInformation(System.Int32,System.Int32)
extern void FormatInformation_doDecodeFormatInformation_mA06A5DF90A5326FBF95A7ABB049D80141594855B (void);
// 0x00000109 ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.FormatInformation::get_ErrorCorrectionLevel()
extern void FormatInformation_get_ErrorCorrectionLevel_mAFB247A0D343207EE8190EC372A91D7C2AD7C08A (void);
// 0x0000010A System.Byte ZXing.QrCode.Internal.FormatInformation::get_DataMask()
extern void FormatInformation_get_DataMask_m3413A9030144B748FC203CC982066781483CA744 (void);
// 0x0000010B System.Int32 ZXing.QrCode.Internal.FormatInformation::GetHashCode()
extern void FormatInformation_GetHashCode_m8FF35549D89FACD5D9E129A6312918C4A0B3ABA9 (void);
// 0x0000010C System.Boolean ZXing.QrCode.Internal.FormatInformation::Equals(System.Object)
extern void FormatInformation_Equals_mB3AF3B66C04CE1A57707F2E7FE8B928979BF490E (void);
// 0x0000010D System.Void ZXing.QrCode.Internal.FormatInformation::.cctor()
extern void FormatInformation__cctor_m233C0C568E89B58AD8D4CA5B66787E17AB64CE84 (void);
// 0x0000010E ZXing.QrCode.Internal.Mode/Names ZXing.QrCode.Internal.Mode::get_Name()
extern void Mode_get_Name_mDD10EE936C24F81A4E3C5BB7A483853C5F22E448 (void);
// 0x0000010F System.Void ZXing.QrCode.Internal.Mode::set_Name(ZXing.QrCode.Internal.Mode/Names)
extern void Mode_set_Name_m75EFECA9B152C3114257443AD3CDD16A9FD5A789 (void);
// 0x00000110 System.Void ZXing.QrCode.Internal.Mode::.ctor(System.Int32[],System.Int32,ZXing.QrCode.Internal.Mode/Names)
extern void Mode__ctor_m6FDE1CD3353C4DEE8ED08CDD722438AF05F38356 (void);
// 0x00000111 ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::forBits(System.Int32)
extern void Mode_forBits_m05EB5F209EF1983F2A8C26CACB8C54DEE9A1C79F (void);
// 0x00000112 System.Int32 ZXing.QrCode.Internal.Mode::getCharacterCountBits(ZXing.QrCode.Internal.Version)
extern void Mode_getCharacterCountBits_m9FD5F0F6766306FBC4AD95DC637904E75FF322D2 (void);
// 0x00000113 System.Void ZXing.QrCode.Internal.Mode::set_Bits(System.Int32)
extern void Mode_set_Bits_m050E659D3B05D670BCE285DE9125B69816015542 (void);
// 0x00000114 System.String ZXing.QrCode.Internal.Mode::ToString()
extern void Mode_ToString_m7BD60BEAA5460C46F62626FBAB01876303C58496 (void);
// 0x00000115 System.Void ZXing.QrCode.Internal.Mode::.cctor()
extern void Mode__cctor_m4389EE3D623C6B93FB61AD2A183B3FCD5A0DDDAF (void);
// 0x00000116 System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::.ctor(System.Boolean)
extern void QRCodeDecoderMetaData__ctor_m42F73134DA32859E57D0626BABCDE9D85BFA5FE2 (void);
// 0x00000117 System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::applyMirroredCorrection(ZXing.ResultPoint[])
extern void QRCodeDecoderMetaData_applyMirroredCorrection_m90E63015B2BF54FD75E5DCE10DB19A4CF26EBE5A (void);
// 0x00000118 System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version/ECBlocks[])
extern void Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F (void);
// 0x00000119 System.Int32 ZXing.QrCode.Internal.Version::get_VersionNumber()
extern void Version_get_VersionNumber_mFF023A4E25FDB34A4001A0355A2FCDBA9CBB8D89 (void);
// 0x0000011A System.Int32[] ZXing.QrCode.Internal.Version::get_AlignmentPatternCenters()
extern void Version_get_AlignmentPatternCenters_mADD8DB8B178CE125E1AA4F2EE89146BA5E460FBF (void);
// 0x0000011B System.Int32 ZXing.QrCode.Internal.Version::get_TotalCodewords()
extern void Version_get_TotalCodewords_m434BD18856962BE8E10F85144B29D13779377269 (void);
// 0x0000011C System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
extern void Version_get_DimensionForVersion_m9589220D4223CADC2D0E87E0949E3B181128B705 (void);
// 0x0000011D ZXing.QrCode.Internal.Version/ECBlocks ZXing.QrCode.Internal.Version::getECBlocksForLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Version_getECBlocksForLevel_m32EDCF33F6787A0C075284CAA1ED12FAF3321880 (void);
// 0x0000011E ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getProvisionalVersionForDimension(System.Int32)
extern void Version_getProvisionalVersionForDimension_mB8DFB555C74A1AE3CCDA5BE3E5C88650A6396FCD (void);
// 0x0000011F ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
extern void Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572 (void);
// 0x00000120 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::decodeVersionInformation(System.Int32)
extern void Version_decodeVersionInformation_mB7EC7E529C9F39F58632DB1A97AE3404DD1CA37D (void);
// 0x00000121 ZXing.Common.BitMatrix ZXing.QrCode.Internal.Version::buildFunctionPattern()
extern void Version_buildFunctionPattern_mD94BB465EDBD252F1A25B1B3EAD67C2468136684 (void);
// 0x00000122 System.String ZXing.QrCode.Internal.Version::ToString()
extern void Version_ToString_m9AEF9ED09794C93CE75C2487757FC2786B8D6A23 (void);
// 0x00000123 ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
extern void Version_buildVersions_m4FF9D42DFED8563CAE3AF70AB90A30046EC1026E (void);
// 0x00000124 System.Void ZXing.QrCode.Internal.Version::.cctor()
extern void Version__cctor_m8BC73054B966C5EF8D307E0BE69564C356A10561 (void);
// 0x00000125 System.Void ZXing.QrCode.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version/ECB[])
extern void ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017 (void);
// 0x00000126 System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_ECCodewordsPerBlock()
extern void ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA (void);
// 0x00000127 ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::getECBlocks()
extern void ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3 (void);
// 0x00000128 System.Void ZXing.QrCode.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
extern void ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784 (void);
// 0x00000129 System.Int32 ZXing.QrCode.Internal.Version/ECB::get_Count()
extern void ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567 (void);
// 0x0000012A System.Int32 ZXing.QrCode.Internal.Version/ECB::get_DataCodewords()
extern void ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F (void);
// 0x0000012B System.Void ZXing.QrCode.Internal.AlignmentPattern::.ctor(System.Single,System.Single,System.Single)
extern void AlignmentPattern__ctor_m561C82DA1B7202ED7051CE02661E2E6AA19A991C (void);
// 0x0000012C System.Boolean ZXing.QrCode.Internal.AlignmentPattern::aboutEquals(System.Single,System.Single,System.Single)
extern void AlignmentPattern_aboutEquals_m0452A61209837E776BFFB2B789086BFC04E1EE1E (void);
// 0x0000012D ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPattern::combineEstimate(System.Single,System.Single,System.Single)
extern void AlignmentPattern_combineEstimate_m55522D2D25D8BC699883543E75AB9FE55687109C (void);
// 0x0000012E System.Void ZXing.QrCode.Internal.AlignmentPatternFinder::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,ZXing.ResultPointCallback)
extern void AlignmentPatternFinder__ctor_mF8F2D9EC295ACA0886A520ED60FFD9B6C54480E0 (void);
// 0x0000012F ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::find()
extern void AlignmentPatternFinder_find_mEF736BE3133B9D9291FEC49F4534AB957F97CE9E (void);
// 0x00000130 System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern void AlignmentPatternFinder_centerFromEnd_m91143BCDF4C242D7B4619258B14D860C6F73FFB0 (void);
// 0x00000131 System.Boolean ZXing.QrCode.Internal.AlignmentPatternFinder::foundPatternCross(System.Int32[])
extern void AlignmentPatternFinder_foundPatternCross_mB38D974447F34D38B9F5F1FE9D80CE3254F81FB0 (void);
// 0x00000132 System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern void AlignmentPatternFinder_crossCheckVertical_mB25462EFA619E29BCDCFB5B57855D027A6F44454 (void);
// 0x00000133 ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32)
extern void AlignmentPatternFinder_handlePossibleCenter_m7859C3002064AD0DE01A712AD1A03DE0E8CBC07E (void);
// 0x00000134 System.Void ZXing.QrCode.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_mA51E6ED117335B751BB0AD7F43905D1191EC4047 (void);
// 0x00000135 ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::detect(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Detector_detect_m202353711911054236A4EB3BC3BA8FDE3058AE61 (void);
// 0x00000136 ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::processFinderPatternInfo(ZXing.QrCode.Internal.FinderPatternInfo)
extern void Detector_processFinderPatternInfo_m1F6BAAB9DCF7D72467C842718E0B163960C8498A (void);
// 0x00000137 ZXing.Common.PerspectiveTransform ZXing.QrCode.Internal.Detector::createTransform(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_createTransform_m3DFB34C80B2032B16F1ACE2BB6C3E2BC11A65E20 (void);
// 0x00000138 ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.Common.PerspectiveTransform,System.Int32)
extern void Detector_sampleGrid_mB89244F0CCE7DA095A01A1B43392D44CB4624595 (void);
// 0x00000139 System.Boolean ZXing.QrCode.Internal.Detector::computeDimension(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Single,System.Int32&)
extern void Detector_computeDimension_m2771F51355F9C0B482EEDF418648BEB11AA6521C (void);
// 0x0000013A System.Single ZXing.QrCode.Internal.Detector::calculateModuleSize(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_calculateModuleSize_m082A25F976C45CBE0D542FFD746DA00F8763FB97 (void);
// 0x0000013B System.Single ZXing.QrCode.Internal.Detector::calculateModuleSizeOneWay(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_calculateModuleSizeOneWay_mB1D93DA3FE041D4CFB5C3D0EEFEC7B68CD1E89F8 (void);
// 0x0000013C System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRunBothWays(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Detector_sizeOfBlackWhiteBlackRunBothWays_m59A0B29C4EB953B60F701889784E53650827190C (void);
// 0x0000013D System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRun(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Detector_sizeOfBlackWhiteBlackRun_m480E379399EA40EA27B8499EB26F92F222D2719D (void);
// 0x0000013E ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.Detector::findAlignmentInRegion(System.Single,System.Int32,System.Int32,System.Single)
extern void Detector_findAlignmentInRegion_mE5D04B173DEF9E7D19F7E1FED956BDAD537A9D2F (void);
// 0x0000013F System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single)
extern void FinderPattern__ctor_mB0A8BB4A6DFD859A208444EFAB594BE77CB45A6D (void);
// 0x00000140 System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single,System.Int32)
extern void FinderPattern__ctor_m3EEB1F7F97F8161ABF067CFC0FAC40BA9A7578E4 (void);
// 0x00000141 System.Single ZXing.QrCode.Internal.FinderPattern::get_EstimatedModuleSize()
extern void FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB (void);
// 0x00000142 System.Int32 ZXing.QrCode.Internal.FinderPattern::get_Count()
extern void FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD (void);
// 0x00000143 System.Boolean ZXing.QrCode.Internal.FinderPattern::aboutEquals(System.Single,System.Single,System.Single)
extern void FinderPattern_aboutEquals_mE6CB8FA276285958E83D55115196BA603F22A5E7 (void);
// 0x00000144 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPattern::combineEstimate(System.Single,System.Single,System.Single)
extern void FinderPattern_combineEstimate_m3B1219F7A1B3F0AC40A6D4FA2167DF26BC31A572 (void);
// 0x00000145 System.Void ZXing.QrCode.Internal.FinderPatternFinder::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPointCallback)
extern void FinderPatternFinder__ctor_m99744301D39C0CEAA402F97AE05E822C84A83A86 (void);
// 0x00000146 ZXing.QrCode.Internal.FinderPatternInfo ZXing.QrCode.Internal.FinderPatternFinder::find(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void FinderPatternFinder_find_mFF4D063D659BBC55DC505814A212DAC53BD4B661 (void);
// 0x00000147 System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern void FinderPatternFinder_centerFromEnd_mEEB777E9A34A7B51B68087436FDDEFA366CE7F45 (void);
// 0x00000148 System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::foundPatternCross(System.Int32[])
extern void FinderPatternFinder_foundPatternCross_m3723765C983F180845F07B636623BC2951D2088B (void);
// 0x00000149 System.Int32[] ZXing.QrCode.Internal.FinderPatternFinder::get_CrossCheckStateCount()
extern void FinderPatternFinder_get_CrossCheckStateCount_m5D76770AA09EC05D1CC337BBA0A26E44CB2BE0E8 (void);
// 0x0000014A System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::crossCheckDiagonal(System.Int32,System.Int32,System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckDiagonal_m7FE440B7FBE9F0182BB7DD2A5D6DCAE061F87A3D (void);
// 0x0000014B System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckVertical_m5E40D06FF9EE64C67611521F2896B92C427F51A1 (void);
// 0x0000014C System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckHorizontal(System.Int32,System.Int32,System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckHorizontal_mF6D602AEE764D1B70E89D8B6E2AE76A286BC52D7 (void);
// 0x0000014D System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32,System.Boolean)
extern void FinderPatternFinder_handlePossibleCenter_mAC18E031D34901C64E6F17365DDBDE2606860CCE (void);
// 0x0000014E System.Int32 ZXing.QrCode.Internal.FinderPatternFinder::findRowSkip()
extern void FinderPatternFinder_findRowSkip_m92FA2A7DCD6E0D9D1C50E7ABD1AF2492BF959856 (void);
// 0x0000014F System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::haveMultiplyConfirmedCenters()
extern void FinderPatternFinder_haveMultiplyConfirmedCenters_m8FFAA60679C5D12829BE1DBF51D2F69A566F0B61 (void);
// 0x00000150 ZXing.QrCode.Internal.FinderPattern[] ZXing.QrCode.Internal.FinderPatternFinder::selectBestPatterns()
extern void FinderPatternFinder_selectBestPatterns_m3FF448C3B7B36B80F0201F6B801149E317534E87 (void);
// 0x00000151 System.Void ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::.ctor(System.Single)
extern void FurthestFromAverageComparator__ctor_m4AE0185AED1653F0E0F2B6B48A79202F3D7428C3 (void);
// 0x00000152 System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern void FurthestFromAverageComparator_Compare_m30841DD8A686609796601BCBD6AFD4FD0FECE93F (void);
// 0x00000153 System.Void ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::.ctor(System.Single)
extern void CenterComparator__ctor_mECCDC684AC2E584356017B90C371F036F4EFEA1B (void);
// 0x00000154 System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern void CenterComparator_Compare_m813791C4BB6556E6BA2628F3CF6FA3DE5C63364C (void);
// 0x00000155 System.Void ZXing.QrCode.Internal.FinderPatternInfo::.ctor(ZXing.QrCode.Internal.FinderPattern[])
extern void FinderPatternInfo__ctor_mD7341E9B171EC066EDF1F56BD9057C7D108D5AB4 (void);
// 0x00000156 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_BottomLeft()
extern void FinderPatternInfo_get_BottomLeft_m94828C8BEC402358A4FF4CD667FADB959F59415A (void);
// 0x00000157 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopLeft()
extern void FinderPatternInfo_get_TopLeft_m04165125ED43A22BE0006DF6128C99E73F0A58BD (void);
// 0x00000158 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopRight()
extern void FinderPatternInfo_get_TopRight_m78E1A0003115723EE9290FF4F6B0C04D9533D483 (void);
// 0x00000159 System.Int32 ZXing.PDF417.PDF417Common::getCodeword(System.Int64)
extern void PDF417Common_getCodeword_mDF71CE71B3AAEFCB36CAD3C8DE45631294808619 (void);
// 0x0000015A System.Void ZXing.PDF417.PDF417Common::.cctor()
extern void PDF417Common__cctor_m15323ED4C983C6F5CFD57508497D93E18A36088F (void);
// 0x0000015B ZXing.Result ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void PDF417Reader_decode_mED4EE6F31440A029D90DAF23DDDE7A5AB2B19591 (void);
// 0x0000015C ZXing.Result[] ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern void PDF417Reader_decode_mE9E96D5F264F7F596351A37B44D0660FB80DD80E (void);
// 0x0000015D System.Int32 ZXing.PDF417.PDF417Reader::getMaxWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern void PDF417Reader_getMaxWidth_mDD46178DCC6969F57173AC060CD4F6C8DD2AEF40 (void);
// 0x0000015E System.Int32 ZXing.PDF417.PDF417Reader::getMinWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern void PDF417Reader_getMinWidth_m93B3075158AFF43C5295FA293FF4193282EBB9AA (void);
// 0x0000015F System.Int32 ZXing.PDF417.PDF417Reader::getMaxCodewordWidth(ZXing.ResultPoint[])
extern void PDF417Reader_getMaxCodewordWidth_mFDA0DF16E67C5D7E749548A0AD63FA260CE55BED (void);
// 0x00000160 System.Int32 ZXing.PDF417.PDF417Reader::getMinCodewordWidth(ZXing.ResultPoint[])
extern void PDF417Reader_getMinCodewordWidth_m2DE1149DF3C1E3DA9F222C46964EB6CC8AA78239 (void);
// 0x00000161 System.Void ZXing.PDF417.PDF417Reader::reset()
extern void PDF417Reader_reset_m25098DD5DCB314AD075584171FD11BBC4C465E35 (void);
// 0x00000162 System.Void ZXing.PDF417.PDF417Reader::.ctor()
extern void PDF417Reader__ctor_m418E06795670458B5292382384741791399715A1 (void);
// 0x00000163 System.Void ZXing.PDF417.PDF417ResultMetadata::set_SegmentIndex(System.Int32)
extern void PDF417ResultMetadata_set_SegmentIndex_m6792A85EE271BFA6063BE4B2A3990743B73C1093 (void);
// 0x00000164 System.Void ZXing.PDF417.PDF417ResultMetadata::set_FileId(System.String)
extern void PDF417ResultMetadata_set_FileId_m6A79CC9F4624FE95AB7A870D742357FE0011F520 (void);
// 0x00000165 System.Int32[] ZXing.PDF417.PDF417ResultMetadata::get_OptionalData()
extern void PDF417ResultMetadata_get_OptionalData_mEFBB80130FA8AE0C7E7657790368E3646220FD7E (void);
// 0x00000166 System.Void ZXing.PDF417.PDF417ResultMetadata::set_OptionalData(System.Int32[])
extern void PDF417ResultMetadata_set_OptionalData_m0D81D688400517D1D65B3E36CF5B9F996B70BC6F (void);
// 0x00000167 System.Void ZXing.PDF417.PDF417ResultMetadata::set_IsLastSegment(System.Boolean)
extern void PDF417ResultMetadata_set_IsLastSegment_m0FC2FB8C14FA37900B7B57397602F5D9DA1E64B8 (void);
// 0x00000168 System.Void ZXing.PDF417.PDF417ResultMetadata::.ctor()
extern void PDF417ResultMetadata__ctor_m967B58DCE1371A9D4AEC7CCA9AA75FE6BF9FBB73 (void);
// 0x00000169 System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ColumnCount()
extern void BarcodeMetadata_get_ColumnCount_m4A9D3AD55F9D68456B8E2DB771835EA55CCDE6C9 (void);
// 0x0000016A System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ColumnCount(System.Int32)
extern void BarcodeMetadata_set_ColumnCount_m176BF152AEF28C1A672DAB8A5BD8278AB083A0DD (void);
// 0x0000016B System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ErrorCorrectionLevel()
extern void BarcodeMetadata_get_ErrorCorrectionLevel_m84BB571AFA2AE2423ED8042DF14BC1DB78EF538C (void);
// 0x0000016C System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ErrorCorrectionLevel(System.Int32)
extern void BarcodeMetadata_set_ErrorCorrectionLevel_m815B1DAB46579F73F17ACBF31E6E39703FD05627 (void);
// 0x0000016D System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountUpper()
extern void BarcodeMetadata_get_RowCountUpper_m03D2B4FC5647BF341DF22E2208586CFE20140301 (void);
// 0x0000016E System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountUpper(System.Int32)
extern void BarcodeMetadata_set_RowCountUpper_m9864EFB99D21684C54F44F9987DB9E1A67A45756 (void);
// 0x0000016F System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountLower()
extern void BarcodeMetadata_get_RowCountLower_m01D867E4FAF15591DB9ADCA2681A4F524909528D (void);
// 0x00000170 System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountLower(System.Int32)
extern void BarcodeMetadata_set_RowCountLower_m8691F9A95CC8158F00B4D521E9A72E965081599E (void);
// 0x00000171 System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCount()
extern void BarcodeMetadata_get_RowCount_mACD2C5E04E6FE23404844F1C4B24B33642C9E93F (void);
// 0x00000172 System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCount(System.Int32)
extern void BarcodeMetadata_set_RowCount_m757D5A46753DA91F51B97CA777A2A30826FCDE85 (void);
// 0x00000173 System.Void ZXing.PDF417.Internal.BarcodeMetadata::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BarcodeMetadata__ctor_m5C156634CF44D7D9118B5FB431D63E07C8F557C1 (void);
// 0x00000174 System.Void ZXing.PDF417.Internal.BarcodeValue::setValue(System.Int32)
extern void BarcodeValue_setValue_mC0EE93EB260DEF90894AF08DE46953BAECE214ED (void);
// 0x00000175 System.Int32[] ZXing.PDF417.Internal.BarcodeValue::getValue()
extern void BarcodeValue_getValue_m21AA86C2788EE335555BF503395B48A85778727A (void);
// 0x00000176 System.Void ZXing.PDF417.Internal.BarcodeValue::.ctor()
extern void BarcodeValue__ctor_mE897E340219BD79E9C751C83409A47FF3AD353E2 (void);
// 0x00000177 ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopLeft()
extern void BoundingBox_get_TopLeft_m5CB4B6EC44B499CD0BA203C9B3B6F8933121724C (void);
// 0x00000178 System.Void ZXing.PDF417.Internal.BoundingBox::set_TopLeft(ZXing.ResultPoint)
extern void BoundingBox_set_TopLeft_mCCCE09F3624FA382B74AC8E0A199AA6D8264FD35 (void);
// 0x00000179 ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopRight()
extern void BoundingBox_get_TopRight_m5A6F9AD58CD8CA92AEA0C76D84C763EECFA40CD3 (void);
// 0x0000017A System.Void ZXing.PDF417.Internal.BoundingBox::set_TopRight(ZXing.ResultPoint)
extern void BoundingBox_set_TopRight_m10405141D9546413D3A065768C78A4DAC520929A (void);
// 0x0000017B ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomLeft()
extern void BoundingBox_get_BottomLeft_m9A8E6255BF210761723976D7C38CF7D118EDFE8F (void);
// 0x0000017C System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomLeft(ZXing.ResultPoint)
extern void BoundingBox_set_BottomLeft_mB4C5B675177B796E7E681F59DB76A625A5620B68 (void);
// 0x0000017D ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomRight()
extern void BoundingBox_get_BottomRight_m2035AC5736DC155C43192B4ECDAB8A4F57EEBB94 (void);
// 0x0000017E System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomRight(ZXing.ResultPoint)
extern void BoundingBox_set_BottomRight_m8C954B0A293AC7B39084CA049E9B386F712A079B (void);
// 0x0000017F System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinX()
extern void BoundingBox_get_MinX_m5F83E51CACD2BBA44036A99706C101CF6016840B (void);
// 0x00000180 System.Void ZXing.PDF417.Internal.BoundingBox::set_MinX(System.Int32)
extern void BoundingBox_set_MinX_mBA6A3362975DA7BD7D6A0F04DD13AE8FE75931C8 (void);
// 0x00000181 System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxX()
extern void BoundingBox_get_MaxX_m3AE590ECC9CEA0B6DF5517CD8C3D182D95511712 (void);
// 0x00000182 System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxX(System.Int32)
extern void BoundingBox_set_MaxX_m3119F44A66FDB33CF14D4B1228679C148B4D82D8 (void);
// 0x00000183 System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinY()
extern void BoundingBox_get_MinY_m1FFEDA362F3B77F1DFE9385F7C3EF419DF0EC394 (void);
// 0x00000184 System.Void ZXing.PDF417.Internal.BoundingBox::set_MinY(System.Int32)
extern void BoundingBox_set_MinY_m595E30BC39CD5ADA2D82C0994B0923BB4C4FF8F0 (void);
// 0x00000185 System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxY()
extern void BoundingBox_get_MaxY_m5CFBC7ECF98C8659323F1A69392E297A82AB285B (void);
// 0x00000186 System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxY(System.Int32)
extern void BoundingBox_set_MaxY_m4F13753AEAEBD5CF67FA851DDCD4D2C586423D0F (void);
// 0x00000187 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void BoundingBox_Create_m7730A11AF8CDE72F3321D7E7643C551FA358C01D (void);
// 0x00000188 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.PDF417.Internal.BoundingBox)
extern void BoundingBox_Create_mE96E20A76E222C99B3D20A22EEE3710DF0B3B020 (void);
// 0x00000189 System.Void ZXing.PDF417.Internal.BoundingBox::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void BoundingBox__ctor_mFCCE35BCBB09B054DCF931F30032AC32C6D954BB (void);
// 0x0000018A ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::merge(ZXing.PDF417.Internal.BoundingBox,ZXing.PDF417.Internal.BoundingBox)
extern void BoundingBox_merge_m93FAC950625B5F8CBE03326E9C262417288059FE (void);
// 0x0000018B ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::addMissingRows(System.Int32,System.Int32,System.Boolean)
extern void BoundingBox_addMissingRows_m5040E28A39740713C627F815942B9D8FE9589AA7 (void);
// 0x0000018C System.Void ZXing.PDF417.Internal.BoundingBox::calculateMinMaxValues()
extern void BoundingBox_calculateMinMaxValues_m662C19EC975EF8E3A5F97DA11E9FD702DA262BCF (void);
// 0x0000018D System.Int32 ZXing.PDF417.Internal.Codeword::get_StartX()
extern void Codeword_get_StartX_m7142A4213335F1F993CA7802368A5AAD32672949 (void);
// 0x0000018E System.Void ZXing.PDF417.Internal.Codeword::set_StartX(System.Int32)
extern void Codeword_set_StartX_mEFB95587ADF18A1371DA69054C6D5600B61B8AD2 (void);
// 0x0000018F System.Int32 ZXing.PDF417.Internal.Codeword::get_EndX()
extern void Codeword_get_EndX_m76D22FBE3E14FCC8FA93068BE2CE2CB08E4E8998 (void);
// 0x00000190 System.Void ZXing.PDF417.Internal.Codeword::set_EndX(System.Int32)
extern void Codeword_set_EndX_m86CF8AFE1193EC119585AFBA4A29B1B80237983C (void);
// 0x00000191 System.Int32 ZXing.PDF417.Internal.Codeword::get_Bucket()
extern void Codeword_get_Bucket_mDC1B59D5B2EF3B9C57825CFA4209B3D20E3D6FB3 (void);
// 0x00000192 System.Void ZXing.PDF417.Internal.Codeword::set_Bucket(System.Int32)
extern void Codeword_set_Bucket_m4ACB8A7E1675CCDAA8AB38072CB91E78BA8C01E7 (void);
// 0x00000193 System.Int32 ZXing.PDF417.Internal.Codeword::get_Value()
extern void Codeword_get_Value_m6E938EB77D32B6E926D4A1DE4D4A14308EBDCEA9 (void);
// 0x00000194 System.Void ZXing.PDF417.Internal.Codeword::set_Value(System.Int32)
extern void Codeword_set_Value_m2704DED6B54EC01D56145587EE2FFEEE3704A85E (void);
// 0x00000195 System.Int32 ZXing.PDF417.Internal.Codeword::get_RowNumber()
extern void Codeword_get_RowNumber_mE9ABCE1EC3AF7CF50363CD1C02C5703C6498F2D0 (void);
// 0x00000196 System.Void ZXing.PDF417.Internal.Codeword::set_RowNumber(System.Int32)
extern void Codeword_set_RowNumber_m099810CF7CB72780C60AA7C1ABA4FA0C8879F935 (void);
// 0x00000197 System.Void ZXing.PDF417.Internal.Codeword::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Codeword__ctor_mEDBEA6F34DC9DB125F589E9E9E2313F395BA2747 (void);
// 0x00000198 System.Int32 ZXing.PDF417.Internal.Codeword::get_Width()
extern void Codeword_get_Width_mDCF12E16B2526AE86B8CF010794DF6D69E6429EC (void);
// 0x00000199 System.Boolean ZXing.PDF417.Internal.Codeword::get_HasValidRowNumber()
extern void Codeword_get_HasValidRowNumber_mD33760CE045220BEB08AD94809038FB48000CACD (void);
// 0x0000019A System.Boolean ZXing.PDF417.Internal.Codeword::IsValidRowNumber(System.Int32)
extern void Codeword_IsValidRowNumber_mF094DFF5EEE87DC59013B4CF8A64EE35A85F04F7 (void);
// 0x0000019B System.Void ZXing.PDF417.Internal.Codeword::setRowNumberAsRowIndicatorColumn()
extern void Codeword_setRowNumberAsRowIndicatorColumn_mBB2625C44347EB1A19B06B3FD061D34DE7416A3B (void);
// 0x0000019C System.String ZXing.PDF417.Internal.Codeword::ToString()
extern void Codeword_ToString_m6EA1E17024D7610A0855AAB9F68D9385FAD7C07A (void);
// 0x0000019D System.Void ZXing.PDF417.Internal.Codeword::.cctor()
extern void Codeword__cctor_mE0B73EA6C7F859955D54304F13A44476CCEADAD4 (void);
// 0x0000019E System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_mB9D2BF21BC4F25824201D6BDA47DEC3ED6D6DD12 (void);
// 0x0000019F ZXing.Common.DecoderResult ZXing.PDF417.Internal.DecodedBitStreamParser::decode(System.Int32[],System.String)
extern void DecodedBitStreamParser_decode_mCAB711DA9541D4207FE04EA1595E2382DB7D2210 (void);
// 0x000001A0 System.Text.Encoding ZXing.PDF417.Internal.DecodedBitStreamParser::getEncoding(System.String)
extern void DecodedBitStreamParser_getEncoding_m4F2E52B89FA0621228BF24408971D1B1358B2BE9 (void);
// 0x000001A1 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::decodeMacroBlock(System.Int32[],System.Int32,ZXing.PDF417.PDF417ResultMetadata)
extern void DecodedBitStreamParser_decodeMacroBlock_m4A049EC02C4FA583A3C2928329DAF07BA99DB82B (void);
// 0x000001A2 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::textCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_textCompaction_mB21BF43D938C2FC7EFA3F5CB4813D8525A08895B (void);
// 0x000001A3 System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::decodeTextCompaction(System.Int32[],System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeTextCompaction_mC2D2CC234B9390D82DB69E394F1E4211886464D7 (void);
// 0x000001A4 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::byteCompaction(System.Int32,System.Int32[],System.Text.Encoding,System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_byteCompaction_m82A12C789AB21F548EA4ABA58163681F42DFABE2 (void);
// 0x000001A5 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::numericCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_numericCompaction_m7BAD747D371C218503853746E0CEF2CDDDBC00F7 (void);
// 0x000001A6 System.String ZXing.PDF417.Internal.DecodedBitStreamParser::decodeBase900toBase10(System.Int32[],System.Int32)
extern void DecodedBitStreamParser_decodeBase900toBase10_mDCC0391FCB0C9A45DEBA1EFCE01DCE94C7B8631E (void);
// 0x000001A7 ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResult::get_Metadata()
extern void DetectionResult_get_Metadata_m28222ACC510CDDE151005C522558517F54F11E31 (void);
// 0x000001A8 System.Void ZXing.PDF417.Internal.DetectionResult::set_Metadata(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResult_set_Metadata_mE75F8B8FF73BE8E6949821C378222EA306F929AF (void);
// 0x000001A9 ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::get_DetectionResultColumns()
extern void DetectionResult_get_DetectionResultColumns_mE527F5B2DC9F258C3A2FDDF03AF1E6C571E3DB68 (void);
// 0x000001AA System.Void ZXing.PDF417.Internal.DetectionResult::set_DetectionResultColumns(ZXing.PDF417.Internal.DetectionResultColumn[])
extern void DetectionResult_set_DetectionResultColumns_m65CE606CF83520C680E1CFEC8AB98D8147807C9A (void);
// 0x000001AB ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResult::get_Box()
extern void DetectionResult_get_Box_m787AE2F48FF618EAD3AA5AC69F0E5CBA0557C03D (void);
// 0x000001AC System.Void ZXing.PDF417.Internal.DetectionResult::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResult_set_Box_m151695A38E2C002A537D09CCB87DAC7DDAF08E13 (void);
// 0x000001AD System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ColumnCount()
extern void DetectionResult_get_ColumnCount_mB43FEE1A3282C41D121B3A5C8220F6684F0266B8 (void);
// 0x000001AE System.Void ZXing.PDF417.Internal.DetectionResult::set_ColumnCount(System.Int32)
extern void DetectionResult_set_ColumnCount_mE0107ECCF65B7CAA5B790C6F8BF4FE68F330DE61 (void);
// 0x000001AF System.Int32 ZXing.PDF417.Internal.DetectionResult::get_RowCount()
extern void DetectionResult_get_RowCount_m2955064EB8DB77E537364DA68D463E61DE488ED9 (void);
// 0x000001B0 System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ErrorCorrectionLevel()
extern void DetectionResult_get_ErrorCorrectionLevel_mD51E566D08111066C6C28587C58C864B7A41357E (void);
// 0x000001B1 System.Void ZXing.PDF417.Internal.DetectionResult::.ctor(ZXing.PDF417.Internal.BarcodeMetadata,ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResult__ctor_m80B9A8731274F6DA21D8BF9AEE3B4BF5489A6652 (void);
// 0x000001B2 ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::getDetectionResultColumns()
extern void DetectionResult_getDetectionResultColumns_mF3F6CCE82B3501899B19968D9B1E245A474FCB06 (void);
// 0x000001B3 System.Void ZXing.PDF417.Internal.DetectionResult::adjustIndicatorColumnRowNumbers(ZXing.PDF417.Internal.DetectionResultColumn)
extern void DetectionResult_adjustIndicatorColumnRowNumbers_m39203C9C3E4286D33A29A6BDE40DE7C6573E0082 (void);
// 0x000001B4 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers()
extern void DetectionResult_adjustRowNumbers_mD9E1BD005874BD60C20463E39CB3AC7DABECA8DE (void);
// 0x000001B5 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersByRow()
extern void DetectionResult_adjustRowNumbersByRow_mFB90217AF87888990064A5B3A4A731026334D7BD (void);
// 0x000001B6 System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromBothRI()
extern void DetectionResult_adjustRowNumbersFromBothRI_m24E9B1892162075C03AD37C5AE99150DC56BFC6E (void);
// 0x000001B7 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromRRI()
extern void DetectionResult_adjustRowNumbersFromRRI_m3AFBFE56375C1080D807EBF5E0BCA1E7326599FD (void);
// 0x000001B8 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromLRI()
extern void DetectionResult_adjustRowNumbersFromLRI_m57216279CFED7E337C46D430FCC029F942C7866A (void);
// 0x000001B9 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumberIfValid(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword)
extern void DetectionResult_adjustRowNumberIfValid_m42DB0F5E8397CADB56156DA96C089AD2E29C1643 (void);
// 0x000001BA System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword[])
extern void DetectionResult_adjustRowNumbers_mC1FD8946ED41571556B0749B8B3E934B6DC60174 (void);
// 0x000001BB System.Boolean ZXing.PDF417.Internal.DetectionResult::adjustRowNumber(ZXing.PDF417.Internal.Codeword,ZXing.PDF417.Internal.Codeword)
extern void DetectionResult_adjustRowNumber_mCAB3FBC9A90361D4512226F8EA48751B6776B17C (void);
// 0x000001BC System.String ZXing.PDF417.Internal.DetectionResult::ToString()
extern void DetectionResult_ToString_m72853A7AC512DB3FFD365B37D02555EB5C0BCEE1 (void);
// 0x000001BD ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResultColumn::get_Box()
extern void DetectionResultColumn_get_Box_m28DE039B3B068F05F1043D74471A84948310A5D4 (void);
// 0x000001BE System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResultColumn_set_Box_m3949C81CBD20F30155D8B7A5B720A809AABCA29B (void);
// 0x000001BF ZXing.PDF417.Internal.Codeword[] ZXing.PDF417.Internal.DetectionResultColumn::get_Codewords()
extern void DetectionResultColumn_get_Codewords_m8C5E2EDFE024EBB4833DAB8E33725D2E2906790A (void);
// 0x000001C0 System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Codewords(ZXing.PDF417.Internal.Codeword[])
extern void DetectionResultColumn_set_Codewords_m507D740FE136F6E201194B24CF7F7BD80CE0CE13 (void);
// 0x000001C1 System.Void ZXing.PDF417.Internal.DetectionResultColumn::.ctor(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResultColumn__ctor_m2ADCD05276AB42F3F9B87A956564F65A2EC7C19F (void);
// 0x000001C2 System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::IndexForRow(System.Int32)
extern void DetectionResultColumn_IndexForRow_m5896C7EB762F1CAECC7727E02A9992F6FC69A660 (void);
// 0x000001C3 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodeword(System.Int32)
extern void DetectionResultColumn_getCodeword_mD5C9F9833553FD8393E35D8E91C763712D7F45B2 (void);
// 0x000001C4 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodewordNearby(System.Int32)
extern void DetectionResultColumn_getCodewordNearby_m9B20AF36EA062C0620C5C17A9095D6E93866FFB6 (void);
// 0x000001C5 System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::imageRowToCodewordIndex(System.Int32)
extern void DetectionResultColumn_imageRowToCodewordIndex_mC82818309D0344F1C54601B2AF0912BA17416D1F (void);
// 0x000001C6 System.Void ZXing.PDF417.Internal.DetectionResultColumn::setCodeword(System.Int32,ZXing.PDF417.Internal.Codeword)
extern void DetectionResultColumn_setCodeword_m6346A8C6E9D28419DA12EE0094E1DA388D64CF48 (void);
// 0x000001C7 System.String ZXing.PDF417.Internal.DetectionResultColumn::ToString()
extern void DetectionResultColumn_ToString_m0FAD8C2D51E3E230E5014AD393A2EBC5DF51AF25 (void);
// 0x000001C8 System.Boolean ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::get_IsLeft()
extern void DetectionResultRowIndicatorColumn_get_IsLeft_mC2C89A027BF107A96DDC704A5C7F5CD9998B5450 (void);
// 0x000001C9 System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::set_IsLeft(System.Boolean)
extern void DetectionResultRowIndicatorColumn_set_IsLeft_m0A5451068AE892E9E3180FD80840869545C3DEB1 (void);
// 0x000001CA System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::.ctor(ZXing.PDF417.Internal.BoundingBox,System.Boolean)
extern void DetectionResultRowIndicatorColumn__ctor_m9E030F19AD1E19C56310368D0770620A10DA9AA7 (void);
// 0x000001CB System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::setRowNumbers()
extern void DetectionResultRowIndicatorColumn_setRowNumbers_m960B0EEED1AE79A426217F9E1930323C23C143E1 (void);
// 0x000001CC System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustCompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_adjustCompleteIndicatorColumnRowNumbers_m01803E543828EB0D95DF33B7EB70BF7FA1B1042C (void);
// 0x000001CD System.Int32[] ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getRowHeights()
extern void DetectionResultRowIndicatorColumn_getRowHeights_m61C468F2DF384FEC1D1D02701A6B0C57CE4FB3DD (void);
// 0x000001CE System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustIncompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_adjustIncompleteIndicatorColumnRowNumbers_mAD04465401EFA5EB8A7303D1E0187A25947D6874 (void);
// 0x000001CF ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getBarcodeMetadata()
extern void DetectionResultRowIndicatorColumn_getBarcodeMetadata_mF776AB0DDF6A408CE7ECE8FB8E879ADA8C6EA204 (void);
// 0x000001D0 System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::removeIncorrectCodewords(ZXing.PDF417.Internal.Codeword[],ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_removeIncorrectCodewords_mE53A4711D0AE25168EE0D052486DDB49C91F0FE9 (void);
// 0x000001D1 System.String ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::ToString()
extern void DetectionResultRowIndicatorColumn_ToString_m78C869F57142063F55722D353ECAB7C23A228E48 (void);
// 0x000001D2 System.Void ZXing.PDF417.Internal.PDF417CodewordDecoder::.cctor()
extern void PDF417CodewordDecoder__cctor_m5FEA07FBF1748F962733E8E88B01CB3F19D08E96 (void);
// 0x000001D3 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedValue(System.Int32[])
extern void PDF417CodewordDecoder_getDecodedValue_m40D6996FC9398DC965614CF651C6C2AD9231BEAD (void);
// 0x000001D4 System.Int32[] ZXing.PDF417.Internal.PDF417CodewordDecoder::sampleBitCounts(System.Int32[])
extern void PDF417CodewordDecoder_sampleBitCounts_mB6EFAB7642E2CEBC109E2989021850C89DB5ABBF (void);
// 0x000001D5 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedCodewordValue(System.Int32[])
extern void PDF417CodewordDecoder_getDecodedCodewordValue_m475269B81E98FD9AF479116F16AF53122E09F5D8 (void);
// 0x000001D6 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getBitValue(System.Int32[])
extern void PDF417CodewordDecoder_getBitValue_mC5EAD27BFFC7C5DAF89BFF26252078543CA514DD (void);
// 0x000001D7 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getClosestDecodedValue(System.Int32[])
extern void PDF417CodewordDecoder_getClosestDecodedValue_m6492E458235E335BC329559FD69F0011C8E0A3BF (void);
// 0x000001D8 ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decode(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_decode_m10CB6196EE3101DEB14720FD37692E243B6F7F4B (void);
// 0x000001D9 ZXing.PDF417.Internal.DetectionResult ZXing.PDF417.Internal.PDF417ScanningDecoder::merge(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_merge_mAD376011CC28294DDE82901D591AB17B2EF37289 (void);
// 0x000001DA ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustBoundingBox(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_adjustBoundingBox_m5DD7BF63787E04D704E09AF9B270CE092BA08E00 (void);
// 0x000001DB System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getMax(System.Int32[])
extern void PDF417ScanningDecoder_getMax_m5CA3014C6BDF4D31A184E27E6E44A9BC89F09D9C (void);
// 0x000001DC ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.PDF417ScanningDecoder::getBarcodeMetadata(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_getBarcodeMetadata_m1F66DF3E1CC9C84ADAC1E2B34B0109E933834790 (void);
// 0x000001DD ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn ZXing.PDF417.Internal.PDF417ScanningDecoder::getRowIndicatorColumn(ZXing.Common.BitMatrix,ZXing.PDF417.Internal.BoundingBox,ZXing.ResultPoint,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_getRowIndicatorColumn_m34A7EAA83649DD3F3E597DE1003490485954ECAC (void);
// 0x000001DE System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordCount(ZXing.PDF417.Internal.DetectionResult,ZXing.PDF417.Internal.BarcodeValue[][])
extern void PDF417ScanningDecoder_adjustCodewordCount_m02FE86F3611624254B0E2B881EB09A6C7AA67175 (void);
// 0x000001DF ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResult(ZXing.PDF417.Internal.DetectionResult)
extern void PDF417ScanningDecoder_createDecoderResult_m959D0A00400736F43A630D07FE3992748021F8A4 (void);
// 0x000001E0 ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResultFromAmbiguousValues(System.Int32,System.Int32[],System.Int32[],System.Int32[],System.Int32[][])
extern void PDF417ScanningDecoder_createDecoderResultFromAmbiguousValues_m0ED71A465F91BA798FB58A9C265B05141F2C7CBD (void);
// 0x000001E1 ZXing.PDF417.Internal.BarcodeValue[][] ZXing.PDF417.Internal.PDF417ScanningDecoder::createBarcodeMatrix(ZXing.PDF417.Internal.DetectionResult)
extern void PDF417ScanningDecoder_createBarcodeMatrix_mEDE1ED0259A57F3CC4685D636326EBAFAB49369F (void);
// 0x000001E2 System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::isValidBarcodeColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32)
extern void PDF417ScanningDecoder_isValidBarcodeColumn_m152DF458D272F7F418ED7ACF0059346817698E26 (void);
// 0x000001E3 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getStartColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32,System.Int32,System.Boolean)
extern void PDF417ScanningDecoder_getStartColumn_m5925DAC51470D099F2B44C260502B6B54CD12E0E (void);
// 0x000001E4 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.PDF417ScanningDecoder::detectCodeword(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_detectCodeword_mE6B9E76EE55F52D518995CB10AFFBD52356DE5B0 (void);
// 0x000001E5 System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getModuleBitCount(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_getModuleBitCount_m7F1997ADD96756AEEAF85BEBDD847420F8E8FC61 (void);
// 0x000001E6 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getNumberOfECCodeWords(System.Int32)
extern void PDF417ScanningDecoder_getNumberOfECCodeWords_m78635E6FFA758779DD5C34B3A30140AE4E9BF2CE (void);
// 0x000001E7 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordStartColumn(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_adjustCodewordStartColumn_mE5A2A0F1AE38DF6D02CCFC8BF9E1D3A153F2CFB1 (void);
// 0x000001E8 System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::checkCodewordSkew(System.Int32,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_checkCodewordSkew_m7D26CDB8EFC68A8EE7E37D65854F986C5A53235F (void);
// 0x000001E9 ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decodeCodewords(System.Int32[],System.Int32,System.Int32[])
extern void PDF417ScanningDecoder_decodeCodewords_mD2123CBC928CED2A245FE6BDF6DD53CF16A0D862 (void);
// 0x000001EA System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::correctErrors(System.Int32[],System.Int32[],System.Int32)
extern void PDF417ScanningDecoder_correctErrors_m5710663F6735DEF09B253BFD185743C635552BC5 (void);
// 0x000001EB System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::verifyCodewordCount(System.Int32[],System.Int32)
extern void PDF417ScanningDecoder_verifyCodewordCount_m2918861003232164A122FB53E56A19269BB90181 (void);
// 0x000001EC System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getBitCountForCodeword(System.Int32)
extern void PDF417ScanningDecoder_getBitCountForCodeword_mA26EAC2BEFBD397623357604A1D10E9675BB4D15 (void);
// 0x000001ED System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32)
extern void PDF417ScanningDecoder_getCodewordBucketNumber_mE94072C68EDB1738088AEF2CC625BB88B8E1A13A (void);
// 0x000001EE System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32[])
extern void PDF417ScanningDecoder_getCodewordBucketNumber_m8FE23D735BFCF3F58951ED46018858E083D89EE8 (void);
// 0x000001EF System.Void ZXing.PDF417.Internal.PDF417ScanningDecoder::.cctor()
extern void PDF417ScanningDecoder__cctor_m8FCE8CB540FD1D4DE669E899DCBC63BEDCC3D3CE (void);
// 0x000001F0 ZXing.PDF417.Internal.PDF417DetectorResult ZXing.PDF417.Internal.Detector::detect(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern void Detector_detect_m77083CAD6731F658875F5CB4887CA0F97A9B53CA (void);
// 0x000001F1 System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.Detector::detect(System.Boolean,ZXing.Common.BitMatrix)
extern void Detector_detect_m012C00104272B911CCD110B43430652514B4F4A9 (void);
// 0x000001F2 ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findVertices(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern void Detector_findVertices_mF74AB91B8535D923B64A1FD95B93CAE47AA7DD92 (void);
// 0x000001F3 System.Void ZXing.PDF417.Internal.Detector::copyToResult(ZXing.ResultPoint[],ZXing.ResultPoint[],System.Int32[])
extern void Detector_copyToResult_mD1AA525F2B3888A81FEF7CE470848582B2640064 (void);
// 0x000001F4 ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findRowsWithPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void Detector_findRowsWithPattern_m950A84E4A8687164E9001698CC8E0AE03F5F22AD (void);
// 0x000001F5 System.Int32[] ZXing.PDF417.Internal.Detector::findGuardPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern void Detector_findGuardPattern_m9A1C17C84D513627FF37C0AF9695A99A49A17FB8 (void);
// 0x000001F6 System.Int32 ZXing.PDF417.Internal.Detector::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern void Detector_patternMatchVariance_m86EBAB1902EB4E64C99797EFBA893B893060AE7F (void);
// 0x000001F7 System.Void ZXing.PDF417.Internal.Detector::.cctor()
extern void Detector__cctor_m705EC43B10096EEF60CC0DD8EA853D5ECDC94B87 (void);
// 0x000001F8 ZXing.Common.BitMatrix ZXing.PDF417.Internal.PDF417DetectorResult::get_Bits()
extern void PDF417DetectorResult_get_Bits_mF582F132EF48BC1E687742C76A1004054C51BCF8 (void);
// 0x000001F9 System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern void PDF417DetectorResult_set_Bits_mBF16DD37F7407142F2C31B184AC52A326E630B3E (void);
// 0x000001FA System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.PDF417DetectorResult::get_Points()
extern void PDF417DetectorResult_get_Points_mE94F3CA82AE031E694E202F8C6151AEF086E5BDA (void);
// 0x000001FB System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Points(System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern void PDF417DetectorResult_set_Points_mA06AC47807901C9A6A34C2CFFD32CF3B3E22B9F6 (void);
// 0x000001FC System.Void ZXing.PDF417.Internal.PDF417DetectorResult::.ctor(ZXing.Common.BitMatrix,System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern void PDF417DetectorResult__ctor_m339E0215B1A0C869F47DBE531BAB0D05A40F6F8F (void);
// 0x000001FD System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::.cctor()
extern void PDF417HighLevelEncoder__cctor_mCA870020187562C5E9BC3A202EBE95A173C289D1 (void);
// 0x000001FE System.Void ZXing.PDF417.Internal.EC.ErrorCorrection::.ctor()
extern void ErrorCorrection__ctor_m34C0B86D00DB6B2984DF14339759A3642B48716E (void);
// 0x000001FF System.Boolean ZXing.PDF417.Internal.EC.ErrorCorrection::decode(System.Int32[],System.Int32,System.Int32[],System.Int32&)
extern void ErrorCorrection_decode_mCA4A579258E2C5E4588FE83A84E76BC0514023E9 (void);
// 0x00000200 ZXing.PDF417.Internal.EC.ModulusPoly[] ZXing.PDF417.Internal.EC.ErrorCorrection::runEuclideanAlgorithm(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32)
extern void ErrorCorrection_runEuclideanAlgorithm_m1D3B22C936F75AC826DA9F06D84A8E4CCDB667CD (void);
// 0x00000201 System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorLocations(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ErrorCorrection_findErrorLocations_m7BC663A9BD39ACC2228A60D042DC1D767EE1F8EC (void);
// 0x00000202 System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorMagnitudes(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32[])
extern void ErrorCorrection_findErrorMagnitudes_mF2A467AFFC3FB6BAD61EC60E934055D1E44332A2 (void);
// 0x00000203 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_Zero()
extern void ModulusGF_get_Zero_m65338B601D7118B1864EBA357548AD7DBB3901B0 (void);
// 0x00000204 System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_Zero(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusGF_set_Zero_m8AD743F6D1B4E3B5C2369DD6DB62FC227C686C5F (void);
// 0x00000205 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_One()
extern void ModulusGF_get_One_m8DE4FAEE75B473FC0E62FDC6232A4D7EAB7DF90C (void);
// 0x00000206 System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_One(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusGF_set_One_m377CF840C61DCD154038F6398B92CA9610250DD8 (void);
// 0x00000207 System.Void ZXing.PDF417.Internal.EC.ModulusGF::.ctor(System.Int32,System.Int32)
extern void ModulusGF__ctor_m8E30BB6B6CA241C9BE5B8BB7A463E5BFDA9A3E62 (void);
// 0x00000208 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::buildMonomial(System.Int32,System.Int32)
extern void ModulusGF_buildMonomial_mBB4893A51C5421DAA8323E2B55EE9EE7E3A51AE4 (void);
// 0x00000209 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::add(System.Int32,System.Int32)
extern void ModulusGF_add_m72492B09261523820329C8D5BAC163268AFA262C (void);
// 0x0000020A System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::subtract(System.Int32,System.Int32)
extern void ModulusGF_subtract_mCEE55A12C1B0748780AA50342695F983ECD1D09D (void);
// 0x0000020B System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::exp(System.Int32)
extern void ModulusGF_exp_mBE3892FD93C8696E5566F95FF17C9E30E61126D2 (void);
// 0x0000020C System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::log(System.Int32)
extern void ModulusGF_log_m5071E565C2047F0C57ED98D0F88A1506143BECF5 (void);
// 0x0000020D System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::inverse(System.Int32)
extern void ModulusGF_inverse_m87D819708B1EA359F732711D12FD515ADA35C8A7 (void);
// 0x0000020E System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::multiply(System.Int32,System.Int32)
extern void ModulusGF_multiply_m3616529BCE9EB3296D041A6103042B32BC044D72 (void);
// 0x0000020F System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::get_Size()
extern void ModulusGF_get_Size_mC294C04425D58D71E491B75007DE0732E5A05171 (void);
// 0x00000210 System.Void ZXing.PDF417.Internal.EC.ModulusGF::.cctor()
extern void ModulusGF__cctor_m01D59044174811ABCB85425A51F25C097727C8E2 (void);
// 0x00000211 System.Void ZXing.PDF417.Internal.EC.ModulusPoly::.ctor(ZXing.PDF417.Internal.EC.ModulusGF,System.Int32[])
extern void ModulusPoly__ctor_m84035B4A06FD998B8B08CCEC6FE8365BDCC9C162 (void);
// 0x00000212 System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::get_Degree()
extern void ModulusPoly_get_Degree_mFEE3AA4873EC95B557B1D4C3A9876EC9918E0483 (void);
// 0x00000213 System.Boolean ZXing.PDF417.Internal.EC.ModulusPoly::get_isZero()
extern void ModulusPoly_get_isZero_m349ABDC976924C7474D1AFCDB8FBF80DEDC86B32 (void);
// 0x00000214 System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::getCoefficient(System.Int32)
extern void ModulusPoly_getCoefficient_m74733E2E45C57BF0030C8FD11140A2FB4A7B5B35 (void);
// 0x00000215 System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::evaluateAt(System.Int32)
extern void ModulusPoly_evaluateAt_mB9195FDF29D0FF03B6CD8CF79AB1FDC58646F925 (void);
// 0x00000216 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::add(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_add_mB314EE922FEBB8C7CF98820DCA7CA077D319AE26 (void);
// 0x00000217 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::subtract(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_subtract_m66A1953DCC6C9864F77A86AF6D2A1338F14FA124 (void);
// 0x00000218 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_multiply_m3177D06E07AC8BBD537A46C6C10AC55FCF858B83 (void);
// 0x00000219 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::getNegative()
extern void ModulusPoly_getNegative_m6A0B499446B92230E80BB2AF718EB2C43A103306 (void);
// 0x0000021A ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(System.Int32)
extern void ModulusPoly_multiply_m2F748E18ED8DBACEA4F63A5610E35041F9D878DA (void);
// 0x0000021B ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiplyByMonomial(System.Int32,System.Int32)
extern void ModulusPoly_multiplyByMonomial_mCB1687D8EBF571F333868354AAE76F2817DC08DE (void);
// 0x0000021C System.String ZXing.PDF417.Internal.EC.ModulusPoly::ToString()
extern void ModulusPoly_ToString_m66F44625F665C2988F67C84DA554D6000C8DB78B (void);
// 0x0000021D System.Void ZXing.OneD.CodaBarReader::.ctor()
extern void CodaBarReader__ctor_m484B6DBF28197DDD4D1FA9AC84E98CAD3387DCA2 (void);
// 0x0000021E ZXing.Result ZXing.OneD.CodaBarReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void CodaBarReader_decodeRow_mEB4BD4CF7AF19F7CA1CFE3A208DDD211418623E1 (void);
// 0x0000021F System.Boolean ZXing.OneD.CodaBarReader::validatePattern(System.Int32)
extern void CodaBarReader_validatePattern_m41B4FC8AFC090163A6CD04D15F8890F853A44266 (void);
// 0x00000220 System.Boolean ZXing.OneD.CodaBarReader::setCounters(ZXing.Common.BitArray)
extern void CodaBarReader_setCounters_m90ED54B610961718821B5DCF7B5CBBBA3FDA9F41 (void);
// 0x00000221 System.Void ZXing.OneD.CodaBarReader::counterAppend(System.Int32)
extern void CodaBarReader_counterAppend_mE03E878BAB1006DDDE4CEB7414051BE5F2C8353F (void);
// 0x00000222 System.Int32 ZXing.OneD.CodaBarReader::findStartPattern()
extern void CodaBarReader_findStartPattern_m98445641345DEE7F9D2D2F063539E51B2B7DA78A (void);
// 0x00000223 System.Boolean ZXing.OneD.CodaBarReader::arrayContains(System.Char[],System.Char)
extern void CodaBarReader_arrayContains_m4BE365E8C94683682C3197BBA5DF20554E914769 (void);
// 0x00000224 System.Int32 ZXing.OneD.CodaBarReader::toNarrowWidePattern(System.Int32)
extern void CodaBarReader_toNarrowWidePattern_m2239004FC8FEE32B3F256848A886A8B68AAA79F8 (void);
// 0x00000225 System.Void ZXing.OneD.CodaBarReader::.cctor()
extern void CodaBarReader__cctor_mEAEB3167A5F23A1332649DE0B94B3D8F27FBED4C (void);
// 0x00000226 System.Int32[] ZXing.OneD.Code128Reader::findStartPattern(ZXing.Common.BitArray)
extern void Code128Reader_findStartPattern_m4C083CA8D07B03333143FF36BCB842D0530C8480 (void);
// 0x00000227 System.Boolean ZXing.OneD.Code128Reader::decodeCode(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32&)
extern void Code128Reader_decodeCode_m694AD1065D2881D176C34B9048D060B4887763CC (void);
// 0x00000228 ZXing.Result ZXing.OneD.Code128Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code128Reader_decodeRow_mC33398DABFADF2C7B03966F7E71819791AAA922E (void);
// 0x00000229 System.Void ZXing.OneD.Code128Reader::.ctor()
extern void Code128Reader__ctor_m269FF0F640AC6D79AF3C62A71EAA0A86963661D1 (void);
// 0x0000022A System.Void ZXing.OneD.Code128Reader::.cctor()
extern void Code128Reader__cctor_m0E30D17B53CED4C68D3C674EF0658BB36BEC4122 (void);
// 0x0000022B System.Void ZXing.OneD.Code39Reader::.ctor(System.Boolean,System.Boolean)
extern void Code39Reader__ctor_mD25930FE523B7AF6ED3602454486BC1C7F7521B8 (void);
// 0x0000022C ZXing.Result ZXing.OneD.Code39Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code39Reader_decodeRow_mAB97F09FA6714E8AD7F92630739FA93CECA114CE (void);
// 0x0000022D System.Int32[] ZXing.OneD.Code39Reader::findAsteriskPattern(ZXing.Common.BitArray,System.Int32[])
extern void Code39Reader_findAsteriskPattern_m9658C527D11C095DAF4E064BACF199BDA26E054E (void);
// 0x0000022E System.Int32 ZXing.OneD.Code39Reader::toNarrowWidePattern(System.Int32[])
extern void Code39Reader_toNarrowWidePattern_m8660375BB660D0F827B44C03263622287634268B (void);
// 0x0000022F System.Boolean ZXing.OneD.Code39Reader::patternToChar(System.Int32,System.Char&)
extern void Code39Reader_patternToChar_m09BE3C039AC3D1591B613D4C902FEB20C702C79A (void);
// 0x00000230 System.String ZXing.OneD.Code39Reader::decodeExtended(System.String)
extern void Code39Reader_decodeExtended_mDDF22D16154768A089F6C51B95B190C4E1FAF25A (void);
// 0x00000231 System.Void ZXing.OneD.Code39Reader::.cctor()
extern void Code39Reader__cctor_m10838688E021DF7AAD1B11D9FEC9AF915399EF9C (void);
// 0x00000232 System.Void ZXing.OneD.Code93Reader::.ctor()
extern void Code93Reader__ctor_m75DC2D34AA896106F2A2D1F251C0D1D4FC969B91 (void);
// 0x00000233 ZXing.Result ZXing.OneD.Code93Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code93Reader_decodeRow_m47A299DCB561048687097B64217B48C9BFF75C16 (void);
// 0x00000234 System.Int32[] ZXing.OneD.Code93Reader::findAsteriskPattern(ZXing.Common.BitArray)
extern void Code93Reader_findAsteriskPattern_m890DAF8AAD38D210557E84AEA49B6D2AC5488116 (void);
// 0x00000235 System.Int32 ZXing.OneD.Code93Reader::toPattern(System.Int32[])
extern void Code93Reader_toPattern_mDB6E68CF216244F378FFC2EC88A4FB5AB46EF40E (void);
// 0x00000236 System.Boolean ZXing.OneD.Code93Reader::patternToChar(System.Int32,System.Char&)
extern void Code93Reader_patternToChar_mEC762541BE74BF312AE53C66655CA7D262E9CBFE (void);
// 0x00000237 System.String ZXing.OneD.Code93Reader::decodeExtended(System.Text.StringBuilder)
extern void Code93Reader_decodeExtended_m4F8AE577C60984DFEBE518581A40A668094FB917 (void);
// 0x00000238 System.Boolean ZXing.OneD.Code93Reader::checkChecksums(System.Text.StringBuilder)
extern void Code93Reader_checkChecksums_m38C55165D557C4F075FB7CC70705029DE4D1C2E5 (void);
// 0x00000239 System.Boolean ZXing.OneD.Code93Reader::checkOneChecksum(System.Text.StringBuilder,System.Int32,System.Int32)
extern void Code93Reader_checkOneChecksum_m18E016D438152A4A848D18E24A636620A2717135 (void);
// 0x0000023A System.Void ZXing.OneD.Code93Reader::.cctor()
extern void Code93Reader__cctor_m4585FC992C039D7579E3FF73EF1A171BF39765AB (void);
// 0x0000023B System.Void ZXing.OneD.EAN13Reader::.ctor()
extern void EAN13Reader__ctor_mB27AD98DBE131034694094CEF3AC6F655CB2F413 (void);
// 0x0000023C System.Int32 ZXing.OneD.EAN13Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void EAN13Reader_decodeMiddle_m9C1B9E58C3B71FB0248A236FB0D77988E31553DF (void);
// 0x0000023D ZXing.BarcodeFormat ZXing.OneD.EAN13Reader::get_BarcodeFormat()
extern void EAN13Reader_get_BarcodeFormat_m2696FCF21F5C2DE197D1E7C24CB86FA514BBADAE (void);
// 0x0000023E System.Boolean ZXing.OneD.EAN13Reader::determineFirstDigit(System.Text.StringBuilder,System.Int32)
extern void EAN13Reader_determineFirstDigit_m685E358317D6748E3A646B773127FE4135642F49 (void);
// 0x0000023F System.Void ZXing.OneD.EAN13Reader::.cctor()
extern void EAN13Reader__cctor_m258F8831422CA974533A3B4469E2C626BE40E7F9 (void);
// 0x00000240 System.Void ZXing.OneD.EAN8Reader::.ctor()
extern void EAN8Reader__ctor_mFC32FFCE655B4E5318E28BF7F9065665B491FB15 (void);
// 0x00000241 System.Int32 ZXing.OneD.EAN8Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void EAN8Reader_decodeMiddle_m613DAB953ED713A5079CBDD86A042D0057E190C5 (void);
// 0x00000242 ZXing.BarcodeFormat ZXing.OneD.EAN8Reader::get_BarcodeFormat()
extern void EAN8Reader_get_BarcodeFormat_mB59527A7D435389C8F357855EE0EE8B19BAF4488 (void);
// 0x00000243 System.String ZXing.OneD.EANManufacturerOrgSupport::lookupCountryIdentifier(System.String)
extern void EANManufacturerOrgSupport_lookupCountryIdentifier_m39008989E9ADA63EB48EF3E2B825FD5719569F8A (void);
// 0x00000244 System.Void ZXing.OneD.EANManufacturerOrgSupport::add(System.Int32[],System.String)
extern void EANManufacturerOrgSupport_add_mCA907430BB14EB8138D1948647C867E1C5E8F7D3 (void);
// 0x00000245 System.Void ZXing.OneD.EANManufacturerOrgSupport::initIfNeeded()
extern void EANManufacturerOrgSupport_initIfNeeded_mEB7559EB7490D00FA7DFC607F8518BC9BD844906 (void);
// 0x00000246 System.Void ZXing.OneD.EANManufacturerOrgSupport::.ctor()
extern void EANManufacturerOrgSupport__ctor_m3F388103A583E6C9A5857BD76956F42334CB3774 (void);
// 0x00000247 ZXing.Result ZXing.OneD.ITFReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void ITFReader_decodeRow_mEB5198257F696BE89A2CA764E908ABEB8CF1846D (void);
// 0x00000248 System.Boolean ZXing.OneD.ITFReader::decodeMiddle(ZXing.Common.BitArray,System.Int32,System.Int32,System.Text.StringBuilder)
extern void ITFReader_decodeMiddle_m763B848E1A2751A3B9B02CD31F8CF43FA5BF0480 (void);
// 0x00000249 System.Int32[] ZXing.OneD.ITFReader::decodeStart(ZXing.Common.BitArray)
extern void ITFReader_decodeStart_mAF2F7C53D4B751F5DBAAED4660EF285A55C99FD4 (void);
// 0x0000024A System.Boolean ZXing.OneD.ITFReader::validateQuietZone(ZXing.Common.BitArray,System.Int32)
extern void ITFReader_validateQuietZone_m3AC577691052C5D3F1DDEF6866C54CC88F896C36 (void);
// 0x0000024B System.Int32 ZXing.OneD.ITFReader::skipWhiteSpace(ZXing.Common.BitArray)
extern void ITFReader_skipWhiteSpace_m1DE503166A0D7AC46C6CCEBA4D30D7D7404E6850 (void);
// 0x0000024C System.Int32[] ZXing.OneD.ITFReader::decodeEnd(ZXing.Common.BitArray)
extern void ITFReader_decodeEnd_m6D94FFA18BD93F4E7873E8546EEA9AED90EA2F93 (void);
// 0x0000024D System.Int32[] ZXing.OneD.ITFReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void ITFReader_findGuardPattern_m747F8361E79E2363D7EAC702F903D2331ED9D264 (void);
// 0x0000024E System.Boolean ZXing.OneD.ITFReader::decodeDigit(System.Int32[],System.Int32&)
extern void ITFReader_decodeDigit_mC94DEAE09913F8E83850AEC72597787CCE2CD965 (void);
// 0x0000024F System.Void ZXing.OneD.ITFReader::.ctor()
extern void ITFReader__ctor_mF5646155A59559C346EB037F8BB5138666850D02 (void);
// 0x00000250 System.Void ZXing.OneD.ITFReader::.cctor()
extern void ITFReader__cctor_mB065228887260AC1F7C4E510622EF3B75BBECDDC (void);
// 0x00000251 System.Void ZXing.OneD.MSIReader::.ctor(System.Boolean)
extern void MSIReader__ctor_m72DFA66E74183876C880C92442B5B1179AA19A77 (void);
// 0x00000252 ZXing.Result ZXing.OneD.MSIReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MSIReader_decodeRow_m66BD71D508442A357E27EF7AFF0789B8F3AED570 (void);
// 0x00000253 System.Int32[] ZXing.OneD.MSIReader::findStartPattern(ZXing.Common.BitArray,System.Int32[])
extern void MSIReader_findStartPattern_mB8621B54FA27E619B58A8B0D9CBEB5AA7C750570 (void);
// 0x00000254 System.Int32[] ZXing.OneD.MSIReader::findEndPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void MSIReader_findEndPattern_m5A32F9776F9E02D655A6D0A6248F36833FAC0755 (void);
// 0x00000255 System.Void ZXing.OneD.MSIReader::calculateAverageCounterWidth(System.Int32[],System.Int32)
extern void MSIReader_calculateAverageCounterWidth_mADD2AC18A5D4C60BA2CFF18E298AD2E6F47FF300 (void);
// 0x00000256 System.Int32 ZXing.OneD.MSIReader::toPattern(System.Int32[],System.Int32)
extern void MSIReader_toPattern_m510927637DCB05A2331C03EDCE319ADDB8D9B73C (void);
// 0x00000257 System.Boolean ZXing.OneD.MSIReader::patternToChar(System.Int32,System.Char&)
extern void MSIReader_patternToChar_m2729755C804D6748C8E9E322EFEF8D6DA722C034 (void);
// 0x00000258 System.Int32 ZXing.OneD.MSIReader::CalculateChecksumLuhn(System.String)
extern void MSIReader_CalculateChecksumLuhn_mF28D12A0444B68557D2D657A76B8A45EC9A5515D (void);
// 0x00000259 System.Void ZXing.OneD.MSIReader::.cctor()
extern void MSIReader__cctor_m8B04187943AC1FF958B3BA2E320CC9F762B9FADE (void);
// 0x0000025A System.Void ZXing.OneD.MultiFormatOneDReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatOneDReader__ctor_mBAB256EF5D3E6DE1F8F9D07F36C35CB322B86EBE (void);
// 0x0000025B ZXing.Result ZXing.OneD.MultiFormatOneDReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatOneDReader_decodeRow_m4B41DEF370E40487C16A35BB20996CFD0E7999DC (void);
// 0x0000025C System.Void ZXing.OneD.MultiFormatOneDReader::reset()
extern void MultiFormatOneDReader_reset_m89383A94DE3FEE31107970E9342F66637C0CC0D9 (void);
// 0x0000025D System.Void ZXing.OneD.MultiFormatUPCEANReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatUPCEANReader__ctor_m142FD956F2C83ACC9190A5FFC92CD5EBB0E602B9 (void);
// 0x0000025E ZXing.Result ZXing.OneD.MultiFormatUPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatUPCEANReader_decodeRow_m217A6231E1CF8076A500ADDBA4A238680AABEF9F (void);
// 0x0000025F System.Void ZXing.OneD.MultiFormatUPCEANReader::reset()
extern void MultiFormatUPCEANReader_reset_m8381B6E531763B7D465C8C13C97793C3A6B2A9AE (void);
// 0x00000260 ZXing.Result ZXing.OneD.OneDReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void OneDReader_decode_m1069EA854C11AEF129843635949AC6F5E484A5ED (void);
// 0x00000261 System.Void ZXing.OneD.OneDReader::reset()
extern void OneDReader_reset_mD4684AA7B384120F8CD01036056E187A61580516 (void);
// 0x00000262 ZXing.Result ZXing.OneD.OneDReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void OneDReader_doDecode_m7386330554C2E9983108D37AB754F76F3C33A1CD (void);
// 0x00000263 System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void OneDReader_recordPattern_m5463FD1B564B14C4F3336F5CEDA01C9E50CC8EA0 (void);
// 0x00000264 System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[],System.Int32)
extern void OneDReader_recordPattern_m7A3B64E2671222FAD4380D53F3A217C182500320 (void);
// 0x00000265 System.Boolean ZXing.OneD.OneDReader::recordPatternInReverse(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void OneDReader_recordPatternInReverse_m28771B72B70B3F253BB2464CD2EBB1626FD48658 (void);
// 0x00000266 System.Int32 ZXing.OneD.OneDReader::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern void OneDReader_patternMatchVariance_m002F3449FAEA9145B68B818F8C57452BA3F87E4A (void);
// 0x00000267 ZXing.Result ZXing.OneD.OneDReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
// 0x00000268 System.Void ZXing.OneD.OneDReader::.ctor()
extern void OneDReader__ctor_mAFBEF18DB6BE34424E6D1CAF0DD435A5086F960E (void);
// 0x00000269 System.Void ZXing.OneD.OneDReader::.cctor()
extern void OneDReader__cctor_m2A0502DE378C0070521B979FECD074D80FDC2855 (void);
// 0x0000026A ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decodeRow_m6DEAF0EFE4B5E5B0D48F5E9FE4023B997DB7C355 (void);
// 0x0000026B ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decodeRow_m342274023104849117E180740F7FBF4FB80D19EE (void);
// 0x0000026C ZXing.Result ZXing.OneD.UPCAReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decode_mB4743BCDCD91C297C903F78F13C6E5279150573F (void);
// 0x0000026D ZXing.BarcodeFormat ZXing.OneD.UPCAReader::get_BarcodeFormat()
extern void UPCAReader_get_BarcodeFormat_mECCE8975175B08669FAF444B7944D17A84F972D3 (void);
// 0x0000026E System.Int32 ZXing.OneD.UPCAReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCAReader_decodeMiddle_mC7A2536BE5F83103BD8A1D1C2DBE8F343F30CD1E (void);
// 0x0000026F ZXing.Result ZXing.OneD.UPCAReader::maybeReturnResult(ZXing.Result)
extern void UPCAReader_maybeReturnResult_m5552BA55D69C4A9F86F1DB8C5B53E2F6CA67D690 (void);
// 0x00000270 System.Void ZXing.OneD.UPCAReader::.ctor()
extern void UPCAReader__ctor_m1B4FAEA66B19FFB146D33E26389D641AED295F16 (void);
// 0x00000271 ZXing.Result ZXing.OneD.UPCEANExtension2Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern void UPCEANExtension2Support_decodeRow_m6FC896F3CC431D7E1A0C4E59F7D08194FA4A4529 (void);
// 0x00000272 System.Int32 ZXing.OneD.UPCEANExtension2Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEANExtension2Support_decodeMiddle_m8BF98AD2D581DC401B66AAFCD7243D28653E91DA (void);
// 0x00000273 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension2Support::parseExtensionString(System.String)
extern void UPCEANExtension2Support_parseExtensionString_mABA4FA5D12A9977C609273533B53388DB327F676 (void);
// 0x00000274 System.Void ZXing.OneD.UPCEANExtension2Support::.ctor()
extern void UPCEANExtension2Support__ctor_m686A4726FAD6FA63B7F4C4C4CD01184C4BCE7076 (void);
// 0x00000275 ZXing.Result ZXing.OneD.UPCEANExtension5Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern void UPCEANExtension5Support_decodeRow_m196369A7519341663C20A55C0F8D9C45A7EB3040 (void);
// 0x00000276 System.Int32 ZXing.OneD.UPCEANExtension5Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEANExtension5Support_decodeMiddle_m50F1F331BB812B61BB6FCDDF1B46A5781EF90EEB (void);
// 0x00000277 System.Int32 ZXing.OneD.UPCEANExtension5Support::extensionChecksum(System.String)
extern void UPCEANExtension5Support_extensionChecksum_mEB6DD3E65430A6C55C2573C13C4843B206487331 (void);
// 0x00000278 System.Boolean ZXing.OneD.UPCEANExtension5Support::determineCheckDigit(System.Int32,System.Int32&)
extern void UPCEANExtension5Support_determineCheckDigit_mFB9CE5C8D8738397310C9CAAE53182DC5D803455 (void);
// 0x00000279 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension5Support::parseExtensionString(System.String)
extern void UPCEANExtension5Support_parseExtensionString_mC63C8894934F2FD8970E71DAF429C303CB2B233E (void);
// 0x0000027A System.String ZXing.OneD.UPCEANExtension5Support::parseExtension5String(System.String)
extern void UPCEANExtension5Support_parseExtension5String_mAA29427FA7B6ACFF6644FA5FAF6182748AA2AE7F (void);
// 0x0000027B System.Void ZXing.OneD.UPCEANExtension5Support::.ctor()
extern void UPCEANExtension5Support__ctor_m11F10C0B38CA47707464E20497FBEEF8AA40EDD1 (void);
// 0x0000027C System.Void ZXing.OneD.UPCEANExtension5Support::.cctor()
extern void UPCEANExtension5Support__cctor_mC06FFD595C84659118A1A7D0FA25597A2260C1F0 (void);
// 0x0000027D ZXing.Result ZXing.OneD.UPCEANExtensionSupport::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32)
extern void UPCEANExtensionSupport_decodeRow_m8EAB71C63C33CD2C3D245B8CE88B75A41D8E08DA (void);
// 0x0000027E System.Void ZXing.OneD.UPCEANExtensionSupport::.ctor()
extern void UPCEANExtensionSupport__ctor_mEFD58F5A846943270C389706158188ECE212A1CC (void);
// 0x0000027F System.Void ZXing.OneD.UPCEANExtensionSupport::.cctor()
extern void UPCEANExtensionSupport__cctor_m2D5EC8E0381D31C986C11A858F5CB23D785E0E7A (void);
// 0x00000280 System.Void ZXing.OneD.UPCEANReader::.cctor()
extern void UPCEANReader__cctor_m1AA1D6C6D2C836C8EF6061E1213CFFD6D331FE37 (void);
// 0x00000281 System.Void ZXing.OneD.UPCEANReader::.ctor()
extern void UPCEANReader__ctor_m46508F781D82EFB4A4B0DDCB298087130C28182B (void);
// 0x00000282 System.Int32[] ZXing.OneD.UPCEANReader::findStartGuardPattern(ZXing.Common.BitArray)
extern void UPCEANReader_findStartGuardPattern_m7D1525CCAEFB9BD70348399B747B87EC41549F77 (void);
// 0x00000283 ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCEANReader_decodeRow_m463C96CD9386A51FC62F759BBE8A0E77C1CDD7A2 (void);
// 0x00000284 ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCEANReader_decodeRow_m53925E4BBDE547114EFC93F408070B4F83C5642D (void);
// 0x00000285 System.Boolean ZXing.OneD.UPCEANReader::checkChecksum(System.String)
extern void UPCEANReader_checkChecksum_mCD628B259BAB466F75334BEDCDBE5FBBF5C459F5 (void);
// 0x00000286 System.Boolean ZXing.OneD.UPCEANReader::checkStandardUPCEANChecksum(System.String)
extern void UPCEANReader_checkStandardUPCEANChecksum_m67F5427D778277C988B54EF6160BC5E55F6ACA32 (void);
// 0x00000287 System.Nullable`1<System.Int32> ZXing.OneD.UPCEANReader::getStandardUPCEANChecksum(System.String)
extern void UPCEANReader_getStandardUPCEANChecksum_mD03061A29CB13073C9656D30981DE0DDF262E249 (void);
// 0x00000288 System.Int32[] ZXing.OneD.UPCEANReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern void UPCEANReader_decodeEnd_m559247BFCB8429F07809B8A7F90F4DA73543E699 (void);
// 0x00000289 System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern void UPCEANReader_findGuardPattern_m6FF2D724D74B132AA384BFCA3F2E624513248F01 (void);
// 0x0000028A System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern void UPCEANReader_findGuardPattern_m3106899BCF69C2D4CD441E776F987F6CE8BF317F (void);
// 0x0000028B System.Boolean ZXing.OneD.UPCEANReader::decodeDigit(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32[][],System.Int32&)
extern void UPCEANReader_decodeDigit_m4BE543A4AD670AFD401D18EB7371E5A880B6389B (void);
// 0x0000028C ZXing.BarcodeFormat ZXing.OneD.UPCEANReader::get_BarcodeFormat()
// 0x0000028D System.Int32 ZXing.OneD.UPCEANReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
// 0x0000028E System.Void ZXing.OneD.UPCEReader::.ctor()
extern void UPCEReader__ctor_m28B34D410BDA4FC0672E606766674E2F9DF427B3 (void);
// 0x0000028F System.Int32 ZXing.OneD.UPCEReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEReader_decodeMiddle_m120EEB29C634C0C89E2D37B957AF23305DFD583A (void);
// 0x00000290 System.Int32[] ZXing.OneD.UPCEReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern void UPCEReader_decodeEnd_m1601ED31792101DC5AF3A7143BB440CAFA3251EF (void);
// 0x00000291 System.Boolean ZXing.OneD.UPCEReader::checkChecksum(System.String)
extern void UPCEReader_checkChecksum_m9C4BC99D78979868C676AABFA8AD90BAFAA168EB (void);
// 0x00000292 System.Boolean ZXing.OneD.UPCEReader::determineNumSysAndCheckDigit(System.Text.StringBuilder,System.Int32)
extern void UPCEReader_determineNumSysAndCheckDigit_mB283BCB6623F90FB779D14EBFCB207DE0EE68228 (void);
// 0x00000293 ZXing.BarcodeFormat ZXing.OneD.UPCEReader::get_BarcodeFormat()
extern void UPCEReader_get_BarcodeFormat_mD212C99DCE762350B95D9B575AD01FF93F082E38 (void);
// 0x00000294 System.String ZXing.OneD.UPCEReader::convertUPCEtoUPCA(System.String)
extern void UPCEReader_convertUPCEtoUPCA_m22D91535A4105233B807FD9DF20017733BC34120 (void);
// 0x00000295 System.Void ZXing.OneD.UPCEReader::.cctor()
extern void UPCEReader__cctor_mD2D99D4851B0E40AB3D4FFB7611EF27085B5EC68 (void);
// 0x00000296 System.Void ZXing.OneD.RSS.AbstractRSSReader::.ctor()
extern void AbstractRSSReader__ctor_mF40A7FBDA94B256961B41741953316F01B90AFE3 (void);
// 0x00000297 System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDecodeFinderCounters()
extern void AbstractRSSReader_getDecodeFinderCounters_mED32F9666161B0865E58916484C59C6C1CF4783A (void);
// 0x00000298 System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDataCharacterCounters()
extern void AbstractRSSReader_getDataCharacterCounters_mFE40D1439AEF15A5DA6AF85EA12455B2D3F9F43F (void);
// 0x00000299 System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getOddRoundingErrors()
extern void AbstractRSSReader_getOddRoundingErrors_mFA135443BBF1E7083C03296670AF3778C74CCD96 (void);
// 0x0000029A System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getEvenRoundingErrors()
extern void AbstractRSSReader_getEvenRoundingErrors_m8F14551A2F044AE1EEDAB0CB557C7B36ECA053A2 (void);
// 0x0000029B System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getOddCounts()
extern void AbstractRSSReader_getOddCounts_m965C8C7732F4025C5348399750823C51C421B5E2 (void);
// 0x0000029C System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getEvenCounts()
extern void AbstractRSSReader_getEvenCounts_m30ED28C8517CEBA6589B89FB7B31E1D38A09A905 (void);
// 0x0000029D System.Boolean ZXing.OneD.RSS.AbstractRSSReader::parseFinderValue(System.Int32[],System.Int32[][],System.Int32&)
extern void AbstractRSSReader_parseFinderValue_mA66A27123A48FBFFDEE7461314C4B8DB678DF640 (void);
// 0x0000029E System.Void ZXing.OneD.RSS.AbstractRSSReader::increment(System.Int32[],System.Single[])
extern void AbstractRSSReader_increment_m8940571FE039D3D1238AD7854A8A3DAA64ABC703 (void);
// 0x0000029F System.Void ZXing.OneD.RSS.AbstractRSSReader::decrement(System.Int32[],System.Single[])
extern void AbstractRSSReader_decrement_m4F91E70AA9B430FCFF828624BAA4980E97A0696B (void);
// 0x000002A0 System.Boolean ZXing.OneD.RSS.AbstractRSSReader::isFinderPattern(System.Int32[])
extern void AbstractRSSReader_isFinderPattern_m23633017DE14B43F170863E13175F0A136E1A3C7 (void);
// 0x000002A1 System.Void ZXing.OneD.RSS.AbstractRSSReader::.cctor()
extern void AbstractRSSReader__cctor_m978220EDBE8044C65655E05347DD4733D7A91BB0 (void);
// 0x000002A2 System.Int32 ZXing.OneD.RSS.DataCharacter::get_Value()
extern void DataCharacter_get_Value_m989F7EFF27F7D5857631A9E6EF3491524F923CB7 (void);
// 0x000002A3 System.Void ZXing.OneD.RSS.DataCharacter::set_Value(System.Int32)
extern void DataCharacter_set_Value_m4877B2E2E40BBF5855567B1E23B0AFA76DB7ACA7 (void);
// 0x000002A4 System.Int32 ZXing.OneD.RSS.DataCharacter::get_ChecksumPortion()
extern void DataCharacter_get_ChecksumPortion_m6798F3EDC1B1BC40E7722823E6E7AC8D113C70FC (void);
// 0x000002A5 System.Void ZXing.OneD.RSS.DataCharacter::set_ChecksumPortion(System.Int32)
extern void DataCharacter_set_ChecksumPortion_mD9394227B2D3A90D5C99BFE4A398B6F1F82504DD (void);
// 0x000002A6 System.Void ZXing.OneD.RSS.DataCharacter::.ctor(System.Int32,System.Int32)
extern void DataCharacter__ctor_m6D6063014D0582BE08A17259846319C7159180D6 (void);
// 0x000002A7 System.String ZXing.OneD.RSS.DataCharacter::ToString()
extern void DataCharacter_ToString_m7782C70D7CF4BFBAA918664E0859D2E557A934EE (void);
// 0x000002A8 System.Boolean ZXing.OneD.RSS.DataCharacter::Equals(System.Object)
extern void DataCharacter_Equals_m12A12C0DEA308EC5BD421DC6D7120411C92D57E7 (void);
// 0x000002A9 System.Int32 ZXing.OneD.RSS.DataCharacter::GetHashCode()
extern void DataCharacter_GetHashCode_mBB8AF0BE9FA0A9AEF84F8F76EEB03F0314245E69 (void);
// 0x000002AA System.Int32 ZXing.OneD.RSS.FinderPattern::get_Value()
extern void FinderPattern_get_Value_m8014DA11325AD1E1E08646912E56C24807170F9F (void);
// 0x000002AB System.Void ZXing.OneD.RSS.FinderPattern::set_Value(System.Int32)
extern void FinderPattern_set_Value_mF2E69AB10949ADD73E4762CDD2060603FA16F1BA (void);
// 0x000002AC System.Int32[] ZXing.OneD.RSS.FinderPattern::get_StartEnd()
extern void FinderPattern_get_StartEnd_mE939A50F1B0D50B4A54D0A78BF100A8D013AF2D2 (void);
// 0x000002AD System.Void ZXing.OneD.RSS.FinderPattern::set_StartEnd(System.Int32[])
extern void FinderPattern_set_StartEnd_m4B827D1750AF31559D0B1AD309FA330BB59768A8 (void);
// 0x000002AE ZXing.ResultPoint[] ZXing.OneD.RSS.FinderPattern::get_ResultPoints()
extern void FinderPattern_get_ResultPoints_m31BDCA7CA9929339A3131636EF96816A8F857828 (void);
// 0x000002AF System.Void ZXing.OneD.RSS.FinderPattern::set_ResultPoints(ZXing.ResultPoint[])
extern void FinderPattern_set_ResultPoints_m6E89079AEB0D1CB82EA4184C9FC8B3E3817F66B6 (void);
// 0x000002B0 System.Void ZXing.OneD.RSS.FinderPattern::.ctor(System.Int32,System.Int32[],System.Int32,System.Int32,System.Int32)
extern void FinderPattern__ctor_m2705DBACE997D573F3DCCCFE905491F44622DD69 (void);
// 0x000002B1 System.Boolean ZXing.OneD.RSS.FinderPattern::Equals(System.Object)
extern void FinderPattern_Equals_mC1678F7A8574449380F1E44081F392A5BF63EB8E (void);
// 0x000002B2 System.Int32 ZXing.OneD.RSS.FinderPattern::GetHashCode()
extern void FinderPattern_GetHashCode_m12CDC90AB7455C69D4DA8C4FA601EB80CDC38BD8 (void);
// 0x000002B3 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Pair::get_FinderPattern()
extern void Pair_get_FinderPattern_m96B759DC05F52541A4D5323517F7B2CC5F754A2A (void);
// 0x000002B4 System.Void ZXing.OneD.RSS.Pair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern void Pair_set_FinderPattern_m5728E8A721DEBE405B277038A44CABE83320C038 (void);
// 0x000002B5 System.Int32 ZXing.OneD.RSS.Pair::get_Count()
extern void Pair_get_Count_m987093D75472F4272235F675E04B92DB38E33F51 (void);
// 0x000002B6 System.Void ZXing.OneD.RSS.Pair::set_Count(System.Int32)
extern void Pair_set_Count_m32CD3B8130984363CB154A45B947A567E950F7A0 (void);
// 0x000002B7 System.Void ZXing.OneD.RSS.Pair::.ctor(System.Int32,System.Int32,ZXing.OneD.RSS.FinderPattern)
extern void Pair__ctor_m9A69104BACB3404917ED67DB3AB2E6E45014081E (void);
// 0x000002B8 System.Void ZXing.OneD.RSS.Pair::incrementCount()
extern void Pair_incrementCount_m1EBBA492AE25F465BF32BBDA4CE39B9263163045 (void);
// 0x000002B9 System.Void ZXing.OneD.RSS.RSS14Reader::.ctor()
extern void RSS14Reader__ctor_m87AFA3FDE73F8934B2FA01EFC98C58814A8E32F9 (void);
// 0x000002BA ZXing.Result ZXing.OneD.RSS.RSS14Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSS14Reader_decodeRow_m49EE9C4D2E066F09CEF28EAAC1C9E149BE194E42 (void);
// 0x000002BB System.Void ZXing.OneD.RSS.RSS14Reader::addOrTally(System.Collections.Generic.IList`1<ZXing.OneD.RSS.Pair>,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_addOrTally_mDA9A209DFE052260303B996EF263591357D1B9B3 (void);
// 0x000002BC System.Void ZXing.OneD.RSS.RSS14Reader::reset()
extern void RSS14Reader_reset_mF2A38239ECD8AC8C4872D152490CF486CE3FC9D5 (void);
// 0x000002BD ZXing.Result ZXing.OneD.RSS.RSS14Reader::constructResult(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_constructResult_mCD7C8E420DD16CFB7DFB57893CBE6ECE7812A1F0 (void);
// 0x000002BE System.Boolean ZXing.OneD.RSS.RSS14Reader::checkChecksum(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_checkChecksum_mC0215EB52084B2FB5D6337DE3C97A4C09EF9B79E (void);
// 0x000002BF ZXing.OneD.RSS.Pair ZXing.OneD.RSS.RSS14Reader::decodePair(ZXing.Common.BitArray,System.Boolean,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSS14Reader_decodePair_mA61B7EDE4781D2842A8C03588C039C4A8434ECF0 (void);
// 0x000002C0 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.RSS14Reader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean)
extern void RSS14Reader_decodeDataCharacter_m19C8CB82286EF6B7FED910139E2D386819552061 (void);
// 0x000002C1 System.Int32[] ZXing.OneD.RSS.RSS14Reader::findFinderPattern(ZXing.Common.BitArray,System.Boolean)
extern void RSS14Reader_findFinderPattern_m9F67BE44AC43A8ABF8416A9DEA379340AFCA44F0 (void);
// 0x000002C2 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.RSS14Reader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern void RSS14Reader_parseFoundFinderPattern_mC72347A9F4570C8B193C0953838DBD6C4685A0FD (void);
// 0x000002C3 System.Boolean ZXing.OneD.RSS.RSS14Reader::adjustOddEvenCounts(System.Boolean,System.Int32)
extern void RSS14Reader_adjustOddEvenCounts_m93F734448E36ADACE5340173C77DF89D0BA057D0 (void);
// 0x000002C4 System.Void ZXing.OneD.RSS.RSS14Reader::.cctor()
extern void RSS14Reader__cctor_m27777BA0CBA1F46A065C5F221E6D02CED741590A (void);
// 0x000002C5 System.Int32 ZXing.OneD.RSS.RSSUtils::getRSSvalue(System.Int32[],System.Int32,System.Boolean)
extern void RSSUtils_getRSSvalue_m576E8B0BF589CEDD5106BDD3711B740A40BD9203 (void);
// 0x000002C6 System.Int32 ZXing.OneD.RSS.RSSUtils::combins(System.Int32,System.Int32)
extern void RSSUtils_combins_mB2E1EAAC76C33CD63A77666B875BC1151FE1F55C (void);
// 0x000002C7 ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.BitArrayBuilder::buildBitArray(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void BitArrayBuilder_buildBitArray_m7AC1150D564696A291F9ED0C40FF572502446184 (void);
// 0x000002C8 System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_MayBeLast(System.Boolean)
extern void ExpandedPair_set_MayBeLast_mF8ED068D7464BB2189835D13E2DAB710A7E534A7 (void);
// 0x000002C9 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_LeftChar()
extern void ExpandedPair_get_LeftChar_mBB6B374F16E0D99A4FBE539CFC986AD770C29754 (void);
// 0x000002CA System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_LeftChar(ZXing.OneD.RSS.DataCharacter)
extern void ExpandedPair_set_LeftChar_m88021F4C7B0A02E06B313F69D82030673388AA6F (void);
// 0x000002CB ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_RightChar()
extern void ExpandedPair_get_RightChar_m04B44D7E072945EB23BD0F9BCF899982F7EC5955 (void);
// 0x000002CC System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_RightChar(ZXing.OneD.RSS.DataCharacter)
extern void ExpandedPair_set_RightChar_m21F9E5A538C4CE938EB4C66EF175C9040D651C15 (void);
// 0x000002CD ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.ExpandedPair::get_FinderPattern()
extern void ExpandedPair_get_FinderPattern_mD3E9E768D8CB35A730ABBBDC5D88475D5A02D4DC (void);
// 0x000002CE System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern void ExpandedPair_set_FinderPattern_m339DD5FC48B79D5D28F185DF8B4CF37B84C23961 (void);
// 0x000002CF System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::.ctor(ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.FinderPattern,System.Boolean)
extern void ExpandedPair__ctor_m862F5517D3648F59B6B85879EFF421AD3D1C926D (void);
// 0x000002D0 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::get_MustBeLast()
extern void ExpandedPair_get_MustBeLast_m6E16AB3FEA0E5A7756A56A69210004168D79B862 (void);
// 0x000002D1 System.String ZXing.OneD.RSS.Expanded.ExpandedPair::ToString()
extern void ExpandedPair_ToString_mB154F7C9524EC199B4121F5465A632BB58AA4A83 (void);
// 0x000002D2 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::Equals(System.Object)
extern void ExpandedPair_Equals_mEAC2025C762D22270D1549CCD2AE5904FEC22B0D (void);
// 0x000002D3 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::EqualsOrNull(System.Object,System.Object)
extern void ExpandedPair_EqualsOrNull_m65EDF0B40323DEB826D08E8AE4B3970DC2C0567D (void);
// 0x000002D4 System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::GetHashCode()
extern void ExpandedPair_GetHashCode_m45B8B1B685652B96DB0CF7200A007FD482A436A9 (void);
// 0x000002D5 System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::hashNotNull(System.Object)
extern void ExpandedPair_hashNotNull_mE7AC4F83E3E0EED7F51D67BC98A2723E26648911 (void);
// 0x000002D6 System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::.ctor(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32,System.Boolean)
extern void ExpandedRow__ctor_mB645649C2C757C19F68A99C5CA86A1BF168715CB (void);
// 0x000002D7 System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.ExpandedRow::get_Pairs()
extern void ExpandedRow_get_Pairs_m50450B2009FE8FD7118F149A6E309560463777D7 (void);
// 0x000002D8 System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_Pairs(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void ExpandedRow_set_Pairs_m302A4E5EE79058640E1744CD6FF68AF6FA0F64F7 (void);
// 0x000002D9 System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::get_RowNumber()
extern void ExpandedRow_get_RowNumber_mB3C677AF04C8D715F41914B552FFCEE5B27FC2A1 (void);
// 0x000002DA System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_RowNumber(System.Int32)
extern void ExpandedRow_set_RowNumber_m5D9E523450193C6945F959DFA191509F771D6C91 (void);
// 0x000002DB System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::get_IsReversed()
extern void ExpandedRow_get_IsReversed_m469685004873C73FC4EFB292A3266BD9D72EA4FD (void);
// 0x000002DC System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_IsReversed(System.Boolean)
extern void ExpandedRow_set_IsReversed_m3D5EEFD4BE1AF4085620F15BABBA06B4F6FC7FFF (void);
// 0x000002DD System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::IsEquivalent(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void ExpandedRow_IsEquivalent_mBC9D011ECCDDBA5E0B991A99307982DF054AAB55 (void);
// 0x000002DE System.String ZXing.OneD.RSS.Expanded.ExpandedRow::ToString()
extern void ExpandedRow_ToString_m6F6E56B0CCA58F3F0C3BC13D2DC01FB87D635BCE (void);
// 0x000002DF System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::Equals(System.Object)
extern void ExpandedRow_Equals_m9A38B41DBCD88D7F85D53E787A24849A0FAE4607 (void);
// 0x000002E0 System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::GetHashCode()
extern void ExpandedRow_GetHashCode_mB6BB50E4647D4B73DE8CF3EBB858D3A8DC324A0F (void);
// 0x000002E1 ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSSExpandedReader_decodeRow_m95FB77A61BE72E381A76E219313CE310B2C6ED7C (void);
// 0x000002E2 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reset()
extern void RSSExpandedReader_reset_m5B4D9B1B22F64763084CF86762531731A9B3C817 (void);
// 0x000002E3 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow2pairs(System.Int32,ZXing.Common.BitArray)
extern void RSSExpandedReader_decodeRow2pairs_m3F043155B173084ED01B1D4FE57C63722F527C6C (void);
// 0x000002E4 System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Boolean)
extern void RSSExpandedReader_checkRows_m14DD3922B4BF1A9CE4009470670E9D21BD87FCB3 (void);
// 0x000002E5 System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>,System.Int32)
extern void RSSExpandedReader_checkRows_mB4E7966FCBF8045F00EB0F8136819CA112D94749 (void);
// 0x000002E6 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isValidSequence(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void RSSExpandedReader_isValidSequence_m9925CA4BA16D77A9DD82864F31D7AE4CC747C842 (void);
// 0x000002E7 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::storeRow(System.Int32,System.Boolean)
extern void RSSExpandedReader_storeRow_m52F91343FD6E4C2B5B81E9E7702DCB59B9417386 (void);
// 0x000002E8 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::removePartialRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern void RSSExpandedReader_removePartialRows_m87249C6F790D43A7EE4845AB429F5620037F15FF (void);
// 0x000002E9 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isPartialRow(System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern void RSSExpandedReader_isPartialRow_m2AA70C656745B80BAC63A6B1D686A4152170C474 (void);
// 0x000002EA ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::constructResult(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void RSSExpandedReader_constructResult_m48C419011064A068C27033B5B9C8EA6A586FF8C6 (void);
// 0x000002EB System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkChecksum()
extern void RSSExpandedReader_checkChecksum_m3F0DC6157968870B9E5579099BBC297B7591FCA9 (void);
// 0x000002EC System.Int32 ZXing.OneD.RSS.Expanded.RSSExpandedReader::getNextSecondBar(ZXing.Common.BitArray,System.Int32)
extern void RSSExpandedReader_getNextSecondBar_m4587BFD839EB6C3C736E9B769273515170F76043 (void);
// 0x000002ED ZXing.OneD.RSS.Expanded.ExpandedPair ZXing.OneD.RSS.Expanded.RSSExpandedReader::retrieveNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern void RSSExpandedReader_retrieveNextPair_m7F15DB7FF5A1FAF6E69039127C92133B4C1FB933 (void);
// 0x000002EE System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::findNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern void RSSExpandedReader_findNextPair_m48D7CC3F0753B5BC0D9C98FA5661BC0DFF688B8A (void);
// 0x000002EF System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reverseCounters(System.Int32[])
extern void RSSExpandedReader_reverseCounters_m69CF30131ED1896B2C43EA6B66CF0D98BE4CB774 (void);
// 0x000002F0 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.RSSExpandedReader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean)
extern void RSSExpandedReader_parseFoundFinderPattern_mA7D7BAD890497FE3D9461D05E940FF8500F4DF39 (void);
// 0x000002F1 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern void RSSExpandedReader_decodeDataCharacter_m8C4F488085EC38008B6B15C8ABA5705420AD12DE (void);
// 0x000002F2 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isNotA1left(ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern void RSSExpandedReader_isNotA1left_m5B3950904593BDCA3C7F53B09E83FB398478B88B (void);
// 0x000002F3 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::adjustOddEvenCounts(System.Int32)
extern void RSSExpandedReader_adjustOddEvenCounts_mF45EAD02D635060D301CE07AA13AC3767BE9E85F (void);
// 0x000002F4 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.ctor()
extern void RSSExpandedReader__ctor_mF316E1DC3B518BECDAB2E743D6223F263872FABC (void);
// 0x000002F5 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.cctor()
extern void RSSExpandedReader__cctor_m6E09458D19115BA68ED0D25F557D91C369AD8737 (void);
// 0x000002F6 System.Void ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::.ctor(ZXing.Common.BitArray)
extern void AbstractExpandedDecoder__ctor_mB9799D8B99CF185E2C391D6552CA06FC185C01CA (void);
// 0x000002F7 ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getInformation()
extern void AbstractExpandedDecoder_getInformation_m257AB42BCB9E5E73D07640A6622B4EA580F60841 (void);
// 0x000002F8 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getGeneralDecoder()
extern void AbstractExpandedDecoder_getGeneralDecoder_m8B7777282EB6EB81EADDCA497FD6BB5007AB94F4 (void);
// 0x000002F9 System.String ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::parseInformation()
// 0x000002FA ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::createDecoder(ZXing.Common.BitArray)
extern void AbstractExpandedDecoder_createDecoder_mF70A102E53E57DD15BE68738080034C5FCEA691D (void);
// 0x000002FB System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::.ctor(ZXing.Common.BitArray)
extern void AI013103decoder__ctor_m82062027815E7E658E7BC90A86BC6B39035D687B (void);
// 0x000002FC System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI013103decoder_addWeightCode_m6F298E99487A3EBBFA9A8573D79AF2A42361A35F (void);
// 0x000002FD System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::checkWeight(System.Int32)
extern void AI013103decoder_checkWeight_m0C47DBC689B0912DE9341BB4450AE8FDD88605B7 (void);
// 0x000002FE System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01320xDecoder__ctor_mF677A21FA914C4300F8A554D5332FE35FDFEDA0A (void);
// 0x000002FF System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI01320xDecoder_addWeightCode_mBB88E5A9F49AC51751FACBD83F154F38A36EBD56 (void);
// 0x00000300 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::checkWeight(System.Int32)
extern void AI01320xDecoder_checkWeight_mD64B473EAE3F0C6397C080CD2589A8F84FC00AED (void);
// 0x00000301 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01392xDecoder__ctor_mBDA229F0B827C00376CFDAFC1E44C89E662BB5A0 (void);
// 0x00000302 System.String ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::parseInformation()
extern void AI01392xDecoder_parseInformation_m14A98F5E76E9301B9399B8BFA8851EC8CFD6B522 (void);
// 0x00000303 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01393xDecoder__ctor_m6E252AB48F93E3C7DB40EC35261041E08D4451CE (void);
// 0x00000304 System.String ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::parseInformation()
extern void AI01393xDecoder_parseInformation_m1F61CF54065F9DA366C2E57AEB51064F5A43AC52 (void);
// 0x00000305 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.cctor()
extern void AI01393xDecoder__cctor_m254A5DA15688A0BD9D1A3FF730D71B4F03A9A575 (void);
// 0x00000306 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.ctor(ZXing.Common.BitArray,System.String,System.String)
extern void AI013x0x1xDecoder__ctor_mA648FD76D874E725A47BC5FE73FC87D9A64E0557 (void);
// 0x00000307 System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::parseInformation()
extern void AI013x0x1xDecoder_parseInformation_mE9C01A98556A24902B53829DB62A5ACF286B609D (void);
// 0x00000308 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::encodeCompressedDate(System.Text.StringBuilder,System.Int32)
extern void AI013x0x1xDecoder_encodeCompressedDate_mEEA08D51F154547D3C5354908F8237536D8C70CA (void);
// 0x00000309 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI013x0x1xDecoder_addWeightCode_mB816E7BD86821B89362FA4AE12C11628C0CB7061 (void);
// 0x0000030A System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::checkWeight(System.Int32)
extern void AI013x0x1xDecoder_checkWeight_m942BB18994811E65424D98A4E0443482BD06E36D (void);
// 0x0000030B System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.cctor()
extern void AI013x0x1xDecoder__cctor_m0AFCDB94780C22917D4B1A0840437F0AB504CF28 (void);
// 0x0000030C System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI013x0xDecoder__ctor_m2525C3A34D8023D521AFE338900412D3C614A9A1 (void);
// 0x0000030D System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::parseInformation()
extern void AI013x0xDecoder_parseInformation_mAB1E19A53D6A8F42639B9BDDEAD53B9D6197BDF1 (void);
// 0x0000030E System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.cctor()
extern void AI013x0xDecoder__cctor_m9E17F0AA26CED529E826A97D14FBB3B99DD25B39 (void);
// 0x0000030F System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.ctor(ZXing.Common.BitArray)
extern void AI01AndOtherAIs__ctor_mF742689F33C79AFD81FB062BE692ABF1627589AA (void);
// 0x00000310 System.String ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::parseInformation()
extern void AI01AndOtherAIs_parseInformation_m6EF5AF4DF9EE1A08E94915CBC66E35CF8C5CE1FC (void);
// 0x00000311 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.cctor()
extern void AI01AndOtherAIs__cctor_mDDA61F3317EB00392B2E74A035C4A3D60D77A213 (void);
// 0x00000312 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.ctor(ZXing.Common.BitArray)
extern void AI01decoder__ctor_m4A31028D1306594903584A411AA3AAE4E8F52EA7 (void);
// 0x00000313 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtin(System.Text.StringBuilder,System.Int32)
extern void AI01decoder_encodeCompressedGtin_m9C0F4D2905DDAA7B7C8B3B66986463150E965F23 (void);
// 0x00000314 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtinWithoutAI(System.Text.StringBuilder,System.Int32,System.Int32)
extern void AI01decoder_encodeCompressedGtinWithoutAI_mF388AD2B0EDE9778AF40C04992AFBC4E205B9401 (void);
// 0x00000315 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::appendCheckDigit(System.Text.StringBuilder,System.Int32)
extern void AI01decoder_appendCheckDigit_m79D074FE82E8F3A862F855441F47E93460F28CE2 (void);
// 0x00000316 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.cctor()
extern void AI01decoder__cctor_mAA032C6D8586A87C3E3F712C457919D200AB2299 (void);
// 0x00000317 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01weightDecoder__ctor_m29D4F73E3322364D9342E2125033DBC5145E2826 (void);
// 0x00000318 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::encodeCompressedWeight(System.Text.StringBuilder,System.Int32,System.Int32)
extern void AI01weightDecoder_encodeCompressedWeight_m881F6B15B9BAF4CA0389BEB81EB25E6745492290 (void);
// 0x00000319 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
// 0x0000031A System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::checkWeight(System.Int32)
// 0x0000031B System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.ctor(ZXing.Common.BitArray)
extern void AnyAIDecoder__ctor_m94B00C3164F6CD7D246311B6BDBC87D3BD448C3C (void);
// 0x0000031C System.String ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::parseInformation()
extern void AnyAIDecoder_parseInformation_mD9E82CDBCB2C3111866140893C41726AA6FBFA18 (void);
// 0x0000031D System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.cctor()
extern void AnyAIDecoder__cctor_mBB14CF9E7627E0F7DB5101B73C09C70F5E9712B6 (void);
// 0x0000031E System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(System.Boolean)
extern void BlockParsedResult__ctor_m6EA5AA15CC5B7B7001744DA4AFA4AD4309ED6BBB (void);
// 0x0000031F System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation,System.Boolean)
extern void BlockParsedResult__ctor_m3F31FFE19040DC4DB80CCA5C11A9D8EED4673167 (void);
// 0x00000320 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::getDecodedInformation()
extern void BlockParsedResult_getDecodedInformation_mE6D6775171844E1F00595D195E36BCE54932709F (void);
// 0x00000321 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::isFinished()
extern void BlockParsedResult_isFinished_mD02B0DED81509D36D6EEADDECD358BC7AF2510C5 (void);
// 0x00000322 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::.ctor()
extern void CurrentParsingState__ctor_m170790F16E1E9616EC0579D98EC7B046BA85FA0F (void);
// 0x00000323 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::getPosition()
extern void CurrentParsingState_getPosition_m5AC694B2AFAC740FB6CA61866BA0ECF20958D5F4 (void);
// 0x00000324 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setPosition(System.Int32)
extern void CurrentParsingState_setPosition_mA95AC8771627C4AE4F0BE8F2C85A0960AB1030A6 (void);
// 0x00000325 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::incrementPosition(System.Int32)
extern void CurrentParsingState_incrementPosition_mD9C1E1FA905D0082C588E4A1E7A9DA0632B8DB9E (void);
// 0x00000326 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isAlpha()
extern void CurrentParsingState_isAlpha_m9EAE46E22EA5DBC6777A44BB42A23F4679852E98 (void);
// 0x00000327 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isIsoIec646()
extern void CurrentParsingState_isIsoIec646_m7A0197F3BF5D2284D3F0942C9FE93E647E4E4B35 (void);
// 0x00000328 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setNumeric()
extern void CurrentParsingState_setNumeric_m84E36C2B3231F4ED636896066E41C1142B5E3BCF (void);
// 0x00000329 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setAlpha()
extern void CurrentParsingState_setAlpha_mEB9D2A978E74AA0195BDBD4EB48656E12364441F (void);
// 0x0000032A System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setIsoIec646()
extern void CurrentParsingState_setIsoIec646_mF16FB7134352AFEA8F52847D2F6AE4C34FE9B6D1 (void);
// 0x0000032B System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.ctor(System.Int32,System.Char)
extern void DecodedChar__ctor_m8DC1E11B4513C8DFF8D51C26F210DC073955CE44 (void);
// 0x0000032C System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::getValue()
extern void DecodedChar_getValue_m18AA998A9CB0D566B4AE53697D08A2D487434E45 (void);
// 0x0000032D System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::isFNC1()
extern void DecodedChar_isFNC1_mBCAB80A8F27EB4FA6DB35EFF7EAA05D393C0B330 (void);
// 0x0000032E System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.cctor()
extern void DecodedChar__cctor_m236091BA5198C0E8C55CF88BBC80870924DDB243 (void);
// 0x0000032F System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String)
extern void DecodedInformation__ctor_m4998E40E554520364E4FDB0405888555E5925366 (void);
// 0x00000330 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String,System.Int32)
extern void DecodedInformation__ctor_mFF70533CD1B84EF014571E06853F8838ECD5E052 (void);
// 0x00000331 System.String ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getNewString()
extern void DecodedInformation_getNewString_m23BDC1AFD69EBAC29404FF5E684CD94E4347BBC5 (void);
// 0x00000332 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::isRemaining()
extern void DecodedInformation_isRemaining_m0013A51713594DEC9E5354A7473296090B15153D (void);
// 0x00000333 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getRemainingValue()
extern void DecodedInformation_getRemainingValue_m1D126F7E01A3A9D31A7AA13B98BFFF3AF0CD4593 (void);
// 0x00000334 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.ctor(System.Int32,System.Int32,System.Int32)
extern void DecodedNumeric__ctor_mC9B474FEB6BF034189AC91BBD114CFC3D9F54982 (void);
// 0x00000335 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getFirstDigit()
extern void DecodedNumeric_getFirstDigit_m8BE710F8DD7B7A16B78F8EE18F71D12AF849F781 (void);
// 0x00000336 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getSecondDigit()
extern void DecodedNumeric_getSecondDigit_m8426B5D553110DD48E9EC4100FAC49E492F75590 (void);
// 0x00000337 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isFirstDigitFNC1()
extern void DecodedNumeric_isFirstDigitFNC1_m90BB548D517532F033F688A03487B4C7224B7811 (void);
// 0x00000338 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isSecondDigitFNC1()
extern void DecodedNumeric_isSecondDigitFNC1_mE9CD925AFFCC93A1B888FC7435CEE42CF1BC8E6B (void);
// 0x00000339 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.cctor()
extern void DecodedNumeric__cctor_mF78A1176F78628EEA09C85BF73A4BF24CA11F125 (void);
// 0x0000033A System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::get_NewPosition()
extern void DecodedObject_get_NewPosition_m995F3D4A19B254CF37BB255961914C45CBDB9D99 (void);
// 0x0000033B System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::set_NewPosition(System.Int32)
extern void DecodedObject_set_NewPosition_mC16722AB1F55B5ABB4C3CDD32D251A20DBB0B3EB (void);
// 0x0000033C System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::.ctor(System.Int32)
extern void DecodedObject__ctor_m5F181A52205913AFE8ED145665786A60836B5DB7 (void);
// 0x0000033D System.Void ZXing.OneD.RSS.Expanded.Decoders.FieldParser::.cctor()
extern void FieldParser__cctor_m83FB3714F17A6E8601096072314ADD6F63B00F77 (void);
// 0x0000033E System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::parseFieldsInGeneralPurpose(System.String)
extern void FieldParser_parseFieldsInGeneralPurpose_mB02CFEC87C7C164CECFC8A711D83492396ECDC9F (void);
// 0x0000033F System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processFixedAI(System.Int32,System.Int32,System.String)
extern void FieldParser_processFixedAI_m900F355C6652EA30CBB0F988A22E319F62BE0C8A (void);
// 0x00000340 System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processVariableAI(System.Int32,System.Int32,System.String)
extern void FieldParser_processVariableAI_m9B0C976DB90E21E5BD6DE9ECDC1FEAD8B4E5AB15 (void);
// 0x00000341 System.Void ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::.ctor(ZXing.Common.BitArray)
extern void GeneralAppIdDecoder__ctor_mB707FE2F1A1103E225C23CE5C23115B305633768 (void);
// 0x00000342 System.String ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAllCodes(System.Text.StringBuilder,System.Int32)
extern void GeneralAppIdDecoder_decodeAllCodes_m75335EFCE0496AF24389835C2C819970ABBBA4DF (void);
// 0x00000343 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillNumeric(System.Int32)
extern void GeneralAppIdDecoder_isStillNumeric_m372D24F2D1CB1ACB08CE61BA9F71870A01938EC7 (void);
// 0x00000344 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeNumeric(System.Int32)
extern void GeneralAppIdDecoder_decodeNumeric_mF17AFC19980D1F0AB422C4B174BECA1F0A17CCEA (void);
// 0x00000345 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(System.Int32,System.Int32)
extern void GeneralAppIdDecoder_extractNumericValueFromBitArray_m7FF4BB86F9D07141618824C59E5F12696AB61EE3 (void);
// 0x00000346 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void GeneralAppIdDecoder_extractNumericValueFromBitArray_mE1B90F38F45D70C7BC2D41CD5E4E377A156999C6 (void);
// 0x00000347 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeGeneralPurposeField(System.Int32,System.String)
extern void GeneralAppIdDecoder_decodeGeneralPurposeField_m4474370A3F232CFA931CB93816F952787F4D8372 (void);
// 0x00000348 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseBlocks()
extern void GeneralAppIdDecoder_parseBlocks_mC87807DC83286C55A52766F235F23D70660BF7E9 (void);
// 0x00000349 ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseNumericBlock()
extern void GeneralAppIdDecoder_parseNumericBlock_mFD020CE3A6AAB72D7FD317AF441EE7177E454B4A (void);
// 0x0000034A ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseIsoIec646Block()
extern void GeneralAppIdDecoder_parseIsoIec646Block_m418361A8A7DCF1E2141EA64CDFE05A3E735A75A4 (void);
// 0x0000034B ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseAlphaBlock()
extern void GeneralAppIdDecoder_parseAlphaBlock_mF8BB16CEB151EDF92611BF7A763BAC8261D7ECF2 (void);
// 0x0000034C System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillIsoIec646(System.Int32)
extern void GeneralAppIdDecoder_isStillIsoIec646_m5E5DE35362E6843D99C0BD84C7DE9976FA92BCAF (void);
// 0x0000034D ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeIsoIec646(System.Int32)
extern void GeneralAppIdDecoder_decodeIsoIec646_mCA511B46280294FB54A1C2187997516648D28135 (void);
// 0x0000034E System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillAlpha(System.Int32)
extern void GeneralAppIdDecoder_isStillAlpha_m728B87258D5E598A233867A42939B692ED904674 (void);
// 0x0000034F ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAlphanumeric(System.Int32)
extern void GeneralAppIdDecoder_decodeAlphanumeric_mBCAC630EC16B84A5C4156EB0DD1C3CE969B3C967 (void);
// 0x00000350 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaTo646ToAlphaLatch(System.Int32)
extern void GeneralAppIdDecoder_isAlphaTo646ToAlphaLatch_m50E16F01815D7733528FCD23C3CE7806151D2502 (void);
// 0x00000351 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaOr646ToNumericLatch(System.Int32)
extern void GeneralAppIdDecoder_isAlphaOr646ToNumericLatch_mF9C16B4F786161CB166AD6968D2AA54C43B44033 (void);
// 0x00000352 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isNumericToAlphaNumericLatch(System.Int32)
extern void GeneralAppIdDecoder_isNumericToAlphaNumericLatch_m591C0AA3C2D8EC6C81A4DB63A059B82531AE4191 (void);
// 0x00000353 ZXing.Result ZXing.Maxicode.MaxiCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MaxiCodeReader_decode_m2F48E27BE1CB0B35CA0A2FF3FB3C95A55BD984F9 (void);
// 0x00000354 System.Void ZXing.Maxicode.MaxiCodeReader::reset()
extern void MaxiCodeReader_reset_m7AE345762EDECEBB448F27D2207506E9EEDBEE50 (void);
// 0x00000355 ZXing.Common.BitMatrix ZXing.Maxicode.MaxiCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern void MaxiCodeReader_extractPureBits_m981377EDECDB3326CF9E1F6D53470E5AAFD3F929 (void);
// 0x00000356 System.Void ZXing.Maxicode.MaxiCodeReader::.ctor()
extern void MaxiCodeReader__ctor_mF9EA7736AF6956580175D21B9FA6BA59A0B7BB6E (void);
// 0x00000357 System.Void ZXing.Maxicode.MaxiCodeReader::.cctor()
extern void MaxiCodeReader__cctor_mEDA5A21485BF82C698A4148F3665BE16B20D705C (void);
// 0x00000358 System.Void ZXing.Maxicode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_m6126F72F80D60C0007BA852ABAEFE7F7A5BE2373 (void);
// 0x00000359 System.Byte[] ZXing.Maxicode.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_m39840CCE52B5BBAF58D70C42D3A4C58EB827B972 (void);
// 0x0000035A System.Void ZXing.Maxicode.Internal.BitMatrixParser::.cctor()
extern void BitMatrixParser__cctor_m8DFFCBE1842142AEF376E813CEB5C436B2D8F351 (void);
// 0x0000035B ZXing.Common.DecoderResult ZXing.Maxicode.Internal.DecodedBitStreamParser::decode(System.Byte[],System.Int32)
extern void DecodedBitStreamParser_decode_mA053FD2AF2C324F88EDB84F9FD0C6EBC61302F6A (void);
// 0x0000035C System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getBit(System.Int32,System.Byte[])
extern void DecodedBitStreamParser_getBit_mE8FC742850F730B5A1CF705536DF14721631DE0D (void);
// 0x0000035D System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getInt(System.Byte[],System.Byte[])
extern void DecodedBitStreamParser_getInt_m74D349CD3A0D0216856D99C37A3EE22CD884FE4D (void);
// 0x0000035E System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getCountry(System.Byte[])
extern void DecodedBitStreamParser_getCountry_mE875E77435231C568678556412E497AE9A554179 (void);
// 0x0000035F System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getServiceClass(System.Byte[])
extern void DecodedBitStreamParser_getServiceClass_m2BDB4929AED4A8DF62018664A21741780ACD3168 (void);
// 0x00000360 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2Length(System.Byte[])
extern void DecodedBitStreamParser_getPostCode2Length_mD377BCA316788B0603D30FECEC61A684A01995A8 (void);
// 0x00000361 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2(System.Byte[])
extern void DecodedBitStreamParser_getPostCode2_m54054686A72F4EC4352A45482A24CA95B2DF696D (void);
// 0x00000362 System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode3(System.Byte[])
extern void DecodedBitStreamParser_getPostCode3_m688A9517F5CB134A6B876A3A108C47726FE97BF8 (void);
// 0x00000363 System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getMessage(System.Byte[],System.Int32,System.Int32)
extern void DecodedBitStreamParser_getMessage_m983CF13314B4487E80808EBF42151C44A5DCF800 (void);
// 0x00000364 System.Void ZXing.Maxicode.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_mBA6D4418D6CC386688A7B75A102F6F46F24880E7 (void);
// 0x00000365 System.Void ZXing.Maxicode.Internal.Decoder::.ctor()
extern void Decoder__ctor_mD15198885F26178D3E554947A4567F237B67E134 (void);
// 0x00000366 ZXing.Common.DecoderResult ZXing.Maxicode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_m7FB80720A904691FD1F1E72B39042D46C8E3B770 (void);
// 0x00000367 System.Boolean ZXing.Maxicode.Internal.Decoder::correctErrors(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void Decoder_correctErrors_mAABBD4168FDA8AD9241554EEF7ACAC80CC96714F (void);
// 0x00000368 System.Void ZXing.IMB.IMBReader::.cctor()
extern void IMBReader__cctor_mBE3570CF56CFBDBD926D349F9B2C539DA1333359 (void);
// 0x00000369 ZXing.Result ZXing.IMB.IMBReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void IMBReader_doDecode_m2A9B60B0370010EBC8F30F82E8DCAF69D948CE56 (void);
// 0x0000036A System.Void ZXing.IMB.IMBReader::reset()
extern void IMBReader_reset_mBEC815D05225D6A5C3BE2B059EE845159DDDE8ED (void);
// 0x0000036B System.UInt16 ZXing.IMB.IMBReader::binaryStringToDec(System.String)
extern void IMBReader_binaryStringToDec_m58A4FCF3F46EE94ED6B8AD10FA9DE5941A40BD2B (void);
// 0x0000036C System.String ZXing.IMB.IMBReader::invertedBinaryString(System.String)
extern void IMBReader_invertedBinaryString_m7CF101B6EBA77EC0831AF40A0BA3AE0B29CF801B (void);
// 0x0000036D System.Boolean ZXing.IMB.IMBReader::getCodeWords(System.Int32[]&,System.String,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Int32[][],System.Char[][])
extern void IMBReader_getCodeWords_mE723FD52CCA0D65D483DDCBA3393FC420D9A7F6C (void);
// 0x0000036E System.String ZXing.IMB.IMBReader::getTrackingNumber(System.String)
extern void IMBReader_getTrackingNumber_m5667D86D9E2FA0FAEBDAE334D7A494E7BBDA3E5B (void);
// 0x0000036F System.Void ZXing.IMB.IMBReader::fillLists(ZXing.Common.BitArray,ZXing.Common.BitArray,ZXing.Common.BitArray,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Int32,System.Int32)
extern void IMBReader_fillLists_mF2EBD957653E8D65115E66725EAFB1314EE52B5A (void);
// 0x00000370 System.Int32 ZXing.IMB.IMBReader::isIMB(ZXing.Common.BitArray,System.Int32&,System.Int32&,System.Int32&)
extern void IMBReader_isIMB_m0C7DA3AE7C6549EA669C8070E37045BE37830F69 (void);
// 0x00000371 System.Int32 ZXing.IMB.IMBReader::getNumberBars(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern void IMBReader_getNumberBars_m5CDD15C3F4DB3C4EA4D631B24C778853E3C2DF54 (void);
// 0x00000372 ZXing.Result ZXing.IMB.IMBReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void IMBReader_decodeRow_m894CA8C8D2229207D81B9AD6FCD51E6ED2B26203 (void);
// 0x00000373 System.Void ZXing.IMB.IMBReader::.ctor()
extern void IMBReader__ctor_m19281820CC2C6EB40472A522525CC853A9C6AECB (void);
// 0x00000374 ZXing.Result ZXing.Datamatrix.DataMatrixReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DataMatrixReader_decode_m9667D4B23324EA969572EE1082035A6B8B670D9F (void);
// 0x00000375 System.Void ZXing.Datamatrix.DataMatrixReader::reset()
extern void DataMatrixReader_reset_m5B52298DC5533019464421E7AB7AE5BDF765DF81 (void);
// 0x00000376 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixReader::extractPureBits(ZXing.Common.BitMatrix)
extern void DataMatrixReader_extractPureBits_m55B4D00DB84E353553E27DC2DB6AF305F4E2DE94 (void);
// 0x00000377 System.Boolean ZXing.Datamatrix.DataMatrixReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Int32&)
extern void DataMatrixReader_moduleSize_m65DE18DE19765DEE2F10F8E256F77A4A9672D151 (void);
// 0x00000378 System.Void ZXing.Datamatrix.DataMatrixReader::.ctor()
extern void DataMatrixReader__ctor_m85BD882FF446AE0CB2CA0D74C66C20CFDC65601D (void);
// 0x00000379 System.Void ZXing.Datamatrix.DataMatrixReader::.cctor()
extern void DataMatrixReader__cctor_m462F62CC105416CD5B1E216C0C707F657D1EBF72 (void);
// 0x0000037A System.Void ZXing.Datamatrix.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_m386F5813F52BF7AE3AD4BFF936D936C92A330605 (void);
// 0x0000037B ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::get_Version()
extern void BitMatrixParser_get_Version_m6C900B2956E44520D95A94CFD07604D3A76984FD (void);
// 0x0000037C ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::readVersion(ZXing.Common.BitMatrix)
extern void BitMatrixParser_readVersion_mDC38CF42367C9CCC61B0F77798D941A1E695C85B (void);
// 0x0000037D System.Byte[] ZXing.Datamatrix.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_m29BBB695C8226588373A8AC1853297F5221AAF97 (void);
// 0x0000037E System.Boolean ZXing.Datamatrix.Internal.BitMatrixParser::readModule(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_readModule_m569B5F40846EEBF8D3A464B36B487AD3373154EA (void);
// 0x0000037F System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readUtah(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_readUtah_m1D28FA81053475B9098633147A1B39E0320F80E7 (void);
// 0x00000380 System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner1(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner1_m20BCD4E9B975BAA3151DE45A742D9116CEF7CFC3 (void);
// 0x00000381 System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner2(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner2_m7569681C0C715115ADB02D279B9760FF866DABDB (void);
// 0x00000382 System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner3(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner3_m6783A509F41B926DBDDF76C099CDB5F9AA86DDAA (void);
// 0x00000383 System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner4(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner4_m5759ACE192A4FDD52F5884872874C261C671C8A3 (void);
// 0x00000384 ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::extractDataRegion(ZXing.Common.BitMatrix)
extern void BitMatrixParser_extractDataRegion_mAC8057721ECA85F1DD55E45D48342F26913CFF5E (void);
// 0x00000385 System.Void ZXing.Datamatrix.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern void DataBlock__ctor_m3586871A95E9094BA03371DA805958C175AA3419 (void);
// 0x00000386 ZXing.Datamatrix.Internal.DataBlock[] ZXing.Datamatrix.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.Datamatrix.Internal.Version)
extern void DataBlock_getDataBlocks_m37BF2B9FBF9187AF4514FFDB8FE949F564B3BFA6 (void);
// 0x00000387 System.Int32 ZXing.Datamatrix.Internal.DataBlock::get_NumDataCodewords()
extern void DataBlock_get_NumDataCodewords_m3DAE253C0A7FAC38E308C613222229CEA9839E62 (void);
// 0x00000388 System.Byte[] ZXing.Datamatrix.Internal.DataBlock::get_Codewords()
extern void DataBlock_get_Codewords_m2492420B306698FD94DFA1699B133A7DF6500393 (void);
// 0x00000389 ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.DecodedBitStreamParser::decode(System.Byte[])
extern void DecodedBitStreamParser_decode_m4EB9F6FA66194EA03B1D611AD9F4AFFAF9A81635 (void);
// 0x0000038A System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAsciiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Text.StringBuilder,ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode&)
extern void DecodedBitStreamParser_decodeAsciiSegment_m01EF6EFB97412C80C47F0E44B3C699B278C417FC (void);
// 0x0000038B System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeC40Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeC40Segment_mF54F17107786036F76699F86C03EB4A11B12B008 (void);
// 0x0000038C System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeTextSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeTextSegment_m6A55FA2F92A488AD358484A759BFEFC5A7D77CC9 (void);
// 0x0000038D System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAnsiX12Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeAnsiX12Segment_mDBC6698B6BE8A111AC17BE0B43ED6F5AE2E861D0 (void);
// 0x0000038E System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::parseTwoBytes(System.Int32,System.Int32,System.Int32[])
extern void DecodedBitStreamParser_parseTwoBytes_mB33EE53F80C351F5D938F6D838DAE4D28945ABBF (void);
// 0x0000038F System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeEdifactSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeEdifactSegment_mE5A97D2D46F3D78B45A99431681BFB087A2E1604 (void);
// 0x00000390 System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeBase256Segment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Collections.Generic.IList`1<System.Byte[]>)
extern void DecodedBitStreamParser_decodeBase256Segment_mD863AA16E8EECC3607BCEFA5A1EBA2A9454FD1FC (void);
// 0x00000391 System.Int32 ZXing.Datamatrix.Internal.DecodedBitStreamParser::unrandomize255State(System.Int32,System.Int32)
extern void DecodedBitStreamParser_unrandomize255State_m0DA91DE833A8851A472008557E600FEDD387F57D (void);
// 0x00000392 System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_m01BE44C8AF8DC8C9837F14FA2DD74B90E29D42BB (void);
// 0x00000393 System.Void ZXing.Datamatrix.Internal.Decoder::.ctor()
extern void Decoder__ctor_m0C30E2BEC990C74646C847BB33709DBE3DECAECB (void);
// 0x00000394 ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.Decoder::decode(ZXing.Common.BitMatrix)
extern void Decoder_decode_m45FAD83731B072C0A21DDE168C61BA34A1EEC7DB (void);
// 0x00000395 System.Boolean ZXing.Datamatrix.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern void Decoder_correctErrors_m6FF19BFFCCB2139A5B0AA904B7C204B374B7477E (void);
// 0x00000396 System.Void ZXing.Datamatrix.Internal.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Datamatrix.Internal.Version/ECBlocks)
extern void Version__ctor_m077FC3507BDA98E6745F77BD591712AFF6B421AF (void);
// 0x00000397 System.Int32 ZXing.Datamatrix.Internal.Version::getVersionNumber()
extern void Version_getVersionNumber_mC240D587FD546AFAA9813B4B8BF21BEBE794554B (void);
// 0x00000398 System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeRows()
extern void Version_getSymbolSizeRows_mD4ED138BC6C1605D3849C4E25B30D01D3F415043 (void);
// 0x00000399 System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeColumns()
extern void Version_getSymbolSizeColumns_m6DBAB1F1A2B07654B4924D8743C462FD2195928B (void);
// 0x0000039A System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeRows()
extern void Version_getDataRegionSizeRows_mB35A7756D8B4E8771CF1AF9D2512F1523E99A895 (void);
// 0x0000039B System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeColumns()
extern void Version_getDataRegionSizeColumns_m7510DAB7150212610408F59B466CBCDBBB2F539C (void);
// 0x0000039C System.Int32 ZXing.Datamatrix.Internal.Version::getTotalCodewords()
extern void Version_getTotalCodewords_m053BB70F73200244636554135BFC54F4F927A19B (void);
// 0x0000039D ZXing.Datamatrix.Internal.Version/ECBlocks ZXing.Datamatrix.Internal.Version::getECBlocks()
extern void Version_getECBlocks_mC10391CC1DE3D77DFDBF84AB96E20568885FAC15 (void);
// 0x0000039E ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.Version::getVersionForDimensions(System.Int32,System.Int32)
extern void Version_getVersionForDimensions_m5AF579DBF7B425B8955E5049EB9FD4329F665B17 (void);
// 0x0000039F System.String ZXing.Datamatrix.Internal.Version::ToString()
extern void Version_ToString_mBFD16FC2B61F9E76FA2E6EB798222772D7A286A5 (void);
// 0x000003A0 ZXing.Datamatrix.Internal.Version[] ZXing.Datamatrix.Internal.Version::buildVersions()
extern void Version_buildVersions_mBE21DC48893776F483675E749F37E1114F16C6B8 (void);
// 0x000003A1 System.Void ZXing.Datamatrix.Internal.Version::.cctor()
extern void Version__cctor_m383D2B792F513387929F5A46B748C7DEDE53A84E (void);
// 0x000003A2 System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB)
extern void ECBlocks__ctor_m5A1EC8BBA032650CDF7599CCA6C47E433E4ACB6B (void);
// 0x000003A3 System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB,ZXing.Datamatrix.Internal.Version/ECB)
extern void ECBlocks__ctor_m36FA0B4968370547116ECD37E8CD4723277379AE (void);
// 0x000003A4 System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECCodewords()
extern void ECBlocks_get_ECCodewords_mA046C3872AF9D771F6679BC80E18D4DB07927BAA (void);
// 0x000003A5 ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECBlocksValue()
extern void ECBlocks_get_ECBlocksValue_m2932E2682570E3954B2C9DAC28850342777821A8 (void);
// 0x000003A6 System.Void ZXing.Datamatrix.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
extern void ECB__ctor_mFB2892A387586B52357CB5496BC67ABD357DED1B (void);
// 0x000003A7 System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_Count()
extern void ECB_get_Count_m36046552D97F93739FF93F152833AAEA321D920A (void);
// 0x000003A8 System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_DataCodewords()
extern void ECB_get_DataCodewords_m93C7355C18323CC41766125F732C858A0725FB1A (void);
// 0x000003A9 System.Void ZXing.Datamatrix.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_m1A3DE9E277A16A11FA8E4E884F65CD14F135A842 (void);
// 0x000003AA ZXing.Common.DetectorResult ZXing.Datamatrix.Internal.Detector::detect()
extern void Detector_detect_m23B78B82C4B007A540A3EB67579CA30EFA1ED559 (void);
// 0x000003AB ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::correctTopRightRectangular(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern void Detector_correctTopRightRectangular_mAEAF34BE6032672C5A4472A5227DA0B7A3544C49 (void);
// 0x000003AC ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::correctTopRight(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_correctTopRight_m86D8CACB051377ABBDF69890B1DFAEF73CC1DE77 (void);
// 0x000003AD System.Boolean ZXing.Datamatrix.Internal.Detector::isValid(ZXing.ResultPoint)
extern void Detector_isValid_m14AA69078ED2F79C1F803B6FCA5D1A54F1C8740C (void);
// 0x000003AE System.Int32 ZXing.Datamatrix.Internal.Detector::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_distance_m49C82D31D47ECB452FEC5B5AB8EBC07C7568E492 (void);
// 0x000003AF System.Void ZXing.Datamatrix.Internal.Detector::increment(System.Collections.Generic.IDictionary`2<ZXing.ResultPoint,System.Int32>,ZXing.ResultPoint)
extern void Detector_increment_m7C12AD7D2D06A1A6D10799149F563F676B057D65 (void);
// 0x000003B0 ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern void Detector_sampleGrid_mC3B9777B9F11EC3AB6F87490B35A194EEA7C3260 (void);
// 0x000003B1 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions ZXing.Datamatrix.Internal.Detector::transitionsBetween(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_transitionsBetween_m2B9B8E029B5F2EE0C163AED9E88BFCA2813864A8 (void);
// 0x000003B2 ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_From()
extern void ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716 (void);
// 0x000003B3 System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_From(ZXing.ResultPoint)
extern void ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985 (void);
// 0x000003B4 ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_To()
extern void ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE (void);
// 0x000003B5 System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_To(ZXing.ResultPoint)
extern void ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D (void);
// 0x000003B6 System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_Transitions()
extern void ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172 (void);
// 0x000003B7 System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_Transitions(System.Int32)
extern void ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927 (void);
// 0x000003B8 System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::.ctor(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void ResultPointsAndTransitions__ctor_m93CA3CE4208D5880629A3B07C230100E1CC0A408 (void);
// 0x000003B9 System.String ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::ToString()
extern void ResultPointsAndTransitions_ToString_m6EAA6FB9BB8C6BE81D5BE8D61E6376D6238F1735 (void);
// 0x000003BA System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::Compare(ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions,ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions)
extern void ResultPointsAndTransitionsComparator_Compare_m9DCE83B6D7A89A8E5F28AB71AB32820E5AFC45E4 (void);
// 0x000003BB System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::.ctor()
extern void ResultPointsAndTransitionsComparator__ctor_mEA8E6FA2CD0BD25E539D9E248043981B9E93A4CB (void);
// 0x000003BC System.Int32 ZXing.Common.BitArray::get_Size()
extern void BitArray_get_Size_m515BDB7E05CABB85B3CF72ED50AF51F89A51ED64 (void);
// 0x000003BD System.Boolean ZXing.Common.BitArray::get_Item(System.Int32)
extern void BitArray_get_Item_m9D13930253334ABFCDA32E1823394164DD51C936 (void);
// 0x000003BE System.Void ZXing.Common.BitArray::set_Item(System.Int32,System.Boolean)
extern void BitArray_set_Item_m8D909D8C966AA5EF13AC3394E5084A214FA2FAF9 (void);
// 0x000003BF System.Void ZXing.Common.BitArray::.ctor(System.Int32)
extern void BitArray__ctor_m0E62557F33A96E8DFB15D9C0CB105A192A0168F1 (void);
// 0x000003C0 System.Int32 ZXing.Common.BitArray::numberOfTrailingZeros(System.Int32)
extern void BitArray_numberOfTrailingZeros_m0295CDB35BEAA90C7828B40CE2DCF0CFF4C33424 (void);
// 0x000003C1 System.Int32 ZXing.Common.BitArray::getNextSet(System.Int32)
extern void BitArray_getNextSet_mF4820F737EFB57D2FC673C813DD85212247FC3C6 (void);
// 0x000003C2 System.Int32 ZXing.Common.BitArray::getNextUnset(System.Int32)
extern void BitArray_getNextUnset_m52ECE8AB097792EBD0C18F31E1E9E5E17279E4D3 (void);
// 0x000003C3 System.Void ZXing.Common.BitArray::setBulk(System.Int32,System.Int32)
extern void BitArray_setBulk_mD99299B2C2FB6A694043DC8E57C68EA578C4247C (void);
// 0x000003C4 System.Void ZXing.Common.BitArray::clear()
extern void BitArray_clear_m560674060049FCA382C2BF398C5A3A81D2793E81 (void);
// 0x000003C5 System.Boolean ZXing.Common.BitArray::isRange(System.Int32,System.Int32,System.Boolean)
extern void BitArray_isRange_m60A143447BB1C594B147D8C8129587F55D5320DA (void);
// 0x000003C6 System.Int32[] ZXing.Common.BitArray::get_Array()
extern void BitArray_get_Array_mECAB2579BAEB0354C48454014D8AFBB06F5901F8 (void);
// 0x000003C7 System.Void ZXing.Common.BitArray::reverse()
extern void BitArray_reverse_mA70D97C502F68EC09EE7240A75CDFD3D598E1F9F (void);
// 0x000003C8 System.Int32[] ZXing.Common.BitArray::makeArray(System.Int32)
extern void BitArray_makeArray_mB70B8A6ED27BE20E1B1858D768E3984596EED992 (void);
// 0x000003C9 System.Boolean ZXing.Common.BitArray::Equals(System.Object)
extern void BitArray_Equals_mF452D2987F58EB25628BD428A09A8E9FF207D9A2 (void);
// 0x000003CA System.Int32 ZXing.Common.BitArray::GetHashCode()
extern void BitArray_GetHashCode_m0529935E99247D2CB21569099E337A41A7F0222D (void);
// 0x000003CB System.String ZXing.Common.BitArray::ToString()
extern void BitArray_ToString_m8B3EE96999F115753716A9A53BCB152577F6E911 (void);
// 0x000003CC System.Void ZXing.Common.BitArray::.cctor()
extern void BitArray__cctor_m53C6769E65798B25E6D906EB3F8FB4236640D9B6 (void);
// 0x000003CD System.Int32 ZXing.Common.BitMatrix::get_Width()
extern void BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8 (void);
// 0x000003CE System.Int32 ZXing.Common.BitMatrix::get_Height()
extern void BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433 (void);
// 0x000003CF System.Void ZXing.Common.BitMatrix::.ctor(System.Int32)
extern void BitMatrix__ctor_mB63ED20D6DC99156079AA7C2F2E1209817F387B2 (void);
// 0x000003D0 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32)
extern void BitMatrix__ctor_mACF00B24CE279465B8BCA1720900297C3A2C54F3 (void);
// 0x000003D1 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32,System.Int32,System.Int32[])
extern void BitMatrix__ctor_m98A83F4452563A005D53D40DCC35640DFA90CB80 (void);
// 0x000003D2 System.Boolean ZXing.Common.BitMatrix::get_Item(System.Int32,System.Int32)
extern void BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475 (void);
// 0x000003D3 System.Void ZXing.Common.BitMatrix::set_Item(System.Int32,System.Int32,System.Boolean)
extern void BitMatrix_set_Item_mEFF2D0155D20FAC7EB7B14F2D8472AB23ACE12F6 (void);
// 0x000003D4 System.Void ZXing.Common.BitMatrix::flip(System.Int32,System.Int32)
extern void BitMatrix_flip_mDAA9A98182F3F6E069C28565E04D88318BF7666D (void);
// 0x000003D5 System.Void ZXing.Common.BitMatrix::flipWhen(System.Func`3<System.Int32,System.Int32,System.Boolean>)
extern void BitMatrix_flipWhen_mD0356F5448B6E953839AD33F451B53879FB29605 (void);
// 0x000003D6 System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614 (void);
// 0x000003D7 ZXing.Common.BitArray ZXing.Common.BitMatrix::getRow(System.Int32,ZXing.Common.BitArray)
extern void BitMatrix_getRow_m64D830BD70060BD2CC5E97B67A4E40860E9142B4 (void);
// 0x000003D8 System.Void ZXing.Common.BitMatrix::setRow(System.Int32,ZXing.Common.BitArray)
extern void BitMatrix_setRow_mD646D01811B9536E03D884F55F440F336A4A6110 (void);
// 0x000003D9 System.Void ZXing.Common.BitMatrix::rotate180()
extern void BitMatrix_rotate180_m7FBE958C1A6844CBC9C2C72605EE1362A2F17989 (void);
// 0x000003DA System.Int32[] ZXing.Common.BitMatrix::getEnclosingRectangle()
extern void BitMatrix_getEnclosingRectangle_mF04C4D6C33E3B88E09252DC04260C83ED84F2B16 (void);
// 0x000003DB System.Int32[] ZXing.Common.BitMatrix::getTopLeftOnBit()
extern void BitMatrix_getTopLeftOnBit_mB0BBC334C2D0A28C95C11F3E7FA51CFCDDA88BA4 (void);
// 0x000003DC System.Int32[] ZXing.Common.BitMatrix::getBottomRightOnBit()
extern void BitMatrix_getBottomRightOnBit_m2876C94848272AD26D9DFACB475E55FE9EDB3106 (void);
// 0x000003DD System.Boolean ZXing.Common.BitMatrix::Equals(System.Object)
extern void BitMatrix_Equals_m734D4BD675645EE531E7457588155F9E6A2FCF00 (void);
// 0x000003DE System.Int32 ZXing.Common.BitMatrix::GetHashCode()
extern void BitMatrix_GetHashCode_m30A56747DED375B1AF8E9497F90601A779EA8A91 (void);
// 0x000003DF System.String ZXing.Common.BitMatrix::ToString()
extern void BitMatrix_ToString_m308533C5AC77EB9A2900DE488E24DBF13FC639B4 (void);
// 0x000003E0 System.String ZXing.Common.BitMatrix::ToString(System.String,System.String,System.String)
extern void BitMatrix_ToString_mC0A89CC33AFD0803601102EAFEA5FC16F56E5CE2 (void);
// 0x000003E1 System.String ZXing.Common.BitMatrix::buildToString(System.String,System.String,System.String)
extern void BitMatrix_buildToString_m05D69D38E2971266B9FF6D55ECC199D605B1B86B (void);
// 0x000003E2 System.Object ZXing.Common.BitMatrix::Clone()
extern void BitMatrix_Clone_m7F2489C0DDC17A91137EF32E9A8B5704840911C5 (void);
// 0x000003E3 System.Void ZXing.Common.BitSource::.ctor(System.Byte[])
extern void BitSource__ctor_mD93557F924046DFE4389990A8276351D0FE40FCF (void);
// 0x000003E4 System.Int32 ZXing.Common.BitSource::get_BitOffset()
extern void BitSource_get_BitOffset_m64D9DD8FFDD2C3C103C2A11B029A2FBC0ABD3DFC (void);
// 0x000003E5 System.Int32 ZXing.Common.BitSource::get_ByteOffset()
extern void BitSource_get_ByteOffset_m80607FC238F3C86CA2E85A166C6CC73BC7F405FD (void);
// 0x000003E6 System.Int32 ZXing.Common.BitSource::readBits(System.Int32)
extern void BitSource_readBits_mF96D3807EE0A18870A2B274E4671B6514ACF7B2E (void);
// 0x000003E7 System.Int32 ZXing.Common.BitSource::available()
extern void BitSource_available_m94210E66EF66047858FB5FBB09D814A9256AA530 (void);
// 0x000003E8 System.String ZXing.Common.CharacterSetECI::get_EncodingName()
extern void CharacterSetECI_get_EncodingName_mD295E1546472DE6599C8FE420851951CCFC616E6 (void);
// 0x000003E9 System.Void ZXing.Common.CharacterSetECI::.cctor()
extern void CharacterSetECI__cctor_mB0DD18E66CB48807E5E388524E9365BDC808C0C0 (void);
// 0x000003EA System.Void ZXing.Common.CharacterSetECI::.ctor(System.Int32,System.String)
extern void CharacterSetECI__ctor_m8C4C5075868AF85208289A7592652C5FD5564BCA (void);
// 0x000003EB System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String)
extern void CharacterSetECI_addCharacterSet_m53BB2C61B9EB98229CBB81A05761C349D4C5BA70 (void);
// 0x000003EC System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String[])
extern void CharacterSetECI_addCharacterSet_mC6EFAB88E93A181A9A7BA9461B068E89EF619EF1 (void);
// 0x000003ED ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByValue(System.Int32)
extern void CharacterSetECI_getCharacterSetECIByValue_m06163BD09B9B9B62657249B391E2B918AE8DDBD8 (void);
// 0x000003EE System.Byte[] ZXing.Common.DecoderResult::get_RawBytes()
extern void DecoderResult_get_RawBytes_m43CA9502BF6A26C8D70D0BFDAB51113CC353E9D5 (void);
// 0x000003EF System.Void ZXing.Common.DecoderResult::set_RawBytes(System.Byte[])
extern void DecoderResult_set_RawBytes_m987DFC589F07D732CBB655528686CE23A23A9C53 (void);
// 0x000003F0 System.Int32 ZXing.Common.DecoderResult::get_NumBits()
extern void DecoderResult_get_NumBits_mD3D9C73C3F9DC29C45E2DABB5E96101829E4FD17 (void);
// 0x000003F1 System.Void ZXing.Common.DecoderResult::set_NumBits(System.Int32)
extern void DecoderResult_set_NumBits_mCA6CD50AF39849DF204C1AF96DF35E45C35B1E95 (void);
// 0x000003F2 System.String ZXing.Common.DecoderResult::get_Text()
extern void DecoderResult_get_Text_mC0B1F01D2776183DA406FF6726629B46F905EE7C (void);
// 0x000003F3 System.Void ZXing.Common.DecoderResult::set_Text(System.String)
extern void DecoderResult_set_Text_m378794910F12A57EFE7501C00B5FD5E7D9C8238C (void);
// 0x000003F4 System.Collections.Generic.IList`1<System.Byte[]> ZXing.Common.DecoderResult::get_ByteSegments()
extern void DecoderResult_get_ByteSegments_m32CE56F80C15164051C7C3AF3619C62DEDECF6D8 (void);
// 0x000003F5 System.Void ZXing.Common.DecoderResult::set_ByteSegments(System.Collections.Generic.IList`1<System.Byte[]>)
extern void DecoderResult_set_ByteSegments_m7A822E31AEDAB0567A23BEB7E853148AF97CAC2C (void);
// 0x000003F6 System.String ZXing.Common.DecoderResult::get_ECLevel()
extern void DecoderResult_get_ECLevel_m29BD97206A9E780E851942193D1247119284FBD2 (void);
// 0x000003F7 System.Void ZXing.Common.DecoderResult::set_ECLevel(System.String)
extern void DecoderResult_set_ECLevel_m3FCA51695B6A6294FF8838F64979774A0F6CE971 (void);
// 0x000003F8 System.Boolean ZXing.Common.DecoderResult::get_StructuredAppend()
extern void DecoderResult_get_StructuredAppend_m072F7507B09F85A21148F5F56229DD9F072B1DCA (void);
// 0x000003F9 System.Void ZXing.Common.DecoderResult::set_ErrorsCorrected(System.Int32)
extern void DecoderResult_set_ErrorsCorrected_m124A1F030FEB55F836070E8536E938BF3E4D49F9 (void);
// 0x000003FA System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendSequenceNumber()
extern void DecoderResult_get_StructuredAppendSequenceNumber_m96111256F7098707D35BE947261A1692F22788E0 (void);
// 0x000003FB System.Void ZXing.Common.DecoderResult::set_StructuredAppendSequenceNumber(System.Int32)
extern void DecoderResult_set_StructuredAppendSequenceNumber_m0B2B6C0023B77A8A777FF06BB2EDFE35CDC8880C (void);
// 0x000003FC System.Void ZXing.Common.DecoderResult::set_Erasures(System.Int32)
extern void DecoderResult_set_Erasures_m08B47141BC1467C774335C8BC6E150C263A40E5D (void);
// 0x000003FD System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendParity()
extern void DecoderResult_get_StructuredAppendParity_m66FEE817E38BE7875BA5F0B0344C10401CB429AC (void);
// 0x000003FE System.Void ZXing.Common.DecoderResult::set_StructuredAppendParity(System.Int32)
extern void DecoderResult_set_StructuredAppendParity_mFB96734D5857717C16310E554B3B4BCBC4FD82C0 (void);
// 0x000003FF System.Object ZXing.Common.DecoderResult::get_Other()
extern void DecoderResult_get_Other_mBA4D7C335AAA256D1D0CBE916AF94439125B980D (void);
// 0x00000400 System.Void ZXing.Common.DecoderResult::set_Other(System.Object)
extern void DecoderResult_set_Other_m090AF96413E60D86B7819D00AD305EABC160489A (void);
// 0x00000401 System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String)
extern void DecoderResult__ctor_m3031DEF2DEF284466CD171FE60098AACE48DA15D (void);
// 0x00000402 System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String,System.Int32,System.Int32)
extern void DecoderResult__ctor_mF7A0215EF3F469A3C965E44E02D62ED208FD8550 (void);
// 0x00000403 System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.Int32,System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String)
extern void DecoderResult__ctor_m6D2E3A74B82FABADCB3A540B82498DE40D89416E (void);
// 0x00000404 System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.Int32,System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String,System.Int32,System.Int32)
extern void DecoderResult__ctor_m2485EEE788AD63BE059467EEDCBE83FD081E6663 (void);
// 0x00000405 System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.Common.DecodingOptions::get_Hints()
extern void DecodingOptions_get_Hints_m49D5EDE8C24D2C69569F27E6B07CCE1A35EB578F (void);
// 0x00000406 System.Void ZXing.Common.DecodingOptions::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodingOptions_set_Hints_m7713D3EAF7335F7964F81FF3DBA36D64E4173716 (void);
// 0x00000407 System.Void ZXing.Common.DecodingOptions::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern void DecodingOptions_add_ValueChanged_m473222ED91D5FCEE2C1FFC48C88242408BB24C33 (void);
// 0x00000408 System.Void ZXing.Common.DecodingOptions::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern void DecodingOptions_remove_ValueChanged_mE667F8E7EA37EA3F6410AB0004C53780A5D4806B (void);
// 0x00000409 System.Void ZXing.Common.DecodingOptions::set_TryHarder(System.Boolean)
extern void DecodingOptions_set_TryHarder_m7BE8D074E88BCAD7EB2077AA78FF1D04E4D0B6DC (void);
// 0x0000040A System.Void ZXing.Common.DecodingOptions::set_UseCode39ExtendedMode(System.Boolean)
extern void DecodingOptions_set_UseCode39ExtendedMode_m49CEF8D8B9A01E7C95936517FCE9827FFD9AA4FC (void);
// 0x0000040B System.Void ZXing.Common.DecodingOptions::set_UseCode39RelaxedExtendedMode(System.Boolean)
extern void DecodingOptions_set_UseCode39RelaxedExtendedMode_m5F57554D05EEE1AEE7D4FCC412AF2284B0E75337 (void);
// 0x0000040C System.Void ZXing.Common.DecodingOptions::.ctor()
extern void DecodingOptions__ctor_m61AA5BB8BBC86EC171973CB29A9C1EC9B2A76568 (void);
// 0x0000040D System.Void ZXing.Common.DecodingOptions::<.ctor>b__43_0(System.Object,System.EventArgs)
extern void DecodingOptions_U3C_ctorU3Eb__43_0_m1588EBB00CA9EB20A446A3776CC4393F0CFE4331 (void);
// 0x0000040E System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
// 0x0000040F System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
// 0x00000410 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::.ctor()
// 0x00000411 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::OnValueChanged()
// 0x00000412 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Add(TKey,TValue)
// 0x00000413 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::ContainsKey(TKey)
// 0x00000414 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Remove(TKey)
// 0x00000415 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000416 TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Item(TKey)
// 0x00000417 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::set_Item(TKey,TValue)
// 0x00000418 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000419 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Clear()
// 0x0000041A System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000041B System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x0000041C System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Count()
// 0x0000041D System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_IsReadOnly()
// 0x0000041E System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000041F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::GetEnumerator()
// 0x00000420 System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000421 ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void DefaultGridSampler_sampleGrid_m3181D8DB3C9D314099209CC74F516C20173B8476 (void);
// 0x00000422 ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern void DefaultGridSampler_sampleGrid_mE2DBADDE1C4B49660CE83BADBF75443CD630D925 (void);
// 0x00000423 System.Void ZXing.Common.DefaultGridSampler::.ctor()
extern void DefaultGridSampler__ctor_m4FABA9538E02F9EBD362386B502CE32554143A14 (void);
// 0x00000424 ZXing.Common.BitMatrix ZXing.Common.DetectorResult::get_Bits()
extern void DetectorResult_get_Bits_m848D10FED0407284D761433AA00A4CB7FB442D1A (void);
// 0x00000425 System.Void ZXing.Common.DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern void DetectorResult_set_Bits_mD70104166B615ACA1AD715C218767A4527906945 (void);
// 0x00000426 ZXing.ResultPoint[] ZXing.Common.DetectorResult::get_Points()
extern void DetectorResult_get_Points_m15DF794C156FD71EEAC5113F7974F991368DDA49 (void);
// 0x00000427 System.Void ZXing.Common.DetectorResult::set_Points(ZXing.ResultPoint[])
extern void DetectorResult_set_Points_mF5A0B8D443D25DE03EFE0962F2FAF1C2FA62DA36 (void);
// 0x00000428 System.Void ZXing.Common.DetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[])
extern void DetectorResult__ctor_mAAC90FD700826AABA68D0E3AD23C520588FF4280 (void);
// 0x00000429 System.Void ZXing.Common.ECI::set_Value(System.Int32)
extern void ECI_set_Value_m9F9CA06BCDBAB6747CDEE9A4598D6B9BAF13443E (void);
// 0x0000042A System.Void ZXing.Common.ECI::.ctor(System.Int32)
extern void ECI__ctor_mCB64605A40D27FECF0AC217645B3BE6BDF0A6696 (void);
// 0x0000042B System.Void ZXing.Common.GlobalHistogramBinarizer::.ctor(ZXing.LuminanceSource)
extern void GlobalHistogramBinarizer__ctor_mB5702A85FAE32BE89C05D12348B80BA879A81F02 (void);
// 0x0000042C ZXing.Common.BitArray ZXing.Common.GlobalHistogramBinarizer::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern void GlobalHistogramBinarizer_getBlackRow_m71E0396C1E7ADF19B279BB76AA7155C9B3A62169 (void);
// 0x0000042D ZXing.Common.BitMatrix ZXing.Common.GlobalHistogramBinarizer::get_BlackMatrix()
extern void GlobalHistogramBinarizer_get_BlackMatrix_mA9355137915D5A8208C8A56C426295650F1A204E (void);
// 0x0000042E ZXing.Binarizer ZXing.Common.GlobalHistogramBinarizer::createBinarizer(ZXing.LuminanceSource)
extern void GlobalHistogramBinarizer_createBinarizer_m06B12CAD41720B5793FA46EE0040F3DD9E4CBB1B (void);
// 0x0000042F System.Void ZXing.Common.GlobalHistogramBinarizer::initArrays(System.Int32)
extern void GlobalHistogramBinarizer_initArrays_m40577BCD3306F34736E375A76B7D6FD2661A869E (void);
// 0x00000430 System.Boolean ZXing.Common.GlobalHistogramBinarizer::estimateBlackPoint(System.Int32[],System.Int32&)
extern void GlobalHistogramBinarizer_estimateBlackPoint_mBCCF12D70FB5E832C46E8BCDCB45BB6A0FED224D (void);
// 0x00000431 System.Void ZXing.Common.GlobalHistogramBinarizer::.cctor()
extern void GlobalHistogramBinarizer__cctor_m8D46D8ADA2C6BB2D1FB750CBFBA2670449C93631 (void);
// 0x00000432 ZXing.Common.GridSampler ZXing.Common.GridSampler::get_Instance()
extern void GridSampler_get_Instance_m5C1CDA117F9E4293B0102B5E3BE10632C59C0C23 (void);
// 0x00000433 ZXing.Common.BitMatrix ZXing.Common.GridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
// 0x00000434 ZXing.Common.BitMatrix ZXing.Common.GridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern void GridSampler_sampleGrid_m09FA91280877A1A39321483EAD4490C1D625B610 (void);
// 0x00000435 System.Boolean ZXing.Common.GridSampler::checkAndNudgePoints(ZXing.Common.BitMatrix,System.Single[])
extern void GridSampler_checkAndNudgePoints_m41323B7C2F66036E56C5F99255C228011F1B3D2E (void);
// 0x00000436 System.Void ZXing.Common.GridSampler::.ctor()
extern void GridSampler__ctor_m2601B7518A0DD0A58FAF7C1916934DA7EDB92D35 (void);
// 0x00000437 System.Void ZXing.Common.GridSampler::.cctor()
extern void GridSampler__cctor_m494589B6664717F8D28921B9837F1CCA6116905B (void);
// 0x00000438 ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::get_BlackMatrix()
extern void HybridBinarizer_get_BlackMatrix_mB3E2BCE7287E0043D3BC90DCFE77A21727373AEC (void);
// 0x00000439 System.Void ZXing.Common.HybridBinarizer::.ctor(ZXing.LuminanceSource)
extern void HybridBinarizer__ctor_mAFDF1DB99F02C26A219AFF9F31A442BBD6C1C1B2 (void);
// 0x0000043A ZXing.Binarizer ZXing.Common.HybridBinarizer::createBinarizer(ZXing.LuminanceSource)
extern void HybridBinarizer_createBinarizer_mF8D1D404E27F231169285CE6C0274AFF5D607EE0 (void);
// 0x0000043B System.Void ZXing.Common.HybridBinarizer::binarizeEntireImage()
extern void HybridBinarizer_binarizeEntireImage_mE086540DF37BE9162D97359B187CAE1D3E3DA1B2 (void);
// 0x0000043C System.Void ZXing.Common.HybridBinarizer::calculateThresholdForBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[][],ZXing.Common.BitMatrix)
extern void HybridBinarizer_calculateThresholdForBlock_mDCCB89059065429B74C004A0E7DEF3F58DC77441 (void);
// 0x0000043D System.Int32 ZXing.Common.HybridBinarizer::cap(System.Int32,System.Int32,System.Int32)
extern void HybridBinarizer_cap_m82FBF47590D61793F96735321DBBD86DF5F70517 (void);
// 0x0000043E System.Void ZXing.Common.HybridBinarizer::thresholdBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Common.BitMatrix)
extern void HybridBinarizer_thresholdBlock_m9A07C7FC134B67EEAEEB1C4E59C25B1FFBF4CE8E (void);
// 0x0000043F System.Int32[][] ZXing.Common.HybridBinarizer::calculateBlackPoints(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void HybridBinarizer_calculateBlackPoints_mF93296EB2A3289BCDE22863CAB111C7C736BF0AD (void);
// 0x00000440 System.Void ZXing.Common.PerspectiveTransform::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform__ctor_mC293E2B705E334C9F03786624B868ACFE25F86E3 (void);
// 0x00000441 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_quadrilateralToQuadrilateral_mC151B93BE07401428029CCB89FE900561E40C2C3 (void);
// 0x00000442 System.Void ZXing.Common.PerspectiveTransform::transformPoints(System.Single[])
extern void PerspectiveTransform_transformPoints_mA95BE5824327F70F2864FE894259A2DC13B28122 (void);
// 0x00000443 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::squareToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_squareToQuadrilateral_m3653583B32B2C0E47C7F4C6F03E5C2685FC0E36F (void);
// 0x00000444 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToSquare(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_quadrilateralToSquare_m6AEB402947628D3F755A1DEF6F2DECF1CC682F06 (void);
// 0x00000445 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::buildAdjoint()
extern void PerspectiveTransform_buildAdjoint_m7AE46903FA0CD91E0A6A880FAA4608943304F9B8 (void);
// 0x00000446 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::times(ZXing.Common.PerspectiveTransform)
extern void PerspectiveTransform_times_m6D739E36CFB8FF630FA15DD4410D885E95B96BB2 (void);
// 0x00000447 System.String ZXing.Common.StringUtils::guessEncoding(System.Byte[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void StringUtils_guessEncoding_mDA7EF9DA458E86FFC80EFA6DA7C5A1C7D4D7545D (void);
// 0x00000448 System.Void ZXing.Common.StringUtils::.cctor()
extern void StringUtils__cctor_mC2C0BC331A6E448A65042C34596B4CB85ACE1C43 (void);
// 0x00000449 System.Void ZXing.Common.ReedSolomon.GenericGF::.ctor(System.Int32,System.Int32,System.Int32)
extern void GenericGF__ctor_mE3666ECD617DD1B225713360DFB9824147CA094C (void);
// 0x0000044A ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_Zero()
extern void GenericGF_get_Zero_m4F0693F9A8302834547052C3CEA776FC7741F58D (void);
// 0x0000044B ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_One()
extern void GenericGF_get_One_m945193587CD2278056A6EB80421F64BEBBE275FD (void);
// 0x0000044C ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::buildMonomial(System.Int32,System.Int32)
extern void GenericGF_buildMonomial_m7A21738A2F293ABCB76386FBA0C071DC58DF2E96 (void);
// 0x0000044D System.Int32 ZXing.Common.ReedSolomon.GenericGF::addOrSubtract(System.Int32,System.Int32)
extern void GenericGF_addOrSubtract_m82AF5317D77A4CD3F9D058E5C1A1CBF6B557D921 (void);
// 0x0000044E System.Int32 ZXing.Common.ReedSolomon.GenericGF::exp(System.Int32)
extern void GenericGF_exp_m1CCB3C8792B2D6E7DADE1DF1701C0A1695D31EB8 (void);
// 0x0000044F System.Int32 ZXing.Common.ReedSolomon.GenericGF::log(System.Int32)
extern void GenericGF_log_m46FB0CC7848529FA72608AD004C670A7126FF991 (void);
// 0x00000450 System.Int32 ZXing.Common.ReedSolomon.GenericGF::inverse(System.Int32)
extern void GenericGF_inverse_m39D5FF366CE86657B6ECF3FB1AF07BFE3A3C3BAA (void);
// 0x00000451 System.Int32 ZXing.Common.ReedSolomon.GenericGF::multiply(System.Int32,System.Int32)
extern void GenericGF_multiply_m0F0CC4B3CDC5B8B784E1920FC05CCDD6990E327C (void);
// 0x00000452 System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_Size()
extern void GenericGF_get_Size_m72D4414196C31B8A73A5F114EB3C2509241C7B30 (void);
// 0x00000453 System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_GeneratorBase()
extern void GenericGF_get_GeneratorBase_mEF5BAB60B96C4ACDF59F048361FB31F79D2D8FA7 (void);
// 0x00000454 System.String ZXing.Common.ReedSolomon.GenericGF::ToString()
extern void GenericGF_ToString_mBE0D0E467C849624166EC3CB206BBFB6D1CC2AF0 (void);
// 0x00000455 System.Void ZXing.Common.ReedSolomon.GenericGF::.cctor()
extern void GenericGF__cctor_m0E768E8F28B963346FA6BCEB3938BB686ABBC4BC (void);
// 0x00000456 System.Void ZXing.Common.ReedSolomon.GenericGFPoly::.ctor(ZXing.Common.ReedSolomon.GenericGF,System.Int32[])
extern void GenericGFPoly__ctor_mC1293E5572D4CE31744EECA65323674BD18BCBC2 (void);
// 0x00000457 System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::get_Degree()
extern void GenericGFPoly_get_Degree_m37346473B1702D9AD3523314A39D053B55ADDDB8 (void);
// 0x00000458 System.Boolean ZXing.Common.ReedSolomon.GenericGFPoly::get_isZero()
extern void GenericGFPoly_get_isZero_mBC8A23CF56F0497942B6474544E00562C4B60DA0 (void);
// 0x00000459 System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::getCoefficient(System.Int32)
extern void GenericGFPoly_getCoefficient_m0D0151C87B80141BB1674C20F3C79D5AA6227CAF (void);
// 0x0000045A System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::evaluateAt(System.Int32)
extern void GenericGFPoly_evaluateAt_m4F0A0A8982D580D37FD26009B4F9E501D9CF009D (void);
// 0x0000045B ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::addOrSubtract(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_addOrSubtract_m8A6A0D1C87B7FD0A387C346B7F8555F1B2E96FFB (void);
// 0x0000045C ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_multiply_mDB3E82073EC6032A8C6140178A03C4368865D39D (void);
// 0x0000045D ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(System.Int32)
extern void GenericGFPoly_multiply_m918F138BD54E0FEC2C9AD6B78AAF04E162109B9E (void);
// 0x0000045E ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiplyByMonomial(System.Int32,System.Int32)
extern void GenericGFPoly_multiplyByMonomial_m7932489896A2263AF636AE2A0BA6657A602713F0 (void);
// 0x0000045F System.String ZXing.Common.ReedSolomon.GenericGFPoly::ToString()
extern void GenericGFPoly_ToString_m89CDBE0BB7C0996C08973224B77782854481A10E (void);
// 0x00000460 System.Void ZXing.Common.ReedSolomon.ReedSolomonDecoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern void ReedSolomonDecoder__ctor_m628D323F539C8056532F489744FD7C9DFE30B0B9 (void);
// 0x00000461 System.Boolean ZXing.Common.ReedSolomon.ReedSolomonDecoder::decode(System.Int32[],System.Int32)
extern void ReedSolomonDecoder_decode_m3AB50DCD211EBEF5D95FB2274CE77D2E987F8CAC (void);
// 0x00000462 ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::runEuclideanAlgorithm(ZXing.Common.ReedSolomon.GenericGFPoly,ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32)
extern void ReedSolomonDecoder_runEuclideanAlgorithm_m71BF4612250474C1905B4339485A232402DBEE3E (void);
// 0x00000463 System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorLocations(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void ReedSolomonDecoder_findErrorLocations_m9DD7E6AAFE5AF94FF51C8533BCC705ABA863B0FA (void);
// 0x00000464 System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorMagnitudes(ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32[])
extern void ReedSolomonDecoder_findErrorMagnitudes_m0C22486723C3C4AF540ABBB442F894526D767DFC (void);
// 0x00000465 System.Int32 ZXing.Common.Detector.MathUtils::round(System.Single)
extern void MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60 (void);
// 0x00000466 System.Single ZXing.Common.Detector.MathUtils::distance(System.Single,System.Single,System.Single,System.Single)
extern void MathUtils_distance_mFD733773EEBEB9B2DE497DB56F877DE8241841EF (void);
// 0x00000467 System.Single ZXing.Common.Detector.MathUtils::distance(System.Int32,System.Int32,System.Int32,System.Int32)
extern void MathUtils_distance_mF24D214F6061CEBFFA827EFC7193450F9547E453 (void);
// 0x00000468 System.Int32 ZXing.Common.Detector.MathUtils::sum(System.Int32[])
extern void MathUtils_sum_m77477B82A6899352E9109B0E996D8C70B60E0577 (void);
// 0x00000469 ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix)
extern void WhiteRectangleDetector_Create_mB7E5C4CCD1DD3D79EB0581A45071CE02B0DECFDF (void);
// 0x0000046A ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern void WhiteRectangleDetector_Create_mCF22DCF654C8C8B88AB289C3A1C9C14D58253658 (void);
// 0x0000046B System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix)
extern void WhiteRectangleDetector__ctor_m65D6591012DC7B1053F586C40156AE32937F67DE (void);
// 0x0000046C System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern void WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8 (void);
// 0x0000046D ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::detect()
extern void WhiteRectangleDetector_detect_mC702724840AE187F538791BAB7DFE6DFAFBE9110 (void);
// 0x0000046E ZXing.ResultPoint ZXing.Common.Detector.WhiteRectangleDetector::getBlackPointOnSegment(System.Single,System.Single,System.Single,System.Single)
extern void WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A (void);
// 0x0000046F ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::centerEdges(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void WhiteRectangleDetector_centerEdges_m627FE076E322023CD2E80416A1D2AE41CEBAA6D3 (void);
// 0x00000470 System.Boolean ZXing.Common.Detector.WhiteRectangleDetector::containsBlackPoint(System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482 (void);
// 0x00000471 ZXing.Result ZXing.Aztec.AztecReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void AztecReader_decode_mE88A7D2F05A41C7802D22DFB1CC4E454C12F4DE9 (void);
// 0x00000472 System.Void ZXing.Aztec.AztecReader::reset()
extern void AztecReader_reset_m535FEB982E13CEC319554B602A65045810F084BD (void);
// 0x00000473 System.Void ZXing.Aztec.AztecReader::.ctor()
extern void AztecReader__ctor_m26BF9B090675D5850A44A180BFD74937D7687AC8 (void);
// 0x00000474 System.Void ZXing.Aztec.AztecResultMetadata::set_Compact(System.Boolean)
extern void AztecResultMetadata_set_Compact_m72E8B973DA813D4E082C976D8A0CE6894D134914 (void);
// 0x00000475 System.Void ZXing.Aztec.AztecResultMetadata::set_Datablocks(System.Int32)
extern void AztecResultMetadata_set_Datablocks_m62A367370AA07C9FC6423F8765F9EFA9781A6904 (void);
// 0x00000476 System.Void ZXing.Aztec.AztecResultMetadata::set_Layers(System.Int32)
extern void AztecResultMetadata_set_Layers_mED123A0C8E0BE7D7DF2CE6E16000B903A4191009 (void);
// 0x00000477 System.Void ZXing.Aztec.AztecResultMetadata::.ctor(System.Boolean,System.Int32,System.Int32)
extern void AztecResultMetadata__ctor_m56BE57B664645174E01DC70B4B17A27243CE998A (void);
// 0x00000478 System.Boolean ZXing.Aztec.Internal.AztecDetectorResult::get_Compact()
extern void AztecDetectorResult_get_Compact_mF9786EF4B4AE2046D4EBC1ACE4421CAF0D8D635C (void);
// 0x00000479 System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_Compact(System.Boolean)
extern void AztecDetectorResult_set_Compact_m5746B4844627342B9D01FE94335BBCB3CC2F27D3 (void);
// 0x0000047A System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbDatablocks()
extern void AztecDetectorResult_get_NbDatablocks_m8C8449F959B74EB58C27F13F685D657A6350EC83 (void);
// 0x0000047B System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbDatablocks(System.Int32)
extern void AztecDetectorResult_set_NbDatablocks_mAF024D9BF8B752D63183CB8A9DC176B3C2231D4C (void);
// 0x0000047C System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbLayers()
extern void AztecDetectorResult_get_NbLayers_m1F624B4B6E994D0DF191D19CAD9285938D14D648 (void);
// 0x0000047D System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbLayers(System.Int32)
extern void AztecDetectorResult_set_NbLayers_m724AC660E7B21D40509ECFC080450022B738C1A8 (void);
// 0x0000047E System.Void ZXing.Aztec.Internal.AztecDetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[],System.Boolean,System.Int32,System.Int32)
extern void AztecDetectorResult__ctor_mC4CD35739E1C542CB29CF62E4EA1FAB5229048DE (void);
// 0x0000047F ZXing.Common.DecoderResult ZXing.Aztec.Internal.Decoder::decode(ZXing.Aztec.Internal.AztecDetectorResult)
extern void Decoder_decode_m7EAE3A74A31B4FB8A18460F548875740B09C9162 (void);
// 0x00000480 System.String ZXing.Aztec.Internal.Decoder::getEncodedData(System.Boolean[])
extern void Decoder_getEncodedData_m30D10AF659536860A7F98FDA49C75FDD3273E777 (void);
// 0x00000481 ZXing.Aztec.Internal.Decoder/Table ZXing.Aztec.Internal.Decoder::getTable(System.Char)
extern void Decoder_getTable_mEF9C9DE887498B67989212F9DC2E172AE8225859 (void);
// 0x00000482 System.String ZXing.Aztec.Internal.Decoder::getCharacter(System.String[],System.Int32)
extern void Decoder_getCharacter_m91E43E10ACE8D523FDA429AE72A6AAFFE9D51D33 (void);
// 0x00000483 System.Boolean[] ZXing.Aztec.Internal.Decoder::correctBits(System.Boolean[])
extern void Decoder_correctBits_m439E671C3921055BFAB1996E721A1A6CB846F951 (void);
// 0x00000484 System.Boolean[] ZXing.Aztec.Internal.Decoder::extractBits(ZXing.Common.BitMatrix)
extern void Decoder_extractBits_m1BCBB3F1E1E7AC5F5EE4A156B2A527BE45069889 (void);
// 0x00000485 System.Int32 ZXing.Aztec.Internal.Decoder::readCode(System.Boolean[],System.Int32,System.Int32)
extern void Decoder_readCode_m44E46F2EE3196F637AF94D310D83332BCB0B3E20 (void);
// 0x00000486 System.Byte ZXing.Aztec.Internal.Decoder::readByte(System.Boolean[],System.Int32)
extern void Decoder_readByte_mFAF8940288212954D760D74A13E3226DC2B4630F (void);
// 0x00000487 System.Byte[] ZXing.Aztec.Internal.Decoder::convertBoolArrayToByteArray(System.Boolean[])
extern void Decoder_convertBoolArrayToByteArray_mC2F4F6B5D14D6859A024980E1FA0FC3668C9BB03 (void);
// 0x00000488 System.Int32 ZXing.Aztec.Internal.Decoder::totalBitsInLayer(System.Int32,System.Boolean)
extern void Decoder_totalBitsInLayer_m32B329C489599BA59D5EBB61DF079D3BE896F7F2 (void);
// 0x00000489 System.Void ZXing.Aztec.Internal.Decoder::.ctor()
extern void Decoder__ctor_m2FE2AD2398737333864D6E06BFB1653467423B5A (void);
// 0x0000048A System.Void ZXing.Aztec.Internal.Decoder::.cctor()
extern void Decoder__cctor_m445CDF8DC20BBFED4F21786E10B39E7862354F2B (void);
// 0x0000048B System.Void ZXing.Aztec.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_mCEC1BEFF4B3F80325F93D1D4951A5DEBAECA16CC (void);
// 0x0000048C ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Detector::detect(System.Boolean)
extern void Detector_detect_m5EEF5A2093EEF14E34E8846806F614DA6D347D74 (void);
// 0x0000048D System.Boolean ZXing.Aztec.Internal.Detector::extractParameters(ZXing.ResultPoint[])
extern void Detector_extractParameters_m0D152B13497848979C3ACCAA9831C1944165A650 (void);
// 0x0000048E System.Int32 ZXing.Aztec.Internal.Detector::getRotation(System.Int32[],System.Int32)
extern void Detector_getRotation_m72EF19FD4CF2B667853C1F50D75DFFB2E0E5244B (void);
// 0x0000048F System.Int32 ZXing.Aztec.Internal.Detector::getCorrectedParameterData(System.Int64,System.Boolean)
extern void Detector_getCorrectedParameterData_m9FF9B8F0A45E8189065391FB7A9FE3AFD1D2934E (void);
// 0x00000490 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getBullsEyeCorners(ZXing.Aztec.Internal.Detector/Point)
extern void Detector_getBullsEyeCorners_mCCA9BC4A3AFCEB1213A476603DE28AA3637C484F (void);
// 0x00000491 ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getMatrixCenter()
extern void Detector_getMatrixCenter_m9BD358D27ACD756F448F7143369A29D8B632A169 (void);
// 0x00000492 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getMatrixCornerPoints(ZXing.ResultPoint[])
extern void Detector_getMatrixCornerPoints_m50C751791C62DB43B3611A222654200CDCEEF114 (void);
// 0x00000493 ZXing.Common.BitMatrix ZXing.Aztec.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_sampleGrid_mADFBCDE2C754101B8BA8C09C744A3B73E78465C6 (void);
// 0x00000494 System.Int32 ZXing.Aztec.Internal.Detector::sampleLine(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_sampleLine_m22A8D1777A1358B21E488B5C4B3048BAB4D72764 (void);
// 0x00000495 System.Boolean ZXing.Aztec.Internal.Detector::isWhiteOrBlackRectangle(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_isWhiteOrBlackRectangle_mC5611EF60C2557157120DB4CB6756D08B8BCA2A7 (void);
// 0x00000496 System.Int32 ZXing.Aztec.Internal.Detector::getColor(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_getColor_mA5EC098FF25118981FA166662CA51D8E53A852D6 (void);
// 0x00000497 ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getFirstDifferent(ZXing.Aztec.Internal.Detector/Point,System.Boolean,System.Int32,System.Int32)
extern void Detector_getFirstDifferent_m6E6550936843B87C7D4770AB70914CCEA3809C11 (void);
// 0x00000498 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::expandSquare(ZXing.ResultPoint[],System.Single,System.Single)
extern void Detector_expandSquare_m35EA098FFC343ACA5D5D8B6658A29B76F516EB2A (void);
// 0x00000499 System.Boolean ZXing.Aztec.Internal.Detector::isValid(System.Int32,System.Int32)
extern void Detector_isValid_m7174FD4F2FB8A6FF1079F4C0BB85C7D949A1BDA1 (void);
// 0x0000049A System.Boolean ZXing.Aztec.Internal.Detector::isValid(ZXing.ResultPoint)
extern void Detector_isValid_m6C9B3B35BA229984BCF86EEB0FA26E0A6729533C (void);
// 0x0000049B System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_distance_mBA1DB831E98AF1940A78943FEB6F41F07B25988D (void);
// 0x0000049C System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_distance_mA402066385A4DC576CD7851540B746414C1E25AE (void);
// 0x0000049D System.Int32 ZXing.Aztec.Internal.Detector::getDimension()
extern void Detector_getDimension_m49D17F0C4D700146BFB16FAC75F43BC5D89C27B2 (void);
// 0x0000049E System.Void ZXing.Aztec.Internal.Detector::.cctor()
extern void Detector__cctor_mB3D360BA3FC96707A4CCB1FBBDB274FF4F22F71A (void);
// 0x0000049F System.Int32 ZXing.Aztec.Internal.Detector/Point::get_X()
extern void Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660 (void);
// 0x000004A0 System.Void ZXing.Aztec.Internal.Detector/Point::set_X(System.Int32)
extern void Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408 (void);
// 0x000004A1 System.Int32 ZXing.Aztec.Internal.Detector/Point::get_Y()
extern void Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605 (void);
// 0x000004A2 System.Void ZXing.Aztec.Internal.Detector/Point::set_Y(System.Int32)
extern void Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5 (void);
// 0x000004A3 ZXing.ResultPoint ZXing.Aztec.Internal.Detector/Point::toResultPoint()
extern void Point_toResultPoint_m01A8B2007545A98090BD3DC9892954E6F0DD5E7E (void);
// 0x000004A4 System.Void ZXing.Aztec.Internal.Detector/Point::.ctor(System.Int32,System.Int32)
extern void Point__ctor_m3D624C53D3AE1A834D9F70F586FB0F4C025E3974 (void);
// 0x000004A5 System.String ZXing.Aztec.Internal.Detector/Point::ToString()
extern void Point_ToString_m8D03498487474567D310763DA959A40D675373B7 (void);
static Il2CppMethodPointer s_methodPointers[1189] = 
{
	Base10BigInteger_set_NumberSign_m318126DE9F52340DD6DE456501EC94FF35210B22,
	Base10BigInteger__ctor_m1EACDEE3EB7FE4EED9A62A61967D37F95C8BF61E,
	Base10BigInteger__ctor_m5116F59B394162C5E93BC7025F74338A1225F407,
	Base10BigInteger__ctor_m1F541EB5E39C1D1EBE5D0361886C3B8417B6DE1C,
	Base10BigInteger_Equals_mA0A8A0A9E01514A9DA511C8666564E4BCCCD1A0F,
	Base10BigInteger_Equals_mF3183754DA871F82B5D9883223C8EE0C6D5B1622,
	Base10BigInteger_GetHashCode_m677E9663A6EEFB10FD85331936580F44103BC42C,
	Base10BigInteger_ToString_mE2763B323015C0BEB3C902555D0439F8B1F3AB59,
	Base10BigInteger_Opposite_mF9A574FA9845236F48B4180C347D150E0AA88C8F,
	Base10BigInteger_Greater_mD738D8DA810F8AF38CD73B444B162B944DAA28F4,
	Base10BigInteger_GreaterOrEqual_mAF734A4EC6D5EF155A9A74EF523EA12053C52E3B,
	Base10BigInteger_SmallerOrEqual_m8451318F61131699607B4E3DA1C13861132EB07C,
	Base10BigInteger_Abs_m13DDD583C13BCD6612B0EE81181A9C6A59E7B8B9,
	Base10BigInteger_Addition_mAA0A8F747456B77BA1039B4DF0ED989E14292B04,
	Base10BigInteger_Multiplication_mC88410FE58D7DC2AA935C2207513038A72DC915F,
	Base10BigInteger_op_Implicit_m0B166017A7F4032C0336666AD605F928670CA5C1,
	Base10BigInteger_op_Equality_m0A2846B4B495CC070850A807A89ABF5F3827604B,
	Base10BigInteger_op_Inequality_m25348FCB953E3068635090D912AD03636B8296A7,
	Base10BigInteger_op_GreaterThanOrEqual_mC0DCBC9AC4F6AD86CF7B31CE1B2D80844918B351,
	Base10BigInteger_op_LessThanOrEqual_m272737DC7C9E5B3C631011D8315844E02D221A68,
	Base10BigInteger_op_UnaryNegation_mF3A4F0A82F8D2343019384B6DDF01CAC4E279E1A,
	Base10BigInteger_op_Addition_m7835CAB2D25921D21E136C0ACED7FEF3072812D9,
	Base10BigInteger_op_Multiply_m6A5D377E80138660C947E4E744EFCBD3E5F3615B,
	Base10BigInteger_Add_m43244C7158D9CA9D19521639D9AD77FE9AB8D532,
	Base10BigInteger_Subtract_m2444C6C00B26D2BFAADAB65E7652733F741C9DCE,
	Base10BigInteger_Multiply_m1711C6FE920A26D1A185EDDB21734BE3570276BA,
	Base10BigInteger__cctor_mCC0EB61B92F3A8EA71C241E0DFCCF3A7D137035C,
	DigitContainer__ctor_m5593EDFE34483051D6AC430CDF0EB4778B0BD712,
	DigitContainer_get_Item_m25983F3EE4061CD2280C4B1B02281E04AD9728AC,
	DigitContainer_set_Item_mD0206461D4866D3E4A36C686CFC3E07AD8343137,
	BigInteger__ctor_mE585575A61DFBB95EE746E71A63DF3D2B9BECB0F,
	BigInteger__ctor_m1D57159256C1D12339C8750BBAA75A0416B68890,
	BigInteger__ctor_m49A47699C6B5C617A47E09096BB5D0D8D115F399,
	BigInteger_Equals_m381F80F7197D4EB806F654B85B7EBACFD8757080,
	BigInteger_Equals_mBA2F863D8E14E8F5A9AF4E444A2DE92BA444FC6B,
	BigInteger_GetHashCode_mBACE4F6815D6FD001F6B703BD012EA7E4022BB56,
	BigInteger_ToString_mD74459FFA1B41C651A4FE687BDDFB60E5B88F8C7,
	BigInteger_CompareTo_m093FA3B527B18E0293BA132C5C411323F562EE5A,
	BigInteger_CompareTo_mEAFF995FAC40B030E4A68B3C01A5951039D5FF02,
	BigInteger_Opposite_mC4439D12CC2C94755C39A47285D00A3745D3C4E0,
	BigInteger_Greater_mA0FBB2DC14AC997F3B645CB1CF7EDCD1F6B9C54D,
	BigInteger_GreaterOrEqual_m4F8B533BAD98285C0ED60BFC71DA14133D4262CA,
	BigInteger_Smaller_m6676EB8CACD9E43E7B9CC6246BC5CDD5E71A6731,
	BigInteger_SmallerOrEqual_m8ED8D8A2356849FF4CFB830781AA8A4B48FCC894,
	BigInteger_Abs_m9811944203BD90CD88DC9040729FA86F062D16DC,
	BigInteger_Addition_m7B30E44292C57CEAF2BC94E5B9ED4725072182C5,
	BigInteger_Subtraction_m83F5F4840253C3EE5DEED372A7311325812DCE73,
	BigInteger_Multiplication_mF2EB37ABD84AE7D3C79B584DC822C85791E1D8CB,
	BigInteger_Division_m2453413637C035B66C36A130D54131DBCC76150F,
	BigInteger_Modulo_m68F951782F89D9FE17791447837B2E43E26F1869,
	BigInteger_op_Implicit_m354439251A263BFF4AF79A6823AAC58342CB7A09,
	BigInteger_op_Implicit_m997AF7B456DDA0CA1B0C53AD70AC8497FD99F807,
	BigInteger_op_Explicit_m45B18F87798C8AF3B8C9991ED3B60A2E082461C1,
	BigInteger_op_Explicit_m5E9F820D69C3058EFAFC54C79BEDF8688D2EADFE,
	BigInteger_op_Equality_m82BD5FC8C50E191A5473B61B304370C57F39F948,
	BigInteger_op_Inequality_m8777CAB30B7852430D8BA019E0E351F8216C8D32,
	BigInteger_op_GreaterThan_m614A102E33649450C7C755ED620630A49A7963CB,
	BigInteger_op_LessThan_mBBD0A175B1A0CE5B88908563FCC011E4C1C244B2,
	BigInteger_op_GreaterThanOrEqual_mE2967F43D21ADDB0626C24475B0EE1FE1F630BA8,
	BigInteger_op_LessThanOrEqual_m84C0E39990D6F9EB1AD04C9B0B00B28631F88EE6,
	BigInteger_op_UnaryNegation_mA43B696B3AFF4E8313A0DBC28D1C212AFF258EFD,
	BigInteger_op_Addition_m8E34B4E094DBC7C8C491F11EF9D36823A07F43CF,
	BigInteger_op_Subtraction_m16A9075CD7BC7DDEA0B8DB49AE537B8A411D2D3A,
	BigInteger_op_Multiply_m0D548C1654E3B10385F6A1BDF2CB4F2A5E9F623A,
	BigInteger_op_Division_mF2F4079FAF5FB32959B2B518ABC004D2169E1571,
	BigInteger_op_Modulus_m36DCE0EAD5AB54B5B75ECE9BF4F7CCAA5106C69A,
	BigInteger_Add_m0F55BF603F40FCD911D02A55E88F02762E1BC0D7,
	BigInteger_Subtract_m0567F5539BB2EAB395191096087B5C02CE229AB0,
	BigInteger_Multiply_m866DE3938B29F970041CFFB62569B8AB9F6F5A1F,
	BigInteger_DivideByOneDigitNumber_mD4BE7852E0B181E33295918FDAB75ADF8ABFC1F6,
	BigInteger_DivideByBigNumber_mC1DC468DBFAC09E7E1BCEA951FAB6778D780B43E,
	BigInteger_DivideByBigNumberSmaller_mDA020A4757FC5FDCD14631F344F6F4087E97C38A,
	BigInteger_Difference_mF4C14ABF636DC4401238FECAE1A9F1B23008B535,
	BigInteger_Trial_m36DB99C930B0AE7933BFB782EB7028DB345C8622,
	BigInteger__cctor_mA0A0D687BAA037C95CA5474D1E27EB441A3E288E,
	DigitContainer__ctor_m173ECCA1CA08D537D379FE12A8A87C6A8211ADE9,
	DigitContainer_get_Item_m9AC7D74C5F872C9ECD3C20A864B51E4DDE03302F,
	DigitContainer_set_Item_m19E1AB570C478C138EAD30A79AFAA8D5DDC2C86C,
	BigIntegerException__ctor_m91613E6288DAD4D5C7ED02CC69740D5D3E74979A,
	BarcodeReader__ctor_m114048B1A41B44644752C421FC901A649F6B940B,
	BarcodeReader__ctor_m5214CF12EAAE38C3394F2E16FB822AC94C93A844,
	BarcodeReader__ctor_m0928B1ADFD9BB574EB50B94179DF79BE72328D16,
	BarcodeReader_get_CreateLuminanceSource_mE01B7DE98E22F02E3EA02E84D4975B27BC185EB0,
	BarcodeReader_Decode_m501C41B855C0A6EE102A22D992AECB963EE53D5B,
	BarcodeReader__cctor_m080759F6071EFB12C9C05883243C647B2C0D093E,
	U3CU3Ec__cctor_mECF73754D3E659397FBAC0C5B1ED9ADC8D5F5B91,
	U3CU3Ec__ctor_mC582116316DB1F8235AB27A5BE58C6B17EA9F24D,
	U3CU3Ec_U3C_cctorU3Eb__9_0_m3A4863AE47CD642B2E628E5ACC01C1E566FF9949,
	BarcodeReaderGeneric_get_Options_mEE13C157F091AF9C1CDEDD600045018AE9CC106E,
	BarcodeReaderGeneric_get_Reader_mC6BC0F8670FC382906E90EC1FFCBB672F169AA03,
	BarcodeReaderGeneric_get_AutoRotate_mA7031123B8C71BC186B614012ED67D719F5BF2B9,
	BarcodeReaderGeneric_set_AutoRotate_mA3F321343B91733403B13B78DB93FA7B5C09EFEE,
	BarcodeReaderGeneric_get_TryInverted_m7CEAFFF9350F50E5EDA598D6CABE2129ECF32AEA,
	BarcodeReaderGeneric_set_TryInverted_m9094235165E632A4E8F4819B8149057B3B53CD5F,
	BarcodeReaderGeneric_get_CreateBinarizer_m39A8C1DD65ACAB46BAC55615B6CD68C497230F4F,
	BarcodeReaderGeneric__ctor_m19DAD94E50637D482E6C9E9DCCFB21B3469BC4E0,
	BarcodeReaderGeneric_Decode_m2AA234799697ECB3D2726038BC8AD8835CA0E280,
	BarcodeReaderGeneric_OnResultFound_mB4AD7EEEA57A464BDEC33E15017779BEAEDD0465,
	BarcodeReaderGeneric__cctor_m0966AB37FB7169FA63D13DF7731451EE0CBFF941,
	BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m5F25CC854255C19BB7387C999AFF6F72224D9641,
	U3CU3Ec__cctor_m12E9FA4BF9DE2A8179EEC86554CE1050DAE4D962,
	U3CU3Ec__ctor_m0EAA14E42607DB22B6B1DBA9E1EEBDD21948DB20,
	U3CU3Ec_U3C_cctorU3Eb__40_0_mEED660C1CC3A9D0ECB0F0AA6B49C72FB2A0A6154,
	U3CU3Ec_U3C_cctorU3Eb__40_1_m68137642FE973A8B9853D6F468F4B46D3FE03F3B,
	BaseLuminanceSource__ctor_m3C977BDA7616746CB83FD6F24897D9D2A3AA2995,
	BaseLuminanceSource_getRow_m39278E1D3319FB64B6949D137FC5CE683A03A306,
	BaseLuminanceSource_get_Matrix_mDB62ED37630CAE1A900EEDFD572B3B641221200C,
	BaseLuminanceSource_rotateCounterClockwise_mA08C035544611E16F47C4B5FE90F61870FFB64F7,
	BaseLuminanceSource_get_RotateSupported_m1DFC1C677FABBAB1F5B6F6AC20D02924ECA73559,
	BaseLuminanceSource_get_InversionSupported_mA66F674120F721E2629C754E09EE849B3C4BBB8F,
	BaseLuminanceSource_invert_mAF6BBA3BCBAE3DEEFB1C6050C3F68C433BAB887D,
	NULL,
	Binarizer__ctor_m0F52AE2DBB19E1285BAFA1FA87A49B829DEE80A2,
	Binarizer_get_LuminanceSource_m96D1FE729022B03C18348C2169311D2F37B7D4CF,
	NULL,
	NULL,
	NULL,
	Binarizer_get_Width_m67E9FE03694646DB8202EA3B1BC4CCFA4BE04B73,
	Binarizer_get_Height_mC1BF6C86B03A8093999E316AB4D8B18C935CE433,
	BinaryBitmap__ctor_m88F06DDDBE0AECE462A72A417A16F7A27675D81D,
	BinaryBitmap_get_Width_m2A94EDDCD504C85C062E9C2E07983DBBE9C017D4,
	BinaryBitmap_get_Height_mF40A3E716EA43DE8D0EF07624B600DF02075C264,
	BinaryBitmap_getBlackRow_m77B2DEE934886812C456E4ACB6E11290EB11D6D4,
	BinaryBitmap_get_BlackMatrix_mE039C6FA6553C4DF01CEBC1F41B246478874A371,
	BinaryBitmap_get_RotateSupported_m311B6F02BD37CD2DEFB5BD0BE3536A5079B39810,
	BinaryBitmap_rotateCounterClockwise_mA0E65FCAD3227B4D14C8C3D99524846E3CA90034,
	BinaryBitmap_ToString_mD1D0EA7561E739ACEF307E158E7F39DE21E6AFF3,
	FormatException__ctor_m647F5EE083E5A609EAECBE9FEBC4227FD4354617,
	FormatException__ctor_m1A8EAA0C4127CB67D646E9CF8074C5441D706316,
	InvertedLuminanceSource__ctor_m22EAC127CBFBC15128C37085F9243F50E3656641,
	InvertedLuminanceSource_getRow_m1B3E682D322C8B6C0CF0367029D0F68C8690DC96,
	InvertedLuminanceSource_get_Matrix_m77A11D0929DD94654C64F3063EF3146C2966D023,
	InvertedLuminanceSource_get_RotateSupported_m31B260FB0294A5BE543F171C059210B4E755031C,
	InvertedLuminanceSource_invert_m6CC3E636C0FC2982AE55193C5A92900096B7F860,
	InvertedLuminanceSource_rotateCounterClockwise_m5E97F77ED08238B267321AA53793256357172081,
	LuminanceSource__ctor_m04CAB451BD316FE4955459E1951E42A8A5592250,
	NULL,
	NULL,
	LuminanceSource_get_Width_m329DEE06AE3A28E458AB338395EAC9C82A1445BB,
	LuminanceSource_get_Height_m7E254CE85D5991BB54E93F97F9952932C08D5F41,
	LuminanceSource_get_RotateSupported_mA29BC600777086CBC16872FA42F4AD98F2536174,
	LuminanceSource_rotateCounterClockwise_mAF9DB3D552B78C9F15DBB6C1CBF67C64DB56954B,
	LuminanceSource_get_InversionSupported_m65DABD8C907996CCCA249F98ACB648D62F03FDCF,
	LuminanceSource_invert_m4FF1F17ECB48E4C17CBDAF77BC64CD93D3186025,
	LuminanceSource_ToString_m32BD816245B5553972092A0BF8211A6D9D586DE7,
	MultiFormatReader_decode_mC245093E036662C897AE475475B55A056B2D54B5,
	MultiFormatReader_decodeWithState_mBA73F14533546926E09AD93E8E4513DD8884634C,
	MultiFormatReader_set_Hints_m871E32EB5212D66C01509F84D27C03E5FAF07D08,
	MultiFormatReader_reset_m2D2ABEBC56020D7F866DCACA9735C9EF54EB1888,
	MultiFormatReader_decodeInternal_mDDB723455EB58FA2585EE80CEDCC4FAE57DF6161,
	MultiFormatReader__ctor_m4CAEE21C0DD38926FE801C39E4223BB5666BCFAE,
	NULL,
	NULL,
	ReaderException__ctor_m24690C0D9D85BCCB26ED6D33C2452AD4BF66D2B7,
	ReaderException__ctor_mAE3743148B371202AFBE5115E3B0C868A534DC58,
	Result_get_Text_m855527BB9C7057F702BDABD42E7DECC1572721EB,
	Result_set_Text_mCE77595E7629B6DC028B96FC6B1001D3553B4827,
	Result_get_RawBytes_m37EADC349C16DD0F5A01A9153DFFBAE55BC6D708,
	Result_set_RawBytes_mAE2D1DC82194151BE42AB1B9D3A9420880111142,
	Result_get_ResultPoints_mB6717EF744141D983630A17EDACB39C11BE452FC,
	Result_set_ResultPoints_mDC64CD4147E895472D1C2417141086D9F8EC15E2,
	Result_get_BarcodeFormat_m446306E9DC37F14EB447E0C711A55A45D5514AA7,
	Result_set_BarcodeFormat_m66740266B609E88E7C1A8496F4369AA9B17F3733,
	Result_get_ResultMetadata_m82489B9ED570D7F50058B680DCEF0D126448129C,
	Result_set_ResultMetadata_mD48ACD9D2F3A263E6E705391CDCA38E9B2958CA4,
	Result_set_Timestamp_m9399B76A449D904D5773C5463656BE35040F70D8,
	Result_set_NumBits_m0EB6FB28263F1E58FB521CB0E151B21A344D74DB,
	Result__ctor_m2E5656F74156AA6A52C41278F52EB2D9A99053B8,
	Result__ctor_mE029B6841F8B010A41DAD6A7D84EEAF5B43387C7,
	Result__ctor_m64289D6F7B1CB69C95F8413B68EE440FA8ADE851,
	Result_putMetadata_mA112D7FE7ED706B541956298BF9F0C9820655980,
	Result_putAllMetadata_mD4A8E3866D1F651A37C0DF12CC87110224AA1FF6,
	Result_addResultPoints_m8D98B36BACD22D5F930B24C2007F93083822FD66,
	Result_ToString_m96C5B816773242D4D1423ABBB7DC4F24D57386F6,
	ResultPoint__ctor_m131F729E2BC39087286E98129EED8B27FF03F765,
	ResultPoint_get_X_m5B20F6C7C7F0BC0D44AC0C33CFE70CA65832167D,
	ResultPoint_get_Y_m261B83F542DE7D81B20CB743E630090963D322D2,
	ResultPoint_Equals_m6CE5C8EC1E965B3F620BDF0B986B06CE4E4E55A2,
	ResultPoint_GetHashCode_m880A744140B803242B7E4208E5B7CFF58DE75241,
	ResultPoint_ToString_mAD26D2C86C2F4EA7E2D6460D7920B718B5A56812,
	ResultPoint_orderBestPatterns_mE5F97B8CC95CD483C6473EF47BF778449D82F0EE,
	ResultPoint_distance_mA230ADB22766B14962659AC7C60BFA148E7C58F7,
	ResultPoint_crossProductZ_mFC376C0601939F03EB399524E60E40B1268A5A7D,
	ResultPointCallback__ctor_m83C444A8F9C641C1BF6224C349BB14A280B85B46,
	ResultPointCallback_Invoke_m70AE55890223412EFFEFA10329D3873820BE81D7,
	ResultPointCallback_BeginInvoke_m10230E7037C140B1B64B0D014C920868D7308153,
	ResultPointCallback_EndInvoke_m7B03D332DC677EBD3478CF875EBB45D24911133E,
	RGBLuminanceSource__ctor_m5C386B71A0E7B462A3A47DD13C41A922913C8B71,
	RGBLuminanceSource__ctor_m3521EC26C240CFA8FA8AF04A3AC09B97ABDB6CB3,
	RGBLuminanceSource_CreateLuminanceSource_mCA8AB621F0FE47E380F0DFC0BFCD44456E9C2804,
	RGBLuminanceSource_DetermineBitmapFormat_mB815B701CD9F38EEACD36DD79111DF0C09AD0B13,
	RGBLuminanceSource_CalculateLuminance_m55FA65667B7EA9296D2E47068DF8EB9CE21B70A9,
	RGBLuminanceSource_CalculateLuminanceRGB565_m2430B7C22B8A3515EC659CA247DEB0E2669025E5,
	RGBLuminanceSource_CalculateLuminanceRGB24_mBDF513DD2E6823159600CDCF3AC657A6C0515D00,
	RGBLuminanceSource_CalculateLuminanceBGR24_m621BA127E825418417A3762C5D59D2EBC72A7ABD,
	RGBLuminanceSource_CalculateLuminanceRGB32_m103C2A4B5F096B56ECC4AFCA30B06121A53C8AF5,
	RGBLuminanceSource_CalculateLuminanceBGR32_mC0FE59A71EA79BF88A6AC9F90E62A5CAD7661AAF,
	RGBLuminanceSource_CalculateLuminanceBGRA32_m7627AC1D7D91F8C6FBC56EC8DAAE275F7CF1FD9C,
	RGBLuminanceSource_CalculateLuminanceRGBA32_m49E384F6BA76FC2D067CDF9E34B77583AA6C7B4E,
	RGBLuminanceSource_CalculateLuminanceARGB32_mA22CFFA0B209F2749BE02338CB67AFD45413A1D3,
	RGBLuminanceSource_CalculateLuminanceUYVY_m6F0893D78E61921B29640B7724607FAF07772232,
	RGBLuminanceSource_CalculateLuminanceYUYV_m1AEC33F069050D448D2D68BFF2F41E43883CBC92,
	RGBLuminanceSource_CalculateLuminanceGray16_m9B8868DDBCE9C24972D25DE9922B2A3C3ADE310A,
	NULL,
	NULL,
	SupportClass_bitCount_m3C9E2E53F2C5AD771F17EDFD0E0E383F1DA72D9D,
	NULL,
	Color32LuminanceSource__ctor_m1FFB91A7BCE26C80BF887FD86EEB4F9F410B1A1C,
	Color32LuminanceSource__ctor_m01FAF95649C75BFDB6BFB71471F2C46E256E5677,
	Color32LuminanceSource_SetPixels_m9BCC155B50914AA2DD6A19F3DDEFF89101FE21E4,
	Color32LuminanceSource_CreateLuminanceSource_m0B591ED16CF4AC52667E888F44FB53DEED789043,
	QRCodeReader_decode_mE6AAD8A5595C3052903C397935D9972B8DEC55C4,
	QRCodeReader_reset_m6A716A221BA9E7260E7A260CAF569B369876DE4B,
	QRCodeReader_extractPureBits_mDD6DA398EB59E894B6086D458FEBA2982764CA7E,
	QRCodeReader_moduleSize_m184DA683EA13B7E7537F596E1D5CBF941E2A7D65,
	QRCodeReader__ctor_m8ED215084135AA81D2674D6C3C7315D534A21EEE,
	QRCodeReader__cctor_m2C6853D3F01D8702770EE09D6221720F91AE7824,
	BitMatrixParser_createBitMatrixParser_m4BD4DBD284167504535F88B8EEB40A28BEB3AB27,
	BitMatrixParser__ctor_m488B886FDE1F58840C88B743269CD00D0EA241D0,
	BitMatrixParser_readFormatInformation_m293370F149F582DC92BC08C9468B151114B8003B,
	BitMatrixParser_readVersion_m66CB3B68B791E7AB65E5555449D050E98CB6160C,
	BitMatrixParser_copyBit_m4666E901BBC1CD2021A26B1BECA67A93198B7A26,
	BitMatrixParser_readCodewords_mC91E8CA4FD5EFAB072C3BE7E63ED1D297434A866,
	BitMatrixParser_remask_m67CAA2E1A31FA3BEF1FF9FA826F06439B1D3DFE6,
	BitMatrixParser_setMirror_m7C1DF63BEF6CFB0F2439432163D2D6163E7D89A4,
	BitMatrixParser_mirror_m9C9AF9F0EDD3B7FF6BC20975676BE6B4955C9C64,
	DataBlock__ctor_mD28EB5FD282FB58109B1BA57DDE9A82EDA8995E1,
	DataBlock_getDataBlocks_m0D5783F85BE7E49F8F4CFE3A46E522C0055E4A86,
	DataBlock_get_NumDataCodewords_mEE8E4881D502BC9C28DDD16073038D94A4A15530,
	DataBlock_get_Codewords_mD329189BB8471B482C4710F3E42687B421832EFC,
	DataMask_unmaskBitMatrix_m58082B0C86D433F96B76CF89D85B2123327581AF,
	DataMask__cctor_m627DDE9E6011A96521E6FBDB2F0B5CB23FD38262,
	U3CU3Ec__cctor_m5C99D6D8DF32E075FFEFEA62D5D8BC0B7794FB0F,
	U3CU3Ec__ctor_m8EBA2F4F5936544E00D4494EBF428E326B8614DC,
	U3CU3Ec_U3C_cctorU3Eb__2_0_m9276BBEA1A2866654015CF183BA1EA34634C2C53,
	U3CU3Ec_U3C_cctorU3Eb__2_1_mC6827C7C353AFD1A41149FB722EC788073198819,
	U3CU3Ec_U3C_cctorU3Eb__2_2_mF88732B03F67C94E33EF8EC66053FF16275BE019,
	U3CU3Ec_U3C_cctorU3Eb__2_3_m518AF36E4A955D6AC2AC63AD6C62148136489428,
	U3CU3Ec_U3C_cctorU3Eb__2_4_mDE0A722B0F44EC9EA4960179BB791ED07E9344C0,
	U3CU3Ec_U3C_cctorU3Eb__2_5_m4B2C1B6AD542F0CD515AE18E5BDFB5355566C7EF,
	U3CU3Ec_U3C_cctorU3Eb__2_6_mCD855E2C5E74C424E0E278B509172854DA829CB1,
	U3CU3Ec_U3C_cctorU3Eb__2_7_mC3E1E3D988091B923B9BD0656968C29A7627142B,
	DecodedBitStreamParser_decode_m0717755E012876AE8DA19D7C4DD368CA219E6777,
	DecodedBitStreamParser_decodeHanziSegment_m7D5E2CAEFEF61FA35B8C25B74E5E9981D90C67BC,
	DecodedBitStreamParser_decodeKanjiSegment_m839D217CA235F452F1ACCC204DF3673A8E4F6DA2,
	DecodedBitStreamParser_decodeByteSegment_mB3F60B838C11286B72424E04A1577219E688E3B5,
	DecodedBitStreamParser_toAlphaNumericChar_mFC19A72906808C99E821F618DDFF9C361D37935A,
	DecodedBitStreamParser_decodeAlphanumericSegment_mF42F9CD847AF5E3A0E29B71F9014DE23A3273BE2,
	DecodedBitStreamParser_decodeNumericSegment_m10D0F971E8655CBC641C240E21F5827876B71695,
	DecodedBitStreamParser_parseECIValue_mE66AFA3A1F395A3EB2CB0C6DA170AF90268E95BB,
	DecodedBitStreamParser__cctor_m2650820A9EB13A483FDCA4CDD64189073D19563C,
	Decoder__ctor_m3EDBF4259494302C6BC25F1C7C1BFB852D1E6A83,
	Decoder_decode_mD106DE4AFF03ABC728700F9AA3AD16D3D1583828,
	Decoder_decode_mD7AFA763EDDF7C72262558330466BEA4EF3F912F,
	Decoder_correctErrors_m7DB9DD42FCEE9EB439BCAA935973376863B71D3B,
	ErrorCorrectionLevel__ctor_m6CC38238785F128FE93C28D42B10D97F5EE4A456,
	ErrorCorrectionLevel_ordinal_mACCE779D514AF2084CA531514F85028357ABE219,
	ErrorCorrectionLevel_ToString_m4AB1D868FF8EBC59C071ED87963ECD11676C5DC7,
	ErrorCorrectionLevel_forBits_m660A8D4F1F8BC5C374511AB8EE04DB3EAFE28A7B,
	ErrorCorrectionLevel__cctor_mAF208B3752A00FA5783DA9DE4DEE16B5C53B112F,
	FormatInformation__ctor_m1AF28518CFD7FD886D43069BE3EC4D7336F436E1,
	FormatInformation_numBitsDiffering_mD6BF158CF9F2D2AE0A41801B858CBECFBC2FBC17,
	FormatInformation_decodeFormatInformation_m58EA6CD596C7C945CEC945A4F47A06C9362DBC52,
	FormatInformation_doDecodeFormatInformation_mA06A5DF90A5326FBF95A7ABB049D80141594855B,
	FormatInformation_get_ErrorCorrectionLevel_mAFB247A0D343207EE8190EC372A91D7C2AD7C08A,
	FormatInformation_get_DataMask_m3413A9030144B748FC203CC982066781483CA744,
	FormatInformation_GetHashCode_m8FF35549D89FACD5D9E129A6312918C4A0B3ABA9,
	FormatInformation_Equals_mB3AF3B66C04CE1A57707F2E7FE8B928979BF490E,
	FormatInformation__cctor_m233C0C568E89B58AD8D4CA5B66787E17AB64CE84,
	Mode_get_Name_mDD10EE936C24F81A4E3C5BB7A483853C5F22E448,
	Mode_set_Name_m75EFECA9B152C3114257443AD3CDD16A9FD5A789,
	Mode__ctor_m6FDE1CD3353C4DEE8ED08CDD722438AF05F38356,
	Mode_forBits_m05EB5F209EF1983F2A8C26CACB8C54DEE9A1C79F,
	Mode_getCharacterCountBits_m9FD5F0F6766306FBC4AD95DC637904E75FF322D2,
	Mode_set_Bits_m050E659D3B05D670BCE285DE9125B69816015542,
	Mode_ToString_m7BD60BEAA5460C46F62626FBAB01876303C58496,
	Mode__cctor_m4389EE3D623C6B93FB61AD2A183B3FCD5A0DDDAF,
	QRCodeDecoderMetaData__ctor_m42F73134DA32859E57D0626BABCDE9D85BFA5FE2,
	QRCodeDecoderMetaData_applyMirroredCorrection_m90E63015B2BF54FD75E5DCE10DB19A4CF26EBE5A,
	Version__ctor_m06413E5AF1A570DF1C4A2A6FDA2B7524BEFB970F,
	Version_get_VersionNumber_mFF023A4E25FDB34A4001A0355A2FCDBA9CBB8D89,
	Version_get_AlignmentPatternCenters_mADD8DB8B178CE125E1AA4F2EE89146BA5E460FBF,
	Version_get_TotalCodewords_m434BD18856962BE8E10F85144B29D13779377269,
	Version_get_DimensionForVersion_m9589220D4223CADC2D0E87E0949E3B181128B705,
	Version_getECBlocksForLevel_m32EDCF33F6787A0C075284CAA1ED12FAF3321880,
	Version_getProvisionalVersionForDimension_mB8DFB555C74A1AE3CCDA5BE3E5C88650A6396FCD,
	Version_getVersionForNumber_m14C7B926F3FEA87CE17EB40D714006682B311572,
	Version_decodeVersionInformation_mB7EC7E529C9F39F58632DB1A97AE3404DD1CA37D,
	Version_buildFunctionPattern_mD94BB465EDBD252F1A25B1B3EAD67C2468136684,
	Version_ToString_m9AEF9ED09794C93CE75C2487757FC2786B8D6A23,
	Version_buildVersions_m4FF9D42DFED8563CAE3AF70AB90A30046EC1026E,
	Version__cctor_m8BC73054B966C5EF8D307E0BE69564C356A10561,
	ECBlocks__ctor_m598A52E20612A06D5C36F96D4C145377AAFA4017,
	ECBlocks_get_ECCodewordsPerBlock_m499DAB3F60B821C4DE6BFF3A835BA7AF1D001DBA,
	ECBlocks_getECBlocks_mDCBA7713FC4A32F1F3A15C5BCF4A6BE73CAAD5B3,
	ECB__ctor_m9024356D06FABC01024A54BB6BFE6BC08032D784,
	ECB_get_Count_mFCC14F4E071AD4E1DF2B4A82018BD1E3D2049567,
	ECB_get_DataCodewords_m7537C0B4DB22195ED48799E246D79DBDBDBC203F,
	AlignmentPattern__ctor_m561C82DA1B7202ED7051CE02661E2E6AA19A991C,
	AlignmentPattern_aboutEquals_m0452A61209837E776BFFB2B789086BFC04E1EE1E,
	AlignmentPattern_combineEstimate_m55522D2D25D8BC699883543E75AB9FE55687109C,
	AlignmentPatternFinder__ctor_mF8F2D9EC295ACA0886A520ED60FFD9B6C54480E0,
	AlignmentPatternFinder_find_mEF736BE3133B9D9291FEC49F4534AB957F97CE9E,
	AlignmentPatternFinder_centerFromEnd_m91143BCDF4C242D7B4619258B14D860C6F73FFB0,
	AlignmentPatternFinder_foundPatternCross_mB38D974447F34D38B9F5F1FE9D80CE3254F81FB0,
	AlignmentPatternFinder_crossCheckVertical_mB25462EFA619E29BCDCFB5B57855D027A6F44454,
	AlignmentPatternFinder_handlePossibleCenter_m7859C3002064AD0DE01A712AD1A03DE0E8CBC07E,
	Detector__ctor_mA51E6ED117335B751BB0AD7F43905D1191EC4047,
	Detector_detect_m202353711911054236A4EB3BC3BA8FDE3058AE61,
	Detector_processFinderPatternInfo_m1F6BAAB9DCF7D72467C842718E0B163960C8498A,
	Detector_createTransform_m3DFB34C80B2032B16F1ACE2BB6C3E2BC11A65E20,
	Detector_sampleGrid_mB89244F0CCE7DA095A01A1B43392D44CB4624595,
	Detector_computeDimension_m2771F51355F9C0B482EEDF418648BEB11AA6521C,
	Detector_calculateModuleSize_m082A25F976C45CBE0D542FFD746DA00F8763FB97,
	Detector_calculateModuleSizeOneWay_mB1D93DA3FE041D4CFB5C3D0EEFEC7B68CD1E89F8,
	Detector_sizeOfBlackWhiteBlackRunBothWays_m59A0B29C4EB953B60F701889784E53650827190C,
	Detector_sizeOfBlackWhiteBlackRun_m480E379399EA40EA27B8499EB26F92F222D2719D,
	Detector_findAlignmentInRegion_mE5D04B173DEF9E7D19F7E1FED956BDAD537A9D2F,
	FinderPattern__ctor_mB0A8BB4A6DFD859A208444EFAB594BE77CB45A6D,
	FinderPattern__ctor_m3EEB1F7F97F8161ABF067CFC0FAC40BA9A7578E4,
	FinderPattern_get_EstimatedModuleSize_mB282049D7A2EB8A17BFECDC254FC69B9AC76F4CB,
	FinderPattern_get_Count_m87CD22FFFD8B192BA5A58BCB046AA4DABA2A30AD,
	FinderPattern_aboutEquals_mE6CB8FA276285958E83D55115196BA603F22A5E7,
	FinderPattern_combineEstimate_m3B1219F7A1B3F0AC40A6D4FA2167DF26BC31A572,
	FinderPatternFinder__ctor_m99744301D39C0CEAA402F97AE05E822C84A83A86,
	FinderPatternFinder_find_mFF4D063D659BBC55DC505814A212DAC53BD4B661,
	FinderPatternFinder_centerFromEnd_mEEB777E9A34A7B51B68087436FDDEFA366CE7F45,
	FinderPatternFinder_foundPatternCross_m3723765C983F180845F07B636623BC2951D2088B,
	FinderPatternFinder_get_CrossCheckStateCount_m5D76770AA09EC05D1CC337BBA0A26E44CB2BE0E8,
	FinderPatternFinder_crossCheckDiagonal_m7FE440B7FBE9F0182BB7DD2A5D6DCAE061F87A3D,
	FinderPatternFinder_crossCheckVertical_m5E40D06FF9EE64C67611521F2896B92C427F51A1,
	FinderPatternFinder_crossCheckHorizontal_mF6D602AEE764D1B70E89D8B6E2AE76A286BC52D7,
	FinderPatternFinder_handlePossibleCenter_mAC18E031D34901C64E6F17365DDBDE2606860CCE,
	FinderPatternFinder_findRowSkip_m92FA2A7DCD6E0D9D1C50E7ABD1AF2492BF959856,
	FinderPatternFinder_haveMultiplyConfirmedCenters_m8FFAA60679C5D12829BE1DBF51D2F69A566F0B61,
	FinderPatternFinder_selectBestPatterns_m3FF448C3B7B36B80F0201F6B801149E317534E87,
	FurthestFromAverageComparator__ctor_m4AE0185AED1653F0E0F2B6B48A79202F3D7428C3,
	FurthestFromAverageComparator_Compare_m30841DD8A686609796601BCBD6AFD4FD0FECE93F,
	CenterComparator__ctor_mECCDC684AC2E584356017B90C371F036F4EFEA1B,
	CenterComparator_Compare_m813791C4BB6556E6BA2628F3CF6FA3DE5C63364C,
	FinderPatternInfo__ctor_mD7341E9B171EC066EDF1F56BD9057C7D108D5AB4,
	FinderPatternInfo_get_BottomLeft_m94828C8BEC402358A4FF4CD667FADB959F59415A,
	FinderPatternInfo_get_TopLeft_m04165125ED43A22BE0006DF6128C99E73F0A58BD,
	FinderPatternInfo_get_TopRight_m78E1A0003115723EE9290FF4F6B0C04D9533D483,
	PDF417Common_getCodeword_mDF71CE71B3AAEFCB36CAD3C8DE45631294808619,
	PDF417Common__cctor_m15323ED4C983C6F5CFD57508497D93E18A36088F,
	PDF417Reader_decode_mED4EE6F31440A029D90DAF23DDDE7A5AB2B19591,
	PDF417Reader_decode_mE9E96D5F264F7F596351A37B44D0660FB80DD80E,
	PDF417Reader_getMaxWidth_mDD46178DCC6969F57173AC060CD4F6C8DD2AEF40,
	PDF417Reader_getMinWidth_m93B3075158AFF43C5295FA293FF4193282EBB9AA,
	PDF417Reader_getMaxCodewordWidth_mFDA0DF16E67C5D7E749548A0AD63FA260CE55BED,
	PDF417Reader_getMinCodewordWidth_m2DE1149DF3C1E3DA9F222C46964EB6CC8AA78239,
	PDF417Reader_reset_m25098DD5DCB314AD075584171FD11BBC4C465E35,
	PDF417Reader__ctor_m418E06795670458B5292382384741791399715A1,
	PDF417ResultMetadata_set_SegmentIndex_m6792A85EE271BFA6063BE4B2A3990743B73C1093,
	PDF417ResultMetadata_set_FileId_m6A79CC9F4624FE95AB7A870D742357FE0011F520,
	PDF417ResultMetadata_get_OptionalData_mEFBB80130FA8AE0C7E7657790368E3646220FD7E,
	PDF417ResultMetadata_set_OptionalData_m0D81D688400517D1D65B3E36CF5B9F996B70BC6F,
	PDF417ResultMetadata_set_IsLastSegment_m0FC2FB8C14FA37900B7B57397602F5D9DA1E64B8,
	PDF417ResultMetadata__ctor_m967B58DCE1371A9D4AEC7CCA9AA75FE6BF9FBB73,
	BarcodeMetadata_get_ColumnCount_m4A9D3AD55F9D68456B8E2DB771835EA55CCDE6C9,
	BarcodeMetadata_set_ColumnCount_m176BF152AEF28C1A672DAB8A5BD8278AB083A0DD,
	BarcodeMetadata_get_ErrorCorrectionLevel_m84BB571AFA2AE2423ED8042DF14BC1DB78EF538C,
	BarcodeMetadata_set_ErrorCorrectionLevel_m815B1DAB46579F73F17ACBF31E6E39703FD05627,
	BarcodeMetadata_get_RowCountUpper_m03D2B4FC5647BF341DF22E2208586CFE20140301,
	BarcodeMetadata_set_RowCountUpper_m9864EFB99D21684C54F44F9987DB9E1A67A45756,
	BarcodeMetadata_get_RowCountLower_m01D867E4FAF15591DB9ADCA2681A4F524909528D,
	BarcodeMetadata_set_RowCountLower_m8691F9A95CC8158F00B4D521E9A72E965081599E,
	BarcodeMetadata_get_RowCount_mACD2C5E04E6FE23404844F1C4B24B33642C9E93F,
	BarcodeMetadata_set_RowCount_m757D5A46753DA91F51B97CA777A2A30826FCDE85,
	BarcodeMetadata__ctor_m5C156634CF44D7D9118B5FB431D63E07C8F557C1,
	BarcodeValue_setValue_mC0EE93EB260DEF90894AF08DE46953BAECE214ED,
	BarcodeValue_getValue_m21AA86C2788EE335555BF503395B48A85778727A,
	BarcodeValue__ctor_mE897E340219BD79E9C751C83409A47FF3AD353E2,
	BoundingBox_get_TopLeft_m5CB4B6EC44B499CD0BA203C9B3B6F8933121724C,
	BoundingBox_set_TopLeft_mCCCE09F3624FA382B74AC8E0A199AA6D8264FD35,
	BoundingBox_get_TopRight_m5A6F9AD58CD8CA92AEA0C76D84C763EECFA40CD3,
	BoundingBox_set_TopRight_m10405141D9546413D3A065768C78A4DAC520929A,
	BoundingBox_get_BottomLeft_m9A8E6255BF210761723976D7C38CF7D118EDFE8F,
	BoundingBox_set_BottomLeft_mB4C5B675177B796E7E681F59DB76A625A5620B68,
	BoundingBox_get_BottomRight_m2035AC5736DC155C43192B4ECDAB8A4F57EEBB94,
	BoundingBox_set_BottomRight_m8C954B0A293AC7B39084CA049E9B386F712A079B,
	BoundingBox_get_MinX_m5F83E51CACD2BBA44036A99706C101CF6016840B,
	BoundingBox_set_MinX_mBA6A3362975DA7BD7D6A0F04DD13AE8FE75931C8,
	BoundingBox_get_MaxX_m3AE590ECC9CEA0B6DF5517CD8C3D182D95511712,
	BoundingBox_set_MaxX_m3119F44A66FDB33CF14D4B1228679C148B4D82D8,
	BoundingBox_get_MinY_m1FFEDA362F3B77F1DFE9385F7C3EF419DF0EC394,
	BoundingBox_set_MinY_m595E30BC39CD5ADA2D82C0994B0923BB4C4FF8F0,
	BoundingBox_get_MaxY_m5CFBC7ECF98C8659323F1A69392E297A82AB285B,
	BoundingBox_set_MaxY_m4F13753AEAEBD5CF67FA851DDCD4D2C586423D0F,
	BoundingBox_Create_m7730A11AF8CDE72F3321D7E7643C551FA358C01D,
	BoundingBox_Create_mE96E20A76E222C99B3D20A22EEE3710DF0B3B020,
	BoundingBox__ctor_mFCCE35BCBB09B054DCF931F30032AC32C6D954BB,
	BoundingBox_merge_m93FAC950625B5F8CBE03326E9C262417288059FE,
	BoundingBox_addMissingRows_m5040E28A39740713C627F815942B9D8FE9589AA7,
	BoundingBox_calculateMinMaxValues_m662C19EC975EF8E3A5F97DA11E9FD702DA262BCF,
	Codeword_get_StartX_m7142A4213335F1F993CA7802368A5AAD32672949,
	Codeword_set_StartX_mEFB95587ADF18A1371DA69054C6D5600B61B8AD2,
	Codeword_get_EndX_m76D22FBE3E14FCC8FA93068BE2CE2CB08E4E8998,
	Codeword_set_EndX_m86CF8AFE1193EC119585AFBA4A29B1B80237983C,
	Codeword_get_Bucket_mDC1B59D5B2EF3B9C57825CFA4209B3D20E3D6FB3,
	Codeword_set_Bucket_m4ACB8A7E1675CCDAA8AB38072CB91E78BA8C01E7,
	Codeword_get_Value_m6E938EB77D32B6E926D4A1DE4D4A14308EBDCEA9,
	Codeword_set_Value_m2704DED6B54EC01D56145587EE2FFEEE3704A85E,
	Codeword_get_RowNumber_mE9ABCE1EC3AF7CF50363CD1C02C5703C6498F2D0,
	Codeword_set_RowNumber_m099810CF7CB72780C60AA7C1ABA4FA0C8879F935,
	Codeword__ctor_mEDBEA6F34DC9DB125F589E9E9E2313F395BA2747,
	Codeword_get_Width_mDCF12E16B2526AE86B8CF010794DF6D69E6429EC,
	Codeword_get_HasValidRowNumber_mD33760CE045220BEB08AD94809038FB48000CACD,
	Codeword_IsValidRowNumber_mF094DFF5EEE87DC59013B4CF8A64EE35A85F04F7,
	Codeword_setRowNumberAsRowIndicatorColumn_mBB2625C44347EB1A19B06B3FD061D34DE7416A3B,
	Codeword_ToString_m6EA1E17024D7610A0855AAB9F68D9385FAD7C07A,
	Codeword__cctor_mE0B73EA6C7F859955D54304F13A44476CCEADAD4,
	DecodedBitStreamParser__cctor_mB9D2BF21BC4F25824201D6BDA47DEC3ED6D6DD12,
	DecodedBitStreamParser_decode_mCAB711DA9541D4207FE04EA1595E2382DB7D2210,
	DecodedBitStreamParser_getEncoding_m4F2E52B89FA0621228BF24408971D1B1358B2BE9,
	DecodedBitStreamParser_decodeMacroBlock_m4A049EC02C4FA583A3C2928329DAF07BA99DB82B,
	DecodedBitStreamParser_textCompaction_mB21BF43D938C2FC7EFA3F5CB4813D8525A08895B,
	DecodedBitStreamParser_decodeTextCompaction_mC2D2CC234B9390D82DB69E394F1E4211886464D7,
	DecodedBitStreamParser_byteCompaction_m82A12C789AB21F548EA4ABA58163681F42DFABE2,
	DecodedBitStreamParser_numericCompaction_m7BAD747D371C218503853746E0CEF2CDDDBC00F7,
	DecodedBitStreamParser_decodeBase900toBase10_mDCC0391FCB0C9A45DEBA1EFCE01DCE94C7B8631E,
	DetectionResult_get_Metadata_m28222ACC510CDDE151005C522558517F54F11E31,
	DetectionResult_set_Metadata_mE75F8B8FF73BE8E6949821C378222EA306F929AF,
	DetectionResult_get_DetectionResultColumns_mE527F5B2DC9F258C3A2FDDF03AF1E6C571E3DB68,
	DetectionResult_set_DetectionResultColumns_m65CE606CF83520C680E1CFEC8AB98D8147807C9A,
	DetectionResult_get_Box_m787AE2F48FF618EAD3AA5AC69F0E5CBA0557C03D,
	DetectionResult_set_Box_m151695A38E2C002A537D09CCB87DAC7DDAF08E13,
	DetectionResult_get_ColumnCount_mB43FEE1A3282C41D121B3A5C8220F6684F0266B8,
	DetectionResult_set_ColumnCount_mE0107ECCF65B7CAA5B790C6F8BF4FE68F330DE61,
	DetectionResult_get_RowCount_m2955064EB8DB77E537364DA68D463E61DE488ED9,
	DetectionResult_get_ErrorCorrectionLevel_mD51E566D08111066C6C28587C58C864B7A41357E,
	DetectionResult__ctor_m80B9A8731274F6DA21D8BF9AEE3B4BF5489A6652,
	DetectionResult_getDetectionResultColumns_mF3F6CCE82B3501899B19968D9B1E245A474FCB06,
	DetectionResult_adjustIndicatorColumnRowNumbers_m39203C9C3E4286D33A29A6BDE40DE7C6573E0082,
	DetectionResult_adjustRowNumbers_mD9E1BD005874BD60C20463E39CB3AC7DABECA8DE,
	DetectionResult_adjustRowNumbersByRow_mFB90217AF87888990064A5B3A4A731026334D7BD,
	DetectionResult_adjustRowNumbersFromBothRI_m24E9B1892162075C03AD37C5AE99150DC56BFC6E,
	DetectionResult_adjustRowNumbersFromRRI_m3AFBFE56375C1080D807EBF5E0BCA1E7326599FD,
	DetectionResult_adjustRowNumbersFromLRI_m57216279CFED7E337C46D430FCC029F942C7866A,
	DetectionResult_adjustRowNumberIfValid_m42DB0F5E8397CADB56156DA96C089AD2E29C1643,
	DetectionResult_adjustRowNumbers_mC1FD8946ED41571556B0749B8B3E934B6DC60174,
	DetectionResult_adjustRowNumber_mCAB3FBC9A90361D4512226F8EA48751B6776B17C,
	DetectionResult_ToString_m72853A7AC512DB3FFD365B37D02555EB5C0BCEE1,
	DetectionResultColumn_get_Box_m28DE039B3B068F05F1043D74471A84948310A5D4,
	DetectionResultColumn_set_Box_m3949C81CBD20F30155D8B7A5B720A809AABCA29B,
	DetectionResultColumn_get_Codewords_m8C5E2EDFE024EBB4833DAB8E33725D2E2906790A,
	DetectionResultColumn_set_Codewords_m507D740FE136F6E201194B24CF7F7BD80CE0CE13,
	DetectionResultColumn__ctor_m2ADCD05276AB42F3F9B87A956564F65A2EC7C19F,
	DetectionResultColumn_IndexForRow_m5896C7EB762F1CAECC7727E02A9992F6FC69A660,
	DetectionResultColumn_getCodeword_mD5C9F9833553FD8393E35D8E91C763712D7F45B2,
	DetectionResultColumn_getCodewordNearby_m9B20AF36EA062C0620C5C17A9095D6E93866FFB6,
	DetectionResultColumn_imageRowToCodewordIndex_mC82818309D0344F1C54601B2AF0912BA17416D1F,
	DetectionResultColumn_setCodeword_m6346A8C6E9D28419DA12EE0094E1DA388D64CF48,
	DetectionResultColumn_ToString_m0FAD8C2D51E3E230E5014AD393A2EBC5DF51AF25,
	DetectionResultRowIndicatorColumn_get_IsLeft_mC2C89A027BF107A96DDC704A5C7F5CD9998B5450,
	DetectionResultRowIndicatorColumn_set_IsLeft_m0A5451068AE892E9E3180FD80840869545C3DEB1,
	DetectionResultRowIndicatorColumn__ctor_m9E030F19AD1E19C56310368D0770620A10DA9AA7,
	DetectionResultRowIndicatorColumn_setRowNumbers_m960B0EEED1AE79A426217F9E1930323C23C143E1,
	DetectionResultRowIndicatorColumn_adjustCompleteIndicatorColumnRowNumbers_m01803E543828EB0D95DF33B7EB70BF7FA1B1042C,
	DetectionResultRowIndicatorColumn_getRowHeights_m61C468F2DF384FEC1D1D02701A6B0C57CE4FB3DD,
	DetectionResultRowIndicatorColumn_adjustIncompleteIndicatorColumnRowNumbers_mAD04465401EFA5EB8A7303D1E0187A25947D6874,
	DetectionResultRowIndicatorColumn_getBarcodeMetadata_mF776AB0DDF6A408CE7ECE8FB8E879ADA8C6EA204,
	DetectionResultRowIndicatorColumn_removeIncorrectCodewords_mE53A4711D0AE25168EE0D052486DDB49C91F0FE9,
	DetectionResultRowIndicatorColumn_ToString_m78C869F57142063F55722D353ECAB7C23A228E48,
	PDF417CodewordDecoder__cctor_m5FEA07FBF1748F962733E8E88B01CB3F19D08E96,
	PDF417CodewordDecoder_getDecodedValue_m40D6996FC9398DC965614CF651C6C2AD9231BEAD,
	PDF417CodewordDecoder_sampleBitCounts_mB6EFAB7642E2CEBC109E2989021850C89DB5ABBF,
	PDF417CodewordDecoder_getDecodedCodewordValue_m475269B81E98FD9AF479116F16AF53122E09F5D8,
	PDF417CodewordDecoder_getBitValue_mC5EAD27BFFC7C5DAF89BFF26252078543CA514DD,
	PDF417CodewordDecoder_getClosestDecodedValue_m6492E458235E335BC329559FD69F0011C8E0A3BF,
	PDF417ScanningDecoder_decode_m10CB6196EE3101DEB14720FD37692E243B6F7F4B,
	PDF417ScanningDecoder_merge_mAD376011CC28294DDE82901D591AB17B2EF37289,
	PDF417ScanningDecoder_adjustBoundingBox_m5DD7BF63787E04D704E09AF9B270CE092BA08E00,
	PDF417ScanningDecoder_getMax_m5CA3014C6BDF4D31A184E27E6E44A9BC89F09D9C,
	PDF417ScanningDecoder_getBarcodeMetadata_m1F66DF3E1CC9C84ADAC1E2B34B0109E933834790,
	PDF417ScanningDecoder_getRowIndicatorColumn_m34A7EAA83649DD3F3E597DE1003490485954ECAC,
	PDF417ScanningDecoder_adjustCodewordCount_m02FE86F3611624254B0E2B881EB09A6C7AA67175,
	PDF417ScanningDecoder_createDecoderResult_m959D0A00400736F43A630D07FE3992748021F8A4,
	PDF417ScanningDecoder_createDecoderResultFromAmbiguousValues_m0ED71A465F91BA798FB58A9C265B05141F2C7CBD,
	PDF417ScanningDecoder_createBarcodeMatrix_mEDE1ED0259A57F3CC4685D636326EBAFAB49369F,
	PDF417ScanningDecoder_isValidBarcodeColumn_m152DF458D272F7F418ED7ACF0059346817698E26,
	PDF417ScanningDecoder_getStartColumn_m5925DAC51470D099F2B44C260502B6B54CD12E0E,
	PDF417ScanningDecoder_detectCodeword_mE6B9E76EE55F52D518995CB10AFFBD52356DE5B0,
	PDF417ScanningDecoder_getModuleBitCount_m7F1997ADD96756AEEAF85BEBDD847420F8E8FC61,
	PDF417ScanningDecoder_getNumberOfECCodeWords_m78635E6FFA758779DD5C34B3A30140AE4E9BF2CE,
	PDF417ScanningDecoder_adjustCodewordStartColumn_mE5A2A0F1AE38DF6D02CCFC8BF9E1D3A153F2CFB1,
	PDF417ScanningDecoder_checkCodewordSkew_m7D26CDB8EFC68A8EE7E37D65854F986C5A53235F,
	PDF417ScanningDecoder_decodeCodewords_mD2123CBC928CED2A245FE6BDF6DD53CF16A0D862,
	PDF417ScanningDecoder_correctErrors_m5710663F6735DEF09B253BFD185743C635552BC5,
	PDF417ScanningDecoder_verifyCodewordCount_m2918861003232164A122FB53E56A19269BB90181,
	PDF417ScanningDecoder_getBitCountForCodeword_mA26EAC2BEFBD397623357604A1D10E9675BB4D15,
	PDF417ScanningDecoder_getCodewordBucketNumber_mE94072C68EDB1738088AEF2CC625BB88B8E1A13A,
	PDF417ScanningDecoder_getCodewordBucketNumber_m8FE23D735BFCF3F58951ED46018858E083D89EE8,
	PDF417ScanningDecoder__cctor_m8FCE8CB540FD1D4DE669E899DCBC63BEDCC3D3CE,
	Detector_detect_m77083CAD6731F658875F5CB4887CA0F97A9B53CA,
	Detector_detect_m012C00104272B911CCD110B43430652514B4F4A9,
	Detector_findVertices_mF74AB91B8535D923B64A1FD95B93CAE47AA7DD92,
	Detector_copyToResult_mD1AA525F2B3888A81FEF7CE470848582B2640064,
	Detector_findRowsWithPattern_m950A84E4A8687164E9001698CC8E0AE03F5F22AD,
	Detector_findGuardPattern_m9A1C17C84D513627FF37C0AF9695A99A49A17FB8,
	Detector_patternMatchVariance_m86EBAB1902EB4E64C99797EFBA893B893060AE7F,
	Detector__cctor_m705EC43B10096EEF60CC0DD8EA853D5ECDC94B87,
	PDF417DetectorResult_get_Bits_mF582F132EF48BC1E687742C76A1004054C51BCF8,
	PDF417DetectorResult_set_Bits_mBF16DD37F7407142F2C31B184AC52A326E630B3E,
	PDF417DetectorResult_get_Points_mE94F3CA82AE031E694E202F8C6151AEF086E5BDA,
	PDF417DetectorResult_set_Points_mA06AC47807901C9A6A34C2CFFD32CF3B3E22B9F6,
	PDF417DetectorResult__ctor_m339E0215B1A0C869F47DBE531BAB0D05A40F6F8F,
	PDF417HighLevelEncoder__cctor_mCA870020187562C5E9BC3A202EBE95A173C289D1,
	ErrorCorrection__ctor_m34C0B86D00DB6B2984DF14339759A3642B48716E,
	ErrorCorrection_decode_mCA4A579258E2C5E4588FE83A84E76BC0514023E9,
	ErrorCorrection_runEuclideanAlgorithm_m1D3B22C936F75AC826DA9F06D84A8E4CCDB667CD,
	ErrorCorrection_findErrorLocations_m7BC663A9BD39ACC2228A60D042DC1D767EE1F8EC,
	ErrorCorrection_findErrorMagnitudes_mF2A467AFFC3FB6BAD61EC60E934055D1E44332A2,
	ModulusGF_get_Zero_m65338B601D7118B1864EBA357548AD7DBB3901B0,
	ModulusGF_set_Zero_m8AD743F6D1B4E3B5C2369DD6DB62FC227C686C5F,
	ModulusGF_get_One_m8DE4FAEE75B473FC0E62FDC6232A4D7EAB7DF90C,
	ModulusGF_set_One_m377CF840C61DCD154038F6398B92CA9610250DD8,
	ModulusGF__ctor_m8E30BB6B6CA241C9BE5B8BB7A463E5BFDA9A3E62,
	ModulusGF_buildMonomial_mBB4893A51C5421DAA8323E2B55EE9EE7E3A51AE4,
	ModulusGF_add_m72492B09261523820329C8D5BAC163268AFA262C,
	ModulusGF_subtract_mCEE55A12C1B0748780AA50342695F983ECD1D09D,
	ModulusGF_exp_mBE3892FD93C8696E5566F95FF17C9E30E61126D2,
	ModulusGF_log_m5071E565C2047F0C57ED98D0F88A1506143BECF5,
	ModulusGF_inverse_m87D819708B1EA359F732711D12FD515ADA35C8A7,
	ModulusGF_multiply_m3616529BCE9EB3296D041A6103042B32BC044D72,
	ModulusGF_get_Size_mC294C04425D58D71E491B75007DE0732E5A05171,
	ModulusGF__cctor_m01D59044174811ABCB85425A51F25C097727C8E2,
	ModulusPoly__ctor_m84035B4A06FD998B8B08CCEC6FE8365BDCC9C162,
	ModulusPoly_get_Degree_mFEE3AA4873EC95B557B1D4C3A9876EC9918E0483,
	ModulusPoly_get_isZero_m349ABDC976924C7474D1AFCDB8FBF80DEDC86B32,
	ModulusPoly_getCoefficient_m74733E2E45C57BF0030C8FD11140A2FB4A7B5B35,
	ModulusPoly_evaluateAt_mB9195FDF29D0FF03B6CD8CF79AB1FDC58646F925,
	ModulusPoly_add_mB314EE922FEBB8C7CF98820DCA7CA077D319AE26,
	ModulusPoly_subtract_m66A1953DCC6C9864F77A86AF6D2A1338F14FA124,
	ModulusPoly_multiply_m3177D06E07AC8BBD537A46C6C10AC55FCF858B83,
	ModulusPoly_getNegative_m6A0B499446B92230E80BB2AF718EB2C43A103306,
	ModulusPoly_multiply_m2F748E18ED8DBACEA4F63A5610E35041F9D878DA,
	ModulusPoly_multiplyByMonomial_mCB1687D8EBF571F333868354AAE76F2817DC08DE,
	ModulusPoly_ToString_m66F44625F665C2988F67C84DA554D6000C8DB78B,
	CodaBarReader__ctor_m484B6DBF28197DDD4D1FA9AC84E98CAD3387DCA2,
	CodaBarReader_decodeRow_mEB4BD4CF7AF19F7CA1CFE3A208DDD211418623E1,
	CodaBarReader_validatePattern_m41B4FC8AFC090163A6CD04D15F8890F853A44266,
	CodaBarReader_setCounters_m90ED54B610961718821B5DCF7B5CBBBA3FDA9F41,
	CodaBarReader_counterAppend_mE03E878BAB1006DDDE4CEB7414051BE5F2C8353F,
	CodaBarReader_findStartPattern_m98445641345DEE7F9D2D2F063539E51B2B7DA78A,
	CodaBarReader_arrayContains_m4BE365E8C94683682C3197BBA5DF20554E914769,
	CodaBarReader_toNarrowWidePattern_m2239004FC8FEE32B3F256848A886A8B68AAA79F8,
	CodaBarReader__cctor_mEAEB3167A5F23A1332649DE0B94B3D8F27FBED4C,
	Code128Reader_findStartPattern_m4C083CA8D07B03333143FF36BCB842D0530C8480,
	Code128Reader_decodeCode_m694AD1065D2881D176C34B9048D060B4887763CC,
	Code128Reader_decodeRow_mC33398DABFADF2C7B03966F7E71819791AAA922E,
	Code128Reader__ctor_m269FF0F640AC6D79AF3C62A71EAA0A86963661D1,
	Code128Reader__cctor_m0E30D17B53CED4C68D3C674EF0658BB36BEC4122,
	Code39Reader__ctor_mD25930FE523B7AF6ED3602454486BC1C7F7521B8,
	Code39Reader_decodeRow_mAB97F09FA6714E8AD7F92630739FA93CECA114CE,
	Code39Reader_findAsteriskPattern_m9658C527D11C095DAF4E064BACF199BDA26E054E,
	Code39Reader_toNarrowWidePattern_m8660375BB660D0F827B44C03263622287634268B,
	Code39Reader_patternToChar_m09BE3C039AC3D1591B613D4C902FEB20C702C79A,
	Code39Reader_decodeExtended_mDDF22D16154768A089F6C51B95B190C4E1FAF25A,
	Code39Reader__cctor_m10838688E021DF7AAD1B11D9FEC9AF915399EF9C,
	Code93Reader__ctor_m75DC2D34AA896106F2A2D1F251C0D1D4FC969B91,
	Code93Reader_decodeRow_m47A299DCB561048687097B64217B48C9BFF75C16,
	Code93Reader_findAsteriskPattern_m890DAF8AAD38D210557E84AEA49B6D2AC5488116,
	Code93Reader_toPattern_mDB6E68CF216244F378FFC2EC88A4FB5AB46EF40E,
	Code93Reader_patternToChar_mEC762541BE74BF312AE53C66655CA7D262E9CBFE,
	Code93Reader_decodeExtended_m4F8AE577C60984DFEBE518581A40A668094FB917,
	Code93Reader_checkChecksums_m38C55165D557C4F075FB7CC70705029DE4D1C2E5,
	Code93Reader_checkOneChecksum_m18E016D438152A4A848D18E24A636620A2717135,
	Code93Reader__cctor_m4585FC992C039D7579E3FF73EF1A171BF39765AB,
	EAN13Reader__ctor_mB27AD98DBE131034694094CEF3AC6F655CB2F413,
	EAN13Reader_decodeMiddle_m9C1B9E58C3B71FB0248A236FB0D77988E31553DF,
	EAN13Reader_get_BarcodeFormat_m2696FCF21F5C2DE197D1E7C24CB86FA514BBADAE,
	EAN13Reader_determineFirstDigit_m685E358317D6748E3A646B773127FE4135642F49,
	EAN13Reader__cctor_m258F8831422CA974533A3B4469E2C626BE40E7F9,
	EAN8Reader__ctor_mFC32FFCE655B4E5318E28BF7F9065665B491FB15,
	EAN8Reader_decodeMiddle_m613DAB953ED713A5079CBDD86A042D0057E190C5,
	EAN8Reader_get_BarcodeFormat_mB59527A7D435389C8F357855EE0EE8B19BAF4488,
	EANManufacturerOrgSupport_lookupCountryIdentifier_m39008989E9ADA63EB48EF3E2B825FD5719569F8A,
	EANManufacturerOrgSupport_add_mCA907430BB14EB8138D1948647C867E1C5E8F7D3,
	EANManufacturerOrgSupport_initIfNeeded_mEB7559EB7490D00FA7DFC607F8518BC9BD844906,
	EANManufacturerOrgSupport__ctor_m3F388103A583E6C9A5857BD76956F42334CB3774,
	ITFReader_decodeRow_mEB5198257F696BE89A2CA764E908ABEB8CF1846D,
	ITFReader_decodeMiddle_m763B848E1A2751A3B9B02CD31F8CF43FA5BF0480,
	ITFReader_decodeStart_mAF2F7C53D4B751F5DBAAED4660EF285A55C99FD4,
	ITFReader_validateQuietZone_m3AC577691052C5D3F1DDEF6866C54CC88F896C36,
	ITFReader_skipWhiteSpace_m1DE503166A0D7AC46C6CCEBA4D30D7D7404E6850,
	ITFReader_decodeEnd_m6D94FFA18BD93F4E7873E8546EEA9AED90EA2F93,
	ITFReader_findGuardPattern_m747F8361E79E2363D7EAC702F903D2331ED9D264,
	ITFReader_decodeDigit_mC94DEAE09913F8E83850AEC72597787CCE2CD965,
	ITFReader__ctor_mF5646155A59559C346EB037F8BB5138666850D02,
	ITFReader__cctor_mB065228887260AC1F7C4E510622EF3B75BBECDDC,
	MSIReader__ctor_m72DFA66E74183876C880C92442B5B1179AA19A77,
	MSIReader_decodeRow_m66BD71D508442A357E27EF7AFF0789B8F3AED570,
	MSIReader_findStartPattern_mB8621B54FA27E619B58A8B0D9CBEB5AA7C750570,
	MSIReader_findEndPattern_m5A32F9776F9E02D655A6D0A6248F36833FAC0755,
	MSIReader_calculateAverageCounterWidth_mADD2AC18A5D4C60BA2CFF18E298AD2E6F47FF300,
	MSIReader_toPattern_m510927637DCB05A2331C03EDCE319ADDB8D9B73C,
	MSIReader_patternToChar_m2729755C804D6748C8E9E322EFEF8D6DA722C034,
	MSIReader_CalculateChecksumLuhn_mF28D12A0444B68557D2D657A76B8A45EC9A5515D,
	MSIReader__cctor_m8B04187943AC1FF958B3BA2E320CC9F762B9FADE,
	MultiFormatOneDReader__ctor_mBAB256EF5D3E6DE1F8F9D07F36C35CB322B86EBE,
	MultiFormatOneDReader_decodeRow_m4B41DEF370E40487C16A35BB20996CFD0E7999DC,
	MultiFormatOneDReader_reset_m89383A94DE3FEE31107970E9342F66637C0CC0D9,
	MultiFormatUPCEANReader__ctor_m142FD956F2C83ACC9190A5FFC92CD5EBB0E602B9,
	MultiFormatUPCEANReader_decodeRow_m217A6231E1CF8076A500ADDBA4A238680AABEF9F,
	MultiFormatUPCEANReader_reset_m8381B6E531763B7D465C8C13C97793C3A6B2A9AE,
	OneDReader_decode_m1069EA854C11AEF129843635949AC6F5E484A5ED,
	OneDReader_reset_mD4684AA7B384120F8CD01036056E187A61580516,
	OneDReader_doDecode_m7386330554C2E9983108D37AB754F76F3C33A1CD,
	OneDReader_recordPattern_m5463FD1B564B14C4F3336F5CEDA01C9E50CC8EA0,
	OneDReader_recordPattern_m7A3B64E2671222FAD4380D53F3A217C182500320,
	OneDReader_recordPatternInReverse_m28771B72B70B3F253BB2464CD2EBB1626FD48658,
	OneDReader_patternMatchVariance_m002F3449FAEA9145B68B818F8C57452BA3F87E4A,
	NULL,
	OneDReader__ctor_mAFBEF18DB6BE34424E6D1CAF0DD435A5086F960E,
	OneDReader__cctor_m2A0502DE378C0070521B979FECD074D80FDC2855,
	UPCAReader_decodeRow_m6DEAF0EFE4B5E5B0D48F5E9FE4023B997DB7C355,
	UPCAReader_decodeRow_m342274023104849117E180740F7FBF4FB80D19EE,
	UPCAReader_decode_mB4743BCDCD91C297C903F78F13C6E5279150573F,
	UPCAReader_get_BarcodeFormat_mECCE8975175B08669FAF444B7944D17A84F972D3,
	UPCAReader_decodeMiddle_mC7A2536BE5F83103BD8A1D1C2DBE8F343F30CD1E,
	UPCAReader_maybeReturnResult_m5552BA55D69C4A9F86F1DB8C5B53E2F6CA67D690,
	UPCAReader__ctor_m1B4FAEA66B19FFB146D33E26389D641AED295F16,
	UPCEANExtension2Support_decodeRow_m6FC896F3CC431D7E1A0C4E59F7D08194FA4A4529,
	UPCEANExtension2Support_decodeMiddle_m8BF98AD2D581DC401B66AAFCD7243D28653E91DA,
	UPCEANExtension2Support_parseExtensionString_mABA4FA5D12A9977C609273533B53388DB327F676,
	UPCEANExtension2Support__ctor_m686A4726FAD6FA63B7F4C4C4CD01184C4BCE7076,
	UPCEANExtension5Support_decodeRow_m196369A7519341663C20A55C0F8D9C45A7EB3040,
	UPCEANExtension5Support_decodeMiddle_m50F1F331BB812B61BB6FCDDF1B46A5781EF90EEB,
	UPCEANExtension5Support_extensionChecksum_mEB6DD3E65430A6C55C2573C13C4843B206487331,
	UPCEANExtension5Support_determineCheckDigit_mFB9CE5C8D8738397310C9CAAE53182DC5D803455,
	UPCEANExtension5Support_parseExtensionString_mC63C8894934F2FD8970E71DAF429C303CB2B233E,
	UPCEANExtension5Support_parseExtension5String_mAA29427FA7B6ACFF6644FA5FAF6182748AA2AE7F,
	UPCEANExtension5Support__ctor_m11F10C0B38CA47707464E20497FBEEF8AA40EDD1,
	UPCEANExtension5Support__cctor_mC06FFD595C84659118A1A7D0FA25597A2260C1F0,
	UPCEANExtensionSupport_decodeRow_m8EAB71C63C33CD2C3D245B8CE88B75A41D8E08DA,
	UPCEANExtensionSupport__ctor_mEFD58F5A846943270C389706158188ECE212A1CC,
	UPCEANExtensionSupport__cctor_m2D5EC8E0381D31C986C11A858F5CB23D785E0E7A,
	UPCEANReader__cctor_m1AA1D6C6D2C836C8EF6061E1213CFFD6D331FE37,
	UPCEANReader__ctor_m46508F781D82EFB4A4B0DDCB298087130C28182B,
	UPCEANReader_findStartGuardPattern_m7D1525CCAEFB9BD70348399B747B87EC41549F77,
	UPCEANReader_decodeRow_m463C96CD9386A51FC62F759BBE8A0E77C1CDD7A2,
	UPCEANReader_decodeRow_m53925E4BBDE547114EFC93F408070B4F83C5642D,
	UPCEANReader_checkChecksum_mCD628B259BAB466F75334BEDCDBE5FBBF5C459F5,
	UPCEANReader_checkStandardUPCEANChecksum_m67F5427D778277C988B54EF6160BC5E55F6ACA32,
	UPCEANReader_getStandardUPCEANChecksum_mD03061A29CB13073C9656D30981DE0DDF262E249,
	UPCEANReader_decodeEnd_m559247BFCB8429F07809B8A7F90F4DA73543E699,
	UPCEANReader_findGuardPattern_m6FF2D724D74B132AA384BFCA3F2E624513248F01,
	UPCEANReader_findGuardPattern_m3106899BCF69C2D4CD441E776F987F6CE8BF317F,
	UPCEANReader_decodeDigit_m4BE543A4AD670AFD401D18EB7371E5A880B6389B,
	NULL,
	NULL,
	UPCEReader__ctor_m28B34D410BDA4FC0672E606766674E2F9DF427B3,
	UPCEReader_decodeMiddle_m120EEB29C634C0C89E2D37B957AF23305DFD583A,
	UPCEReader_decodeEnd_m1601ED31792101DC5AF3A7143BB440CAFA3251EF,
	UPCEReader_checkChecksum_m9C4BC99D78979868C676AABFA8AD90BAFAA168EB,
	UPCEReader_determineNumSysAndCheckDigit_mB283BCB6623F90FB779D14EBFCB207DE0EE68228,
	UPCEReader_get_BarcodeFormat_mD212C99DCE762350B95D9B575AD01FF93F082E38,
	UPCEReader_convertUPCEtoUPCA_m22D91535A4105233B807FD9DF20017733BC34120,
	UPCEReader__cctor_mD2D99D4851B0E40AB3D4FFB7611EF27085B5EC68,
	AbstractRSSReader__ctor_mF40A7FBDA94B256961B41741953316F01B90AFE3,
	AbstractRSSReader_getDecodeFinderCounters_mED32F9666161B0865E58916484C59C6C1CF4783A,
	AbstractRSSReader_getDataCharacterCounters_mFE40D1439AEF15A5DA6AF85EA12455B2D3F9F43F,
	AbstractRSSReader_getOddRoundingErrors_mFA135443BBF1E7083C03296670AF3778C74CCD96,
	AbstractRSSReader_getEvenRoundingErrors_m8F14551A2F044AE1EEDAB0CB557C7B36ECA053A2,
	AbstractRSSReader_getOddCounts_m965C8C7732F4025C5348399750823C51C421B5E2,
	AbstractRSSReader_getEvenCounts_m30ED28C8517CEBA6589B89FB7B31E1D38A09A905,
	AbstractRSSReader_parseFinderValue_mA66A27123A48FBFFDEE7461314C4B8DB678DF640,
	AbstractRSSReader_increment_m8940571FE039D3D1238AD7854A8A3DAA64ABC703,
	AbstractRSSReader_decrement_m4F91E70AA9B430FCFF828624BAA4980E97A0696B,
	AbstractRSSReader_isFinderPattern_m23633017DE14B43F170863E13175F0A136E1A3C7,
	AbstractRSSReader__cctor_m978220EDBE8044C65655E05347DD4733D7A91BB0,
	DataCharacter_get_Value_m989F7EFF27F7D5857631A9E6EF3491524F923CB7,
	DataCharacter_set_Value_m4877B2E2E40BBF5855567B1E23B0AFA76DB7ACA7,
	DataCharacter_get_ChecksumPortion_m6798F3EDC1B1BC40E7722823E6E7AC8D113C70FC,
	DataCharacter_set_ChecksumPortion_mD9394227B2D3A90D5C99BFE4A398B6F1F82504DD,
	DataCharacter__ctor_m6D6063014D0582BE08A17259846319C7159180D6,
	DataCharacter_ToString_m7782C70D7CF4BFBAA918664E0859D2E557A934EE,
	DataCharacter_Equals_m12A12C0DEA308EC5BD421DC6D7120411C92D57E7,
	DataCharacter_GetHashCode_mBB8AF0BE9FA0A9AEF84F8F76EEB03F0314245E69,
	FinderPattern_get_Value_m8014DA11325AD1E1E08646912E56C24807170F9F,
	FinderPattern_set_Value_mF2E69AB10949ADD73E4762CDD2060603FA16F1BA,
	FinderPattern_get_StartEnd_mE939A50F1B0D50B4A54D0A78BF100A8D013AF2D2,
	FinderPattern_set_StartEnd_m4B827D1750AF31559D0B1AD309FA330BB59768A8,
	FinderPattern_get_ResultPoints_m31BDCA7CA9929339A3131636EF96816A8F857828,
	FinderPattern_set_ResultPoints_m6E89079AEB0D1CB82EA4184C9FC8B3E3817F66B6,
	FinderPattern__ctor_m2705DBACE997D573F3DCCCFE905491F44622DD69,
	FinderPattern_Equals_mC1678F7A8574449380F1E44081F392A5BF63EB8E,
	FinderPattern_GetHashCode_m12CDC90AB7455C69D4DA8C4FA601EB80CDC38BD8,
	Pair_get_FinderPattern_m96B759DC05F52541A4D5323517F7B2CC5F754A2A,
	Pair_set_FinderPattern_m5728E8A721DEBE405B277038A44CABE83320C038,
	Pair_get_Count_m987093D75472F4272235F675E04B92DB38E33F51,
	Pair_set_Count_m32CD3B8130984363CB154A45B947A567E950F7A0,
	Pair__ctor_m9A69104BACB3404917ED67DB3AB2E6E45014081E,
	Pair_incrementCount_m1EBBA492AE25F465BF32BBDA4CE39B9263163045,
	RSS14Reader__ctor_m87AFA3FDE73F8934B2FA01EFC98C58814A8E32F9,
	RSS14Reader_decodeRow_m49EE9C4D2E066F09CEF28EAAC1C9E149BE194E42,
	RSS14Reader_addOrTally_mDA9A209DFE052260303B996EF263591357D1B9B3,
	RSS14Reader_reset_mF2A38239ECD8AC8C4872D152490CF486CE3FC9D5,
	RSS14Reader_constructResult_mCD7C8E420DD16CFB7DFB57893CBE6ECE7812A1F0,
	RSS14Reader_checkChecksum_mC0215EB52084B2FB5D6337DE3C97A4C09EF9B79E,
	RSS14Reader_decodePair_mA61B7EDE4781D2842A8C03588C039C4A8434ECF0,
	RSS14Reader_decodeDataCharacter_m19C8CB82286EF6B7FED910139E2D386819552061,
	RSS14Reader_findFinderPattern_m9F67BE44AC43A8ABF8416A9DEA379340AFCA44F0,
	RSS14Reader_parseFoundFinderPattern_mC72347A9F4570C8B193C0953838DBD6C4685A0FD,
	RSS14Reader_adjustOddEvenCounts_m93F734448E36ADACE5340173C77DF89D0BA057D0,
	RSS14Reader__cctor_m27777BA0CBA1F46A065C5F221E6D02CED741590A,
	RSSUtils_getRSSvalue_m576E8B0BF589CEDD5106BDD3711B740A40BD9203,
	RSSUtils_combins_mB2E1EAAC76C33CD63A77666B875BC1151FE1F55C,
	BitArrayBuilder_buildBitArray_m7AC1150D564696A291F9ED0C40FF572502446184,
	ExpandedPair_set_MayBeLast_mF8ED068D7464BB2189835D13E2DAB710A7E534A7,
	ExpandedPair_get_LeftChar_mBB6B374F16E0D99A4FBE539CFC986AD770C29754,
	ExpandedPair_set_LeftChar_m88021F4C7B0A02E06B313F69D82030673388AA6F,
	ExpandedPair_get_RightChar_m04B44D7E072945EB23BD0F9BCF899982F7EC5955,
	ExpandedPair_set_RightChar_m21F9E5A538C4CE938EB4C66EF175C9040D651C15,
	ExpandedPair_get_FinderPattern_mD3E9E768D8CB35A730ABBBDC5D88475D5A02D4DC,
	ExpandedPair_set_FinderPattern_m339DD5FC48B79D5D28F185DF8B4CF37B84C23961,
	ExpandedPair__ctor_m862F5517D3648F59B6B85879EFF421AD3D1C926D,
	ExpandedPair_get_MustBeLast_m6E16AB3FEA0E5A7756A56A69210004168D79B862,
	ExpandedPair_ToString_mB154F7C9524EC199B4121F5465A632BB58AA4A83,
	ExpandedPair_Equals_mEAC2025C762D22270D1549CCD2AE5904FEC22B0D,
	ExpandedPair_EqualsOrNull_m65EDF0B40323DEB826D08E8AE4B3970DC2C0567D,
	ExpandedPair_GetHashCode_m45B8B1B685652B96DB0CF7200A007FD482A436A9,
	ExpandedPair_hashNotNull_mE7AC4F83E3E0EED7F51D67BC98A2723E26648911,
	ExpandedRow__ctor_mB645649C2C757C19F68A99C5CA86A1BF168715CB,
	ExpandedRow_get_Pairs_m50450B2009FE8FD7118F149A6E309560463777D7,
	ExpandedRow_set_Pairs_m302A4E5EE79058640E1744CD6FF68AF6FA0F64F7,
	ExpandedRow_get_RowNumber_mB3C677AF04C8D715F41914B552FFCEE5B27FC2A1,
	ExpandedRow_set_RowNumber_m5D9E523450193C6945F959DFA191509F771D6C91,
	ExpandedRow_get_IsReversed_m469685004873C73FC4EFB292A3266BD9D72EA4FD,
	ExpandedRow_set_IsReversed_m3D5EEFD4BE1AF4085620F15BABBA06B4F6FC7FFF,
	ExpandedRow_IsEquivalent_mBC9D011ECCDDBA5E0B991A99307982DF054AAB55,
	ExpandedRow_ToString_m6F6E56B0CCA58F3F0C3BC13D2DC01FB87D635BCE,
	ExpandedRow_Equals_m9A38B41DBCD88D7F85D53E787A24849A0FAE4607,
	ExpandedRow_GetHashCode_mB6BB50E4647D4B73DE8CF3EBB858D3A8DC324A0F,
	RSSExpandedReader_decodeRow_m95FB77A61BE72E381A76E219313CE310B2C6ED7C,
	RSSExpandedReader_reset_m5B4D9B1B22F64763084CF86762531731A9B3C817,
	RSSExpandedReader_decodeRow2pairs_m3F043155B173084ED01B1D4FE57C63722F527C6C,
	RSSExpandedReader_checkRows_m14DD3922B4BF1A9CE4009470670E9D21BD87FCB3,
	RSSExpandedReader_checkRows_mB4E7966FCBF8045F00EB0F8136819CA112D94749,
	RSSExpandedReader_isValidSequence_m9925CA4BA16D77A9DD82864F31D7AE4CC747C842,
	RSSExpandedReader_storeRow_m52F91343FD6E4C2B5B81E9E7702DCB59B9417386,
	RSSExpandedReader_removePartialRows_m87249C6F790D43A7EE4845AB429F5620037F15FF,
	RSSExpandedReader_isPartialRow_m2AA70C656745B80BAC63A6B1D686A4152170C474,
	RSSExpandedReader_constructResult_m48C419011064A068C27033B5B9C8EA6A586FF8C6,
	RSSExpandedReader_checkChecksum_m3F0DC6157968870B9E5579099BBC297B7591FCA9,
	RSSExpandedReader_getNextSecondBar_m4587BFD839EB6C3C736E9B769273515170F76043,
	RSSExpandedReader_retrieveNextPair_m7F15DB7FF5A1FAF6E69039127C92133B4C1FB933,
	RSSExpandedReader_findNextPair_m48D7CC3F0753B5BC0D9C98FA5661BC0DFF688B8A,
	RSSExpandedReader_reverseCounters_m69CF30131ED1896B2C43EA6B66CF0D98BE4CB774,
	RSSExpandedReader_parseFoundFinderPattern_mA7D7BAD890497FE3D9461D05E940FF8500F4DF39,
	RSSExpandedReader_decodeDataCharacter_m8C4F488085EC38008B6B15C8ABA5705420AD12DE,
	RSSExpandedReader_isNotA1left_m5B3950904593BDCA3C7F53B09E83FB398478B88B,
	RSSExpandedReader_adjustOddEvenCounts_mF45EAD02D635060D301CE07AA13AC3767BE9E85F,
	RSSExpandedReader__ctor_mF316E1DC3B518BECDAB2E743D6223F263872FABC,
	RSSExpandedReader__cctor_m6E09458D19115BA68ED0D25F557D91C369AD8737,
	AbstractExpandedDecoder__ctor_mB9799D8B99CF185E2C391D6552CA06FC185C01CA,
	AbstractExpandedDecoder_getInformation_m257AB42BCB9E5E73D07640A6622B4EA580F60841,
	AbstractExpandedDecoder_getGeneralDecoder_m8B7777282EB6EB81EADDCA497FD6BB5007AB94F4,
	NULL,
	AbstractExpandedDecoder_createDecoder_mF70A102E53E57DD15BE68738080034C5FCEA691D,
	AI013103decoder__ctor_m82062027815E7E658E7BC90A86BC6B39035D687B,
	AI013103decoder_addWeightCode_m6F298E99487A3EBBFA9A8573D79AF2A42361A35F,
	AI013103decoder_checkWeight_m0C47DBC689B0912DE9341BB4450AE8FDD88605B7,
	AI01320xDecoder__ctor_mF677A21FA914C4300F8A554D5332FE35FDFEDA0A,
	AI01320xDecoder_addWeightCode_mBB88E5A9F49AC51751FACBD83F154F38A36EBD56,
	AI01320xDecoder_checkWeight_mD64B473EAE3F0C6397C080CD2589A8F84FC00AED,
	AI01392xDecoder__ctor_mBDA229F0B827C00376CFDAFC1E44C89E662BB5A0,
	AI01392xDecoder_parseInformation_m14A98F5E76E9301B9399B8BFA8851EC8CFD6B522,
	AI01393xDecoder__ctor_m6E252AB48F93E3C7DB40EC35261041E08D4451CE,
	AI01393xDecoder_parseInformation_m1F61CF54065F9DA366C2E57AEB51064F5A43AC52,
	AI01393xDecoder__cctor_m254A5DA15688A0BD9D1A3FF730D71B4F03A9A575,
	AI013x0x1xDecoder__ctor_mA648FD76D874E725A47BC5FE73FC87D9A64E0557,
	AI013x0x1xDecoder_parseInformation_mE9C01A98556A24902B53829DB62A5ACF286B609D,
	AI013x0x1xDecoder_encodeCompressedDate_mEEA08D51F154547D3C5354908F8237536D8C70CA,
	AI013x0x1xDecoder_addWeightCode_mB816E7BD86821B89362FA4AE12C11628C0CB7061,
	AI013x0x1xDecoder_checkWeight_m942BB18994811E65424D98A4E0443482BD06E36D,
	AI013x0x1xDecoder__cctor_m0AFCDB94780C22917D4B1A0840437F0AB504CF28,
	AI013x0xDecoder__ctor_m2525C3A34D8023D521AFE338900412D3C614A9A1,
	AI013x0xDecoder_parseInformation_mAB1E19A53D6A8F42639B9BDDEAD53B9D6197BDF1,
	AI013x0xDecoder__cctor_m9E17F0AA26CED529E826A97D14FBB3B99DD25B39,
	AI01AndOtherAIs__ctor_mF742689F33C79AFD81FB062BE692ABF1627589AA,
	AI01AndOtherAIs_parseInformation_m6EF5AF4DF9EE1A08E94915CBC66E35CF8C5CE1FC,
	AI01AndOtherAIs__cctor_mDDA61F3317EB00392B2E74A035C4A3D60D77A213,
	AI01decoder__ctor_m4A31028D1306594903584A411AA3AAE4E8F52EA7,
	AI01decoder_encodeCompressedGtin_m9C0F4D2905DDAA7B7C8B3B66986463150E965F23,
	AI01decoder_encodeCompressedGtinWithoutAI_mF388AD2B0EDE9778AF40C04992AFBC4E205B9401,
	AI01decoder_appendCheckDigit_m79D074FE82E8F3A862F855441F47E93460F28CE2,
	AI01decoder__cctor_mAA032C6D8586A87C3E3F712C457919D200AB2299,
	AI01weightDecoder__ctor_m29D4F73E3322364D9342E2125033DBC5145E2826,
	AI01weightDecoder_encodeCompressedWeight_m881F6B15B9BAF4CA0389BEB81EB25E6745492290,
	NULL,
	NULL,
	AnyAIDecoder__ctor_m94B00C3164F6CD7D246311B6BDBC87D3BD448C3C,
	AnyAIDecoder_parseInformation_mD9E82CDBCB2C3111866140893C41726AA6FBFA18,
	AnyAIDecoder__cctor_mBB14CF9E7627E0F7DB5101B73C09C70F5E9712B6,
	BlockParsedResult__ctor_m6EA5AA15CC5B7B7001744DA4AFA4AD4309ED6BBB,
	BlockParsedResult__ctor_m3F31FFE19040DC4DB80CCA5C11A9D8EED4673167,
	BlockParsedResult_getDecodedInformation_mE6D6775171844E1F00595D195E36BCE54932709F,
	BlockParsedResult_isFinished_mD02B0DED81509D36D6EEADDECD358BC7AF2510C5,
	CurrentParsingState__ctor_m170790F16E1E9616EC0579D98EC7B046BA85FA0F,
	CurrentParsingState_getPosition_m5AC694B2AFAC740FB6CA61866BA0ECF20958D5F4,
	CurrentParsingState_setPosition_mA95AC8771627C4AE4F0BE8F2C85A0960AB1030A6,
	CurrentParsingState_incrementPosition_mD9C1E1FA905D0082C588E4A1E7A9DA0632B8DB9E,
	CurrentParsingState_isAlpha_m9EAE46E22EA5DBC6777A44BB42A23F4679852E98,
	CurrentParsingState_isIsoIec646_m7A0197F3BF5D2284D3F0942C9FE93E647E4E4B35,
	CurrentParsingState_setNumeric_m84E36C2B3231F4ED636896066E41C1142B5E3BCF,
	CurrentParsingState_setAlpha_mEB9D2A978E74AA0195BDBD4EB48656E12364441F,
	CurrentParsingState_setIsoIec646_mF16FB7134352AFEA8F52847D2F6AE4C34FE9B6D1,
	DecodedChar__ctor_m8DC1E11B4513C8DFF8D51C26F210DC073955CE44,
	DecodedChar_getValue_m18AA998A9CB0D566B4AE53697D08A2D487434E45,
	DecodedChar_isFNC1_mBCAB80A8F27EB4FA6DB35EFF7EAA05D393C0B330,
	DecodedChar__cctor_m236091BA5198C0E8C55CF88BBC80870924DDB243,
	DecodedInformation__ctor_m4998E40E554520364E4FDB0405888555E5925366,
	DecodedInformation__ctor_mFF70533CD1B84EF014571E06853F8838ECD5E052,
	DecodedInformation_getNewString_m23BDC1AFD69EBAC29404FF5E684CD94E4347BBC5,
	DecodedInformation_isRemaining_m0013A51713594DEC9E5354A7473296090B15153D,
	DecodedInformation_getRemainingValue_m1D126F7E01A3A9D31A7AA13B98BFFF3AF0CD4593,
	DecodedNumeric__ctor_mC9B474FEB6BF034189AC91BBD114CFC3D9F54982,
	DecodedNumeric_getFirstDigit_m8BE710F8DD7B7A16B78F8EE18F71D12AF849F781,
	DecodedNumeric_getSecondDigit_m8426B5D553110DD48E9EC4100FAC49E492F75590,
	DecodedNumeric_isFirstDigitFNC1_m90BB548D517532F033F688A03487B4C7224B7811,
	DecodedNumeric_isSecondDigitFNC1_mE9CD925AFFCC93A1B888FC7435CEE42CF1BC8E6B,
	DecodedNumeric__cctor_mF78A1176F78628EEA09C85BF73A4BF24CA11F125,
	DecodedObject_get_NewPosition_m995F3D4A19B254CF37BB255961914C45CBDB9D99,
	DecodedObject_set_NewPosition_mC16722AB1F55B5ABB4C3CDD32D251A20DBB0B3EB,
	DecodedObject__ctor_m5F181A52205913AFE8ED145665786A60836B5DB7,
	FieldParser__cctor_m83FB3714F17A6E8601096072314ADD6F63B00F77,
	FieldParser_parseFieldsInGeneralPurpose_mB02CFEC87C7C164CECFC8A711D83492396ECDC9F,
	FieldParser_processFixedAI_m900F355C6652EA30CBB0F988A22E319F62BE0C8A,
	FieldParser_processVariableAI_m9B0C976DB90E21E5BD6DE9ECDC1FEAD8B4E5AB15,
	GeneralAppIdDecoder__ctor_mB707FE2F1A1103E225C23CE5C23115B305633768,
	GeneralAppIdDecoder_decodeAllCodes_m75335EFCE0496AF24389835C2C819970ABBBA4DF,
	GeneralAppIdDecoder_isStillNumeric_m372D24F2D1CB1ACB08CE61BA9F71870A01938EC7,
	GeneralAppIdDecoder_decodeNumeric_mF17AFC19980D1F0AB422C4B174BECA1F0A17CCEA,
	GeneralAppIdDecoder_extractNumericValueFromBitArray_m7FF4BB86F9D07141618824C59E5F12696AB61EE3,
	GeneralAppIdDecoder_extractNumericValueFromBitArray_mE1B90F38F45D70C7BC2D41CD5E4E377A156999C6,
	GeneralAppIdDecoder_decodeGeneralPurposeField_m4474370A3F232CFA931CB93816F952787F4D8372,
	GeneralAppIdDecoder_parseBlocks_mC87807DC83286C55A52766F235F23D70660BF7E9,
	GeneralAppIdDecoder_parseNumericBlock_mFD020CE3A6AAB72D7FD317AF441EE7177E454B4A,
	GeneralAppIdDecoder_parseIsoIec646Block_m418361A8A7DCF1E2141EA64CDFE05A3E735A75A4,
	GeneralAppIdDecoder_parseAlphaBlock_mF8BB16CEB151EDF92611BF7A763BAC8261D7ECF2,
	GeneralAppIdDecoder_isStillIsoIec646_m5E5DE35362E6843D99C0BD84C7DE9976FA92BCAF,
	GeneralAppIdDecoder_decodeIsoIec646_mCA511B46280294FB54A1C2187997516648D28135,
	GeneralAppIdDecoder_isStillAlpha_m728B87258D5E598A233867A42939B692ED904674,
	GeneralAppIdDecoder_decodeAlphanumeric_mBCAC630EC16B84A5C4156EB0DD1C3CE969B3C967,
	GeneralAppIdDecoder_isAlphaTo646ToAlphaLatch_m50E16F01815D7733528FCD23C3CE7806151D2502,
	GeneralAppIdDecoder_isAlphaOr646ToNumericLatch_mF9C16B4F786161CB166AD6968D2AA54C43B44033,
	GeneralAppIdDecoder_isNumericToAlphaNumericLatch_m591C0AA3C2D8EC6C81A4DB63A059B82531AE4191,
	MaxiCodeReader_decode_m2F48E27BE1CB0B35CA0A2FF3FB3C95A55BD984F9,
	MaxiCodeReader_reset_m7AE345762EDECEBB448F27D2207506E9EEDBEE50,
	MaxiCodeReader_extractPureBits_m981377EDECDB3326CF9E1F6D53470E5AAFD3F929,
	MaxiCodeReader__ctor_mF9EA7736AF6956580175D21B9FA6BA59A0B7BB6E,
	MaxiCodeReader__cctor_mEDA5A21485BF82C698A4148F3665BE16B20D705C,
	BitMatrixParser__ctor_m6126F72F80D60C0007BA852ABAEFE7F7A5BE2373,
	BitMatrixParser_readCodewords_m39840CCE52B5BBAF58D70C42D3A4C58EB827B972,
	BitMatrixParser__cctor_m8DFFCBE1842142AEF376E813CEB5C436B2D8F351,
	DecodedBitStreamParser_decode_mA053FD2AF2C324F88EDB84F9FD0C6EBC61302F6A,
	DecodedBitStreamParser_getBit_mE8FC742850F730B5A1CF705536DF14721631DE0D,
	DecodedBitStreamParser_getInt_m74D349CD3A0D0216856D99C37A3EE22CD884FE4D,
	DecodedBitStreamParser_getCountry_mE875E77435231C568678556412E497AE9A554179,
	DecodedBitStreamParser_getServiceClass_m2BDB4929AED4A8DF62018664A21741780ACD3168,
	DecodedBitStreamParser_getPostCode2Length_mD377BCA316788B0603D30FECEC61A684A01995A8,
	DecodedBitStreamParser_getPostCode2_m54054686A72F4EC4352A45482A24CA95B2DF696D,
	DecodedBitStreamParser_getPostCode3_m688A9517F5CB134A6B876A3A108C47726FE97BF8,
	DecodedBitStreamParser_getMessage_m983CF13314B4487E80808EBF42151C44A5DCF800,
	DecodedBitStreamParser__cctor_mBA6D4418D6CC386688A7B75A102F6F46F24880E7,
	Decoder__ctor_mD15198885F26178D3E554947A4567F237B67E134,
	Decoder_decode_m7FB80720A904691FD1F1E72B39042D46C8E3B770,
	Decoder_correctErrors_mAABBD4168FDA8AD9241554EEF7ACAC80CC96714F,
	IMBReader__cctor_mBE3570CF56CFBDBD926D349F9B2C539DA1333359,
	IMBReader_doDecode_m2A9B60B0370010EBC8F30F82E8DCAF69D948CE56,
	IMBReader_reset_mBEC815D05225D6A5C3BE2B059EE845159DDDE8ED,
	IMBReader_binaryStringToDec_m58A4FCF3F46EE94ED6B8AD10FA9DE5941A40BD2B,
	IMBReader_invertedBinaryString_m7CF101B6EBA77EC0831AF40A0BA3AE0B29CF801B,
	IMBReader_getCodeWords_mE723FD52CCA0D65D483DDCBA3393FC420D9A7F6C,
	IMBReader_getTrackingNumber_m5667D86D9E2FA0FAEBDAE334D7A494E7BBDA3E5B,
	IMBReader_fillLists_mF2EBD957653E8D65115E66725EAFB1314EE52B5A,
	IMBReader_isIMB_m0C7DA3AE7C6549EA669C8070E37045BE37830F69,
	IMBReader_getNumberBars_m5CDD15C3F4DB3C4EA4D631B24C778853E3C2DF54,
	IMBReader_decodeRow_m894CA8C8D2229207D81B9AD6FCD51E6ED2B26203,
	IMBReader__ctor_m19281820CC2C6EB40472A522525CC853A9C6AECB,
	DataMatrixReader_decode_m9667D4B23324EA969572EE1082035A6B8B670D9F,
	DataMatrixReader_reset_m5B52298DC5533019464421E7AB7AE5BDF765DF81,
	DataMatrixReader_extractPureBits_m55B4D00DB84E353553E27DC2DB6AF305F4E2DE94,
	DataMatrixReader_moduleSize_m65DE18DE19765DEE2F10F8E256F77A4A9672D151,
	DataMatrixReader__ctor_m85BD882FF446AE0CB2CA0D74C66C20CFDC65601D,
	DataMatrixReader__cctor_m462F62CC105416CD5B1E216C0C707F657D1EBF72,
	BitMatrixParser__ctor_m386F5813F52BF7AE3AD4BFF936D936C92A330605,
	BitMatrixParser_get_Version_m6C900B2956E44520D95A94CFD07604D3A76984FD,
	BitMatrixParser_readVersion_mDC38CF42367C9CCC61B0F77798D941A1E695C85B,
	BitMatrixParser_readCodewords_m29BBB695C8226588373A8AC1853297F5221AAF97,
	BitMatrixParser_readModule_m569B5F40846EEBF8D3A464B36B487AD3373154EA,
	BitMatrixParser_readUtah_m1D28FA81053475B9098633147A1B39E0320F80E7,
	BitMatrixParser_readCorner1_m20BCD4E9B975BAA3151DE45A742D9116CEF7CFC3,
	BitMatrixParser_readCorner2_m7569681C0C715115ADB02D279B9760FF866DABDB,
	BitMatrixParser_readCorner3_m6783A509F41B926DBDDF76C099CDB5F9AA86DDAA,
	BitMatrixParser_readCorner4_m5759ACE192A4FDD52F5884872874C261C671C8A3,
	BitMatrixParser_extractDataRegion_mAC8057721ECA85F1DD55E45D48342F26913CFF5E,
	DataBlock__ctor_m3586871A95E9094BA03371DA805958C175AA3419,
	DataBlock_getDataBlocks_m37BF2B9FBF9187AF4514FFDB8FE949F564B3BFA6,
	DataBlock_get_NumDataCodewords_m3DAE253C0A7FAC38E308C613222229CEA9839E62,
	DataBlock_get_Codewords_m2492420B306698FD94DFA1699B133A7DF6500393,
	DecodedBitStreamParser_decode_m4EB9F6FA66194EA03B1D611AD9F4AFFAF9A81635,
	DecodedBitStreamParser_decodeAsciiSegment_m01EF6EFB97412C80C47F0E44B3C699B278C417FC,
	DecodedBitStreamParser_decodeC40Segment_mF54F17107786036F76699F86C03EB4A11B12B008,
	DecodedBitStreamParser_decodeTextSegment_m6A55FA2F92A488AD358484A759BFEFC5A7D77CC9,
	DecodedBitStreamParser_decodeAnsiX12Segment_mDBC6698B6BE8A111AC17BE0B43ED6F5AE2E861D0,
	DecodedBitStreamParser_parseTwoBytes_mB33EE53F80C351F5D938F6D838DAE4D28945ABBF,
	DecodedBitStreamParser_decodeEdifactSegment_mE5A97D2D46F3D78B45A99431681BFB087A2E1604,
	DecodedBitStreamParser_decodeBase256Segment_mD863AA16E8EECC3607BCEFA5A1EBA2A9454FD1FC,
	DecodedBitStreamParser_unrandomize255State_m0DA91DE833A8851A472008557E600FEDD387F57D,
	DecodedBitStreamParser__cctor_m01BE44C8AF8DC8C9837F14FA2DD74B90E29D42BB,
	Decoder__ctor_m0C30E2BEC990C74646C847BB33709DBE3DECAECB,
	Decoder_decode_m45FAD83731B072C0A21DDE168C61BA34A1EEC7DB,
	Decoder_correctErrors_m6FF19BFFCCB2139A5B0AA904B7C204B374B7477E,
	Version__ctor_m077FC3507BDA98E6745F77BD591712AFF6B421AF,
	Version_getVersionNumber_mC240D587FD546AFAA9813B4B8BF21BEBE794554B,
	Version_getSymbolSizeRows_mD4ED138BC6C1605D3849C4E25B30D01D3F415043,
	Version_getSymbolSizeColumns_m6DBAB1F1A2B07654B4924D8743C462FD2195928B,
	Version_getDataRegionSizeRows_mB35A7756D8B4E8771CF1AF9D2512F1523E99A895,
	Version_getDataRegionSizeColumns_m7510DAB7150212610408F59B466CBCDBBB2F539C,
	Version_getTotalCodewords_m053BB70F73200244636554135BFC54F4F927A19B,
	Version_getECBlocks_mC10391CC1DE3D77DFDBF84AB96E20568885FAC15,
	Version_getVersionForDimensions_m5AF579DBF7B425B8955E5049EB9FD4329F665B17,
	Version_ToString_mBFD16FC2B61F9E76FA2E6EB798222772D7A286A5,
	Version_buildVersions_mBE21DC48893776F483675E749F37E1114F16C6B8,
	Version__cctor_m383D2B792F513387929F5A46B748C7DEDE53A84E,
	ECBlocks__ctor_m5A1EC8BBA032650CDF7599CCA6C47E433E4ACB6B,
	ECBlocks__ctor_m36FA0B4968370547116ECD37E8CD4723277379AE,
	ECBlocks_get_ECCodewords_mA046C3872AF9D771F6679BC80E18D4DB07927BAA,
	ECBlocks_get_ECBlocksValue_m2932E2682570E3954B2C9DAC28850342777821A8,
	ECB__ctor_mFB2892A387586B52357CB5496BC67ABD357DED1B,
	ECB_get_Count_m36046552D97F93739FF93F152833AAEA321D920A,
	ECB_get_DataCodewords_m93C7355C18323CC41766125F732C858A0725FB1A,
	Detector__ctor_m1A3DE9E277A16A11FA8E4E884F65CD14F135A842,
	Detector_detect_m23B78B82C4B007A540A3EB67579CA30EFA1ED559,
	Detector_correctTopRightRectangular_mAEAF34BE6032672C5A4472A5227DA0B7A3544C49,
	Detector_correctTopRight_m86D8CACB051377ABBDF69890B1DFAEF73CC1DE77,
	Detector_isValid_m14AA69078ED2F79C1F803B6FCA5D1A54F1C8740C,
	Detector_distance_m49C82D31D47ECB452FEC5B5AB8EBC07C7568E492,
	Detector_increment_m7C12AD7D2D06A1A6D10799149F563F676B057D65,
	Detector_sampleGrid_mC3B9777B9F11EC3AB6F87490B35A194EEA7C3260,
	Detector_transitionsBetween_m2B9B8E029B5F2EE0C163AED9E88BFCA2813864A8,
	ResultPointsAndTransitions_get_From_m397F3F22B4123B09B1639DE275F8311DF9D40716,
	ResultPointsAndTransitions_set_From_m0153FBD64ACD0726ED99FD71B1A5F1568CF99985,
	ResultPointsAndTransitions_get_To_m5E0366DC66B5FFB3293C2DF2A483B0EDD8A0CCFE,
	ResultPointsAndTransitions_set_To_m2A80928FA846D7B6973D55E7369B8E727BC4ED6D,
	ResultPointsAndTransitions_get_Transitions_mF81AFA64929BD4E3BB92E9E4E443F9BC8F0ED172,
	ResultPointsAndTransitions_set_Transitions_mB68EAC6B5646D49715EDAE3CC8DA4D6CA2E5D927,
	ResultPointsAndTransitions__ctor_m93CA3CE4208D5880629A3B07C230100E1CC0A408,
	ResultPointsAndTransitions_ToString_m6EAA6FB9BB8C6BE81D5BE8D61E6376D6238F1735,
	ResultPointsAndTransitionsComparator_Compare_m9DCE83B6D7A89A8E5F28AB71AB32820E5AFC45E4,
	ResultPointsAndTransitionsComparator__ctor_mEA8E6FA2CD0BD25E539D9E248043981B9E93A4CB,
	BitArray_get_Size_m515BDB7E05CABB85B3CF72ED50AF51F89A51ED64,
	BitArray_get_Item_m9D13930253334ABFCDA32E1823394164DD51C936,
	BitArray_set_Item_m8D909D8C966AA5EF13AC3394E5084A214FA2FAF9,
	BitArray__ctor_m0E62557F33A96E8DFB15D9C0CB105A192A0168F1,
	BitArray_numberOfTrailingZeros_m0295CDB35BEAA90C7828B40CE2DCF0CFF4C33424,
	BitArray_getNextSet_mF4820F737EFB57D2FC673C813DD85212247FC3C6,
	BitArray_getNextUnset_m52ECE8AB097792EBD0C18F31E1E9E5E17279E4D3,
	BitArray_setBulk_mD99299B2C2FB6A694043DC8E57C68EA578C4247C,
	BitArray_clear_m560674060049FCA382C2BF398C5A3A81D2793E81,
	BitArray_isRange_m60A143447BB1C594B147D8C8129587F55D5320DA,
	BitArray_get_Array_mECAB2579BAEB0354C48454014D8AFBB06F5901F8,
	BitArray_reverse_mA70D97C502F68EC09EE7240A75CDFD3D598E1F9F,
	BitArray_makeArray_mB70B8A6ED27BE20E1B1858D768E3984596EED992,
	BitArray_Equals_mF452D2987F58EB25628BD428A09A8E9FF207D9A2,
	BitArray_GetHashCode_m0529935E99247D2CB21569099E337A41A7F0222D,
	BitArray_ToString_m8B3EE96999F115753716A9A53BCB152577F6E911,
	BitArray__cctor_m53C6769E65798B25E6D906EB3F8FB4236640D9B6,
	BitMatrix_get_Width_mD5565C844DCB780CCBBEE3023F7F3DB9A04CBDA8,
	BitMatrix_get_Height_m277BE26EADD0D689CB8B79F62ED94CC6414C1433,
	BitMatrix__ctor_mB63ED20D6DC99156079AA7C2F2E1209817F387B2,
	BitMatrix__ctor_mACF00B24CE279465B8BCA1720900297C3A2C54F3,
	BitMatrix__ctor_m98A83F4452563A005D53D40DCC35640DFA90CB80,
	BitMatrix_get_Item_m841087B724849DDBAC8E8BC5891CDEDB8D18A475,
	BitMatrix_set_Item_mEFF2D0155D20FAC7EB7B14F2D8472AB23ACE12F6,
	BitMatrix_flip_mDAA9A98182F3F6E069C28565E04D88318BF7666D,
	BitMatrix_flipWhen_mD0356F5448B6E953839AD33F451B53879FB29605,
	BitMatrix_setRegion_m60080927EC9438668A7C73F95AB1D29157D46614,
	BitMatrix_getRow_m64D830BD70060BD2CC5E97B67A4E40860E9142B4,
	BitMatrix_setRow_mD646D01811B9536E03D884F55F440F336A4A6110,
	BitMatrix_rotate180_m7FBE958C1A6844CBC9C2C72605EE1362A2F17989,
	BitMatrix_getEnclosingRectangle_mF04C4D6C33E3B88E09252DC04260C83ED84F2B16,
	BitMatrix_getTopLeftOnBit_mB0BBC334C2D0A28C95C11F3E7FA51CFCDDA88BA4,
	BitMatrix_getBottomRightOnBit_m2876C94848272AD26D9DFACB475E55FE9EDB3106,
	BitMatrix_Equals_m734D4BD675645EE531E7457588155F9E6A2FCF00,
	BitMatrix_GetHashCode_m30A56747DED375B1AF8E9497F90601A779EA8A91,
	BitMatrix_ToString_m308533C5AC77EB9A2900DE488E24DBF13FC639B4,
	BitMatrix_ToString_mC0A89CC33AFD0803601102EAFEA5FC16F56E5CE2,
	BitMatrix_buildToString_m05D69D38E2971266B9FF6D55ECC199D605B1B86B,
	BitMatrix_Clone_m7F2489C0DDC17A91137EF32E9A8B5704840911C5,
	BitSource__ctor_mD93557F924046DFE4389990A8276351D0FE40FCF,
	BitSource_get_BitOffset_m64D9DD8FFDD2C3C103C2A11B029A2FBC0ABD3DFC,
	BitSource_get_ByteOffset_m80607FC238F3C86CA2E85A166C6CC73BC7F405FD,
	BitSource_readBits_mF96D3807EE0A18870A2B274E4671B6514ACF7B2E,
	BitSource_available_m94210E66EF66047858FB5FBB09D814A9256AA530,
	CharacterSetECI_get_EncodingName_mD295E1546472DE6599C8FE420851951CCFC616E6,
	CharacterSetECI__cctor_mB0DD18E66CB48807E5E388524E9365BDC808C0C0,
	CharacterSetECI__ctor_m8C4C5075868AF85208289A7592652C5FD5564BCA,
	CharacterSetECI_addCharacterSet_m53BB2C61B9EB98229CBB81A05761C349D4C5BA70,
	CharacterSetECI_addCharacterSet_mC6EFAB88E93A181A9A7BA9461B068E89EF619EF1,
	CharacterSetECI_getCharacterSetECIByValue_m06163BD09B9B9B62657249B391E2B918AE8DDBD8,
	DecoderResult_get_RawBytes_m43CA9502BF6A26C8D70D0BFDAB51113CC353E9D5,
	DecoderResult_set_RawBytes_m987DFC589F07D732CBB655528686CE23A23A9C53,
	DecoderResult_get_NumBits_mD3D9C73C3F9DC29C45E2DABB5E96101829E4FD17,
	DecoderResult_set_NumBits_mCA6CD50AF39849DF204C1AF96DF35E45C35B1E95,
	DecoderResult_get_Text_mC0B1F01D2776183DA406FF6726629B46F905EE7C,
	DecoderResult_set_Text_m378794910F12A57EFE7501C00B5FD5E7D9C8238C,
	DecoderResult_get_ByteSegments_m32CE56F80C15164051C7C3AF3619C62DEDECF6D8,
	DecoderResult_set_ByteSegments_m7A822E31AEDAB0567A23BEB7E853148AF97CAC2C,
	DecoderResult_get_ECLevel_m29BD97206A9E780E851942193D1247119284FBD2,
	DecoderResult_set_ECLevel_m3FCA51695B6A6294FF8838F64979774A0F6CE971,
	DecoderResult_get_StructuredAppend_m072F7507B09F85A21148F5F56229DD9F072B1DCA,
	DecoderResult_set_ErrorsCorrected_m124A1F030FEB55F836070E8536E938BF3E4D49F9,
	DecoderResult_get_StructuredAppendSequenceNumber_m96111256F7098707D35BE947261A1692F22788E0,
	DecoderResult_set_StructuredAppendSequenceNumber_m0B2B6C0023B77A8A777FF06BB2EDFE35CDC8880C,
	DecoderResult_set_Erasures_m08B47141BC1467C774335C8BC6E150C263A40E5D,
	DecoderResult_get_StructuredAppendParity_m66FEE817E38BE7875BA5F0B0344C10401CB429AC,
	DecoderResult_set_StructuredAppendParity_mFB96734D5857717C16310E554B3B4BCBC4FD82C0,
	DecoderResult_get_Other_mBA4D7C335AAA256D1D0CBE916AF94439125B980D,
	DecoderResult_set_Other_m090AF96413E60D86B7819D00AD305EABC160489A,
	DecoderResult__ctor_m3031DEF2DEF284466CD171FE60098AACE48DA15D,
	DecoderResult__ctor_mF7A0215EF3F469A3C965E44E02D62ED208FD8550,
	DecoderResult__ctor_m6D2E3A74B82FABADCB3A540B82498DE40D89416E,
	DecoderResult__ctor_m2485EEE788AD63BE059467EEDCBE83FD081E6663,
	DecodingOptions_get_Hints_m49D5EDE8C24D2C69569F27E6B07CCE1A35EB578F,
	DecodingOptions_set_Hints_m7713D3EAF7335F7964F81FF3DBA36D64E4173716,
	DecodingOptions_add_ValueChanged_m473222ED91D5FCEE2C1FFC48C88242408BB24C33,
	DecodingOptions_remove_ValueChanged_mE667F8E7EA37EA3F6410AB0004C53780A5D4806B,
	DecodingOptions_set_TryHarder_m7BE8D074E88BCAD7EB2077AA78FF1D04E4D0B6DC,
	DecodingOptions_set_UseCode39ExtendedMode_m49CEF8D8B9A01E7C95936517FCE9827FFD9AA4FC,
	DecodingOptions_set_UseCode39RelaxedExtendedMode_m5F57554D05EEE1AEE7D4FCC412AF2284B0E75337,
	DecodingOptions__ctor_m61AA5BB8BBC86EC171973CB29A9C1EC9B2A76568,
	DecodingOptions_U3C_ctorU3Eb__43_0_m1588EBB00CA9EB20A446A3776CC4393F0CFE4331,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultGridSampler_sampleGrid_m3181D8DB3C9D314099209CC74F516C20173B8476,
	DefaultGridSampler_sampleGrid_mE2DBADDE1C4B49660CE83BADBF75443CD630D925,
	DefaultGridSampler__ctor_m4FABA9538E02F9EBD362386B502CE32554143A14,
	DetectorResult_get_Bits_m848D10FED0407284D761433AA00A4CB7FB442D1A,
	DetectorResult_set_Bits_mD70104166B615ACA1AD715C218767A4527906945,
	DetectorResult_get_Points_m15DF794C156FD71EEAC5113F7974F991368DDA49,
	DetectorResult_set_Points_mF5A0B8D443D25DE03EFE0962F2FAF1C2FA62DA36,
	DetectorResult__ctor_mAAC90FD700826AABA68D0E3AD23C520588FF4280,
	ECI_set_Value_m9F9CA06BCDBAB6747CDEE9A4598D6B9BAF13443E,
	ECI__ctor_mCB64605A40D27FECF0AC217645B3BE6BDF0A6696,
	GlobalHistogramBinarizer__ctor_mB5702A85FAE32BE89C05D12348B80BA879A81F02,
	GlobalHistogramBinarizer_getBlackRow_m71E0396C1E7ADF19B279BB76AA7155C9B3A62169,
	GlobalHistogramBinarizer_get_BlackMatrix_mA9355137915D5A8208C8A56C426295650F1A204E,
	GlobalHistogramBinarizer_createBinarizer_m06B12CAD41720B5793FA46EE0040F3DD9E4CBB1B,
	GlobalHistogramBinarizer_initArrays_m40577BCD3306F34736E375A76B7D6FD2661A869E,
	GlobalHistogramBinarizer_estimateBlackPoint_mBCCF12D70FB5E832C46E8BCDCB45BB6A0FED224D,
	GlobalHistogramBinarizer__cctor_m8D46D8ADA2C6BB2D1FB750CBFBA2670449C93631,
	GridSampler_get_Instance_m5C1CDA117F9E4293B0102B5E3BE10632C59C0C23,
	NULL,
	GridSampler_sampleGrid_m09FA91280877A1A39321483EAD4490C1D625B610,
	GridSampler_checkAndNudgePoints_m41323B7C2F66036E56C5F99255C228011F1B3D2E,
	GridSampler__ctor_m2601B7518A0DD0A58FAF7C1916934DA7EDB92D35,
	GridSampler__cctor_m494589B6664717F8D28921B9837F1CCA6116905B,
	HybridBinarizer_get_BlackMatrix_mB3E2BCE7287E0043D3BC90DCFE77A21727373AEC,
	HybridBinarizer__ctor_mAFDF1DB99F02C26A219AFF9F31A442BBD6C1C1B2,
	HybridBinarizer_createBinarizer_mF8D1D404E27F231169285CE6C0274AFF5D607EE0,
	HybridBinarizer_binarizeEntireImage_mE086540DF37BE9162D97359B187CAE1D3E3DA1B2,
	HybridBinarizer_calculateThresholdForBlock_mDCCB89059065429B74C004A0E7DEF3F58DC77441,
	HybridBinarizer_cap_m82FBF47590D61793F96735321DBBD86DF5F70517,
	HybridBinarizer_thresholdBlock_m9A07C7FC134B67EEAEEB1C4E59C25B1FFBF4CE8E,
	HybridBinarizer_calculateBlackPoints_mF93296EB2A3289BCDE22863CAB111C7C736BF0AD,
	PerspectiveTransform__ctor_mC293E2B705E334C9F03786624B868ACFE25F86E3,
	PerspectiveTransform_quadrilateralToQuadrilateral_mC151B93BE07401428029CCB89FE900561E40C2C3,
	PerspectiveTransform_transformPoints_mA95BE5824327F70F2864FE894259A2DC13B28122,
	PerspectiveTransform_squareToQuadrilateral_m3653583B32B2C0E47C7F4C6F03E5C2685FC0E36F,
	PerspectiveTransform_quadrilateralToSquare_m6AEB402947628D3F755A1DEF6F2DECF1CC682F06,
	PerspectiveTransform_buildAdjoint_m7AE46903FA0CD91E0A6A880FAA4608943304F9B8,
	PerspectiveTransform_times_m6D739E36CFB8FF630FA15DD4410D885E95B96BB2,
	StringUtils_guessEncoding_mDA7EF9DA458E86FFC80EFA6DA7C5A1C7D4D7545D,
	StringUtils__cctor_mC2C0BC331A6E448A65042C34596B4CB85ACE1C43,
	GenericGF__ctor_mE3666ECD617DD1B225713360DFB9824147CA094C,
	GenericGF_get_Zero_m4F0693F9A8302834547052C3CEA776FC7741F58D,
	GenericGF_get_One_m945193587CD2278056A6EB80421F64BEBBE275FD,
	GenericGF_buildMonomial_m7A21738A2F293ABCB76386FBA0C071DC58DF2E96,
	GenericGF_addOrSubtract_m82AF5317D77A4CD3F9D058E5C1A1CBF6B557D921,
	GenericGF_exp_m1CCB3C8792B2D6E7DADE1DF1701C0A1695D31EB8,
	GenericGF_log_m46FB0CC7848529FA72608AD004C670A7126FF991,
	GenericGF_inverse_m39D5FF366CE86657B6ECF3FB1AF07BFE3A3C3BAA,
	GenericGF_multiply_m0F0CC4B3CDC5B8B784E1920FC05CCDD6990E327C,
	GenericGF_get_Size_m72D4414196C31B8A73A5F114EB3C2509241C7B30,
	GenericGF_get_GeneratorBase_mEF5BAB60B96C4ACDF59F048361FB31F79D2D8FA7,
	GenericGF_ToString_mBE0D0E467C849624166EC3CB206BBFB6D1CC2AF0,
	GenericGF__cctor_m0E768E8F28B963346FA6BCEB3938BB686ABBC4BC,
	GenericGFPoly__ctor_mC1293E5572D4CE31744EECA65323674BD18BCBC2,
	GenericGFPoly_get_Degree_m37346473B1702D9AD3523314A39D053B55ADDDB8,
	GenericGFPoly_get_isZero_mBC8A23CF56F0497942B6474544E00562C4B60DA0,
	GenericGFPoly_getCoefficient_m0D0151C87B80141BB1674C20F3C79D5AA6227CAF,
	GenericGFPoly_evaluateAt_m4F0A0A8982D580D37FD26009B4F9E501D9CF009D,
	GenericGFPoly_addOrSubtract_m8A6A0D1C87B7FD0A387C346B7F8555F1B2E96FFB,
	GenericGFPoly_multiply_mDB3E82073EC6032A8C6140178A03C4368865D39D,
	GenericGFPoly_multiply_m918F138BD54E0FEC2C9AD6B78AAF04E162109B9E,
	GenericGFPoly_multiplyByMonomial_m7932489896A2263AF636AE2A0BA6657A602713F0,
	GenericGFPoly_ToString_m89CDBE0BB7C0996C08973224B77782854481A10E,
	ReedSolomonDecoder__ctor_m628D323F539C8056532F489744FD7C9DFE30B0B9,
	ReedSolomonDecoder_decode_m3AB50DCD211EBEF5D95FB2274CE77D2E987F8CAC,
	ReedSolomonDecoder_runEuclideanAlgorithm_m71BF4612250474C1905B4339485A232402DBEE3E,
	ReedSolomonDecoder_findErrorLocations_m9DD7E6AAFE5AF94FF51C8533BCC705ABA863B0FA,
	ReedSolomonDecoder_findErrorMagnitudes_m0C22486723C3C4AF540ABBB442F894526D767DFC,
	MathUtils_round_mC7908FACB18165A1524BBA3C8DB4724096EC4A60,
	MathUtils_distance_mFD733773EEBEB9B2DE497DB56F877DE8241841EF,
	MathUtils_distance_mF24D214F6061CEBFFA827EFC7193450F9547E453,
	MathUtils_sum_m77477B82A6899352E9109B0E996D8C70B60E0577,
	WhiteRectangleDetector_Create_mB7E5C4CCD1DD3D79EB0581A45071CE02B0DECFDF,
	WhiteRectangleDetector_Create_mCF22DCF654C8C8B88AB289C3A1C9C14D58253658,
	WhiteRectangleDetector__ctor_m65D6591012DC7B1053F586C40156AE32937F67DE,
	WhiteRectangleDetector__ctor_m9F3823CC6BF260491B3FD35FD585AE8663AE52B8,
	WhiteRectangleDetector_detect_mC702724840AE187F538791BAB7DFE6DFAFBE9110,
	WhiteRectangleDetector_getBlackPointOnSegment_mBF3542E810C0709EFEAE974EEB27EA8DB9A6902A,
	WhiteRectangleDetector_centerEdges_m627FE076E322023CD2E80416A1D2AE41CEBAA6D3,
	WhiteRectangleDetector_containsBlackPoint_m1C0466E4BFDAAE2B7A53049B3E8D5931ED466482,
	AztecReader_decode_mE88A7D2F05A41C7802D22DFB1CC4E454C12F4DE9,
	AztecReader_reset_m535FEB982E13CEC319554B602A65045810F084BD,
	AztecReader__ctor_m26BF9B090675D5850A44A180BFD74937D7687AC8,
	AztecResultMetadata_set_Compact_m72E8B973DA813D4E082C976D8A0CE6894D134914,
	AztecResultMetadata_set_Datablocks_m62A367370AA07C9FC6423F8765F9EFA9781A6904,
	AztecResultMetadata_set_Layers_mED123A0C8E0BE7D7DF2CE6E16000B903A4191009,
	AztecResultMetadata__ctor_m56BE57B664645174E01DC70B4B17A27243CE998A,
	AztecDetectorResult_get_Compact_mF9786EF4B4AE2046D4EBC1ACE4421CAF0D8D635C,
	AztecDetectorResult_set_Compact_m5746B4844627342B9D01FE94335BBCB3CC2F27D3,
	AztecDetectorResult_get_NbDatablocks_m8C8449F959B74EB58C27F13F685D657A6350EC83,
	AztecDetectorResult_set_NbDatablocks_mAF024D9BF8B752D63183CB8A9DC176B3C2231D4C,
	AztecDetectorResult_get_NbLayers_m1F624B4B6E994D0DF191D19CAD9285938D14D648,
	AztecDetectorResult_set_NbLayers_m724AC660E7B21D40509ECFC080450022B738C1A8,
	AztecDetectorResult__ctor_mC4CD35739E1C542CB29CF62E4EA1FAB5229048DE,
	Decoder_decode_m7EAE3A74A31B4FB8A18460F548875740B09C9162,
	Decoder_getEncodedData_m30D10AF659536860A7F98FDA49C75FDD3273E777,
	Decoder_getTable_mEF9C9DE887498B67989212F9DC2E172AE8225859,
	Decoder_getCharacter_m91E43E10ACE8D523FDA429AE72A6AAFFE9D51D33,
	Decoder_correctBits_m439E671C3921055BFAB1996E721A1A6CB846F951,
	Decoder_extractBits_m1BCBB3F1E1E7AC5F5EE4A156B2A527BE45069889,
	Decoder_readCode_m44E46F2EE3196F637AF94D310D83332BCB0B3E20,
	Decoder_readByte_mFAF8940288212954D760D74A13E3226DC2B4630F,
	Decoder_convertBoolArrayToByteArray_mC2F4F6B5D14D6859A024980E1FA0FC3668C9BB03,
	Decoder_totalBitsInLayer_m32B329C489599BA59D5EBB61DF079D3BE896F7F2,
	Decoder__ctor_m2FE2AD2398737333864D6E06BFB1653467423B5A,
	Decoder__cctor_m445CDF8DC20BBFED4F21786E10B39E7862354F2B,
	Detector__ctor_mCEC1BEFF4B3F80325F93D1D4951A5DEBAECA16CC,
	Detector_detect_m5EEF5A2093EEF14E34E8846806F614DA6D347D74,
	Detector_extractParameters_m0D152B13497848979C3ACCAA9831C1944165A650,
	Detector_getRotation_m72EF19FD4CF2B667853C1F50D75DFFB2E0E5244B,
	Detector_getCorrectedParameterData_m9FF9B8F0A45E8189065391FB7A9FE3AFD1D2934E,
	Detector_getBullsEyeCorners_mCCA9BC4A3AFCEB1213A476603DE28AA3637C484F,
	Detector_getMatrixCenter_m9BD358D27ACD756F448F7143369A29D8B632A169,
	Detector_getMatrixCornerPoints_m50C751791C62DB43B3611A222654200CDCEEF114,
	Detector_sampleGrid_mADFBCDE2C754101B8BA8C09C744A3B73E78465C6,
	Detector_sampleLine_m22A8D1777A1358B21E488B5C4B3048BAB4D72764,
	Detector_isWhiteOrBlackRectangle_mC5611EF60C2557157120DB4CB6756D08B8BCA2A7,
	Detector_getColor_mA5EC098FF25118981FA166662CA51D8E53A852D6,
	Detector_getFirstDifferent_m6E6550936843B87C7D4770AB70914CCEA3809C11,
	Detector_expandSquare_m35EA098FFC343ACA5D5D8B6658A29B76F516EB2A,
	Detector_isValid_m7174FD4F2FB8A6FF1079F4C0BB85C7D949A1BDA1,
	Detector_isValid_m6C9B3B35BA229984BCF86EEB0FA26E0A6729533C,
	Detector_distance_mBA1DB831E98AF1940A78943FEB6F41F07B25988D,
	Detector_distance_mA402066385A4DC576CD7851540B746414C1E25AE,
	Detector_getDimension_m49D17F0C4D700146BFB16FAC75F43BC5D89C27B2,
	Detector__cctor_mB3D360BA3FC96707A4CCB1FBBDB274FF4F22F71A,
	Point_get_X_m86CFB71BEA1B3839C4828369652A0E84F5207660,
	Point_set_X_m40FF2920644C9C750130BB161D8C2315CCADC408,
	Point_get_Y_m721D41396A31F942CC2EA8D6DD88B119EEE87605,
	Point_set_Y_m659A9819B835F6F0B0B8450F16887FB20E6799E5,
	Point_toResultPoint_m01A8B2007545A98090BD3DC9892954E6F0DD5E7E,
	Point__ctor_m3D624C53D3AE1A834D9F70F586FB0F4C025E3974,
	Point_ToString_m8D03498487474567D310763DA959A40D675373B7,
};
static const int32_t s_InvokerIndices[1189] = 
{
	1798,
	2183,
	1799,
	1812,
	1547,
	1547,
	2133,
	2147,
	3552,
	3164,
	3164,
	3164,
	3552,
	3083,
	3083,
	3550,
	3164,
	3164,
	3164,
	3164,
	3552,
	3083,
	3083,
	3083,
	3083,
	3083,
	3676,
	2183,
	1313,
	969,
	2183,
	1799,
	1812,
	1547,
	1547,
	2133,
	2147,
	1258,
	1258,
	3552,
	3164,
	3164,
	3164,
	3164,
	3552,
	3083,
	3083,
	3083,
	3083,
	3083,
	3550,
	3549,
	3496,
	3507,
	3164,
	3164,
	3164,
	3164,
	3164,
	3164,
	3552,
	3083,
	3083,
	3083,
	3083,
	3083,
	3083,
	3083,
	3083,
	3081,
	3083,
	2648,
	2690,
	2570,
	3676,
	2183,
	1313,
	969,
	1056,
	2183,
	640,
	412,
	2147,
	502,
	3676,
	3676,
	2183,
	502,
	2147,
	2147,
	2169,
	1831,
	2169,
	1831,
	2147,
	640,
	1337,
	1812,
	3676,
	1056,
	3676,
	2183,
	1337,
	306,
	968,
	769,
	2147,
	2147,
	2169,
	2169,
	2147,
	502,
	1812,
	2147,
	769,
	2147,
	1337,
	2133,
	2133,
	1812,
	2133,
	2133,
	769,
	2147,
	2169,
	2147,
	2147,
	2183,
	1812,
	1812,
	769,
	2147,
	2169,
	2147,
	2147,
	968,
	769,
	2147,
	2133,
	2133,
	2169,
	2147,
	2169,
	2147,
	2147,
	775,
	1337,
	1812,
	2183,
	1337,
	2183,
	775,
	2183,
	2183,
	1812,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2133,
	1798,
	2147,
	1812,
	1799,
	1798,
	411,
	201,
	122,
	980,
	1812,
	1812,
	2147,
	1087,
	2171,
	2171,
	1547,
	2133,
	2147,
	3629,
	3234,
	2848,
	1055,
	1812,
	507,
	1812,
	968,
	399,
	502,
	2747,
	1053,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	1812,
	-1,
	-1,
	3492,
	-1,
	968,
	630,
	1812,
	502,
	775,
	2183,
	3552,
	2830,
	2183,
	3676,
	3552,
	1812,
	2147,
	2147,
	449,
	2147,
	2183,
	1831,
	2183,
	980,
	2799,
	2133,
	2147,
	2935,
	3676,
	3676,
	2183,
	811,
	811,
	811,
	811,
	811,
	811,
	811,
	811,
	2613,
	2831,
	2831,
	2307,
	3482,
	2649,
	2831,
	3496,
	3676,
	2183,
	775,
	775,
	823,
	606,
	2133,
	2147,
	3549,
	3676,
	1798,
	3023,
	3073,
	3073,
	2147,
	2169,
	2133,
	1547,
	3676,
	2133,
	1798,
	630,
	3549,
	1258,
	1798,
	2147,
	3676,
	1831,
	1812,
	610,
	2133,
	2147,
	2133,
	2133,
	1337,
	3549,
	3549,
	3549,
	2147,
	2147,
	3660,
	3676,
	980,
	2133,
	2147,
	968,
	2133,
	2133,
	664,
	569,
	519,
	68,
	2147,
	2976,
	1547,
	217,
	502,
	1812,
	1337,
	1337,
	2394,
	2798,
	2412,
	575,
	858,
	366,
	366,
	331,
	664,
	433,
	2171,
	2133,
	569,
	519,
	1056,
	1337,
	2976,
	3580,
	2147,
	352,
	217,
	217,
	361,
	2133,
	2169,
	2147,
	1833,
	719,
	1833,
	719,
	1812,
	2147,
	2147,
	2147,
	3493,
	3676,
	775,
	2801,
	3035,
	3035,
	3496,
	3496,
	2183,
	2183,
	1798,
	1812,
	2147,
	1812,
	1831,
	2183,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	380,
	1798,
	2147,
	2183,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2395,
	3552,
	203,
	3083,
	492,
	2183,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	2133,
	1798,
	380,
	2133,
	2169,
	1532,
	2183,
	2147,
	3676,
	3676,
	3083,
	3552,
	2748,
	2748,
	2691,
	2345,
	2748,
	3080,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2133,
	1798,
	2133,
	2133,
	1056,
	2147,
	1812,
	2133,
	2133,
	2183,
	2133,
	2133,
	2742,
	606,
	3164,
	2147,
	2147,
	1812,
	2147,
	1812,
	1812,
	1247,
	1334,
	1334,
	1247,
	980,
	2147,
	2169,
	1831,
	1060,
	2183,
	1812,
	2147,
	1812,
	2147,
	1056,
	2147,
	3676,
	3496,
	3552,
	3496,
	3496,
	3496,
	2257,
	3083,
	3552,
	3496,
	3083,
	2295,
	3164,
	3552,
	2388,
	3552,
	3163,
	2539,
	2241,
	2291,
	3492,
	2279,
	2822,
	2794,
	2750,
	3163,
	3549,
	3492,
	3496,
	3676,
	2801,
	3091,
	2793,
	2966,
	2290,
	2255,
	2750,
	3676,
	2147,
	1812,
	2147,
	1812,
	1056,
	3676,
	2183,
	362,
	506,
	1337,
	507,
	2147,
	1812,
	2147,
	1812,
	968,
	768,
	708,
	708,
	1247,
	1247,
	1247,
	708,
	2133,
	3676,
	1056,
	2133,
	2169,
	1247,
	1247,
	1337,
	1337,
	1337,
	2147,
	1334,
	768,
	2147,
	2183,
	494,
	1532,
	1547,
	1798,
	2133,
	3162,
	1247,
	3676,
	3552,
	2647,
	494,
	2183,
	3676,
	1081,
	494,
	3083,
	3496,
	3153,
	3552,
	3676,
	2183,
	494,
	1337,
	3496,
	3153,
	3552,
	3580,
	2827,
	3676,
	2183,
	456,
	2133,
	3163,
	3676,
	2183,
	456,
	2133,
	1337,
	1056,
	2183,
	2183,
	494,
	2641,
	1337,
	823,
	3496,
	1337,
	2794,
	3161,
	2183,
	3676,
	1831,
	494,
	775,
	503,
	1053,
	718,
	3153,
	3496,
	3676,
	1812,
	494,
	2183,
	1812,
	494,
	2183,
	775,
	2183,
	775,
	2828,
	2643,
	2828,
	2750,
	494,
	2183,
	3676,
	301,
	494,
	775,
	2133,
	456,
	3552,
	2183,
	494,
	456,
	3552,
	2183,
	494,
	456,
	3496,
	3153,
	3552,
	3552,
	2183,
	3676,
	493,
	2183,
	3676,
	3676,
	2183,
	3552,
	494,
	301,
	1547,
	3580,
	3420,
	774,
	2609,
	2392,
	2411,
	2133,
	456,
	2183,
	456,
	774,
	1547,
	3163,
	2133,
	3552,
	3676,
	2183,
	2147,
	2147,
	2147,
	2147,
	2147,
	2147,
	2830,
	3369,
	3369,
	3580,
	3676,
	2133,
	1798,
	2133,
	1798,
	968,
	2147,
	1547,
	2133,
	2133,
	1798,
	2147,
	1812,
	2147,
	1812,
	185,
	1547,
	2133,
	2147,
	1812,
	2133,
	1798,
	606,
	2183,
	2183,
	494,
	3369,
	2183,
	3083,
	3164,
	320,
	509,
	777,
	311,
	833,
	3676,
	2749,
	3023,
	3552,
	1831,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	413,
	2169,
	2147,
	1547,
	3164,
	2133,
	3496,
	633,
	2147,
	1812,
	2133,
	1798,
	2169,
	1831,
	1547,
	2147,
	1547,
	2133,
	494,
	2183,
	813,
	1339,
	774,
	3580,
	992,
	3369,
	3164,
	3552,
	2169,
	3033,
	506,
	559,
	3629,
	504,
	317,
	2835,
	1532,
	2183,
	3676,
	1812,
	2147,
	2147,
	2147,
	3552,
	1812,
	1053,
	1247,
	1812,
	1053,
	1247,
	1812,
	2147,
	1812,
	2147,
	3676,
	640,
	2147,
	1053,
	1053,
	1247,
	3676,
	1812,
	2147,
	3676,
	1812,
	2147,
	3676,
	1812,
	1053,
	630,
	3366,
	3676,
	1812,
	630,
	1053,
	1247,
	1812,
	2147,
	3676,
	1831,
	1060,
	2147,
	2169,
	2183,
	2133,
	1798,
	1798,
	2169,
	2169,
	2183,
	2183,
	2183,
	967,
	2132,
	2169,
	3676,
	980,
	609,
	2147,
	2169,
	2133,
	605,
	2133,
	2133,
	2169,
	2169,
	3676,
	2133,
	1798,
	1798,
	3676,
	3552,
	2783,
	2783,
	1812,
	774,
	1532,
	1334,
	708,
	2747,
	769,
	2147,
	2147,
	2147,
	2147,
	1532,
	1334,
	1532,
	1334,
	1532,
	1532,
	1532,
	775,
	2183,
	3552,
	2183,
	3676,
	1812,
	2147,
	3676,
	3080,
	3024,
	3035,
	3496,
	3496,
	3496,
	3496,
	3552,
	2793,
	3676,
	2183,
	775,
	160,
	3676,
	775,
	2183,
	1194,
	1337,
	99,
	1337,
	50,
	250,
	260,
	494,
	2183,
	775,
	2183,
	3552,
	2830,
	2183,
	3676,
	1812,
	2147,
	3552,
	2147,
	352,
	228,
	708,
	708,
	708,
	708,
	1337,
	980,
	3083,
	2133,
	2147,
	3552,
	2650,
	3164,
	3164,
	3164,
	2933,
	3164,
	2832,
	3023,
	3676,
	2183,
	1337,
	823,
	107,
	2133,
	2133,
	2133,
	2133,
	2133,
	2133,
	2147,
	3073,
	2147,
	3660,
	3676,
	980,
	610,
	2133,
	2147,
	968,
	2133,
	2133,
	1812,
	2147,
	92,
	153,
	1547,
	3035,
	3369,
	2257,
	775,
	2147,
	1812,
	2147,
	1812,
	2133,
	1798,
	639,
	2147,
	719,
	2183,
	2133,
	1532,
	992,
	1798,
	3492,
	1247,
	1247,
	968,
	2183,
	550,
	2147,
	2183,
	3549,
	1547,
	2133,
	2147,
	3676,
	2133,
	2133,
	1798,
	968,
	382,
	811,
	607,
	968,
	1812,
	380,
	769,
	980,
	2183,
	2147,
	2147,
	2147,
	1547,
	2133,
	2147,
	507,
	507,
	2147,
	1812,
	2133,
	2133,
	1247,
	2133,
	2147,
	3676,
	980,
	3321,
	3321,
	3549,
	2147,
	1812,
	2133,
	1798,
	2147,
	1812,
	2147,
	1812,
	2147,
	1812,
	2169,
	1798,
	2133,
	1798,
	1798,
	2133,
	1798,
	2147,
	1812,
	412,
	126,
	196,
	69,
	2147,
	1812,
	1812,
	1812,
	1831,
	1831,
	1831,
	2183,
	1056,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	307,
	2183,
	2147,
	1812,
	2147,
	1812,
	1056,
	1798,
	1798,
	1812,
	769,
	2147,
	1337,
	1798,
	3161,
	3676,
	3660,
	3,
	307,
	3164,
	2183,
	3676,
	2147,
	1812,
	1337,
	2183,
	2269,
	2741,
	2321,
	2390,
	29,
	2227,
	1812,
	2243,
	2243,
	2147,
	1337,
	3083,
	3676,
	605,
	2147,
	2147,
	768,
	3023,
	1247,
	1247,
	1247,
	708,
	2133,
	2133,
	2147,
	3676,
	1056,
	2133,
	2169,
	1247,
	1247,
	1337,
	1337,
	1334,
	768,
	2147,
	1812,
	823,
	506,
	1337,
	775,
	3498,
	2662,
	2660,
	3496,
	3552,
	2606,
	1812,
	399,
	2147,
	332,
	316,
	353,
	775,
	2183,
	2183,
	1831,
	1798,
	1798,
	657,
	2169,
	1831,
	2133,
	1798,
	2133,
	1798,
	205,
	1337,
	3552,
	3491,
	3080,
	1337,
	1337,
	2747,
	3163,
	3552,
	3025,
	2183,
	3676,
	1812,
	1339,
	1547,
	3033,
	3027,
	1337,
	2147,
	1337,
	154,
	455,
	363,
	719,
	319,
	2805,
	811,
	1547,
	3234,
	3234,
	2133,
	3676,
	2133,
	1798,
	2133,
	1798,
	2147,
	968,
	2147,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000091, { 1, 6 } },
	{ 0x060000CF, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 174 },
	{ (Il2CppRGCTXDataType)2, 730 },
	{ (Il2CppRGCTXDataType)3, 1575 },
	{ (Il2CppRGCTXDataType)2, 1082 },
	{ (Il2CppRGCTXDataType)3, 1128 },
	{ (Il2CppRGCTXDataType)2, 1014 },
	{ (Il2CppRGCTXDataType)2, 1135 },
};
extern const CustomAttributesCacheGenerator g_zxing_unity_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_zxing_unity_CodeGenModule;
const Il2CppCodeGenModule g_zxing_unity_CodeGenModule = 
{
	"zxing.unity.dll",
	1189,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_zxing_unity_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
