# ARUnlockApp

AR Color Unlock

This project is an Unity project that can be directly opened in Unity.
It was only tested with unity version 2020.3.0f1. 
It should be used along with our unlock screen Android app.

The AR Color Unlock project consists of two scenes: the qrcode_scanner and the color-dot-scene.
After starting, the qrcode_scanner with the barcode scanner (https://github.com/kefniark/UnityBarcodeScanner) is loaded, which we modified for our application. It contains buttons to start the scanner and also to stop it from running. After starting, the scanning process runs until a valid code is found or it is stopped manually by the user. After finding a valid code, the scanner is stopped and the content of the code is stored as a string in a container. Once the content of the QR code has been saved, the color-dot-scene is loaded. This consists of an AR Session and an AR Session Origin, which contains the AR Camera. Also the AR Tracked Image Manager script that performs the lock-screen detection is linked here. It is a pre-built script from ARFoundation. The image (part of the background and the dots) for the detection is stored as a compressed image with extracted features from the gray-scale data. After the image is detected, the pattern, which is saved as aprefab asset, is displayed. It consists of 9 cylinders with a very shallow depth, so they are perceived as circles. At the same moment, a script is called, which is executed on the object. It reads the content of the QR code from the container and changes the colors of each cylinder according to the instruction from the string. The color is set using the renderer of the respective cylinder and material.SetColor("_Color", color_i"). The color is set to 50% transparency and the material was initially set transparent to keep the finger visible through the transparent object.
